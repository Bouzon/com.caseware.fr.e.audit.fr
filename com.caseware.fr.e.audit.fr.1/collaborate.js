// Declaring our FR Generic object
var FRImpl = function () {
  this.id = "4b521c32-2bb3-11e8-b467-0ed5f89f718b";

  function notContact() {
    return !CWI.ProfileManager.get().isContact();
  }

  this.canOpen = notContact();
  this.canCreate = notContact();
  this.canDelete = notContact();

  this.openInNewTab = true;
  this.supportsQueries = true;

  this.canRollForward = true;
  this.versions = [{"id":"com.caseware.fr.e.audit.fr.1", "name":"2020-21"}];
  this.versionsHeading = "Year";
  this.duplicateVersionWarning = "An FR SmartCaC Engagement for the selected year already exists.";

  this.firmSettingsPages = [{
      "heading": "Manage Template 2020-21",
      "anchorId": "1",
      "url": "../e/template.jsp?t=" + this.id + "&s=com.caseware.fr.e.audit.fr.1"
  }];

  this.gridIconSVG = "../e/prod/com.caseware.fr.e.audit.fr.1/icon.svg";
  this.doormatSVG = "../e/prod/com.caseware.fr.e.audit.fr.1/icon.svg";

  return this;
};

// FRImpl @extends CollaborateImpl
FRImpl.prototype = new CollaborateImpl();
collaborate.addModel(new FRImpl());
