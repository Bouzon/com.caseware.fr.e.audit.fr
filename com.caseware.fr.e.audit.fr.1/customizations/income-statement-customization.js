(function() {
  var /** @const */ custom = wpw.financials.custom;
  var is = wpw.financials.IncomeStatement.customizations;

	//Produits d'exploitation
	is.additions.push(new custom.Addition('Produits d\'exploitation', false, 'insert_produits_dexploitation', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7'], custom.ADD_SCHEDULE_LINE.TOP, {}, 1, wpw.financials.custom.TYPE.TITLE));
	
	//Ventes de marchandises
	is.additions.push(new custom.Addition('Ventes de marchandises', false, 'insert_C.1.1.1.01', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7.0.7'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_produits_dexploitation', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Production vendue
	is.additions.push(new custom.Addition('Production vendue', false, 'insert_C.1.1.1.02', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7.0.1','7.0.2','7.0.3','7.0.4','7.0.5','7.0.6','7.0.8','7.0.9'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.1.1.1.01', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Montant net du chiffre d'affaires
	is.additions.push(new custom.Addition('Montant net du chiffre d\'affaire', false, 'somme_C.1.1.1.01_C.1.1.1.02', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_C.1.1.1.01','insert_C.1.1.1.02'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.1.1.1.02', hideWhenZero: 'true', underline: 'none'}, 1, wpw.financials.custom.TYPE.TOTAL));
	
	//Production stockée
	is.additions.push(new custom.Addition('Production stockée', false, 'insert_C.1.1.2.01', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7.1.3'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'somme_C.1.1.1.01_C.1.1.1.02', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Production immobilisée
	is.additions.push(new custom.Addition('Production immobilisée', false, 'insert_C.1.1.2.02', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7.2','7.2.1','7.2.2'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.1.1.2.01', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Subvention d'exploitation
	is.additions.push(new custom.Addition('Subvention d\'exploitation', false, 'insert_C.1.1.2.03', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7.4'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.1.1.2.02', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Reprises sur prov. (et amort.), transf. de charges
	is.additions.push(new custom.Addition('Reprises sur prov. (et amort.), transf. de charges', false, 'insert_C.1.1.2.04', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7.8.1.1','7.8.1.5','7.8.1.6','7.8.1.7',/*'781730','781740',*/'7.9.1'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.1.1.2.03', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Autres produits
	is.additions.push(new custom.Addition('Autres produits', false, 'insert_C.1.1.2.05', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7.5.1','7.5.2','7.5.3','7.5.4','7.5.8'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.1.1.2.04', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//TOTAL I (Total Produits d'exploitation)
	is.additions.push(new custom.Addition('Total Produits d\'exploitation (I)', false, 'insert_total_I', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['somme_C.1.1.1.01_C.1.1.1.02','insert_C.1.1.2.01','insert_C.1.1.2.02','insert_C.1.1.2.03','insert_C.1.1.2.04','insert_C.1.1.2.05'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.1.1.2.05'}, 0, wpw.financials.custom.TYPE.TOTAL));
	
	//Charges d'exploitation
	is.additions.push(new custom.Addition('Charges d\'exploitation', false, 'insert_charges_dexploitation', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6'], custom.ADD_SCHEDULE_LINE.TOP, {}, 1, wpw.financials.custom.TYPE.TITLE));
	
	//Achats de marchandises
	is.additions.push(new custom.Addition('Achats de marchandises', false, 'insert_C.2.1.1.01', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.0.7','6.0.8','6.0.9.7','6.0.9.8'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_charges_dexploitation', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Variation de stock de marchandises
	is.additions.push(new custom.Addition('Variation de stock de marchandises', false, 'insert_C.2.1.1.02', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.0.3.7'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.1.1.01', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Achats de matières premières et autres appro.
	is.additions.push(new custom.Addition('Achats de matières premières et autres appro.', false, 'insert_C.2.1.1.03', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.0.1','6.0.2','6.0.9.1','6.0.9.2'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.1.1.02', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Variation de stock de matières premières et appro.
	is.additions.push(new custom.Addition('Variation de stock de matières premières et appro.', false, 'insert_C.2.1.1.04', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.0.3.1','6.0.3.2'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.1.1.03', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Autres charges externes
	is.additions.push(new custom.Addition('Autres charges externes', false, 'insert_C.2.1.1.05', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.0.4','6.0.5','6.0.6','6.0.9.4','6.0.9.5','6.0.9.6','6.1.1','6.1.2','6.1.3','6.1.4','6.1.5','6.1.6','6.1.7','6.1.8','6.1.9','6.2'/*,'621000','622000','623000','624000','625000','626000','627000','628000','629000'*/], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.1.1.04', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Impôts, taxes et versements assimilés
	is.additions.push(new custom.Addition('Impôts, taxes et versements assimilés', false, 'insert_C.2.1.1.06', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.3'/*,'631000','633000','635000','637000'*/], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.1.1.05', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Salaires et traitements
	is.additions.push(new custom.Addition('Salaires et traitements', false, 'insert_C.2.1.1.07', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.4.1','6.4.4'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.1.1.06', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Charges sociales
	is.additions.push(new custom.Addition('Charges sociales', false, 'insert_C.2.1.1.08', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.4.5','6.4.6','6.4.7','6.4.8','6.4.9'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.1.1.07', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Dotations aux amortissements et dépréciations
	is.additions.push(new custom.Addition('Dotations aux amortissements et dépréciations', false, 'insert_dotations_aux_amortissements_et_depreciations', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6'], custom.ADD_SCHEDULE_LINE.TOP, {}, 1, wpw.financials.custom.TYPE.TITLE));
	
	//Sur immo : dotations aux amortissements
	is.additions.push(new custom.Addition('Sur immo : dotations aux amortissements', false, 'insert_C.2.1.1.09.00', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.8.1.1','6.8.1.2'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_dotations_aux_amortissements_et_depreciations', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Sur immo : dotations aux dépréciations
	is.additions.push(new custom.Addition('Sur immo : dotations aux dépréciations', false, 'insert_C.2.1.1.09.10', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.8.1.6'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.1.1.09.00', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Sur actif circulant : dotations aux dépréciations
	is.additions.push(new custom.Addition('Sur actif circulant : dotations aux dépréciations', false, 'insert_C.2.1.1.09.20', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.8.1.7'/*,'681730','681740'*/], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.1.1.09.10', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Dotations aux provisions
	is.additions.push(new custom.Addition('Dotations aux provisions', false, 'insert_C.2.1.1.10', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.8.1.5'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.1.1.09.20', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Autres charges
	is.additions.push(new custom.Addition('Autres charges', false, 'insert_C.2.1.1.11', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.5.1','6.5.3','6.5.4','6.5.8'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.1.1.10', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//TOTAL II Total Charges d'exploitation (II)
	is.additions.push(new custom.Addition('Total Charges d\'exploitation (II)', false, 'insert_total_II', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_C.2.1.1.01','insert_C.2.1.1.02','insert_C.2.1.1.03','insert_C.2.1.1.04','insert_C.2.1.1.05','insert_C.2.1.1.06','insert_C.2.1.1.07','insert_C.2.1.1.08','insert_C.2.1.1.09.00','insert_C.2.1.1.09.10','insert_C.2.1.1.09.20','insert_C.2.1.1.10','insert_C.2.1.1.11'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.1.1.11'}, 0, wpw.financials.custom.TYPE.TOTAL));
	
	//Résultat d'exploitation I-II
	is.additions.push(new custom.Addition('Résultat d\'exploitation (I-II)', false, 'insert_resultat_dexploitation', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_total_I','insert_total_II'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_total_II'}, 0, wpw.financials.custom.TYPE.TOTAL));
	
	//Quote-part de résultat sur op. faites en commun 
	is.additions.push(new custom.Addition('Quote-parts de résultat sur op. faites en commun', false, 'insert_Quote_parts_de_resultat_sur_op_faites_en_commun', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7'], custom.ADD_SCHEDULE_LINE.TOP, {}, 1, wpw.financials.custom.TYPE.TITLE));
	
	//Bénéfice ou perte transférée III
	is.additions.push(new custom.Addition('Bénéfice ou perte transférée III', false, 'insert_C.1.2.1.01', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7.5.5'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_Quote_parts_de_resultat_sur_op_faites_en_commun'}, 1, wpw.financials.custom.TYPE.LINE));
	
	//Perte ou bénéfice transféré IV
	is.additions.push(new custom.Addition('Perte ou bénéfice transféré IV', false, 'insert_C.2.2.1.01', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.5.5'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.1.2.1.01'}, 1, wpw.financials.custom.TYPE.LINE));
	
	//Produits Financiers
	is.additions.push(new custom.Addition('Produits Financiers', false, 'insert_produits_financiers', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7'], custom.ADD_SCHEDULE_LINE.TOP, {}, 1, wpw.financials.custom.TYPE.TITLE));
	
	//De participation
	is.additions.push(new custom.Addition('De participation', false, 'insert_C.1.3.1.01', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7.6.1'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_produits_financiers', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//D'autres valeurs mob. et créances de l'actif immo.
	is.additions.push(new custom.Addition('D\'autres valeurs mob. et créances de l\'actif immo.', false, 'insert_C.1.3.1.02', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7.6.2'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.1.3.1.01', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Autres intérêts et produits assimilés
	is.additions.push(new custom.Addition('Autres intérêts et produits assimilés', false, 'insert_C.1.3.1.03', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7.6.3','7.6.3.1','7.6.3.8','7.6.4','7.6.5','7.6.8'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.1.3.1.02', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Rep. sur prov. et dep. et transferts de charges
	is.additions.push(new custom.Addition('Rep. sur prov. et dep. et transferts de charges', false, 'insert_C.1.3.1.04', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7.8.6','7.8.6.5','7.8.6.6','7.8.6.6.2','7.8.6.6.5','7.9.6'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.1.3.1.03', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Différences positives de change
	is.additions.push(new custom.Addition('Différences positives de change', false, 'insert_C.1.3.1.05', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7.6.6'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.1.3.1.04', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Produits nets sur cessions de VMP
	is.additions.push(new custom.Addition('Produits nets sur cessions de VMP', false, 'insert_C.1.3.1.06', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7.6.7'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.1.3.1.05', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//TOTAL V Total Produits Financiers (V)
	is.additions.push(new custom.Addition('Total Produits Financiers (V)', false, 'insert_total_V', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_C.1.3.1.01','insert_C.1.3.1.02','insert_C.1.3.1.03','insert_C.1.3.1.04','insert_C.1.3.1.05','insert_C.1.3.1.06'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.1.3.1.06'}, 0, wpw.financials.custom.TYPE.TOTAL));
	
	//Charges financières
	is.additions.push(new custom.Addition('Charges financières', false, 'insert_charges_financieres', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6'], custom.ADD_SCHEDULE_LINE.TOP, {}, 1, wpw.financials.custom.TYPE.TITLE));
	
	//Dot. aux amort., aux dep. et aux provisions
	is.additions.push(new custom.Addition('Dot. aux amort., aux dep. et aux provisions', false, 'insert_C.2.3.1.01', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.8.6'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_charges_financieres', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Intérêts et charges assimilées
	is.additions.push(new custom.Addition('Intérêts et charges assimilées', false, 'insert_C.2.3.1.02', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.6.1'/*,'661100','661200','661500','661600','661700','661800'*/,'6.6.4','6.6.5','6.6.8'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.3.1.01', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Différences négatives de change
	is.additions.push(new custom.Addition('Différences négatives de change', false, 'insert_C.2.3.1.03', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.6.6'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.3.1.02', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Charges nettes sur cessions de VMP
	is.additions.push(new custom.Addition('Charges nettes sur cessions de VMP', false, 'insert_C.2.3.1.04', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.6.7'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.3.1.03', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//TOTAL VI Total Charges Financières (VI)
	is.additions.push(new custom.Addition('Total Charges Financières (VI)', false, 'insert_total_VI', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_C.2.3.1.01','insert_C.2.3.1.02','insert_C.2.3.1.03','insert_C.2.3.1.04'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.3.1.04'}, 0, wpw.financials.custom.TYPE.TOTAL));
	
	//Résultat Financier (V-VI)
	is.additions.push(new custom.Addition('Résultat Financier (V-VI)', false, 'insert_resultat_financier', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_total_V','insert_total_VI'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_total_VI'}, 0, wpw.financials.custom.TYPE.TOTAL));
	
	//Résultat Courant av. impôts (I-II+II-IV+V-VI)
	is.additions.push(new custom.Addition('Résultat Courant av. impôts (I-II+II-IV+V-VI)', false, 'insert_resultat_courant_av_impots', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_total_I','insert_total_II','insert_C.1.2.1.01','insert_C.2.2.1.01','insert_total_V','insert_total_VI'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_resultat_financier'}, 0, wpw.financials.custom.TYPE.TOTAL));
	
	//Produits Exceptionnels
	is.additions.push(new custom.Addition('Produits Exceptionnels', false, 'insert_produits_exceptionnels', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7'], custom.ADD_SCHEDULE_LINE.TOP, {}, 1, wpw.financials.custom.TYPE.TITLE));
	
	//Sur opérations de gestion
	is.additions.push(new custom.Addition('Sur opérations de gestion', false, 'insert_C.1.4.1.01', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7.7.1','7.7.2','7.7.4'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_produits_exceptionnels', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Sur opérations en capital
	is.additions.push(new custom.Addition('Sur opérations en capital', false, 'insert_C.1.4.1.02', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7.7.5','7.7.5.1','7.7.5.2','7.7.5.6','7.7.5.8','7.7.7','7.7.8'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.1.4.1.01', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Rep. sur prov. et dep. et tranferts de charges
	is.additions.push(new custom.Addition('Rep. sur prov. et dep. et tranferts de charges', false, 'insert_C.1.4.1.03', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['7.8.7.2','7.8.7.2.5','7.8.7.2.6','7.8.7.2.7','7.8.7.3','7.8.7.4','7.8.7.5','7.8.7.6','7.9.7'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.1.4.1.02', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//TOTAL VII Total Produits Exceptionnels (VII)
	is.additions.push(new custom.Addition('Total Produits Exceptionnels (VII)', false, 'insert_total_VII', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_C.1.4.1.01','insert_C.1.4.1.02','insert_C.1.4.1.03'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.1.4.1.03'}, 0, wpw.financials.custom.TYPE.TOTAL));
	
	//Charges Exceptionnelles
	is.additions.push(new custom.Addition('Charges Exceptionnelles', false, 'insert_charges_exceptionnelles', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6'], custom.ADD_SCHEDULE_LINE.TOP, {}, 1, wpw.financials.custom.TYPE.TITLE));
	
	//Sur opérations de gestion
	is.additions.push(new custom.Addition('Sur opérations de gestion', false, 'insert_C.2.4.1.01', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.7.1','6.7.2','6.7.4','6.7.4.1','6.7.4.2'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_charges_exceptionnelles', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Sur opérations en capital
	is.additions.push(new custom.Addition('Sur opérations de gestion', false, 'insert_C.2.4.1.02', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.7.5','6.7.5.1','6.7.5.2','6.7.5.3','6.7.5.6','6.7.5.8','6.7.8'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.4.1.01', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Dot. aux amort., aux prov. et aux provisions
	is.additions.push(new custom.Addition('Dot. aux amort., aux prov. et aux provisions', false, 'insert_C.2.4.1.03', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.8.7.1','6.8.7.2','6.8.7.3','6.8.7.4','6.8.7.5','6.8.7.6'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.4.1.02', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//TOTAL VIII Total Charges Exceptionnelles (VIII)
	is.additions.push(new custom.Addition('Total Charges Exceptionnelles (VIII)', false, 'insert_total_VIII', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_C.2.4.1.01','insert_C.2.4.1.02','insert_C.2.4.1.03'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.4.1.03'}, 0, wpw.financials.custom.TYPE.TOTAL));
	
	//Résultat Exceptionnel (VII-VIII)
	is.additions.push(new custom.Addition('Résultat Exceptionnel (VII-VIII)', false, 'insert_resultat_exceptionnel', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_total_VII','insert_total_VIII'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_total_VIII'}, 0, wpw.financials.custom.TYPE.TOTAL));
	
	//Participation des salariés aux résultats (IX)
	is.additions.push(new custom.Addition('Participation des salariés aux résultats (IX)', false, 'insert_C.2.5', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.9.1'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_resultat_exceptionnel', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Impôts sur les bénéfices (X)
	is.additions.push(new custom.Addition('Impôts sur les bénéfices (X)', false, 'insert_C.2.6', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['6.9.5','6.9.6','6.9.8','6.9.9'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.5', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Total des produits (I+III+V+VII)
	is.additions.push(new custom.Addition('Total des produits (I+III+V+VII)', false, 'insert_total_des_produits_I_III_V_VII', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_total_I','insert_C.1.2.1.01','insert_total_V','insert_total_VII'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_C.2.6'}, 0, wpw.financials.custom.TYPE.TOTAL));
	
	//Total des charges (II+IV+VI+VIII+IX+X)
	is.additions.push(new custom.Addition('Total des charges (II+IV+VI+VIII+IX+X)', false, 'insert_total_des_charges_II_IV_VI_VIII_IX_X', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_total_II','insert_C.2.2.1.01','insert_total_VI','insert_total_VIII','insert_C.2.5','insert_C.2.6'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_total_des_produits_I_III_V_VII'}, 0, wpw.financials.custom.TYPE.TOTAL));
	
	//Bénéfice ou perte
	is.additions.push(new custom.Addition('Bénéfice ou perte', false, 'insert_benefice_perte', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_total_des_produits_I_III_V_VII','insert_total_des_charges_II_IV_VI_VIII_IX_X'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_total_des_charges_II_IV_VI_VIII_IX_X'}, 0, wpw.financials.custom.TYPE.TOTAL));
	
	
})();
