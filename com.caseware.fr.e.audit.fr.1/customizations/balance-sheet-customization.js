(function() {
  var /** @const */ custom = wpw.financials.custom;

	// Balance sheet customizations
	var bsAdditions = wpw.financials.BalanceSheet.customizations.additions;

	//ACTIF
	bsAdditions.push(new custom.Addition('ACTIF', true, 'actif', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1'], custom.ADD_SCHEDULE_LINE.TOP, {}, 1, wpw.financials.custom.TYPE.TITLE));
	
	//Capital souscrit non appele
	bsAdditions.push(new custom.Addition('Capital souscrit non appelé', true, 'insert_B.1.1.1', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.0.9'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'actif', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//ACTIF Immobilisé
	bsAdditions.push(new custom.Addition('Actif Immobilisé', true, 'actif_immobilise', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2'], custom.ADD_SCHEDULE_LINE.TOP, {}, 1, wpw.financials.custom.TYPE.TITLE));
	
	//Immobilisations incorporelles
	bsAdditions.push(new custom.Addition('Immobilisations incorporelles', true, 'insert_B.1.1.2.01', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [/*'280000',*/'2.9'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'actif_immobilise', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Frais d'établissement
	bsAdditions.push(new custom.Addition('Frais d\'établissement', true, 'insert_B.1.1.2.01.00', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2.0.1','2.8.0.1','2.9.0.5'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.01', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Frais de recherche et de développement
	bsAdditions.push(new custom.Addition('Frais de recherche et de développement', true, 'insert_B.1.1.2.01.10', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2.0.3','2.8.0.3'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.01.00', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Concession, brevets et droits similaires
	bsAdditions.push(new custom.Addition('Concession, brevets et droits similaires', true, 'insert_B.1.1.2.01.20', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2.0.5','2.8.0.5'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.01.10', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Fonds commercial
	bsAdditions.push(new custom.Addition('Fonds commercial', true, 'insert_B.1.1.2.01.30', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2.0.6','2.0.7','2.8.0.7','2.9.0.6','2.9.0.7'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.01.20', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Autres immobilisations incorporelles
	bsAdditions.push(new custom.Addition('Autres immobilisations incorporelles', true, 'insert_B.1.1.2.01.40', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2.0.8','2.8.0.8','2.9.0.8'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.01.30', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Immobilisations incorporelles en cours
	bsAdditions.push(new custom.Addition('Immobilisations incorporelles en cours', true, 'insert_B.1.1.2.01.50', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2.3.2'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.01.40', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Avances et acomptes
	bsAdditions.push(new custom.Addition('Avances et acomptes', true, 'insert_B.1.1.2.01.60', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2.3.7'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.01.50', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Immobilisations corporelles
	bsAdditions.push(new custom.Addition('Immobilisations corporelles', true, 'insert_B.1.1.2.02', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [/*'281000',*/'2.9.1'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.01.60', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Terrains
	bsAdditions.push(new custom.Addition('Terrains', true, 'insert_B.1.1.2.02.00', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2.1.1','2.1.2','2.8.1.1','2.8.1.2','2.9.1.1','2.9.1.2'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.02', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Constructions
	bsAdditions.push(new custom.Addition('Constructions', true, 'insert_B.1.1.2.02.10', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2.1.3','2.8.1.3','2.8.1.4','2.9.1.3','2.9.1.4'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.02.00', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Inst. Tech. matériel et outillage industriels
	bsAdditions.push(new custom.Addition('Inst. Tech. matériel et outillage industriels', true, 'insert_B.1.1.2.02.20', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2.1.5','2.8.1.5','2.9.1.5'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.02.10', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Autres immobilisations corporelles
	bsAdditions.push(new custom.Addition('Autres immobilisations corporelles', true, 'insert_B.1.1.2.02.30', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2.1.8','2.2','2.8.1.8','2.8.2','2.9.1.8','2.9.2'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.02.20', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Immobilisations corporelles en cours
	bsAdditions.push(new custom.Addition('Immobilisations corporelles en cours', true, 'insert_B.1.1.2.02.40', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2.3.1','2.9.3'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.02.30', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Avances et acomptes
	bsAdditions.push(new custom.Addition('Avances et acomptes', true, 'insert_B.1.1.2.02.50', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2.3.8'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.02.40', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Participations
	bsAdditions.push(new custom.Addition('Participations', true, 'insert_B.1.1.2.03.00', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2.5','2.6.1','2.6.1.1','2.6.1.8','2.6.6','2.9.6.1','2.9.6.6','2.9.6.7'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.02.50', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Créances rattachées à des participations
	bsAdditions.push(new custom.Addition('Créances rattachées à des participations', true, 'insert_B.1.1.2.03.10', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2.6.7','2.6.8','2.9.6.8'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.03.00', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Titres immobilisés de l'activité de portefeuille
	bsAdditions.push(new custom.Addition('Titres immobilisés de l\'activité de portefeuille', true, 'insert_B.1.1.2.03.20', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2.7.3'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.03.10', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Autres titres immobilisés
	bsAdditions.push(new custom.Addition('Autres titres immobilisés', true, 'insert_B.1.1.2.03.30', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2.7.1','2.7.2','2.7.6.8.2','2.7.7','2.7.7.1','2.7.7.2'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.03.20', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Prêts
	bsAdditions.push(new custom.Addition('Prêts', true, 'insert_B.1.1.2.03.40', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2.7.4','2.7.6.8.4'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.03.30', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Autres immobilisations financières
	bsAdditions.push(new custom.Addition('Autres immobilisations financières', true, 'insert_B.1.1.2.03.50', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2.7.5','2.7.6','2.7.6.1','2.7.6.8','2.7.6.8.5','2.7.6.8.8','2.9.7'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.03.40', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//TOTAL I Actif immobilisé
	bsAdditions.push(new custom.Addition('Total Actif immobilisé (I)', false, 'insert_total_I', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_B.1.1.1','insert_B.1.1.2.01','insert_B.1.1.2.01.00','insert_B.1.1.2.01.10','insert_B.1.1.2.01.20','insert_B.1.1.2.01.30','insert_B.1.1.2.01.40','insert_B.1.1.2.01.50','insert_B.1.1.2.01.60','insert_B.1.1.2.02','insert_B.1.1.2.02.00','insert_B.1.1.2.02.10','insert_B.1.1.2.02.20','insert_B.1.1.2.02.30','insert_B.1.1.2.02.40','insert_B.1.1.2.02.50','insert_B.1.1.2.03.00','insert_B.1.1.2.03.10','insert_B.1.1.2.03.20','insert_B.1.1.2.03.30','insert_B.1.1.2.03.40','insert_B.1.1.2.03.50'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.1.2.03.50', underline: 'none'}, 1, wpw.financials.custom.TYPE.TOTAL));
	
	//ACTIF Circulant
	bsAdditions.push(new custom.Addition('Actif Circulant', true, 'actif_circulant', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['3'], custom.ADD_SCHEDULE_LINE.TOP, {}, 1, wpw.financials.custom.TYPE.TITLE));
	
	//Matières premières et autres approvisionnements
	bsAdditions.push(new custom.Addition('Matières premières et autres approvisionnements', true, 'insert_B.1.2.1.01.00', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['3.1','3.2','3.6','3.8','3.9'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'actif_circulant', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//En-cours de production (biens et services)
	bsAdditions.push(new custom.Addition('En-cours de production (biens et services)', true, 'insert_B.1.2.1.01.10', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['3.3','3.4'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.2.1.01.00', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Produits intermédiaires et finis
	bsAdditions.push(new custom.Addition('Produits intermédiaires et finis', true, 'insert_B.1.2.1.01.20', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['3.5'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.2.1.01.10', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Marchandises
	bsAdditions.push(new custom.Addition('Marchandises', true, 'insert_B.1.2.1.01.30', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['3.7'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.2.1.01.20', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Avances et acomptes versés sur commandes
	bsAdditions.push(new custom.Addition('Avances et acomptes versés sur commandes', true, 'insert_B.1.2.1.02', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['4.0.9.1'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.2.1.01.30', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Créances clients et comptes rattachés
	bsAdditions.push(new custom.Addition('Créances clients et comptes rattachés', true, 'insert_B.1.2.1.03.00', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['4.1.1','4.1.3','4.1.6','4.1.8','4.9.1','4.9.5','4.9.6'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.2.1.02', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Autres créances
	bsAdditions.push(new custom.Addition('Autres créances', true, 'insert_B.1.2.1.03.10', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['4.0.9','4.2.2','4.2.5','4.3.1','4.3.7','4.3.8','4.4.1','4.4.3','4.4.4','4.4.5','4.4.8','4.5.1','4.5.5','4.5.6','4.5.6.1','4.5.6.3','4.5.6.4','4.5.6.6','4.5.6.7','4.5.8','4.6.2','4.6.5','4.6.7','4.7.1','4.7.2','4.7.3','4.7.4','4.7.5','4.7.8'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.2.1.03.00', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Capital souscrit - appelé, non versé
	bsAdditions.push(new custom.Addition('Capital souscrit - appelé, non versé', true, 'insert_B.1.2.1.04', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['4.5.6.2'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.2.1.03.10', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Actions propres
	bsAdditions.push(new custom.Addition('Actions propres', true, 'insert_B.1.2.1.05.00', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['5.0.2','5.9.0','5.9.0.3'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.2.1.04', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Autres titres
	bsAdditions.push(new custom.Addition('Autres titres', true, 'insert_B.1.2.1.05.10', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['5.0.1','5.0.3','5.0.4','5.0.5','5.0.6','5.0.7','5.0.8','5.9.0.4','5.9.0.6','5.9.0.8'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.2.1.05.00', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Instruments de trésorerie
	bsAdditions.push(new custom.Addition('Instruments de trésorerie', true, 'insert_B.1.2.1.06', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['5.2'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.2.1.05.10', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Disponibilités
	bsAdditions.push(new custom.Addition('Disponibilités', true, 'insert_B.1.2.1.07', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['5.1.1','5.1.2','5.1.4','5.1.5','5.1.6','5.1.7','5.1.8.1','5.1.8.8','5.1.9','5.3','5.4','5.8'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.2.1.06', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Charges constatées d'avance
	bsAdditions.push(new custom.Addition('Charges constatées d\'avance', true, 'insert_B.1.2.1.08', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['4.8.6','4.8.8.6'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.2.1.07', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Actif circulant II
	bsAdditions.push(new custom.Addition('Total Actif circulant (II)', false, 'insert_total_II', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_B.1.2.1.01.00','insert_B.1.2.1.01.10','insert_B.1.2.1.01.20','insert_B.1.2.1.01.30','insert_B.1.2.1.02','insert_B.1.2.1.03.00','insert_B.1.2.1.03.10','insert_B.1.2.1.04','insert_B.1.2.1.05.00','insert_B.1.2.1.05.10','insert_B.1.2.1.06','insert_B.1.2.1.07','insert_B.1.2.1.08'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.2.1.08'}, 1, wpw.financials.custom.TYPE.TOTAL));
	
	//Charges à répartir sur plusieurs exercices (III)
	bsAdditions.push(new custom.Addition('Charges à répartir sur plusieurs exercices (III)', true, 'insert_B.1.3.1.01', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['4.8.1'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_total_II'}, 1, wpw.financials.custom.TYPE.LINE));
	
	//Primes de remboursement des emprunts (IV)
	bsAdditions.push(new custom.Addition('Primes de remboursement des emprunts (IV)', true, 'insert_B.1.3.1.02', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.6.9'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.3.1.01'}, 1, wpw.financials.custom.TYPE.LINE));
	
	//Ecartsde conversion Actif (V)
	bsAdditions.push(new custom.Addition('Ecarts de conversion Actif (V)', true, 'insert_B.1.3.1.03', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['4.7.6'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.3.1.02'}, 1, wpw.financials.custom.TYPE.LINE));

	//TOTAL GENERAL (I + II + III + IV + V)
	bsAdditions.push(new custom.Addition('TOTAL GENERAL (I + II + III + IV + V)', false, 'insert_total_general_I_II_III_IV_V', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_total_I','insert_total_II','insert_B.1.3.1.01','insert_B.1.3.1.02','insert_B.1.3.1.03'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.1.3.1.03'}, 0, wpw.financials.custom.TYPE.TOTAL));
	
	//PASSIF
	bsAdditions.push(new custom.Addition('PASSIF', true, 'passif', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1'], custom.ADD_SCHEDULE_LINE.TOP, {}, 0, wpw.financials.custom.TYPE.TITLE));
	
	//CAPITAUX PROPRES
	bsAdditions.push(new custom.Addition('CAPITAUX PROPRES', true, 'capitaux_propres', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1'], custom.ADD_SCHEDULE_LINE.TOP, {}, 1, wpw.financials.custom.TYPE.TITLE));
	
	//Capital
	bsAdditions.push(new custom.Addition('Capital', true, 'insert_B.2.1.1.01', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.0.1','1.0.8'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'capitaux_propres', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Prime d'émission, de fusion, d'apport
	bsAdditions.push(new custom.Addition('Prime d\'émission, de fusion, d\'apport', true, 'insert_B.2.1.1.02', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.0.2','1.0.4'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.1.1.01', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Ecarts de réévaluation
	bsAdditions.push(new custom.Addition('Ecarts de réévaluation', true, 'insert_B.2.1.1.03', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.0.5'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.1.1.02', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Ecart d'équivalence
	bsAdditions.push(new custom.Addition('Ecart d\'équivalence', true, 'insert_B.2.1.1.04', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.0.7'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.1.1.03', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Réserve légale
	bsAdditions.push(new custom.Addition('Réserve légale', true, 'insert_B.2.1.1.05.00', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.0.6.1'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.1.1.04', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Réserves statutaires ou contractuelles
	bsAdditions.push(new custom.Addition('Réserves statutaires ou contractuelles', true, 'insert_B.2.1.1.05.10', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.0.6.3'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.1.1.05.00', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Réserves réglementées
	bsAdditions.push(new custom.Addition('Réserves réglementées', true, 'insert_B.2.1.1.05.20', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.0.6.2','1.0.6.4'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.1.1.05.10', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Autres
	bsAdditions.push(new custom.Addition('Autres', true, 'insert_B.2.1.1.05.30', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.0.6.8'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.1.1.05.20', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Report à nouveau
	bsAdditions.push(new custom.Addition('Report à nouveau', true, 'insert_B.2.1.1.06', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.1.0','1.1.9'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.1.1.05.30', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));

	//Résultat de l'exercice (bénéfice ou perte)
	bsAdditions.push(new custom.Addition('Résultat de l\'exercice (bénéfice ou perte)', true, 'insert_netinc', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.2'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.1.1.06', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Subventions d'investissement
	bsAdditions.push(new custom.Addition('Subventions d\'investissement', true, 'insert_B.2.1.1.08', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.3.1','1.3.8','1.3.9'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_netinc', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Provisions réglementées
	bsAdditions.push(new custom.Addition('Provisions réglementées', true, 'insert_B.2.1.1.09', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, [/*'140000',*/'1.4.2','1.4.3','1.4.4','1.4.5','1.4.6','1.4.7','1.4.8'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.1.1.08', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//TOTAL I Total Capitaux Propres (I)
	bsAdditions.push(new custom.Addition('Total Capitaux Propres (I)', false, 'insert_total_I_passif', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_B.2.1.1.01','insert_B.2.1.1.02','insert_B.2.1.1.03','insert_B.2.1.1.04','insert_B.2.1.1.05.00','insert_B.2.1.1.05.10','insert_B.2.1.1.05.20','insert_B.2.1.1.05.30','insert_B.2.1.1.06','insert_netinc','insert_B.2.1.1.08','insert_B.2.1.1.09'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.1.1.09'}, 1, wpw.financials.custom.TYPE.TOTAL));
	
	//AUTRES FONDS PROPRES
	bsAdditions.push(new custom.Addition('Autres fonds propres', true, 'autres_fonds_propres', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1'], custom.ADD_SCHEDULE_LINE.TOP, {}, 1, wpw.financials.custom.TYPE.TITLE));
	
	//Avances conditionnées
	bsAdditions.push(new custom.Addition('Avances conditionnées', true, 'insert_B.2.2.1.02', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.6.7.1'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'autres_fonds_propres', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Autres
	bsAdditions.push(new custom.Addition('Autres', true, 'insert_B.2.2.1.03', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.6.7.4'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.2.1.02', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//TOTAL I BIS
	bsAdditions.push(new custom.Addition('TOTAL I BIS', false, 'insert_total_I_bis', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_B.2.2.1.02','insert_B.2.2.1.03'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.2.1.03'}, 0, wpw.financials.custom.TYPE.TOTAL));
	
	//PROVISIONS
	bsAdditions.push(new custom.Addition('Provisions', true, 'provisions', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1'], custom.ADD_SCHEDULE_LINE.TOP, {}, 1, wpw.financials.custom.TYPE.TITLE));
	
	//Provisions pour risques
	bsAdditions.push(new custom.Addition('Provisions pour risques', true, 'insert_B.2.3.1.01', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.5.1'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'provisions', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Provisions pour charges
	bsAdditions.push(new custom.Addition('Provisions pour charges', true, 'insert_B.2.3.1.02', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.5.3','1.5.4','1.5.5','1.5.6','1.5.7','1.5.8'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.3.1.01', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//TOTAL II Total Provisions (II)
	bsAdditions.push(new custom.Addition('Total Provisions (II)', false, 'insert_total_II_provisions', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_B.2.3.1.01','insert_B.2.3.1.02'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.3.1.02'}, 1, wpw.financials.custom.TYPE.TOTAL));

	//DETTES
	bsAdditions.push(new custom.Addition('DETTES', true, 'dettes', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1'], custom.ADD_SCHEDULE_LINE.TOP, {}, 1, wpw.financials.custom.TYPE.TITLE));
	
	//Emprunts obligataires convertibles
	bsAdditions.push(new custom.Addition('Emprunts obligataires convertibles', true, 'insert_B.2.4.1.01', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.6.1','1.6.8.8.1'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'dettes', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Autres emprunts obligataires
	bsAdditions.push(new custom.Addition('Autres emprunts obligataires', true, 'insert_B.2.4.1.02', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.6.2','1.6.3','1.6.8.8.3'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.4.1.01', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Emprunts et dettes auprès des étab. de crédit
	bsAdditions.push(new custom.Addition('Emprunts et dettes auprès des étab. de crédit', true, 'insert_B.2.4.1.03', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.6.4','1.6.8.8.4'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.4.1.02', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Emprunts et dettes financières divers
	bsAdditions.push(new custom.Addition('Emprunts et dettes financières divers', true, 'insert_B.2.4.1.04', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['1.8','1.8.1','1.8.6','1.8.7','1.8.8','1.6.5','1.6.6','1.6.7.5'/*,'168000'*/,'1.6.8.8.5','1.6.8.8.6','1.6.8.8.7','1.6.8.8.8','1.7.1','1.7.4','1.7.8'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.4.1.03', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Avances et acomptes reçus sur commandes en cours
	bsAdditions.push(new custom.Addition('Avances et acomptes reçus sur commandes en cours', true, 'insert_B.2.4.1.05', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['4.1.9'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.4.1.04', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Dettes fournisseurs et comptes rattachés
	bsAdditions.push(new custom.Addition('Dettes fournisseurs et comptes rattachés', true, 'insert_B.2.4.1.06', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['4.0.1','4.0.3','4.0.4','4.0.5','4.0.8'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.4.1.05', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Dettes fiscales et sociales
	bsAdditions.push(new custom.Addition('Dettes fiscales et sociales', true, 'insert_B.2.4.1.07', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['4.2.1','4.2.4','4.2.6','4.2.7','4.2.8','4.4.2','4.4.6','4.4.7','4.4.9'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.4.1.06', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Dettes sur immobilisations et comptes rattachés
	bsAdditions.push(new custom.Addition('Dettes sur immobilisations et comptes rattachés', true, 'insert_B.2.4.1.08', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['2.6.9','2.7.9'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.4.1.07', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Autres dettes
	bsAdditions.push(new custom.Addition('Autres dettes', true, 'insert_B.2.4.1.09', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['4.5.7','4.6.4','5.0.9'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.4.1.08', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//Produits constatés d'avance
	bsAdditions.push(new custom.Addition('Produits constatés d\'avance', true, 'insert_B.2.4.1.11', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['4.8.7','4.8.8.7'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.4.1.09', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//TOTAL III Total Dettes (III)
	bsAdditions.push(new custom.Addition('Total Dettes (III)', false, 'insert_total_III', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_B.2.4.1.01','insert_B.2.4.1.02','insert_B.2.4.1.03','insert_B.2.4.1.04','insert_B.2.4.1.05','insert_B.2.4.1.06','insert_B.2.4.1.07','insert_B.2.4.1.08','insert_B.2.4.1.09','insert_B.2.4.1.11'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.4.1.11'}, 0, wpw.financials.custom.TYPE.TOTAL));
	
	//Ecarts de conversion Passif (IV)
	bsAdditions.push(new custom.Addition('Ecarts de conversion Passif (IV)', true, 'insert_B.2.5.1.01', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['4.7.7'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_total_III', hideWhenZero: 'true'}, 2, wpw.financials.custom.TYPE.LINE));
	
	//TOTAL GENERAL (I + I bis + II + III + IV)
	bsAdditions.push(new custom.Addition('TOTAL GENERAL (I + I bis + II + III + IV)', false, 'insert_total__general_I_Ibis_II_III_IV', wpw.models.Tag.SIGN.DEBIT, custom.PLACEMENT.INSERT, ['insert_total_I_passif','insert_total_I_bis','insert_total_II_provisions','insert_total_III','insert_B.2.5.1.01'], custom.ADD_SCHEDULE_LINE.AFTER, {anchor: 'insert_B.2.5.1.01'}, 0, wpw.financials.custom.TYPE.TOTAL));
	
})();
