(function() 
{

	function findMatch(groupNumbers, accountNumber)
	{
		if (!accountNumber) 
		{
			return null;
		}
		//Trouve le groupe correspondant au compte
		var tag = groupNumbers.find(function(groupNumber) {
			switch (accountNumber) 
			{
				case "109":
					return groupNumber === "BA.1";
				case "28":
					return groupNumber === "BA.2.02.05.03";
				case "280":
					return groupNumber === "BA.2.01.01";
				case "29":
					return groupNumber === "BA.2.02.05.05";
				case "290":
					return groupNumber === "BA.2.01.02";
				case "20":
					return groupNumber === "BA.2.01.07.01";
				case "201":
					return groupNumber === "BA.2.01.03.01";
				case "2801":
					return groupNumber === "BA.2.01.03.02";
				case "2905":
					return groupNumber === "BA.2.01.03.03";
				case "203":
					return groupNumber === "BA.2.01.04.01";
				case "2803": 
					return groupNumber === "BA.2.01.04.02";
				case "205":
					return groupNumber === "BA.2.01.05.01";
				case "2805":
					return groupNumber === "BA.2.01.05.02";
				case "206":
					return groupNumber === "BA.2.01.06.01";
				case "207":
					return groupNumber === "BA.2.01.06.02";
				case "2807":
					return groupNumber === "BA.2.01.06.03";
				case "2906":
					return groupNumber === "BA.2.01.06.04";
				case "2907":
					return groupNumber === "BA.2.01.06.05";
				case "208":
					return groupNumber === "BA.2.01.07.01";
				case "2808":
					return groupNumber === "BA.2.01.07.02";
				case "2908":
					return groupNumber === "BA.2.01.07.03";
				case "232":
					return groupNumber === "BA.2.01.08.01";
				case "237":
					return groupNumber === "BA.2.01.09";
				case "281":
					return groupNumber === "BA.2.02";
				case "291":
					return groupNumber === "BA.2.02.01";
				case "21":
					return groupNumber === "BA.2.02.05.01";
				case "211":
					return groupNumber === "BA.2.02.02.01";
				case "212":
					return groupNumber === "BA.2.02.02.02";
				case "2811":
					return groupNumber === "BA.2.02.02.03";
				case "2812":
					return groupNumber === "BA.2.02.02.04";
				case "2911":
					return groupNumber === "BA.2.02.02.05";
				case "2912":
					return groupNumber === "BA.2.02.02.06";
				case "213":
					return groupNumber === "BA.2.02.03.01";
				case "2813":
					return groupNumber === "BA.2.02.03.02";
				case "2814":
					return groupNumber === "BA.2.02.03.03";
				case "2913":
					return groupNumber === "BA.2.02.03.04";
				case "2914":
					return groupNumber === "BA.2.02.03.05";
				case "215":
					return groupNumber === "BA.2.02.04.01";
				case "2815":
					return groupNumber === "BA.2.02.04.02";
				case "2915":
					return groupNumber === "BA.2.02.04.03";
				case "218":
					return groupNumber === "BA.2.02.05.01";
				case "22":
					return groupNumber === "BA.2.02.05.02";
				case "2818":
					return groupNumber === "BA.2.02.05.03";
				case "282":
					return groupNumber === "BA.2.02.05.04";
				case "2918":
					return groupNumber === "BA.2.02.05.05";
				case "292":
					return groupNumber === "BA.2.02.05.06";
				case "23":
					return groupNumber === "BA.2.02.06.01";
				case "231":
					return groupNumber === "BA.2.02.06.01";
				case "293":
					return groupNumber === "BA.2.02.06.02";
				case "2931":
					return groupNumber === "BA.2.02.06.02";
				case "2932":
					return groupNumber === "BA.2.01.08.02";
				case "238":
					return groupNumber === "BA.2.02.07";
				case "25":
					return groupNumber === "BA.2.03.01.01";
				case "26":
					return groupNumber === "BA.2.03.01.02";
				case "261":
					return groupNumber === "BA.2.03.01.02";
				case "2611":
					return groupNumber === "BA.2.03.01.03";
				case "2618":
					return groupNumber === "BA.2.03.01.04";
				case "266":
					return groupNumber === "BA.2.03.01.05";
				case "296":
					return groupNumber === "BA.2.03.01.06";
				case "2961":
					return groupNumber === "BA.2.03.01.07";
				case "2966":
					return groupNumber === "BA.2.03.01.08";
				case "2967":
					return groupNumber === "BA.2.03.01.09";
				case "18":
					return groupNumber === "BA.2.03.02.01";
				case "181":
					return groupNumber === "BA.2.03.02.02";
				case "186":
					return groupNumber === "BA.2.03.02.03";
				case "187":
					return groupNumber === "BA.2.03.02.04";
				case "188":
					return groupNumber === "BA.2.03.02.05";
				case "267":
					return groupNumber === "BA.2.03.02.06";
				case "268":
					return groupNumber === "BA.2.03.02.07";
				case "2968":
					return groupNumber === "BA.2.03.02.08";
				case "273":
					return groupNumber === "BA.2.03.03";
				case "27":
					return groupNumber === "BA.2.03.04.01";
				case "271":
					return groupNumber === "BA.2.03.04.01";
				case "272":
					return groupNumber === "BA.2.03.04.02";
				case "27682":
					return groupNumber === "BA.2.03.04.03";
				case "277":
					return groupNumber === "BA.2.03.04.04";
				case "2771":
					return groupNumber === "BA.2.03.04.05";
				case "2772":
					return groupNumber === "BA.2.03.04.06";
				case "274":
					return groupNumber === "BA.2.03.05.01";
				case "27684":
					return groupNumber === "BA.2.03.05.02";
				case "275":
					return groupNumber === "BA.2.03.06.01";
				case "276":
					return groupNumber === "BA.2.03.06.02";
				case "2761":
					return groupNumber === "BA.2.03.06.03";
				case "2768":
					return groupNumber === "BA.2.03.06.04";
				case "27685":
					return groupNumber === "BA.2.03.06.05";
				case "27688":
					return groupNumber === "BA.2.03.06.06";
				case "297":
					return groupNumber === "BA.2.03.06.07";
				case "2971":
					return groupNumber === "BA.2.03.06.08";
				case "2972":
					return groupNumber === "BA.2.03.06.09";
				case "2973":
					return groupNumber === "BA.2.03.06.10";
				case "2974":
					return groupNumber === "BA.2.03.06.11";
				case "2975":
					return groupNumber === "BA.2.03.06.12";
				case "2976":
					return groupNumber === "BA.2.03.06.13";
				case "31":
					return groupNumber === "BA.3.01.01.02";
				case "32":
					return groupNumber === "BA.3.01.01.03";
				case "36":
					return groupNumber === "BA.3.01.01.04";
				case "38":
					return groupNumber === "BA.3.01.01.05";
				case "39":
					return groupNumber === "BA.3.01.01.06";
				case "391":
					return groupNumber === "BA.3.01.01.06";
				case "3917":
					return groupNumber === "BA.3.01.01.07";
				case "392":
					return groupNumber === "BA.3.01.01.08";
				case "3921":
					return groupNumber === "BA.3.01.01.09";
				case "3922":
					return groupNumber === "BA.3.01.01.10";
				case "3926":
					return groupNumber === "BA.3.01.01.11";
				case "33":
					return groupNumber === "BA.3.01.02.01";
				case "34":
					return groupNumber === "BA.3.01.02.02";
				case "393":
					return groupNumber === "BA.3.01.02.03";
				case "3931":
					return groupNumber === "BA.3.01.02.04";
				case "3935":
					return groupNumber === "BA.3.01.02.05";
				case "394":
					return groupNumber === "BA.3.01.02.06";
				case "3941":
					return groupNumber === "BA.3.01.02.07";
				case "3945":
					return groupNumber === "BA.3.01.02.08";
				case "35":
					return groupNumber === "BA.3.01.03.01";
				case "395":
					return groupNumber === "BA.3.01.03.04";
				case "3951":
					return groupNumber === "BA.3.01.03.02";
				case "3955":
					return groupNumber === "BA.3.01.03.03";
				case "37":
					return groupNumber === "BA.3.01.04.01";
				case "397":
					return groupNumber === "BA.3.01.04.02";
				case "41":
					return groupNumber === "BA.3.03.01.01";
				case "411":
					return groupNumber === "BA.3.03.01.01";
				case "413":
					return groupNumber === "BA.3.03.01.02";
				case "416":
					return groupNumber === "BA.3.03.01.03";
				case "418":
					return groupNumber === "BA.3.03.01.04";
				case "49":
					return groupNumber === "BA.3.03.01.05";
				case "491":
					return groupNumber === "BA.3.03.01.05";
				case "495":
					return groupNumber === "BA.3.03.01.06";
				case "4951":
					return groupNumber === "BA.3.03.01.07";
				case "4955":
					return groupNumber === "BA.3.03.01.08";
				case "4958":
					return groupNumber === "BA.3.03.01.09";
				case "496":
					return groupNumber === "BA.3.03.01.10";
				case "4962":
					return groupNumber === "BA.3.03.01.11";
				case "4965":
					return groupNumber === "BA.3.03.01.12";
				case "4967":
					return groupNumber === "BA.3.03.01.13";
				case "42":
					return groupNumber === "BA.3.03.02.07";
				case "421":
					return groupNumber === "BA.3.03.02.07";
				case "422":
					return groupNumber === "BA.3.03.02.08";
				case "424":
					return groupNumber === "BA.3.03.02.09";
				case "425":
					return groupNumber === "BA.3.03.02.10";
				case "426":
					return groupNumber === "BA.3.03.02.11";
				case "427":
					return groupNumber === "BA.3.03.02.12";
				case "428":
					return groupNumber === "BA.3.03.02.13";
				case "43":
					return groupNumber === "BA.3.03.02.14";
				case "431":
					return groupNumber === "BA.3.03.02.14";
				case "437":
					return groupNumber === "BA.3.03.02.15";
				case "438":
					return groupNumber === "BA.3.03.02.16";
				case "44":
					return groupNumber === "BA.3.03.02.17";
				case "441":
					return groupNumber === "BA.3.03.02.17";
				case "442":
					return groupNumber === "BA.3.03.02.18";
				case "443":
					return groupNumber === "BA.3.03.02.19";
				case "444":
					return groupNumber === "BA.3.03.02.20";
				case "445":
					return groupNumber === "BA.3.03.02.21";
				case "446":
					return groupNumber === "BA.3.03.02.22";
				case "447":
					return groupNumber === "BA.3.03.02.23";
				case "448":
					return groupNumber === "BA.3.03.02.24";
				case "449":
					return groupNumber === "BA.3.03.02.25";
				case "45":
					return groupNumber === "BA.3.03.02.26";
				case "451":
					return groupNumber === "BA.3.03.02.26";
				case "455":
					return groupNumber === "BA.3.03.02.27";
				case "456":
					return groupNumber === "BA.3.03.02.28";
				case "4561":
					return groupNumber === "BA.3.03.02.29";
				case "4563":
					return groupNumber === "BA.3.03.02.30";
				case "4564":
					return groupNumber === "BA.3.03.02.31";
				case "4566":
					return groupNumber === "BA.3.03.02.32";
				case "4567":
					return groupNumber === "BA.3.03.02.33";
				case "458":
					return groupNumber === "BA.3.03.02.34";
				case "46":
					return groupNumber === "BA.3.03.02.35";
				case "462":
					return groupNumber === "BA.3.03.02.35";
				case "465":
					return groupNumber === "BA.3.03.02.36";
				case "467":
					return groupNumber === "BA.3.03.02.37";
				case "468":
					return groupNumber === "BA.3.03.02.38";
				case "4686":
					return groupNumber === "BA.3.03.02.38";
				case "4687":
					return groupNumber === "BA.3.03.02.38";
				case "47":
					return groupNumber === "BA.3.03.02.39";
				case "471":
					return groupNumber === "BA.3.03.02.39";
				case "472":
					return groupNumber === "BA.3.03.02.39";	
				case "473":
					return groupNumber === "BA.3.03.02.39";	
				case "474":
					return groupNumber === "BA.3.03.02.39";
				case "475":
					return groupNumber === "BA.3.03.02.39";	
				case "478":
					return groupNumber === "BA.3.03.02.44";
				case "4887":
					return groupNumber === "BA.3.03.02.45";
				case "4562":
					return groupNumber === "BA.3.04";
				case "502":
					return groupNumber === "BA.3.05.01.01";
				case "59":
					return groupNumber === "BA.3.05.01.02";
				case "590":
					return groupNumber === "BA.3.05.01.02";
				case "5903":
					return groupNumber === "BA.3.05.01.03";
				case "50":
					return groupNumber === "BA.3.05.02.01";
				case "501":
					return groupNumber === "BA.3.05.02.01";
				case "503":
					return groupNumber === "BA.3.05.02.02";
				case "504":
					return groupNumber === "BA.3.05.02.03";
				case "505":
					return groupNumber === "BA.3.05.02.04";
				case "506":
					return groupNumber === "BA.3.05.02.05";
				case "507":
					return groupNumber === "BA.3.05.02.06";
				case "508":
					return groupNumber === "BA.3.05.02.07";
				case "5904":
					return groupNumber === "BA.3.05.02.08";
				case "5906":
					return groupNumber === "BA.3.05.02.09";
				case "5908":
					return groupNumber === "BA.3.05.02.10";
				case "52":
					return groupNumber === "BA.3.06";
				case "51":
					return groupNumber === "BA.3.07.01";
				case "511":
					return groupNumber === "BA.3.07.01";
				case "512":
					return groupNumber === "BA.3.07.02";
				case "5121":
					return groupNumber === "BA.3.07.02";
				case "5124":
					return groupNumber === "BA.3.07.02";
				case "514":
					return groupNumber === "BA.3.07.03";
				case "515":
					return groupNumber === "BA.3.07.04";
				case "516":
					return groupNumber === "BA.3.07.05";
				case "517":
					return groupNumber === "BA.3.07.06";
				case "518":
					return groupNumber === "BA.3.07.07";
				case "5181":
					return groupNumber === "BA.3.07.07";
				case "5186":
					return groupNumber === "BA.3.07.07";
				case "5188":
					return groupNumber === "BA.3.07.08";
				case "5187":
					return groupNumber === "BA.3.07.08";
				case "519":
					return groupNumber === "BA.3.07.09";
				case "53":
					return groupNumber === "BA.3.07.10";
				case "54":
					return groupNumber === "BA.3.07.11";
				case "58":
					return groupNumber === "BA.3.07.12";
				case "486":
					return groupNumber === "BA.3.08.01";
				case "4886":
					return groupNumber === "BA.3.08.02";
				case "48":
					return groupNumber === "BA.3.09";
				case "481":
					return groupNumber === "BA.3.09";
				case "169":
					return groupNumber === "BA.3.10";
				case "476":
					return groupNumber === "BA.3.11";
				case "10":
					return groupNumber === "BP.1.01.01";
				case "101":
					return groupNumber === "BP.1.01.01";
				case "108":
					return groupNumber === "BP.1.01.02";
				case "102":	
					return groupNumber === "BP.1.02.01";
				case "104":
					return groupNumber === "BP.1.02.02";
				case "105":
					return groupNumber === "BP.1.03";
				case "107":
					return groupNumber === "BP.1.04";
				case "106":
					return groupNumber === "BP.1.05.01";
				case "1061":
					return groupNumber === "BP.1.05.01";
				case "1063":
					return groupNumber === "BP.1.05.02";
				case "1062":
					return groupNumber === "BP.1.05.03.01";
				case "1064":
					return groupNumber === "BP.1.05.03.02";
				case "1068":
					return groupNumber === "BP.1.05.04";
				case "11":
					return groupNumber === "BP.1.06.01";
				case "110":
					return groupNumber === "BP.1.06.01";
				case "119":
					return groupNumber === "BP.1.06.02";
				case "12":
					return groupNumber === "BP.1.07.01";
				case "120":
					return groupNumber === "BP.1.07.01";
				case "129":
					return groupNumber === "BP.1.07.01";
				case "13":
					return groupNumber === "BP.1.08.01";
				case "131":
					return groupNumber === "BP.1.08.01";
				case "138":
					return groupNumber === "BP.1.08.02";
				case "139":
					return groupNumber === "BP.1.08.03";
				case "14":
					return groupNumber === "BP.1.09.01";
				case "142":
					return groupNumber === "BP.1.09.02";
				case "143":
					return groupNumber === "BP.1.09.03";
				case "144":
					return groupNumber === "BP.1.09.04";
				case "145":
					return groupNumber === "BP.1.09.05";
				case "146":
					return groupNumber === "BP.1.09.06";
				case "147":
					return groupNumber === "BP.1.09.07";
				case "148":
					return groupNumber === "BP.1.09.08";
				case "167":
					return groupNumber === "BP.2.01";
				case "1671":
					return groupNumber === "BP.2.01";
				case "1674":
					return groupNumber === "BP.2.02";
				case "15":
					return groupNumber === "BP.3.01";
				case "151":
					return groupNumber === "BP.3.01";
				case "153":
					return groupNumber === "BP.3.02.01";
				case "154":
					return groupNumber === "BP.3.02.02";
				case "155":
					return groupNumber === "BP.3.02.03";
				case "156":
					return groupNumber === "BP.3.02.04";
				case "157":
					return groupNumber === "BP.3.02.05";
				case "158":
					return groupNumber === "BP.3.02.06";
				case "16":
					return groupNumber === "BP.4.01.01";
				case "161":
					return groupNumber === "BP.4.01.01";
				case "1688":
					return groupNumber === "BP.4.01.02";	
				case "16881":
					return groupNumber === "BP.4.01.02";
				case "162":
					return groupNumber === "BP.4.02.01";
				case "163":
					return groupNumber === "BP.4.02.02";
				case "16883":
					return groupNumber === "BP.4.02.03";
				case "164":
					return groupNumber === "BP.4.03.01";
				case "16884":
					return groupNumber === "BP.4.03.02";
				case "165":
					return groupNumber === "BP.4.04.01";
				case "166":
					return groupNumber === "BP.4.04.02";
				case "1675":
					return groupNumber === "BP.4.04.03";
				case "168":
					return groupNumber === "BP.4.04.04";
				case "16885":
					return groupNumber === "BP.4.04.05";
				case "16886":
					return groupNumber === "BP.4.04.06";
				case "16887":
					return groupNumber === "BP.4.04.07";
				case "16888":
					return groupNumber === "BP.4.04.08";
				case "17":
					return groupNumber === "BP.4.04.09";
				case "171":
					return groupNumber === "BP.4.04.09";
				case "174":
					return groupNumber === "BP.4.04.10";
				case "178":
					return groupNumber === "BP.4.04.11";
				case "40":
					return groupNumber === "BP.4.05.01";
				case "401":
					return groupNumber === "BP.4.05.01";
				case "403":
					return groupNumber === "BP.4.05.04";
				case "404":
					return groupNumber === "BP.4.05.05";
				case "405":
					return groupNumber === "BP.4.05.08";
				case "408":
					return groupNumber === "BP.4.05.09";
				case "4081":
					return groupNumber === "BP.4.05.09";
				case "4084":
					return groupNumber === "BP.4.05.10";
				case "4088":
					return groupNumber === "BP.4.05.11";
				case "409":
					return groupNumber === "BA.3.02";
				case "4091":
					return groupNumber === "BA.3.02";
				case "4096":
					return groupNumber === "BA.3.02.01";
				case "4097":
					return groupNumber === "BA.3.02.02";
				case "4098":
					return groupNumber === "BA.3.02";
				case "457":
					return groupNumber === "BP.4.06.01";
				case "4419":
					return groupNumber === "BP.4.06.20";
				case "4424":
					return groupNumber === "BP.4.06.22";
				case "4425":
					return groupNumber === "BP.4.06.23";
				case "269":
					return groupNumber === "BP.4.07.01";
				case "279":
					return groupNumber === "BP.4.07.02";
				case "419":
					return groupNumber === "BP.4.08.22";
				case "464":
					return groupNumber === "BP.4.08.23";
				case "509":
					return groupNumber === "BP.4.08.27";
				case "487":
					return groupNumber === "BP.4.10.01";
				case "477":
					return groupNumber === "BP.4.11";
				case "607":
					return groupNumber === "RC.1.01.01";
				case "608":
					return groupNumber === "RC.1.01.02";
				case "6097":
					return groupNumber === "RC.1.01.03";
				case "6098":
					return groupNumber === "RC.1.01.04";
				case "6037":
					return groupNumber === "RC.1.02";
				case "60":
					return groupNumber === "RC.1.03.01";
				case "601":
					return groupNumber === "RC.1.03.01";
				case "602":
					return groupNumber === "RC.1.03.02";
				case "609":
					return groupNumber === "RC.1.03.03";
				case "6091":
					return groupNumber === "RC.1.03.03";
				case "603":
					return groupNumber === "RC.1.04.01";
				case "6031":
					return groupNumber === "RC.1.04.01";
				case "6032":
					return groupNumber === "RC.1.04.02";
				case "604":
					return groupNumber === "RC.1.05.01";
				case "605":
					return groupNumber === "RC.1.05.02";
				case "606":
					return groupNumber === "RC.1.05.03";
				case "6094":
					return groupNumber === "RC.1.05.04";
				case "6092":
					return groupNumber === "RC.1.03.04";
				case "6095":
					return groupNumber === "RC.1.05.05";
				case "6096":
					return groupNumber === "RC.1.05.06";
				case "61":
					return groupNumber === "RC.1.05.07";
				case "611":
					return groupNumber === "RC.1.05.07";
				case "612":
					return groupNumber === "RC.1.05.08";
				case "613":
					return groupNumber === "RC.1.05.09";
				case "614":
					return groupNumber === "RC.1.05.10";
				case "615":
					return groupNumber === "RC.1.05.11";
				case "616":
					return groupNumber === "RC.1.05.12";
				case "617":
					return groupNumber === "RC.1.05.13";
				case "618":
					return groupNumber === "RC.1.05.14";
				case "619":
					return groupNumber === "RC.1.05.15";
				case "62":
					return groupNumber === "RC.1.05.16";
				case "621":
					return groupNumber === "RC.1.05.17";
				case "622":
					return groupNumber === "RC.1.05.18";
				case "623":
					return groupNumber === "RC.1.05.19";
				case "624":
					return groupNumber === "RC.1.05.20";
				case "625":
					return groupNumber === "RC.1.05.21";
				case "626":
					return groupNumber === "RC.1.05.22";
				case "627":
					return groupNumber === "RC.1.05.23";
				case "628":
					return groupNumber === "RC.1.05.24";
				case "629":
					return groupNumber === "RC.1.05.25";
				case "63":
					return groupNumber === "RC.1.06.01";
				case "631":
					return groupNumber === "RC.1.06.02";
				case "633":
					return groupNumber === "RC.1.06.03";
				case "635":
					return groupNumber === "RC.1.06.04";
				case "637":
					return groupNumber === "RC.1.06.05";
				case "64":
					return groupNumber === "RC.1.07.01";
				case "641":
					return groupNumber === "RC.1.07.01";
				case "644":
					return groupNumber === "RC.1.07.02";
				case "645":
					return groupNumber === "RC.1.08.01";
				case "646":
					return groupNumber === "RC.1.08.02";
				case "647":
					return groupNumber === "RC.1.08.03";
				case "648":
					return groupNumber === "RC.1.08.04";
				case "649":
					return groupNumber === "RC.1.08.05";
				case "68":
					return groupNumber === "RC.1.09.01";
				case "681":
					return groupNumber === "RC.1.09.01";
				case "6811":
					return groupNumber === "RC.1.09.01";
				case "6812":
					return groupNumber === "RC.1.09.02";
				case "6816":
					return groupNumber === "RC.1.09.03";
				case "6817":
					return groupNumber === "RC.1.10.01";
				case "68173":
					return groupNumber === "RC.1.10.02";
				case "68174":
					return groupNumber === "RC.1.10.03";
				case "6815":
					return groupNumber === "RC.1.11";
				case "65":
					return groupNumber === "RC.1.12.01";
				case "651":
					return groupNumber === "RC.1.12.01";
				case "653":
					return groupNumber === "RC.1.12.02";
				case "654":
					return groupNumber === "RC.1.12.03";
				case "658":
					return groupNumber === "RC.1.12.04";
		        case "656":
					return groupNumber === "RC.1.12.04";
				case "655":
					return groupNumber === "RC.2.01";
				case "686":
					return groupNumber === "RC.3.01";
				case "66":
					return groupNumber === "RC.3.02.01";
				case "661":
					return groupNumber === "RC.3.02.01";
				case "6611":
					return groupNumber === "RC.3.02.02";
				case "6612":
					return groupNumber === "RC.3.02.03";
				case "6615":
					return groupNumber === "RC.3.02.04";
				case "6616":
					return groupNumber === "RC.3.02.05";
				case "6617":
					return groupNumber === "RC.3.02.06";
				case "6618":
					return groupNumber === "RC.3.02.07";
				case "664":
					return groupNumber === "RC.3.02.08";
				case "665":
					return groupNumber === "RC.3.02.09";
				case "668":
					return groupNumber === "RC.3.02.10";
				case "666":
					return groupNumber === "RC.3.03";
				case "667":
					return groupNumber === "RC.3.04";
				case "67":
					return groupNumber === "RC.4.01.01";
				case "671":
					return groupNumber === "RC.4.01.01";
				case "672":
					return groupNumber === "RC.4.01.02";
				case "674":
					return groupNumber === "RC.4.01.03";
				case "6741":
					return groupNumber === "RC.4.01.04";
				case "6742":
					return groupNumber === "RC.4.01.05";
				case "675":
					return groupNumber === "RC.4.02.01";
				case "6751":
					return groupNumber === "RC.4.02.02";
				case "6752":
					return groupNumber === "RC.4.02.03";
				case "6756":
					return groupNumber === "RC.4.02.04";
				case "6758":
					return groupNumber === "RC.4.02.05";
				case "678":
					return groupNumber === "RC.4.02.06";
				case "687":
					return groupNumber === "RC.4.03.01";
				case "6871":
					return groupNumber === "RC.4.03.01";
				case "6872":
					return groupNumber === "RC.4.03.02";
				case "6873":
					return groupNumber === "RC.4.03.03";
				case "6874":
					return groupNumber === "RC.4.03.04";
				case "6875":
					return groupNumber === "RC.4.03.05";
				case "6876":
					return groupNumber === "RC.4.03.06";
				case "69":
					return groupNumber === "RC.5";
				case "691":
					return groupNumber === "RC.5";
				case "695":
					return groupNumber === "RC.6.01";
				case "696":
					return groupNumber === "RC.6.02";
				case "698":
					return groupNumber === "RC.6.03";
				case "699":
					return groupNumber === "RC.6.04";
				case "707":
					return groupNumber === "RP.1.01";
				case "70":
					return groupNumber === "RP.1.02.01";
				case "701":
					return groupNumber === "RP.1.02.01";
				case "702":
					return groupNumber === "RP.1.02.02";
				case "703":
					return groupNumber === "RP.1.02.03";
				case "704":
					return groupNumber === "RP.1.02.04";
				case "705":
					return groupNumber === "RP.1.02.05";
				case "706":
					return groupNumber === "RP.1.02.06";
				case "708":
					return groupNumber === "RP.1.02.07";
				case "709":
					return groupNumber === "RP.1.02.08";
				case "71":
					return groupNumber === "RP.1.03";
				case "713":
					return groupNumber === "RP.1.03";
				case "72":
					return groupNumber === "RP.1.04.01";
				case "721":
					return groupNumber === "RP.1.04.02";
				case "722":
					return groupNumber === "RP.1.04.03";
				case "74":
					return groupNumber === "RP.1.05";
				case "78":
					return groupNumber === "RP.1.06.01";
				case "781":
					return groupNumber === "RP.1.06.01";
				case "7811":
					return groupNumber === "RP.1.06.01";
				case "7815":
					return groupNumber === "RP.1.06.02";
				case "7816":
					return groupNumber === "RP.1.06.03";
				case "7817":
					return groupNumber === "RP.1.06.04";
				case "78173":
					return groupNumber === "RP.1.06.05";
				case "78174":
					return groupNumber === "RP.1.06.06";
				case "79":
					return groupNumber === "RP.1.06.07";
				case "791":
					return groupNumber === "RP.1.06.07";
				case "75":
					return groupNumber === "RP.1.07.01";
				case "751":
					return groupNumber === "RP.1.07.01";
				case "752":
					return groupNumber === "RP.1.07.02";
				case "753":
					return groupNumber === "RP.1.07.03";
				case "754":
					return groupNumber === "RP.1.07.04";
				case "758":
					return groupNumber === "RP.1.07.05";
				case "755":
					return groupNumber === "RP.2.01";
				case "76":
					return groupNumber === "RP.3.01";
				case "761":
					return groupNumber === "RP.3.01";
				case "762":
					return groupNumber === "RP.3.02";
				case "763":
					return groupNumber === "RP.3.03.01";
				case "7631":
					return groupNumber === "RP.3.03.02";
				case "7638":
					return groupNumber === "RP.3.03.03";
				case "764":
					return groupNumber === "RP.3.03.04";
				case "765":
					return groupNumber === "RP.3.03.05";
				case "768":
					return groupNumber === "RP.3.03.06";
				case "786":
					return groupNumber === "RP.3.04.01";
				case "7865":
					return groupNumber === "RP.3.04.02";
				case "7866":
					return groupNumber === "RP.3.04.03";
				case "78662":
					return groupNumber === "RP.3.04.04";
				case "78665":
					return groupNumber === "RP.3.04.05";
				case "796":
					return groupNumber === "RP.3.04.06";
				case "766":
					return groupNumber === "RP.3.05";
				case "767":
					return groupNumber === "RP.3.06";
				case "77":
					return groupNumber === "RP.4.01.01";
				case "771":
					return groupNumber === "RP.4.01.01";
				case "772":
					return groupNumber === "RP.4.01.02";
				case "774":
					return groupNumber === "RP.4.01.03";
				case "775":
					return groupNumber === "RP.4.02.01";
				case "7751":
					return groupNumber === "RP.4.02.02";
				case "7752":
					return groupNumber === "RP.4.02.03";
				case "7756":
					return groupNumber === "RP.4.02.04";
				case "7758":
					return groupNumber === "RP.4.02.05";
				case "777":
					return groupNumber === "RP.4.02.06";
				case "778":
					return groupNumber === "RP.4.02.07";
				case "787":
					return groupNumber === "RP.4.03.01";
				case "7872":
					return groupNumber === "RP.4.03.01";
				case "78725":
					return groupNumber === "RP.4.03.02";
				case "78726":
					return groupNumber === "RP.4.03.03";
				case "78727":
					return groupNumber === "RP.4.03.04";
				case "7873":
					return groupNumber === "RP.4.03.05";
				case "7874":
					return groupNumber === "RP.4.03.06";
				case "7875":
					return groupNumber === "RP.4.03.07";
				case "7876":
					return groupNumber === "RP.4.03.08";
				case "797":
					return groupNumber === "RP.4.03.09";
			}
		});
		if (tag) 
		{
			return tag;
		}
		
		var parentNumber = '';
		//Réduis la chaine de 1 et relance la même fonction
		parentNumber = accountNumber.substring(0,accountNumber.length-1);
		return findMatch(groupNumbers, parentNumber);

	}

	
	wpw.tb.importUtilities.setAutoMapFunction(function(groupNumbers, accountNumber) {
		return findMatch(groupNumbers, accountNumber)
	})
})();