(function () {
  wpw.tax.create.tables("PDM", {
    infosGTable: {
      fixedRows: true,
      infoTable: true,
      staticTable: true,
      rowDeleteConfirmation: true,
      columns: [
        { type: "none", colClass: "alignCenter", header: "Nomination" },
        { type: "none", colClass: "std-spacing-width" },
        { type: "text", cellClass: "alignCenter", header: "Titulaire" },
        { type: "none", colClass: "std-spacing-width" },
        { type: "text", colClass: "alignCenter", header: "Signataire" },
        { type: "none", colClass: "std-spacing-width" },
        { type: "text", colClass: "alignCenter", header: "Date de nomination" },
        { type: "none", colClass: "std-spacing-width" },
        {
          type: "text",
          colClass: "alignCenter",
          header: "Date cloture dernier exercice contrôlé",
        },
        { type: "none", colClass: "std-spacing-width" },
        { type: "text", cellClass: "alignCenter", header: "Réf" },
      ],
      cells: [
        {
          0: { label: "CAC" },
          2: { num: "titulaireinfosGTable" },
          4: { num: "signataireinfosGTable" },
          6: { num: "datenominationinfosGTable" },
          8: { num: "cloturelastexoinfosGTable" },
          10: { num: "refinfosGTable" },
        },
        {
          0: { label: "CO-CAC" },
          2: { num: "titulaireinfosGTable1" },
          4: { num: "signataireinfosGTable1" },
          6: { num: "datenominationinfosGTable1" },
          8: { num: "cloturelastexoinfosGTable1" },
          10: { num: "refinfosGTable1" },
        },
      ],
    },
    techaudit: {
      fixedRows: true,
      infoTable: false,
      staticTable: true,
      rowDeleteConfirmation: true,
      columns: [
        {
          type: "none",
          colClass: "alignCenter",
          header: "Technique",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          colClass: "alignCenter",
          header: "Retenue",
          headerClass: "tableHeaderCells ",
        },
        {
          type: "none",
          colClass: "alignCenter",
          header: "Justification/Commentaires",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: { label: "Demandes de confirmations directes :" },
        },
        {
          0: { label: "Fournisseurs" },
          1: {
            type: "dropdown",
            num: "130",
            init: "1",
            options: [
              { option: "Oui", value: 1 },
              { option: "Non", value: 0 },
            ],
          },
          2: { type: "text", num: "techauditfournisseurs" },
        },
        {
          0: { label: "Clients" },
          1: {
            type: "dropdown",
            num: "131",
            init: "1",
            options: [
              { option: "Oui", value: 1 },
              { option: "Non", value: 0 },
            ],
          },
          2: { type: "text", num: "techauditclients" },
        },
        {
          0: { label: "Banques" },
          1: {
            type: "dropdown",
            num: "132",
            init: "1",
            options: [
              { option: "Oui", value: 1 },
              { option: "Non", value: 0 },
            ],
          },
          2: { type: "text", num: "techauditbanques" },
        },
        {
          0: { label: "Avocats" },
          1: {
            type: "dropdown",
            num: "133",
            init: "1",
            options: [
              { option: "Oui", value: 1 },
              { option: "Non", value: 0 },
            ],
          },
          2: { type: "text", num: "techauditavocats" },
        },
        {
          0: { label: "Tiers détenteurs de stocks" },
          1: {
            type: "dropdown",
            num: "134",
            init: "1",
            options: [
              { option: "Oui", value: 1 },
              { option: "Non", value: 0 },
            ],
          },
          2: { type: "text", num: "techaudittiersstocks" },
        },
        {
          0: { label: "Administration fiscale" },
          1: {
            type: "dropdown",
            num: "135",
            init: "1",
            options: [
              { option: "Oui", value: 1 },
              { option: "Non", value: 0 },
            ],
          },
          2: { type: "text", num: "techauditadminfiscale" },
        },
        {
          0: { label: "Administration sociale" },
          1: {
            type: "dropdown",
            num: "136",
            init: "1",
            options: [
              { option: "Oui", value: 1 },
              { option: "Non", value: 0 },
            ],
          },
          2: { type: "text", num: "techauditadminsociale" },
        },
        {
          0: { label: "Inventaire physique" },
          1: {
            type: "dropdown",
            num: "137",
            init: "1",
            options: [
              { option: "Oui", value: 1 },
              { option: "Non", value: 0 },
            ],
          },
          2: { type: "text", num: "techauditinventaire" },
        },
        {
          0: { label: "Déclaration de la Direction" },
          1: {
            type: "dropdown",
            num: "138",
            init: "1",
            options: [
              { option: "Oui", value: 1 },
              { option: "Non", value: 0 },
            ],
          },
          2: { type: "text", num: "techauditdéclar" },
        },
      ],
    },
    compteresultatHeader: {
      fixedRows: true,
      staticTable: true,
      infoTable: true,
      columns: [
        {
          width: "20%",
        },
        {
          width: "10%",
        },
        {
          width: "10%",
        },
        {
          width: "10%",
        },
        {
          width: "20%",
        },
      ],
      cells: [
        {
          0: { label: " ", labelClass: "center", type: "none" },
          1: {
            label: "N",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells leftCornerRounded",
            type: "none",
            labelSource: {
              fieldId: "yearN",
            },
          },
          2: {
            label: "N-1",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells",
            type: "none",
            labelSource: {
              fieldId: "yearN-1",
            },
          },
          3: {
            label: "Variations",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells rightCornerRounded",
            type: "none",
            labelSource: {
              fieldId: "dates",
            },
          },
          4: {
            label: "  ",
            labelClass: "center",
            type: "none",
          },
        },
      ],
    },
    compteresultat: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          width: "20%",
          header: "Compte de Résultat",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "5%",
          header: "K",
          headerSource: { fieldId: "NK€" },
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "5%",
          header: "%",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "5%",
          header: "K",
          headerSource: { fieldId: "NK€" },
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "5%",
          header: "%",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "5%",
          header: "Montant",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "5%",
          header: "%",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "20%",
          header: "Commentaire",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelcompteresultat1" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "peNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "peNK%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "peNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "peNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "peVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "peVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "peCOM", cellClass: "textArea" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelcompteresultat2" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "ceNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "ceNK%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "ceNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "ceNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "ceVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "ceVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "ceCOM" },
        },
        {
          0: {
            label: "Resultat d’exploitation",
            labelSource: { fieldId: "labelcompteresultat3" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "reNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "reN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "reNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "reNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "reVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "reVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "reCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelcompteresultat4" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "bptNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "bptN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "bptNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "bptNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "bptVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "bptVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "bptCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelcompteresultat5" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "pbtNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "pbtN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "pbtNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "pbtNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "pbtVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "pbtVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "pbtCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelcompteresultat6" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "pfNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "pfN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "pfNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "pfNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "pfVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "pfVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "pfCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelcompteresultat7" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "cfNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "cfN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "cfNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "cfNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "cfVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "cfVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "cfCOM" },
        },
        {
          0: {
            label: "Résultat financier",
            labelSource: { fieldId: "labelcompteresultat8" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "rfNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "rfN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "rfNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "rfNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "rfVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "rfVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "rfCOM" },
        },
        {
          0: {
            label: "Resultat courant avant impôt ",
            labelSource: { fieldId: "labelcompteresultat9" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "rcaiNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "rcaiN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "rcaiNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "rcaiNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "rcaiVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "rcaiVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "rcaiCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelcompteresultat10" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "pexcepNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "pexcepN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "pexcepNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "pexcepNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "pexcepVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "pexcepVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "pexcepCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelcompteresultat11" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "psrNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "psrN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "psrNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "psrNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "psrVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "psrVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "psrCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelcompteresultat12" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "ibNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "ibN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "ibNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "ibNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "ibVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "ibVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "ibCOM" },
        },
        {
          0: {
            label: "Résultat Net",
            labelSource: { fieldId: "labelcompteresultat13" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "rnNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "rnN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "rnNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "rnNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "rnVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "rnVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "rnCOM" },
        },
      ],
    },
    actifHeader: {
      fixedRows: true,
      staticTable: true,
      infoTable: true,
      columns: [
        {
          width: "20%",
        },
        {
          width: "10%",
        },
        {
          width: "10%",
        },
        {
          width: "10%",
        },
        {
          width: "20%",
        },
      ],
      cells: [
        {
          0: { label: " ", labelClass: "center", type: "none" },
          1: {
            label: "N",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells leftCornerRounded",
            type: "none",
            labelSource: {
              fieldId: "yearN",
            },
          },
          2: {
            label: "N-1",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells",
            type: "none",
            labelSource: {
              fieldId: "yearN-1",
            },
          },
          3: {
            label: "Variations",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells rightCornerRounded",
            type: "none",
            labelSource: {
              fieldId: "dates",
            },
          },
          4: {
            label: "  ",
            labelClass: "center",
            type: "none",
          },
        },
      ],
    },
    actif: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          width: "20%",
          header: "Bilan Actif",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "K",
          headerSource: { fieldId: "NK€" },
          headerClass: "tableHeaderCells",
          width: "10%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "%",
          headerClass: "tableHeaderCells",
          width: "10%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "K",
          headerSource: { fieldId: "NM1K€" },
          headerClass: "tableHeaderCells",
          width: "10%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "%",
          headerClass: "tableHeaderCells",
          width: "10%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "10%",
          header: "Montant",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "10%",
          header: "%",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "20%",
          header: "Commentaire",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: "label",
            colClass: "testCell",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label1" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "csnaNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "csnaN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "csnaNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "csnaNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "csnaVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "csnaVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "csnaCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label2" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "iiNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "iiN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "iiNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "iiNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "iiVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "iiVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "iiCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label3" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "icNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "icN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "icNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "icNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "icVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "icVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "icCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label4" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "ifNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "ifN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "ifNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "ifNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "ifVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "ifVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "ifCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "label5" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "taiNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "taiN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "taiNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "taiNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "taiVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "taiVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "taiCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label6" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "seecNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "seecN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "seecNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "seecNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "seecVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "seecVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "seecCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label7" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "cNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "cN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "cNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "cNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "cVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "cVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "cCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label8" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "vmdpNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "vmdpN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "vmdpNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "vmdpNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "vmdpVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "vmdpVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "vmdpCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label9" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "instruNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "instruN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "instruNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "instruNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "instruVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "instruVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "instruCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label10" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "dNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "dN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "dNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "dNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "dVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "dVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "dCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label11" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "ccaNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "ccaN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "ccaNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "ccaNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "ccaVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "ccaVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "ccaCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label12" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "aeavscNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "aeavscN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "aeavscNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "aeavscNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "aeavscVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "aeavscVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "aeavscCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "label13" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "tacecNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "tacecN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "tacecNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "tacecNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "tacecVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "tacecVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "tacecCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label14" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "crspeNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "crspeN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "crspeNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "crspeNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "crspeVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "crspeVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "crspeCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label15" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "preNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "preN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "preNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "preNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "preVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "preVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "preCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label16" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "ecaNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "ecaN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "ecaNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "ecaNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "ecaVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "ecaVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "ecaCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "label17" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "tbaNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "tbaN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "tbaNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "tbaNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "tbaVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "tbaVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "tbaCOM" },
        },
      ],
    },
    passifHeader: {
      fixedRows: true,
      staticTable: true,
      infoTable: true,
      columns: [
        {
          width: "20%",
        },
        {
          width: "10%",
        },
        {
          width: "10%",
        },
        {
          width: "10%",
        },
        {
          width: "20%",
        },
      ],
      cells: [
        {
          0: { label: " ", labelClass: "center", type: "none" },
          1: {
            label: "N",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells leftCornerRounded",
            type: "none",
            labelSource: {
              fieldId: "yearN",
            },
          },
          2: {
            label: "N-1",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells",
            type: "none",
            labelSource: {
              fieldId: "yearN-1",
            },
          },
          3: {
            label: "Variations",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells rightCornerRounded",
            type: "none",
            labelSource: {
              fieldId: "dates",
            },
          },
          4: {
            label: "  ",
            labelClass: "center",
            type: "none",
          },
        },
      ],
    },
    passif: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          width: "20%",
          header: "Bilan Passif",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "10%",
          header: "K",
          headerSource: { fieldId: "NK€" },
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "10%",
          header: "%",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "10%",
          header: "K",
          headerSource: { fieldId: "NM1K€" },
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "10%",
          header: "%",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "10%",
          header: "Montant",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "10%",
          header: "%",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "20%",
          header: "Commentaire",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif1" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "capiPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "capiPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "capiPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "capiPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "capiVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "capiVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "capiCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif2" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "primesPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "primesPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "primesPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "primesPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "primesVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "primesVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "primesCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif3" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "ecartsreevalPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "ecartsreevalPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "ecartsreevalPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "ecartsreevalPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "ecartsreevalVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "ecartsreevalVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "ecartsreevalCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif4" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "ecartequivalencePNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "ecartequivalencePN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "ecartequivalencePNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "ecartequivalencePNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "ecartequivalenceVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "ecartequivalenceVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "ecartequivalenceCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif5" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "reservesPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "reservesPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "reservesPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "reservesPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "reservesVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "reservesVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "reservesCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif6" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "reportnouveauPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "reportnouveauPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "reportnouveauPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "reportnouveauPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "reportnouveauVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "reportnouveauVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "reportnouveauCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif7" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "resultatexercicePNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "resultatexercicePN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "resultatexercicePNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "resultatexercicePNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "resultatexerciceVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "resultatexerciceVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "resultatexerciceCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif8" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "subventionPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "subventionPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "subventionPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "subventionPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "subventionVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "subventionVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "subventionCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif9" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "provisionPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "provisionPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "provisionPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "provisionPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "provisionVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "provisionVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "provisionCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "labelPassif10" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "capitauxPNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "capitauxPN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "capitauxPNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "capitauxPNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "capitauxVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "capitauxVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "capitauxCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "labelPassif11" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "autresfondsPNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "autresfondsPN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "autresfondsPNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "autresfondsPNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "autresfondsVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "autresfondsVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "autresfondsCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "labelPassif12" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "provisionsPNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "provisionsPN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "provisionsPNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "provisionsPNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "provisionsVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "provisionsVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "provisionsCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif13" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "eocPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "eocPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "eocPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "eocPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "eocVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "eocVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "eocCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif14" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "aeoPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "aeoPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "aeoPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "aeoPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "aeoVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "aeoVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "aeoCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif15" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "edecPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "edecPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "edecPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "edecPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "edecVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "edecVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "edecCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif16" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "edfdPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "edfdPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "edfdPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "edfdPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "edfdVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "edfdVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "edfdCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif17" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "dfcrPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "dfcrPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "dfcrPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "dfcrPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "dfcrVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "dfcrVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "dfcrCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif18" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "dfsPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "dfsPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "dfsPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "dfsPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "dfsVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "dfsVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "dfsCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif19" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "dicrPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "dicrPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "dicrPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "dicrPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "dicrVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "dicrVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "dicrCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif20" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "autresdettesPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "autresdettesPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "autresdettesPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "autresdettesPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "autresdettesVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "autresdettesVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "autresdettesCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif21" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "itPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "itPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "itPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "itPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "itVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "itVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "itCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif22" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "pcaPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "pcaPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "pcaPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "pcaPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "pcaVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "pcaVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "pcaCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif23" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "ecpPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "ecpPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "ecpPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "ecpPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "ecpVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "ecpVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "ecpCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "labelPassif24" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "dettesPNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "dettesPN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "dettesPNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "dettesPNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "dettesVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "dettesVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "dettesCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "labelPassif25" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "passifPNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "passifPN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "passifPNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "passifPNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "passifVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "passifVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "passifCOM" },
        },
      ],
    },
    pdcdos: {
      fixedRows: false,
      infoTable: true,
      staticTable: true,
      rowDeleteConfirmation: true,
      columns: [
        { type: "text", colClass: "alignCenter", header: "Objectif" },
        { type: "none", colClass: "std-spacing-width" },
        {
          type: "text",
          cellClass: "alignCenter",
          header: "Stratégie mis en oeuvre",
        },
        { type: "none", colClass: "std-spacing-width" },
        {
          type: "text",
          colClass: "alignCenter",
          header: "Facteurs clés de succès",
        },
        { type: "none", colClass: "std-spacing-width" },
        { type: "text", cellClass: "alignCenter", header: "Indicateurs" },
        { type: "none", colClass: "std-spacing-width" },
        { type: "text", cellClass: "alignCenter", header: "Responsable" },
        { type: "none", colClass: "std-spacing-width" },
        { type: "text", cellClass: "alignCenter", header: "Réf" },
      ],
      cells: [
        {
          0: { num: "Objectifpdcdos" },
          2: { num: "Stratégie mis en oeuvrepdcdos" },
          4: { num: "Facteurs clés de succèspdcdos" },
          6: { num: "Indicateurspdcdos" },
          8: { num: "Responsablepdcdos" },
          10: { num: "refpdcdos" },
        },
      ],
    },
    pariesliées: {
      fixedRows: false,
      infoTable: false,
      staticTable: true,
      rowDeleteConfirmation: true,
      columns: [
        { type: "text", colClass: "alignCenter", header: "Partie liée" },
        {
          type: "text",
          cellClass: "alignCenter",
          header: "Représentant",
        },
        {
          type: "text",
          colClass: "alignCenter",
          header: "Mandat",
        },
        {
          type: "text",
          cellClass: "alignCenter",
          header: "Date de fin de mandat",
        },
        {
          type: "text",
          cellClass: "alignCenter",
          header: "Nature de la convention",
        },
        { type: "text", cellClass: "alignCenter", header: "Commentaire" },
      ],
      cells: [
        {
          0: { num: "partiesliéesPartie" },
          2: { num: "partiesliéesReprésentant" },
          4: { num: "partiesliéesMandat" },
          6: { num: "partiesliéesDateFin" },
          8: { num: "partiesliéesNature" },
          10: { num: "partiesliéesCOM" },
        },
      ],
    },
    filiales: {
      fixedRows: false,
      infoTable: false,
      staticTable: true,
      rowDeleteConfirmation: true,
      columns: [
        { type: "text", colClass: "alignCenter", header: "Forme juridique" },
        {
          type: "text",
          cellClass: "alignCenter",
          header: "Dénomination",
        },
        {
          type: "text",
          colClass: "alignCenter",
          header: "N° SIREN / Nationalité",
        },
        { type: "text", cellClass: "alignCenter", header: "Auditeur (CAC)" },
        { type: "text", cellClass: "alignCenter", header: "Nb de parts" },
        { type: "text", cellClass: "alignCenter", header: "% détention" },
        { type: "text", cellClass: "alignCenter", header: "Nb droits de vote" },
        { type: "text", cellClass: "alignCenter", header: "% droit vote" },
        { type: "date", cellClass: "alignCenter", header: "date acquisition" },
        { type: "textArea", cellClass: "alignCenter", header: "Commentaire" },
      ],
      cells: [],
    },
    contrats: {
      fixedRows: false,
      infoTable: false,
      staticTable: true,
      rowDeleteConfirmation: true,
      columns: [
        { type: "text", colClass: "alignCenter", header: "Contrat" },
        {
          type: "date",
          cellClass: "alignCenter",
          header: "Date",
        },
        {
          type: "textArea",
          colClass: "alignCenter",
          header: "Description",
        },
        { type: "textArea", cellClass: "alignCenter", header: "Commentaire" },
        { type: "date", cellClass: "alignCenter", header: "Date Validité" },
      ],
      cells: [],
    },
    logiciels: {
      fixedRows: false,
      infoTable: false,
      staticTable: true,
      rowDeleteConfirmation: true,
      columns: [
        { type: "text", colClass: "alignCenter", header: "Nom du logiciel" },
        {
          type: "text",
          cellClass: "alignCenter",
          header: "Numéro de version",
        },
        {
          type: "date",
          colClass: "alignCenter",
          header: "Date de mise en place",
        },
        {
          type: "text",
          cellClass: "alignCenter",
          header: "Domaine fonctionnel",
        },
        {
          type: "text",
          cellClass: "alignCenter",
          header: "Liaision avec un autre logiciel",
        },
        { type: "text", cellClass: "alignCenter", header: "Source" },
        {
          type: "text",
          cellClass: "alignCenter",
          header: "Identité du vendeur",
        },
        { type: "text", cellClass: "alignCenter", header: "Type de service" },
        { type: "textArea", cellClass: "alignCenter", header: "Commentaire" },
      ],
      cells: [],
    },
    taches34: {
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          header: "Libellé",
          headerClass: "tableHeaderCells leftCornerRounded",
          type: "none",
          width: "30%",
        },
        {
          header: "Description / resolution",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "70%",
        },
        {
          header: "Statut",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "10%",
        },
        {
          header: "Date de fin",
          headerClass: "tableHeaderCells rightCornerRounded",
          type: "none",
          width: "10%",
        },
      ],
      cells: [],
    },
    rappeldesseuils: {
      fixedRows: true,
      infoTable: false,
      staticTable: true,
      rowDeleteConfirmation: true,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Période",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Seuils de Signification",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Seuils de Planification",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Anomalies insignifiantes",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: "Période précédente",
            cellClass: "labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "periodePrecedenteSI" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "periodePrecedentePlan" },
            cellClass: "labelRight",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "periodePrecedenteInsi" },
            cellClass: "labelRight",
          },
        },
        {
          0: {
            label: "Préliminaire",
            cellClass: "labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "preliSI" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "preliPlan" },
            cellClass: "labelRight",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "preliInsi" },
            cellClass: "labelRight",
          },
        },
        {
          0: {
            label: "Final",
            cellClass: "labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "FinalSI" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "FinalPlan" },
            cellClass: "labelRight",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "FinalInsi" },
            cellClass: "labelRight",
          },
        },
      ],
    },
    cdle: {
      fixedRows: false,
      infoTable: true,
      staticTable: true,
      rowDeleteConfirmation: true,
      columns: [
        { type: "text", colClass: "alignCenter", header: "Rôle" },
        { type: "none", colClass: "std-spacing-width" },
        { type: "text", cellClass: "alignCenter", header: "Raison sociale" },
        { type: "none", colClass: "std-spacing-width" },
        { type: "text", colClass: "alignCenter", header: "Nom" },
        { type: "none", colClass: "std-spacing-width" },
        {
          type: "text",
          cellClass: "alignCenter",
          header: "Nature de la mission",
        },
        { type: "none", colClass: "std-spacing-width" },
        { type: "text", cellClass: "alignCenter", header: "Adresse" },
        { type: "none", colClass: "std-spacing-width" },
        { type: "text", cellClass: "alignCenter", header: "Téléphone" },
        { type: "none", colClass: "std-spacing-width" },
        { type: "text", cellClass: "alignCenter", header: "E-mail" },
      ],
      cells: [
        {
          0: { num: "rôlecdle" },
          2: { num: "raisonsocialecdle" },
          4: { num: "nomcdle" },
          6: { num: "naturedelamission" },
          8: { num: "adressecdle" },
          10: { num: "téléphonecdle" },
          12: { num: "mailcdle" },
        },
      ],
    },
    risque921: {
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          type: "none",
          header: "Libellé",
          headerClass: "tableHeaderCells leftCornerRounded",
          colClass: "alignCenter",
        },
        {
          type: "none",
          header: "Cycles/assertions",
          headerClass: "tableHeaderCells",
          colClass: "alignCenter",
          width: "350px",
        },
        {
          type: "none",
          header: "Type",
          headerClass: "tableHeaderCells",
          colClass: "alignCenter",
          width: "80px",
        },
        {
          type: "none",
          header: "Réponse entité",
          headerClass: "tableHeaderCells",
          colClass: "alignCenter",
        },
        {
          type: "none",
          header: "Rep Audit",
          headerClass: "tableHeaderCells rightCornerRounded",
          colClass: "alignCenter",
        },
      ],
      cells: [],
    },
    taches93: {
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          header: "Libellé",
          headerClass: "tableHeaderCells leftCornerRounded",
          type: "none",
          width: "30%",
        },
        {
          header: "Description / resolution",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "70%",
        },
        {
          header: "Statut",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "10%",
        },
        {
          header: "Date de fin",
          headerClass: "tableHeaderCells rightCornerRounded",
          type: "none",
          width: "10%",
        },
      ],
      cells: [],
    },
    cycle1: {
      staticTable: true,
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          header: "Cycle Fournisseur",
          headerClass: "tableHeaderCells leftCornerRounded",
          type: "none",
          width: "30%",
        },
        {
          header: " ",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ass.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "4%",
        },
        {
          header: "Inh.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ctrl.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "R.A.S",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Synthèse Niveau de risque",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "30%",
        },
        {
          header: "Base",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ana.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Détails",
          headerClass: "tableHeaderCells rightCornerRounded",
          type: "none",
          width: "5%",
        },
      ],
      cells: [
        {
          2: {
            label: "E",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle1Einhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle1Econtrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle1ERAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle1EBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle1EAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle1EDétails",
          },
        },
        {
          1: {
            label: "Solde",
            labelSource: { fieldId: "solde" },
          },
          2: {
            label: "R",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle1Rinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle1Rcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle1RRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle1RBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle1RAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle1RDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle1label1" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle1solde1" },
          },
          2: {
            label: "C",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle1Cinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle1Ccontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle1CRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle1CBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle1CAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle1CDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle1label2" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle1solde2" },
          },
          2: {
            label: "M",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle1Minhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle1Mcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle1MRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle1MBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle1MAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle1MDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle1label3" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle1solde4" },
          },
          2: {
            label: "D",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle1Dinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle1Dcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle1DRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle1DBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle1DAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle1DDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle1label4" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle1solde4" },
          },
          2: {
            label: "S",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle1Sinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle1Scontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle1SRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle1SBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle1SAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle1SDétails",
          },
        },
      ],
    },

    cycle2: {
      staticTable: true,
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          header: "Cycle Clients",
          headerClass: "tableHeaderCells leftCornerRounded",
          type: "none",
          width: "30%",
        },
        {
          header: " ",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ass.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "4%",
        },
        {
          header: "Inh.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ctrl.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "R.A.S",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Synthèse Niveau de risque",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "30%",
        },
        {
          header: "Base",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ana.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Détails",
          headerClass: "tableHeaderCells rightCornerRounded",
          type: "none",
          width: "5%",
        },
      ],
      cells: [
        {
          2: {
            label: "E",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle2Einhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle2Econtrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle2ERAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle2EBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle2EAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle2EDétails",
          },
        },
        {
          1: {
            label: "Solde",
            labelSource: { fieldId: "solde" },
          },
          2: {
            label: "R",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle2Rinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle2Rcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle2RRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle2RBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle2RAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle2RDétails",
          },
        },
        {
          0: {
            label: "Comptes Clients",
            labelSource: { fieldId: "cycle2label1" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle2solde1" },
          },
          2: {
            label: "C",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle2Cinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle2Ccontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle2CRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle2CBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle2CAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle2CDétails",
          },
        },
        {
          0: {
            label: "Avances / Dettes cptes client",
            labelSource: { fieldId: "cycle2label2" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle2solde2" },
          },
          2: {
            label: "M",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle2Minhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle2Mcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle2MRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle2MBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle2MAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle2MDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle2label3" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle2solde3" },
          },
          2: {
            label: "D",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle2Dinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle2Dcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle2DRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle2DBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle2DAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle2DDétails",
          },
        },
        {
          0: {
            label: "Créances Fournisseurs",
            labelSource: { fieldId: "cycle2label4" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle2solde4" },
          },
          2: {
            label: "S",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle2Sinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle2Scontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle2SRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle2SBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle2SAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle2SDétails",
          },
        },
      ],
    },
    cycle3: {
      staticTable: true,
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          header: "Cycle Stock",
          headerClass: "tableHeaderCells leftCornerRounded",
          type: "none",
          width: "30%",
        },
        {
          header: " ",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ass.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "4%",
        },
        {
          header: "Inh.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ctrl.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "R.A.S",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Synthèse Niveau de risque",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "30%",
        },
        {
          header: "Base",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ana.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Détails",
          headerClass: "tableHeaderCells rightCornerRounded",
          type: "none",
          width: "5%",
        },
      ],
      cells: [
        {
          2: {
            label: "E",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle3Einhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle3Econtrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle3ERAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle3EBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle3EAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle3EDétails",
          },
        },
        {
          1: {
            label: "Solde",
            labelSource: { fieldId: "solde" },
          },
          2: {
            label: "R",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle3Rinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle3Rcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle3RRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle3RBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle3RAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle3RDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle3label1" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle3solde1" },
          },
          2: {
            label: "C",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle3Cinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle3Ccontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle3CRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle3CBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle3CAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle3CDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle3label2" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle3solde2" },
          },
          2: {
            label: "M",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle3Minhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle3Mcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle3MRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle3MBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle3MAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle3MDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle3label3" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle3solde3" },
          },
          2: {
            label: "D",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle3Dinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle3Dcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle3DRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle3DBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle3DAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle3DDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            //labelSource: {fieldId:'cycle3label4'}
          },
          2: {
            label: "S",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle3Sinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle3Scontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle3SRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle3SBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle3SAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle3SDétails",
          },
        },
      ],
    },

    cycle4: {
      staticTable: true,
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          header: "Cycle Personnel",
          headerClass: "tableHeaderCells leftCornerRounded",
          type: "none",
          width: "30%",
        },
        {
          header: " ",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ass.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "4%",
        },
        {
          header: "Inh.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ctrl.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "R.A.S",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Synthèse Niveau de risque",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "30%",
        },
        {
          header: "Base",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ana.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Détails",
          headerClass: "tableHeaderCells rightCornerRounded",
          type: "none",
          width: "5%",
        },
      ],
      cells: [
        {
          2: {
            label: "E",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle4Einhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle4Econtrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle4ERAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle4EBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle4EAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle4EDétails",
          },
        },
        {
          1: {
            label: "Solde",
            labelSource: { fieldId: "solde" },
          },
          2: {
            label: "R",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle4Rinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle4Rcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle4RRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle4RBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle4RAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle4RDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle4label1" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle4solde1" },
          },
          2: {
            label: "C",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle4Cinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle4Ccontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle4CRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle4CBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle4CAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle4CDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle4label2" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle4solde2" },
          },
          2: {
            label: "M",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle4Minhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle4Mcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle4MRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle4MBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle4MAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle4MDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle4label3" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle4solde3" },
          },
          2: {
            label: "D",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle4Dinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle4Dcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle4DRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle4DBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle4DAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle4DDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
          },
          2: {
            label: "S",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle4Sinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle4Scontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle4SRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle4SBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle4SAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle4SDétails",
          },
        },
      ],
    },
    cycle5: {
      staticTable: true,
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          header: "Cycle Trésorerie Financement",
          headerClass: "tableHeaderCells leftCornerRounded",
          type: "none",
          width: "30%",
        },
        {
          header: " ",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ass.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "4%",
        },
        {
          header: "Inh.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ctrl.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "R.A.S",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Synthèse Niveau de risque",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "30%",
        },
        {
          header: "Base",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ana.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Détails",
          headerClass: "tableHeaderCells rightCornerRounded",
          type: "none",
          width: "5%",
        },
      ],
      cells: [
        {
          2: {
            label: "E",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle5Einhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle5Econtrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle5ERAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle5EBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle5EAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle5EDétails",
          },
        },
        {
          1: {
            label: "Solde",
            labelSource: { fieldId: "solde" },
          },
          2: {
            label: "R",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle5Rinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle5Rcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle5RRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle5RBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle5RAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle5RDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle5label1" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle5solde1" },
          },
          2: {
            label: "C",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle5Cinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle5Ccontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle5CRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle5CBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle5CAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle5CDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle5label2" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle5solde2" },
          },
          2: {
            label: "M",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle5Minhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle5Mcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle5MRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle5MBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle5MAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle5MDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle5label3" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle5solde3" },
          },
          2: {
            label: "D",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle5Dinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle5Dcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle5DRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle5DBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle5DAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle5DDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle5label4" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle5solde4" },
          },
          2: {
            label: "S",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle5Sinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle5Scontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle5SRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle5SBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle5SAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle5SDétails",
          },
        },
      ],
    },
    cycle6: {
      staticTable: true,
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          header: "Cycle Immo Corp. Incorp.",
          headerClass: "tableHeaderCells leftCornerRounded",
          type: "none",
          width: "30%",
        },
        {
          header: " ",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ass.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "4%",
        },
        {
          header: "Inh.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ctrl.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "R.A.S",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Synthèse Niveau de risque",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "30%",
        },
        {
          header: "Base",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ana.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Détails",
          headerClass: "tableHeaderCells rightCornerRounded",
          type: "none",
          width: "5%",
        },
      ],
      cells: [
        {
          2: {
            label: "E",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle6Einhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle6Econtrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle6ERAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle6EBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle6EAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle6EDétails",
          },
        },
        {
          1: {
            label: "Solde",
            labelSource: { fieldId: "solde" },
          },
          2: {
            label: "R",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle6Rinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle6Rcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle6RRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle6RBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle6RAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle6RDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle6label1" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle6solde1" },
          },
          2: {
            label: "C",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle6Cinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle6Ccontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle6CRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle6CBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle6CAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle6CDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle6label2" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle6solde2" },
          },
          2: {
            label: "M",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle6Minhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle6Mcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle6MRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle6MBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle6MAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle6MDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle6label3" },
          },
          2: {
            label: "D",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle6Dinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle6Dcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle6DRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle6DBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle6DAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle6DDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle6label4" },
          },
          2: {
            label: "S",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle6Sinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle6Scontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle6SRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle6SBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle6SAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle6SDétails",
          },
        },
      ],
    },
    cycle7: {
      staticTable: true,
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          header: "Cycle Immo Fin.",
          headerClass: "tableHeaderCells leftCornerRounded",
          type: "none",
          width: "30%",
        },
        {
          header: " ",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ass.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "4%",
        },
        {
          header: "Inh.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ctrl.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "R.A.S",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Synthèse Niveau de risque",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "30%",
        },
        {
          header: "Base",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ana.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Détails",
          headerClass: "tableHeaderCells rightCornerRounded",
          type: "none",
          width: "5%",
        },
      ],
      cells: [
        {
          2: {
            label: "E",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle7Einhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle7Econtrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle7ERAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle7EBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle7EAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle7EDétails",
          },
        },
        {
          1: {
            label: "Solde",
            labelSource: { fieldId: "solde" },
          },
          2: {
            label: "R",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle7Rinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle7Rcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle7RRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle7RBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle7RAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle7RDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle7label1" },
          },
          2: {
            label: "C",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle7Cinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle7Ccontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle7CRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle7CBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle7CAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle7CDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle7label2" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle7solde2" },
          },
          2: {
            label: "M",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle7Minhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle7Mcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle7MRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle7MBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle7MAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle7MDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
          },
          2: {
            label: "D",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle7Dinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle7Dcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle7DRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle7DBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle7DBAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle7DDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
          },
          2: {
            label: "S",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle7Sinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle7Scontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle7SRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle7SBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle7SAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle7SDétails",
          },
        },
      ],
    },
    cycle8: {
      staticTable: true,
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          header: "Cycle Capitaux Propres",
          headerClass: "tableHeaderCells leftCornerRounded",
          type: "none",
          width: "30%",
        },
        {
          header: " ",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ass.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "4%",
        },
        {
          header: "Inh.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ctrl.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "R.A.S",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Synthèse Niveau de risque",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "30%",
        },
        {
          header: "Base",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ana.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Détails",
          headerClass: "tableHeaderCells rightCornerRounded",
          type: "none",
          width: "5%",
        },
      ],
      cells: [
        {
          2: {
            label: "E",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle8Einhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle8Econtrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle8ERAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle8EBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle8EAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle8EDétails",
          },
        },
        {
          1: {
            label: "Solde",
            labelSource: { fieldId: "solde" },
          },
          2: {
            label: "R",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle8Rinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle8Rcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle8RRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle8RBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle8RAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle8RDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            //labelSource: {fieldId:'cycle8label1'}
          },
          2: {
            label: "C",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle8Cinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle8Ccontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle8CRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle8CBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle8CAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle8CDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle8label2" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle8solde2" },
          },
          2: {
            label: "M",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle8Minhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle8Mcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle8MRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle8MBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle8MAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle8MDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
          },
          2: {
            label: "D",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle8Dinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle8Dcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle8DRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle8DBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle8DAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle8DDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            //labelSource: {fieldId:'cycle8label4'}
          },
          2: {
            label: "S",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle8Sinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle8Scontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle8SRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle8SBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle8SAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle8SDétails",
          },
        },
      ],
    },
    cycle9: {
      staticTable: true,
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          header: "Cycle Provisions pour risques et charges",
          headerClass: "tableHeaderCells leftCornerRounded",
          type: "none",
          width: "30%",
        },
        {
          header: " ",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ass.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "4%",
        },
        {
          header: "Inh.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ctrl.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "R.A.S",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Synthèse Niveau de risque",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "30%",
        },
        {
          header: "Base",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ana.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Détails",
          headerClass: "tableHeaderCells rightCornerRounded",
          type: "none",
          width: "5%",
        },
      ],
      cells: [
        {
          2: {
            label: "E",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle9Einhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle9Econtrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle9ERAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle9EBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle9EAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle9EDétails",
          },
        },
        {
          1: {
            label: "Solde",
            labelSource: { fieldId: "solde" },
          },
          2: {
            label: "R",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle9Rinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle9Rcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle9RRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle9RBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle9RAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle9RDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle9label1" },
          },
          2: {
            label: "C",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle9Cinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle9Ccontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle9CRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle9CBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle9CAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle9CDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle9label2" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle9solde2" },
          },
          2: {
            label: "M",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle9Minhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle9Mcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle9MRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle9MBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle9MAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle9MDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle9label3" },
          },
          2: {
            label: "D",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle9Dinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle9Dcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle9DRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle9DBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle9DAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle9DDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle9label4" },
          },
          2: {
            label: "S",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle9Sinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle9Scontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle9SRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle9SBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle9SAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle9SDétails",
          },
        },
      ],
    },
    cycle10: {
      staticTable: true,
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          header: "Cycle Impots et taxes",
          headerClass: "tableHeaderCells leftCornerRounded",
          type: "none",
          width: "30%",
        },
        {
          header: " ",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ass.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "4%",
        },
        {
          header: "Inh.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ctrl.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "R.A.S",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Synthèse Niveau de risque",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "30%",
        },
        {
          header: "Base",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ana.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Détails",
          headerClass: "tableHeaderCells rightCornerRounded",
          type: "none",
          width: "5%",
        },
      ],
      cells: [
        {
          2: {
            label: "E",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle10Einhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle10Econtrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle10ERAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle10EBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle10EAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle10EDétails",
          },
        },
        {
          1: {
            label: "Solde",
            labelSource: { fieldId: "solde" },
          },
          2: {
            label: "R",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle10Rinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle10Rcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle10RRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle10RBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle10RAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle10RDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle10label1" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle10solde1" },
          },
          2: {
            label: "C",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle10Cinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle10Ccontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle10CRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle10CBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle10CAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle10CDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle10label2" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle10solde2" },
          },
          2: {
            label: "M",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle10Minhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle10Mcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle10MRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle10MBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle10MAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle10MDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle10label3" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle10solde3" },
          },
          2: {
            label: "D",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle10Dinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle10Dcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle10DRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle10DBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle10DAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle10DDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            //labelSource: {fieldId:'cycle10label4'}
          },
          2: {
            label: "S",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle10Sinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle10Scontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle10SRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle10SBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle10SAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle10SDétails",
          },
        },
      ],
    },
    cycle11: {
      staticTable: true,
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          header: "Cycle Autres creances et dettes",
          headerClass: "tableHeaderCells leftCornerRounded",
          type: "none",
          width: "30%",
        },
        {
          header: " ",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ass.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "4%",
        },
        {
          header: "Inh.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ctrl.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "R.A.S",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Synthèse Niveau de risque",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "30%",
        },
        {
          header: "Base",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ana.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Détails",
          headerClass: "tableHeaderCells rightCornerRounded",
          type: "none",
          width: "5%",
        },
      ],
      cells: [
        {
          2: {
            label: "E",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle11Einhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle11Econtrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle11ERAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle11EBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle11EAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle11EDétails",
          },
        },
        {
          1: {
            label: "Solde",
            labelSource: { fieldId: "solde" },
          },
          2: {
            label: "R",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle11Rinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle11Rcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle11RRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle11RBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle11RAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle11RDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle11label1" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle11solde1" },
          },
          2: {
            label: "C",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle11Cinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle11Ccontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle11CRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle11CBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle11CAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle11CDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle11label2" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle11solde2" },
          },
          2: {
            label: "M",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle11Minhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle11Mcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle11MRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle11MBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle11MAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle11MDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle11label3" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle11solde3" },
          },
          2: {
            label: "D",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle11Dinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle11Dcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle11DRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle11DBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle11DAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle11DDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle11label4" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle11solde4" },
          },
          2: {
            label: "S",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle11Sinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle11Scontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle11SRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle11SBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle11SAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle11SDétails",
          },
        },
      ],
    },
    cycle12: {
      staticTable: true,
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          header: "Cycle Hors bilan",
          headerClass: "tableHeaderCells leftCornerRounded",
          type: "none",
          width: "30%",
        },
        {
          header: " ",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ass.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "4%",
        },
        {
          header: "Inh.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ctrl.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "R.A.S",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Synthèse Niveau de risque",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "30%",
        },
        {
          header: "Base",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ana.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Détails",
          headerClass: "tableHeaderCells rightCornerRounded",
          type: "none",
          width: "5%",
        },
      ],
      cells: [
        {
          2: {
            label: "E",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle12Einhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle12Econtrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle12ERAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle12EBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle12EAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle12EDétails",
          },
        },
        {
          1: {
            label: "Solde",
            labelSource: { fieldId: "solde" },
          },
          2: {
            label: "R",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle12Rinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle12Rcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle12RRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle12RBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle12RAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle12RDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle12label1" },
          },
          2: {
            label: "C",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle12Cinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle12Ccontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle12CRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle12CBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle12CAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle12CDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle12label2" },
          },
          2: {
            label: "M",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle12Minhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle12Mcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle12MRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle12MBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle12MAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle12MDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle12labe3" },
          },
          2: {
            label: "D",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle12Dinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle12Dcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle12DRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle12DBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle12DAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle12DDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle12label4" },
          },
          2: {
            label: "S",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle12Sinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle12Scontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle12SRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle12SBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle12SAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle12SDétails",
          },
        },
      ],
    },
    cycle13: {
      staticTable: true,
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          header: "Cycle Resultat exceptionnel",
          headerClass: "tableHeaderCells leftCornerRounded",
          type: "none",
          width: "30%",
        },
        {
          header: " ",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ass.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "4%",
        },
        {
          header: "Inh.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ctrl.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "R.A.S",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Synthèse Niveau de risque",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "30%",
        },
        {
          header: "Base",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Ana.",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "5%",
        },
        {
          header: "Détails",
          headerClass: "tableHeaderCells rightCornerRounded",
          type: "none",
          width: "5%",
        },
      ],
      cells: [
        {
          2: {
            label: "E",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle13Einhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle13Econtrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle13ERAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle13EBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle13EAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle13EDétails",
          },
        },
        {
          1: {
            label: "Solde",
            labelSource: { fieldId: "solde" },
          },
          2: {
            label: "R",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle13Rinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle13Rcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle13RRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle13RBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle13RAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle13RDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle13label1" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle13solde1" },
          },
          2: {
            label: "C",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle13Cinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle13Ccontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle13CRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle13CBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle13CAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle13CDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "cycle13label2" },
          },
          1: {
            label: " ",
            cellClass: "labelRight",
            labelSource: { fieldId: "cycle13solde2" },
          },
          2: {
            label: "M",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle13Minhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle13Mcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle13MRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle13MBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle13MAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle13MDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "" },
          },
          2: {
            label: "D",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle13Dinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle13Dcontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle13DRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle13DBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle13DAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle13DDétails",
          },
        },
        {
          0: {
            label: " ",
            labelClass: "labelLeft",
            labelSource: { fieldId: "" },
          },
          2: {
            label: "S",
            cellClass: "alignCenter",
          },
          3: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle13Sinhérent" },
          },
          4: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle13Scontrole" },
          },
          5: {
            label: " ",
            cellClass: "labelCenter",
            labelSource: { fieldId: "cycle13SRAS" },
          },
          6: {
            type: "textArea",
          },
          7: {
            type: "singleCheckbox",
            num: "cycle13SBase",
          },
          8: {
            type: "singleCheckbox",
            num: "cycle13SAna",
          },
          9: {
            type: "singleCheckbox",
            num: "cycle13SDétails",
          },
        },
      ],
    },

    input102: {
      fixedRows: true,
      infoTable: false,
      newTable: true,
      columns: [
        {
          width: "100%",
          headerClass: "borderHide",
        },
      ],
      cells: [
        {
          0: {
            type: "textArea",
            num: "textArea102",
            cellClass: "borderHide",
          },
        },
      ],
    },
    delivrables: {
      fixedRows: true,
      infoTable: false,
      staticTable: true,
      rowDeleteConfirmation: true,
      columns: [
        { type: "none", colClass: "alignCenter" },
        { type: "none", colClass: "alignCenter" },
        { type: "none", colClass: "alignCenter" },
      ],
      cells: [
        {
          0: { label: "Lettre d'acceptation du mandat" },
          1: { label: " " },
          2: { type: "date", num: "delivrablesmandat" },
        },
        {
          0: { label: "Lettre au précédent auditeur" },
          1: { label: " " },
          2: { type: "date", num: "delivrablesprecauditeur" },
        },
        {
          0: { label: "Lettre de début de mission" },
          1: { label: " " },
          2: { type: "date", num: "delivrablesdebutmission" },
        },
        {
          0: { label: "Lettre de demande de dérogation" },
          1: { label: " " },
          2: { type: "date", num: "delivrablesderogation" },
        },
        {
          0: { label: "Projet de lettre d'affirmation" },
          1: { label: " " },
          2: { type: "date", num: "delivrablesaffirmation" },
        },
        {
          0: { label: "Rapports" },
          1: { label: " " },
          2: { type: "date", num: "delivrablesrapports" },
        },
        {
          0: { label: "Facture liquidative" },
          1: { label: " " },
          2: { type: "date", num: "delivrablesfactureliqui" },
        },
        {
          0: { label: "Déclaration d'activité" },
          1: { label: " " },
          2: { type: "date", num: "delivrablesactivite" },
        },
      ],
    },
    controlsPDM: {
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          type: "none",
          colClass: "std-spacing-width",
          header: "Titre",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          colClass: "std-spacing-width",
          header: "Type",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          colClass: "std-spacing-width",
          header: "Cycles/assertions",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          colClass: "std-spacing-width",
          header: "Risques associés",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          colClass: "std-spacing-width",
          header: "Contrôle clé",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          colClass: "std-spacing-width",
          header: "Exécution",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          colClass: "std-spacing-width",
          header: "Classification",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          colClass: "std-spacing-width",
          header: "Fréquence",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [],
    },
  });
})();
