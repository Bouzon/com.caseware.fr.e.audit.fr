(function (wpw) {
  "use strict";

  wpw.tax.create.calcBlocks("PDM", function (calcUtils) {
    let tablesParameters = "";
    function numbersFR(data) {
      let newData = "";
      for (let i = 0; i < data.length; i++) {
        newData = data[i];
        const newNumbers = newData.replace(",", ".").replace(".", "");
        data[i] = newNumbers;
      }
      return data;
    }

    calcUtils.calc(function (calcUtils, field) {
      tablesParameters = field("500").get();
      tablesParameters[1] ? field("NK€").set("K €") : field("NK€").set("€");
      tablesParameters[1] ? field("NM1K€").set("K €") : field("NM1K€").set("€");
    });

    calcUtils.calc(function (calcUtils, field) {
      return wpw.tax
        .evaluate([
          'engprop("relativeperiodend", 0, 0, "longDate")',
          'formatnumeric(year(engprop("yearend", 0)))',
          'formatnumeric(year(engprop("yearend", 0)) - 1)',
        ])
        .then(function (dates) {
          const parsedDate = dates[0].split(" ");
          const yearNM1 = parseFloat(parsedDate[2] - 1);
          field("yearN").set(dates[0]);
          field("yearN-1").set(`${parsedDate[0]} ${parsedDate[1]} ${yearNM1}`);
          field("dates").set(dates[1] + " " + dates[2]);
        });
    });
    calcUtils.calc(function (calcUtils, field) {
      return wpw.tax
        .evaluate([
          'engprop("name")',
          'collaborate("businessNumber")',
          'procedureprop("@HAEH9-f5QlWIPtvM3xt9Jw", "responseAnswer", "@lpElbedORbqz7RkY9dQlFA", "@om0tTY_uTmebbg-hCqEAOg")',
          'collaborate("primaryIndustryCode")',
          'engprop("name") + " " + collaborate("clientAddressId", "address1", 4) + " " + collaborate("clientAddressId", "city", 4) + " " + collaborate("clientAddressId", "province", 4) + " " + collaborate("clientAddressId", "country", 4) + " " + collaborate("clientAddressId", "postalCode", 4)',
          'engprop("name") + " " + collaborate("clientAddressId", "address1", 6) + " " + collaborate("clientAddressId", "city", 6) + " " + collaborate("clientAddressId", "province", 6) + " " + collaborate("clientAddressId", "country", 6) + " " + collaborate("clientAddressId", "postalCode", 6)',
          'procedureprop("@zfcnZU9jSkSVvp-P2trGMw", "responseAnswer", "@twbDEvHlR96_5sb3xZxEaA", "@QFEghk_gSKGeRFN2q1_vrQ")',
        ])
        .then(function (dataENTITE) {
          const numsTabs2 = ["rs2", "rcsN2", "fj2", "ap2", "ss2", "le2", "et2"];
          for (let i = 0; i < numsTabs2.length; i++) {
            field(numsTabs2[i]).set(dataENTITE[i]);
          }
        });
    });

    calcUtils.calc(function (calcUtils, field) {
      wpw.tax
        .evaluate([
          'procedureprop("@9PcqjvGsRG6ZF5HXGXrcCQ", "responseAnswer", "@1ZsTFgrKS2aiaogpaf3h3A", "@ODyfegUSRFO2L4PQ8RCThA")',
        ])
        .then(function (dataCONTACT) {
          const numsTabs2 = ["dirig2"];
          for (let i = 0; i < numsTabs2.length; i++) {
            field(numsTabs2[i]).set(dataCONTACT[i]);
          }
        });
    });

    calcUtils.calc(function (calcUtils, field) {
      return wpw.tax
        .evaluate([
          'tagprop(entityref("BA.1"), "name")',
          'tagprop(entityref("BA.2.01"), "name")',
          'tagprop(entityref("BA.2.02"), "name")',
          'tagprop(entityref("BA.2.03"), "name")',
          'tagprop(entityref("BA.2"), "name")',
          'tagprop(entityref("BA.3.01"), "name")',
          'tagprop(entityref("BA.3.03"), "name")',
          'tagprop(entityref("BA.3.05"), "name")',
          'tagprop(entityref("BA.3.06"), "name")',
          'tagprop(entityref("BA.3.07"), "name")',
          'tagprop(entityref("BA.3.08"), "name")',
          'tagprop(entityref("BA.3.02"), "name")',
          'tagprop(entityref("BA.3"), "name")',
          'tagprop(entityref("BA.3.09"), "name")',
          'tagprop(entityref("BA.3.10"), "name")',
          'tagprop(entityref("BA.3.11"), "name")',
          'tagprop(entityref("BA"), "name")',
          'tagprop(entityref("BP.1.01"), "name")',
          'tagprop(entityref("BP.1.02"), "name")',
          'tagprop(entityref("BP.1.03"), "name")',
          'tagprop(entityref("BP.1.04"), "name")',
          'tagprop(entityref("BP.1.05"), "name")',
          'tagprop(entityref("BP.1.06"), "name")',
          'tagprop(entityref("BP.1.07"), "name")',
          'tagprop(entityref("BP.1.08"), "name")',
          'tagprop(entityref("BP.1.09"), "name")',
          'tagprop(entityref("BP.1"), "name")',
          'tagprop(entityref("BP.2"), "name")',
          'tagprop(entityref("BP.3"), "name")',
          'tagprop(entityref("BP.4.01"), "name")',
          'tagprop(entityref("BP.4.02"), "name")',
          'tagprop(entityref("BP.4.03"), "name")',
          'tagprop(entityref("BP.4.04"), "name")',
          'tagprop(entityref("BP.4.05"), "name")',
          'tagprop(entityref("BP.4.06"), "name")',
          'tagprop(entityref("BP.4.07"), "name")',
          'tagprop(entityref("BP.4.08"), "name")',
          'tagprop(entityref("BP.4.09"), "name")',
          'tagprop(entityref("BP.4.10"), "name")',
          'tagprop(entityref("BP.4.11"), "name")',
          'tagprop(entityref("BP.4"), "name")',
          'tagprop(entityref("BP"), "name")',
          'tagprop(entityref("RP.1"), "name")',
          'tagprop(entityref("RC.1"), "name")',
          'tagprop(entityref("RP.2.01"), "name")',
          'tagprop(entityref("RC.2.01"), "name")',
          'tagprop(entityref("RP.3"), "name")',
          'tagprop(entityref("RC.3"), "name")',
          'tagprop(entityref("RP.4"), "name")',
          'tagprop(entityref("RC.5"), "name")',
          'tagprop(entityref("RC.6"), "name")',
        ])
        .then(function (labels) {
          const arrayLabels = [
            "label1",
            "label2",
            "label3",
            "label4",
            "label5",
            "label6",
            "label7",
            "label8",
            "label9",
            "label10",
            "label11",
            "label12",
            "label13",
            "label14",
            "label15",
            "label16",
            "label17",
            "labelPassif1",
            "labelPassif2",
            "labelPassif3",
            "labelPassif4",
            "labelPassif5",
            "labelPassif6",
            "labelPassif7",
            "labelPassif8",
            "labelPassif9",
            "labelPassif10",
            "labelPassif11",
            "labelPassif12",
            "labelPassif13",
            "labelPassif14",
            "labelPassif15",
            "labelPassif16",
            "labelPassif17",
            "labelPassif18",
            "labelPassif19",
            "labelPassif20",
            "labelPassif21",
            "labelPassif22",
            "labelPassif23",
            "labelPassif24",
            "labelPassif25",
            "labelcompteresultat1",
            "labelcompteresultat2",
            "labelcompteresultat4",
            "labelcompteresultat5",
            "labelcompteresultat6",
            "labelcompteresultat7",
            "labelcompteresultat10",
            "labelcompteresultat11",
            "labelcompteresultat12",
          ];
          for (let i = 0; i < arrayLabels.length; i++) {
            field(arrayLabels[i]).set(labels[i]);
          }
        });
    });

    calcUtils.calc(function (calcUtils, field) {
      return wpw.tax
        .evaluate([
          'tagbal(entityref("RP.1"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RP.1"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RC.1"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RC.1"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RP.2.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RP.2.01"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RC.2.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RC.2.01"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RP.3"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RP.3"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RC.3"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RC.3"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RP.4"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RP.4"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RC.5"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RC.5"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RC.6"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RC.6"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        ])
        .then(function (dataCR) {
          dataCR = numbersFR(dataCR);

          const numsCRN = [
            "peNK",
            "ceNK",
            "bptNK",
            "pbtNK",
            "pfNK",
            "cfNK",
            "pexcepNK",
            "psrNK",
            "ibNK",
          ];

          const numsCRNPourcent = [
            "peNK%",
            "ceNK%",
            "bptN%",
            "pbtN%",
            "pfN%",
            "cfN%",
            "pexcepN%",
            "psrN%",
            "ibN%",
          ];

          const numsCRNM1 = [
            "peNM1K",
            "ceNM1K",
            "bptNM1K",
            "pbtNM1K",
            "pfNM1K",
            "cfNM1K",
            "pexcepNM1K",
            "psrNM1K",
            "ibNM1K",
          ];

          const numsCRNM1Pourcent = [
            "peNM1%",
            "ceNM1%",
            "bptNM1%",
            "pbtNM1%",
            "pfNM1%",
            "cfNM1%",
            "pexcepNM1%",
            "psrNM1%",
            "ibNM1%",
          ];

          const numsCRVAR = [
            "peVAR",
            "ceVAR",
            "bptVAR",
            "pbtVAR",
            "pfVAR",
            "cfVAR",
            "pexcepVAR",
            "psrVAR",
            "ibVAR",
          ];

          const numsCRVARPourcent = [
            "peVAR%",
            "ceVAR%",
            "bptVAR%",
            "pbtVAR%",
            "pfVAR%",
            "cfVAR%",
            "pexcepVAR%",
            "psrVAR%",
            "ibVAR%",
          ];

          const dataResults = {
            reNK: parseFloat(dataCR[0]) + parseFloat(dataCR[2]),
            reNM1K: parseFloat(dataCR[1]) + parseFloat(dataCR[3]),
            rfNK: parseFloat(dataCR[8]) + parseFloat(dataCR[10]),
            rfNM1K: parseFloat(dataCR[9]) + parseFloat(dataCR[11]),
            rcaiNK:
              parseFloat(dataCR[0]) +
              parseFloat(dataCR[2]) +
              parseFloat(dataCR[4]) +
              parseFloat(dataCR[6]) +
              parseFloat(dataCR[8]) +
              parseFloat(dataCR[10]) +
              parseFloat(dataCR[12]) +
              parseFloat(dataCR[14]) +
              parseFloat(dataCR[16]),
            rcaiNM1K:
              parseFloat(dataCR[1]) +
              parseFloat(dataCR[3]) +
              parseFloat(dataCR[5]) +
              parseFloat(dataCR[7]) +
              parseFloat(dataCR[9]) +
              parseFloat(dataCR[11]) +
              parseFloat(dataCR[13]) +
              parseFloat(dataCR[15]) +
              parseFloat(dataCR[17]),
            rnNK:
              parseFloat(dataCR[0]) +
              parseFloat(dataCR[2]) +
              parseFloat(dataCR[4]) +
              parseFloat(dataCR[6]) +
              parseFloat(dataCR[8]) +
              parseFloat(dataCR[10]),
            rnNM1K:
              parseFloat(dataCR[1]) +
              parseFloat(dataCR[3]) +
              parseFloat(dataCR[5]) +
              parseFloat(dataCR[7]) +
              parseFloat(dataCR[9]) +
              parseFloat(dataCR[11]),
            reVAR:
              parseFloat(dataCR[0]) +
              parseFloat(dataCR[2]) -
              (parseFloat(dataCR[1]) + parseFloat(dataCR[3])),
            rfVAR:
              parseFloat(dataCR[8]) +
              parseFloat(dataCR[10]) -
              (parseFloat(dataCR[9]) + parseFloat(dataCR[11])),
            rcaiVAR:
              parseFloat(dataCR[0]) +
              parseFloat(dataCR[2]) +
              parseFloat(dataCR[4]) +
              parseFloat(dataCR[6]) +
              parseFloat(dataCR[8]) +
              parseFloat(dataCR[10]) +
              parseFloat(dataCR[12]) +
              parseFloat(dataCR[14]) +
              parseFloat(dataCR[16]) -
              (parseFloat(dataCR[1]) +
                parseFloat(dataCR[3]) +
                parseFloat(dataCR[5]) +
                parseFloat(dataCR[7]) +
                parseFloat(dataCR[9]) +
                parseFloat(dataCR[11]) +
                parseFloat(dataCR[13]) +
                parseFloat(dataCR[15]) +
                parseFloat(dataCR[17])),
            rnVAR:
              parseFloat(dataCR[0]) +
              parseFloat(dataCR[2]) +
              parseFloat(dataCR[4]) +
              parseFloat(dataCR[6]) +
              parseFloat(dataCR[8]) +
              parseFloat(dataCR[10]) -
              (parseFloat(dataCR[1]) +
                parseFloat(dataCR[3]) +
                parseFloat(dataCR[5]) +
                parseFloat(dataCR[7]) +
                parseFloat(dataCR[9]) +
                parseFloat(dataCR[11])),
          };
          const numsResults = [
            "reNK",
            "reNM1K",
            "rfNK",
            "rfNM1K",
            "rcaiNK",
            "rcaiNM1K",
            "rnNK",
            "rnNM1K",
            "reVAR",
            "rfVAR",
            "rcaiVAR",
            "rnVAR",
          ];
          const numsResultsNPourcent = ["reN%", "rfN%", "rcaiN%", "rnN%"];
          const numsResultsNM1Pourcent = [
            "reNM1%",
            "rfNM1%",
            "rcaiNM1%",
            "rnNM1%",
          ];
          const numsResultsVARPourcent = [
            "reVAR%",
            "rfVAR%",
            "rcaiVAR%",
            "rnVAR%",
          ];

          let countN = 0;
          for (let i = 0; i < numsCRN.length; i++) {
            if (dataCR[countN] === undefined) {
              field(numsCRN[i]).set("N/A");
            } else if (
              tablesParameters["2"] === true &&
              parseFloat(dataCR[countN]) == 0
            ) {
              field(numsCRN[i]).set("hide");
            } else if (tablesParameters["1"] === false || undefined) {
              field(numsCRN[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataCR[countN])
              );
            } else {
              field(numsCRN[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataCR[countN] / 1000)
              );
            }
            countN += 2;
          }

          let countM1 = 1;
          for (let i = 0; i < numsCRNM1.length; i++) {
            if (dataCR[countM1] === undefined) {
              field(numsCRNM1[i]).set("N/A");
            } else if (
              tablesParameters["2"] === true &&
              parseFloat(dataCR[countM1]) == 0
            ) {
              field(numsCRNM1[i]).set("hide");
            } else if (tablesParameters["1"] === false || undefined) {
              field(numsCRNM1[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataCR[countM1])
              );
            } else {
              field(numsCRNM1[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataCR[countM1] / 1000)
              );
            }
            countM1 += 2;
          }

          for (let i = 0; i < numsResults.length; i++) {
            if (dataResults[numsResults[i]] === undefined) {
              field(numsResults[i]).set("N/A");
            } else if (
              tablesParameters["2"] === true &&
              parseFloat(dataResults[numsResults[i]]) == 0
            ) {
              field(numsResults[i]).set("hide");
            } else if (tablesParameters["1"] === false || undefined) {
              field(numsResults[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataResults[numsResults[i]])
              );
            } else {
              field(numsResults[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataResults[numsResults[i]] / 1000)
              );
            }
          }

          let countVAR = 0;
          for (let i = 0; i < numsCRVAR.length; i++) {
            field(numsCRVAR[i]).set(
              new Intl.NumberFormat("fr-FR", {
                maximumFractionDigits: 0,
              }).format(dataCR[countVAR] - dataCR[countVAR + 1])
            );
            countVAR += 2;
          }

          let countNPourcent = 0;
          for (let i = 0; i < numsCRNPourcent.length; i++) {
            const nPourcent =
              (parseFloat(dataCR[countNPourcent]) * 100) / dataResults.rnNK;
            Number.isNaN(nPourcent)
              ? field(numsCRNPourcent[i]).set("0")
              : field(numsCRNPourcent[i]).set(nPourcent.toFixed(0));
            countNPourcent += 2;
          }

          let countNM1Pourcent = 1;
          for (let i = 0; i < numsCRNM1Pourcent.length; i++) {
            const nm1Pourcent =
              (parseFloat(dataCR[countNM1Pourcent]) * 100) / dataResults.rnNM1K;
            Number.isNaN(nm1Pourcent)
              ? field(numsCRNM1Pourcent[i]).set("0")
              : field(numsCRNM1Pourcent[i]).set(nm1Pourcent.toFixed(0));
            countNM1Pourcent += 2;
          }

          let N = 0;
          for (let i = 0; i < numsResultsNPourcent.length; i++) {
            const nResultsPourcent =
              (dataResults[numsResults[N]] * 100) / dataResults.rnNK;
            Number.isNaN(nResultsPourcent)
              ? field(numsResultsNPourcent[i]).set("0")
              : field(numsResultsNPourcent[i]).set(nResultsPourcent.toFixed(0));
            N += 2;
          }

          let NM = 1;
          for (let i = 0; i < numsResultsNM1Pourcent.length; i++) {
            const nm1ResultsPourcent =
              (dataResults[numsResults[NM]] * 100) / dataResults.rnNM1K;
            Number.isNaN(nm1ResultsPourcent)
              ? field(numsResultsNM1Pourcent[i]).set("0")
              : field(numsResultsNM1Pourcent[i]).set(
                  nm1ResultsPourcent.toFixed(0)
                );
            NM += 2;
          }

          let VARResults = 0;
          for (let i = 0; i < numsResultsVARPourcent.length; i++) {
            const varResultsPourcent =
              ((dataResults[numsResults[VARResults]] -
                dataResults[numsResults[VARResults + 1]]) /
                dataResults[numsResults[VARResults + 1]]) *
              100;
            Number.isNaN(varResultsPourcent)
              ? field(numsResultsVARPourcent[i]).set("0")
              : field(numsResultsVARPourcent[i]).set(
                  varResultsPourcent.toFixed(0)
                );
            VARResults += 2;
          }

          let VAR = 0;
          for (let i = 0; i < numsCRVARPourcent.length; i++) {
            const varResultsPourcent =
              ((dataCR[VAR] - dataCR[VAR + 1]) / dataCR[VAR + 1]) * 100;
            Number.isNaN(varResultsPourcent)
              ? field(numsCRVARPourcent[i]).set("0")
              : field(numsCRVARPourcent[i]).set(varResultsPourcent.toFixed(0));
            VAR += 2;
          }
        });
    });

    calcUtils.calc(function (calcUtils, field) {
      return wpw.tax
        .evaluate([
          'tagbal(entityref("BA.1"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.1"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.2.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.2.01"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.2.02"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.2.02"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.2.03"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.2.03"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.2"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.2"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.01"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.03"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.03"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.05"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.05"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.06"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.06"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.07"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.07"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.08"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.08"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.02"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.02"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.09"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.09"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.10"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.10"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.11"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.11"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        ])
        .then(function (dataBA) {
          dataBA = numbersFR(dataBA);

          const numsBAN = [
            "csnaNK",
            "iiNK",
            "icNK",
            "ifNK",
            "taiNK",
            "seecNK",
            "cNK",
            "vmdpNK",
            "instruNK",
            "dNK",
            "ccaNK",
            "aeavscNK",
            "tacecNK",
            "crspeNK",
            "preNK",
            "ecaNK",
            "tbaNK",
          ];

          const numsBANM1 = [
            "csnaNM1K",
            "iiNM1K",
            "icNM1K",
            "ifNM1K",
            "taiNM1K",
            "seecNM1K",
            "cNM1K",
            "vmdpNM1K",
            "instruNM1K",
            "dNM1K",
            "ccaNM1K",
            "aeavscNM1K",
            "tacecNM1K",
            "crspeNM1K",
            "preNM1K",
            "ecaNM1K",
            "tbaNM1K",
          ];

          const numsNPourcent = [
            "csnaN%",
            "iiN%",
            "icN%",
            "ifN%",
            "taiN%",
            "seecN%",
            "cN%",
            "vmdpN%",
            "instruN%",
            "dN%",
            "ccaN%",
            "aeavscN%",
            "tacecN%",
            "crspeN%",
            "preN%",
            "ecaN%",
            "tbaN%",
          ];

          const numsNM1Pourcent = [
            "csnaNM1%",
            "iiNM1%",
            "icNM1%",
            "ifNM1%",
            "taiNM1%",
            "seecNM1%",
            "cNM1%",
            "vmdpNM1%",
            "instruNM1%",
            "dNM1%",
            "ccaNM1%",
            "aeavscNM1%",
            "tacecNM1%",
            "crspeNM1%",
            "preNM1%",
            "ecaNM1%",
            "tbaNM1%",
          ];

          const numsVarPourCent = [
            "csnaVAR%",
            "iiVAR%",
            "icVAR%",
            "ifVAR%",
            "taiVAR%",
            "seecVAR%",
            "cVAR%",
            "vmdpVAR%",
            "instruVAR%",
            "dVAR%",
            "ccaVAR%",
            "aeavscVAR%",
            "tacecVAR%",
            "crspeVAR%",
            "preVAR%",
            "ecaVAR%",
            "tbaVAR%",
          ];

          const numsVar = [
            "csnaVAR",
            "iiVAR",
            "icVAR",
            "ifVAR",
            "taiVAR",
            "seecVAR",
            "cVAR",
            "vmdpVAR",
            "instruVAR",
            "dVAR",
            "ccaVAR",
            "aeavscVAR",
            "tacecVAR",
            "crspeVAR",
            "preVAR",
            "ecaVAR",
            "tbaVAR",
          ];

          let countN = 0;
          for (let i = 0; i < numsBAN.length; i++) {
            if (dataBA[countN] === undefined) {
              field(numsBAN[i]).set("N/A");
            } else if (
              tablesParameters["2"] === true &&
              parseFloat(dataBA[countN]) == 0
            ) {
              field(numsBAN[i]).set("hide");
            } else if (tablesParameters["1"] === false || undefined) {
              field(numsBAN[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataBA[countN])
              );
            } else {
              field(numsBAN[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataBA[countN] / 1000)
              );
            }
            countN += 2;
          }

          let countM1 = 1;
          for (let i = 0; i < numsBANM1.length; i++) {
            if (dataBA[countM1] === undefined) {
              field(numsBANM1[i]).set("N/A");
            } else if (
              tablesParameters["2"] === true &&
              parseFloat(dataBA[countM1]) == 0
            ) {
              field(numsBANM1[i]).set("hide");
            } else if (tablesParameters["1"] === false || undefined) {
              field(numsBANM1[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataBA[countM1])
              );
            } else {
              field(numsBANM1[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataBA[countM1] / 1000)
              );
            }
            countM1 += 2;
          }

          let countNPourcent = 0;
          for (let i = 0; i < numsNPourcent.length; i++) {
            const nPourcent =
              (parseFloat(dataBA[countNPourcent]) * 100) /
              parseFloat(dataBA[32]);
            Number.isNaN(nPourcent)
              ? field(numsNPourcent[i]).set("0")
              : field(numsNPourcent[i]).set(nPourcent.toFixed(0));
            countNPourcent += 2;
          }

          let countNM1Pourcent = 1;
          for (let i = 0; i < numsNM1Pourcent.length; i++) {
            const nm1Pourcent =
              (parseFloat(dataBA[countNM1Pourcent]) * 100) /
              parseFloat(dataBA[33]);
            Number.isNaN(nm1Pourcent)
              ? field(numsNM1Pourcent[i]).set("0")
              : field(numsNM1Pourcent[i]).set(nm1Pourcent.toFixed(0));
            countNM1Pourcent += 2;
          }

          let countVAR = 0;
          for (let i = 0; i < numsVar.length; i++) {
            field(numsVar[i]).set(
              new Intl.NumberFormat("fr-FR", {
                maximumFractionDigits: 0,
              }).format(dataBA[countVAR] - dataBA[countVAR + 1])
            );
            countVAR += 2;
          }

          let countVARPourcent = 0;
          for (let i = 0; i < numsVarPourCent.length; i++) {
            const variationPourcent =
              ((parseFloat(dataBA[countVARPourcent]) -
                parseFloat(dataBA[countVARPourcent + 1])) /
                parseFloat(dataBA[countVARPourcent + 1])) *
              100;

            Number.isNaN(variationPourcent)
              ? field(numsVarPourCent[i]).set("0")
              : field(numsVarPourCent[i]).set(variationPourcent.toFixed(0));
            countVARPourcent += 2;
          }
        });
    });

    calcUtils.calc(function (calcUtils, field) {
      return wpw.tax
        .evaluate([
          'tagbal(entityref("BP.1.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.01"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.02"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.02"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.03"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.03"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.04"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.04"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.05"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.05"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.06"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.06"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.07"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.07"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.08"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.08"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.09"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.09"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.2"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.2"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.3"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.3"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.01"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.02"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.02"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.03"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.03"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.04"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.04"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.05"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.05"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.06"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.06"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.07"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.07"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.08"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.08"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.09"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.09"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.10"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.10"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.11"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.11"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        ])
        .then(function (dataBP) {
          dataBP = numbersFR(dataBP);

          const numsBPN = [
            "capiPNK",
            "primesPNK",
            "ecartsreevalPNK",
            "ecartequivalencePNK",
            "reservesPNK",
            "reportnouveauPNK",
            "resultatexercicePNK",
            "subventionPNK",
            "provisionPNK",
            "capitauxPNK",
            "autresfondsPNK",
            "provisionsPNK",
            "eocPNK",
            "aeoPNK",
            "edecPNK",
            "edfdPNK",
            "dfcrPNK",
            "dfsPNK",
            "dicrPNK",
            "autresdettesPNK",
            "itPNK",
            "pcaPNK",
            "ecpPNK",
            "dettesPNK",
            "passifPNK",
          ];

          const numsBPNPourcent = [
            "capiPN%",
            "primesPN%",
            "ecartsreevalPN%",
            "ecartequivalencePN%",
            "reservesPN%",
            "reportnouveauPN%",
            "resultatexercicePN%",
            "subventionPN%",
            "provisionPN%",
            "capitauxPN%",
            "autresfondsPN%",
            "provisionsPN%",
            "eocPN%",
            "aeoPN%",
            "edecPN%",
            "edfdPN%",
            "dfcrPN%",
            "dfsPN%",
            "dicrPN%",
            "autresdettesPN%",
            "itPN%",
            "pcaPN%",
            "ecpPN%",
            "dettesPN%",
            "passifPN%",
          ];

          const numsBPNM1K = [
            "capiPNM1K",
            "primesPNM1K",
            "ecartsreevalPNM1K",
            "ecartequivalencePNM1K",
            "reservesPNM1K",
            "reportnouveauPNM1K",
            "resultatexercicePNM1K",
            "subventionPNM1K",
            "provisionPNM1K",
            "capitauxPNM1K",
            "autresfondsPNM1K",
            "provisionsPNM1K",
            "eocPNM1K",
            "aeoPNM1K",
            "edecPNM1K",
            "edfdPNM1K",
            "dfcrPNM1K",
            "dfsPNM1K",
            "dicrPNM1K",
            "autresdettesPNM1K",
            "itPNM1K",
            "pcaPNM1K",
            "ecpPNM1K",
            "dettesPNM1K",
            "passifPNM1K",
          ];

          const numsBPNM1Pourcent = [
            "capiPNM1%",
            "primesPNM1%",
            "ecartsreevalPNM1%",
            "ecartequivalencePNM1%",
            "reservesPNM1%",
            "reportnouveauPNM1%",
            "resultatexercicePNM1%",
            "subventionPNM1%",
            "provisionPNM1%",
            "capitauxPNM1%",
            "autresfondsPNM1%",
            "provisionsPNM1%",
            "eocPNM1%",
            "aeoPNM1%",
            "edecPNM1%",
            "edfdPNM1%",
            "dfcrPNM1%",
            "dfsPNM1%",
            "dicrPNM1%",
            "autresdettesPNM1%",
            "itPNM1%",
            "pcaPNM1%",
            "ecpPNM1%",
            "dettesPNM1%",
            "passifPNM1%",
          ];

          const numsBPVAR = [
            "capiVAR",
            "primesVAR",
            "ecartsreevalVAR",
            "ecartequivalenceVAR",
            "reservesVAR",
            "reportnouveauVAR",
            "resultatexerciceVAR",
            "subventionVAR",
            "provisionVAR",
            "capitauxVAR",
            "autresfondsVAR",
            "provisionsVAR",
            "eocVAR",
            "aeoVAR",
            "edecVAR",
            "edfdVAR",
            "dfcrVAR",
            "dfsVAR",
            "dicrVAR",
            "autresdettesVAR",
            "itVAR",
            "pcaVAR",
            "ecpVAR",
            "dettesVAR",
            "passifVAR",
          ];

          const numsBPVARPourcent = [
            "capiVAR%",
            "primesVAR%",
            "ecartsreevalVAR%",
            "ecartequivalenceVAR%",
            "reservesVAR%",
            "reportnouveauVAR%",
            "resultatexerciceVAR%",
            "subventionVAR%",
            "provisionVAR%",
            "capitauxVAR%",
            "autresfondsVAR%",
            "provisionsVAR%",
            "eocVAR%",
            "aeoVAR%",
            "edecVAR%",
            "edfdVAR%",
            "dfcrVAR%",
            "dfsVAR%",
            "dicrVAR%",
            "autresdettesVAR%",
            "itVAR%",
            "pcaVAR%",
            "ecpVAR%",
            "dettesVAR%",
            "passifVAR%",
          ];

          let countN = 0;
          for (let i = 0; i < numsBPN.length; i++) {
            if (dataBP[countN] === undefined) {
              field(numsBPN[i]).set("N/A");
            } else if (
              tablesParameters["2"] === true &&
              parseFloat(dataBP[countN]) == 0
            ) {
              field(numsBPN[i]).set("hide");
            } else if (tablesParameters["1"] === false || undefined) {
              field(numsBPN[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataBP[countN])
              );
            } else {
              field(numsBPN[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataBP[countN] / 1000)
              );
            }
            countN += 2;
          }

          let countM1 = 1;
          for (let i = 0; i < numsBPNM1K.length; i++) {
            if (dataBP[countM1] === undefined) {
              field(numsBPNM1K[i]).set("N/A");
            } else if (
              tablesParameters["2"] === true &&
              parseFloat(dataBP[countM1]) == 0
            ) {
              field(numsBPNM1K[i]).set("hide");
            } else if (tablesParameters["1"] === false || undefined) {
              field(numsBPNM1K[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataBP[countM1])
              );
            } else {
              field(numsBPNM1K[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataBP[countM1] / 1000)
              );
            }
            countM1 += 2;
          }

          let countNPourcent = 0;
          for (let i = 0; i < numsBPNPourcent.length; i++) {
            const nPourcent =
              (parseFloat(dataBP[countNPourcent]) * 100) /
              parseFloat(dataBP[48]);
            Number.isNaN(nPourcent)
              ? field(numsBPNPourcent[i]).set("0")
              : field(numsBPNPourcent[i]).set(nPourcent.toFixed(0));
            countNPourcent += 2;
          }

          let countNM1Pourcent = 1;
          for (let i = 0; i < numsBPNM1Pourcent.length; i++) {
            const nm1Pourcent =
              (parseFloat(dataBP[countNM1Pourcent]) * 100) /
              parseFloat(dataBP[49]);
            Number.isNaN(nm1Pourcent)
              ? field(numsBPNM1Pourcent[i]).set("0")
              : field(numsBPNM1Pourcent[i]).set(nm1Pourcent.toFixed(0));
            countNM1Pourcent += 2;
          }

          let countVAR = 0;
          for (let i = 0; i < numsBPVAR.length; i++) {
            field(numsBPVAR[i]).set(
              new Intl.NumberFormat("fr-FR", {
                maximumFractionDigits: 0,
              }).format(dataBP[countVAR] - dataBP[countVAR + 1])
            );
            countVAR += 2;
          }

          let countVARPourcent = 0;
          for (let i = 0; i < numsBPVARPourcent.length; i++) {
            const variationPourcent =
              ((parseFloat(dataBP[countVARPourcent]) -
                parseFloat(dataBP[countVARPourcent + 1])) /
                parseFloat(dataBP[countVARPourcent + 1])) *
              100;

            Number.isNaN(variationPourcent)
              ? field(numsBPVARPourcent[i]).set("0")
              : field(numsBPVARPourcent[i]).set(variationPourcent.toFixed(0));
            countVARPourcent += 2;
          }
        });
    });

    calcUtils.onFormLoad("PDM", function (calcUtils, field) {
      wpw.tax.getIssues().then(function (issueProperties) {
        let myTable = field("taches34");
        let datetext = "";
        const statut = {
          closed: "Fermé",
          open: "Ouvert",
          resolved: "Résolu",
        };

        let tableLength = myTable.getRows().length;
        for (let i = 0; i < tableLength; i++) {
          calcUtils.removeTableRow("PDM", "taches34", 0);
        }

        for (let i = 0; i < issueProperties.length; i++) {
          if (issueProperties[i].type !== "todo") {
            issueProperties.splice(i, 1);
            i--;
          }
        }

        for (let i = 0; i < issueProperties.length; i++) {
          const arrayResponses = Object.values(issueProperties[i].responses);
          const description = arrayResponses[0].text
            .replace(RegExp(/<\/p>/gi), "\n")
            .replaceAll("&#160;", "\n")
            .replaceAll("&#38;", "&")
            .replace(RegExp(/<[^>]+>/gi), "");

          for (let j = 0; j < arrayResponses.length; j++) {
            const dateEU = arrayResponses[j].date.slice(0, 10).split("-");
            arrayResponses[j].status !== "open" &&
            issueProperties[i].status !== "open"
              ? (datetext = `${dateEU[2]}-${dateEU[1]}-${dateEU[0]}`)
              : (datetext = "En cours");
          }

          calcUtils.addTableRow("PDM", "taches34", i);
          myTable.cell(i, 0).setCellLabel("À faire");
          myTable.cell(i, 2).setCellLabel(description);
          myTable.cell(i, 4).setCellLabel(statut[issueProperties[i].status]);
          myTable.cell(i, 6).setCellLabel(datetext);
        }
      });
    });

    calcUtils.calc(function (calcUtils, field) {
      return wpw.tax.getMateriality().then(function (materialityProperties) {
        const parameterseuil = [
          "com.caseware.materiality;preliminary;-1",
          "com.caseware.materiality;preliminary;0",
          "com.caseware.materiality;report;0",
        ];
        const row1 = [
          "periodePrecedenteSI",
          "periodePrecedentePlan",
          "periodePrecedenteInsi",
        ];
        const row2 = ["preliSI", "preliPlan", "preliInsi"];
        const row3 = ["FinalSI", "FinalPlan", "FinalInsi"];

        for (let i = 0; i < row1.length; i++) {
          materialityProperties[i].balances[parameterseuil[0]] === undefined
            ? field(row1[i]).set("-")
            : field(row1[i]).set(
                materialityProperties[i].balances[parameterseuil[0]].amount /
                  1000
              );
        }

        for (let i = 0; i < row2.length; i++) {
          materialityProperties[i].balances[parameterseuil[1]] === undefined
            ? field(row2[i]).set("-")
            : field(row2[i]).set(
                materialityProperties[i].balances[parameterseuil[1]].amount /
                  1000
              );
        }

        for (let i = 0; i < row3.length; i++) {
          materialityProperties[i].balances[parameterseuil[2]] === undefined
            ? field(row3[i]).set("-")
            : field(row3[i]).set(
                materialityProperties[i].balances[parameterseuil[2]].amount /
                  1000
              );
        }
      });
    }),
      calcUtils.calc(function (calcUtils, field) {
        return wpw.tax
          .evaluate([
            'procedureprop("@4JKuNWdOTA-5EesbNkbbAA", "responseAnswer", "@I_YSTVHyRiaAAj-55WWbwQ", "@rBYE74LZSnSkhWWcXgfDDw")',
            'procedureprop("@VMEhEs3jTOWC0BY-WVdHGw", "responseAnswer", "@6Sz2rTuFQ06Ec5Az3vtMLQ", "@KnSyKSwfRKKMpSlf6vgKYg")',
            'procedureprop("@a-OFtcgWSSScCjeVxsPpog", "responseAnswer", "@yzqVrSweTYu1zDYCZeQe0w", "@YhWUjEl1T3ak7_2a6OmUAw")',
            'procedureprop("@unLQHnVsRVai2UmW4ttzBQ", "responseAnswer", "@02tuY5qXTb-s1nDcQKrHtA", "@jRqdpwN_TPWfMPuOMgMxtw")',
            'procedureprop("@rhN76tASQAyO1Ytppbna3Q", "responseAnswer", "@0dzgFS9AR-CGV6SKor1VMw", "@buebF31fRX2vby2gRgsm6w")',
            'procedureprop("@UTMp350VTF-4cwyqaq-Gog", "responseAnswer", "@TwufDB1uQ8aoeTuxFZlbXw", "@RYPEh1qPQKyLdfWuSUAnew")',
            'procedureprop("@n-wVmNzMSZajT4OcAtmXNw", "responseAnswer", "@ZXaMhqkmRyOgwWFcTdx2Hw", "@9qebBL6JQIe9sBzDZDsvwg")',
            'procedureprop("@V65BCQ_7QfmUKPYWxceSrQ", "responseAnswer", "@GEmpGReOTAewu6I3K1EfnQ", "@8thCaesLSSqU3HUmXkskHg")',
          ])
          .then(function (allConclusions) {
            const numsConclusions = [
              "syntfmi2",
              "conclusion42",
              "syntadci5",
              "conclusion63",
              "conclusion62",
              "titulaireinfosGTable1",
              "signataireinfosGTable",
              "datenominationinfosGTable",
            ];
            for (let i = 0; i < numsConclusions.length; i++) {
              allConclusions[i] === undefined
                ? field(numsConclusions[i]).set(
                    "Conclusion non disponible pour le moment"
                  )
                : field(numsConclusions[i]).set(
                    allConclusions[i].replace(RegExp(/<[^>]+>/gi), "")
                  );
            }
          });
      }),
      calcUtils.calc(function (calcUtils, field) {
        return wpw.tax
          .evaluate([
            'tagprop(entityref("BA.3.03.02.01"), "name")',
            'tagprop(entityref("BP.4.05"), "name")',
            'tagprop(entityref("RC.1.05"), "name")',
            'tagprop(entityref("RP.3.03"), "name")',
            'tagprop(entityref("BA.3.03.01"), "name")',
            'tagprop(entityref("BP.4.10"), "name")',
            'tagprop(entityref("RC.1.12.03"), "name")',
            'tagprop(entityref("RP.1"), "name")',
            'tagprop(entityref("BA.3.01"), "name")',
            'tagprop(entityref("RC.1.04.01"), "name")',
            'tagprop(entityref("RP.1.03"), "name")',
            'tagprop(entityref("BA.3.03.02.10"), "name")',
            'tagprop(entityref("BP.4.06.14"), "name")',
            'tagprop(entityref("RC.1.07"), "name")',
            'tagprop(entityref("BA.3.07"), "name")',
            'tagprop(entityref("BP.4.03"), "name")',
            'tagprop(entityref("RC.3"), "name")',
            'tagprop(entityref("RP.3"), "name")',
            'tagprop(entityref("BA.2"), "name")',
            'tagprop(entityref("RC.1.09"), "name")',
            'tagprop(entityref("BA.2.03"), "name")',
            'tagprop(entityref("BP.1"), "name")',
            'tagprop(entityref("BP.3"), "name")',
            'tagprop(entityref("BA.3.03.02.23"), "name")',
            'tagprop(entityref("BP.4.06.35"), "name")',
            'tagprop(entityref("RC.1.06"), "name")',
            'tagprop(entityref("BA.3.03.02.38"), "name")',
            'tagprop(entityref("BP.4.08.37"), "name")',
            'tagprop(entityref("RC.1.12"), "name")',
            'tagprop(entityref("RP.1.07"), "name")',
            'tagprop(entityref("RC.4"), "name")',
            'tagprop(entityref("RP.4"), "name")',
          ])
          .then(function (labelCycles) {
            const arrayLabels = [
              "cycle1label1",
              "cycle1label2",
              "cycle1label3",
              "cycle1label4",
              "cycle2label1",
              "cycle2label2",
              "cycle2label3",
              "cycle2label4",
              "cycle3label1",
              "cycle3label2",
              "cycle3label3",
              "cycle4label1",
              "cycle4label2",
              "cycle4label3",
              "cycle5label1",
              "cycle5label2",
              "cycle5label3",
              "cycle5label4",
              "cycle6label1",
              "cycle6label2",
              "cycle7label2",
              "cycle8label2",
              "cycle9label2",
              "cycle10label1",
              "cycle10label2",
              "cycle10label3",
              "cycle11label1",
              "cycle11label2",
              "cycle11label3",
              "cycle11label4",
              "cycle13label1",
              "cycle13label2",
            ];
            for (let i = 0; i < arrayLabels.length; i++) {
              field(arrayLabels[i]).set(labelCycles[i]);
            }
          });
      }),
      calcUtils.calc(function (calcUtils, field) {
        return wpw.tax
          .evaluate([
            'tagbal(entityref("BA.3.03.02.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("BP.4.05"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("RC.1.05"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("RP.3.03"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("BA.3.03.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("BP.4.10"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("RC.1.12.03"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("RP.1"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("BA.3.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("RC.1.04.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("RP.1.03"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("BA.3.03.02.10"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("BP.4.06.14"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("RC.1.07"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("BA.3.07"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("BP.4.03"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("RC.3"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("RP.3"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("BA.2"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("RC.1.09"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("BA.2.03"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("BP.1"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("BP.3"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("BA.3.03.02.23"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("BP.4.06.35"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("RC.1.06"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("BA.3.03.02.38"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("BP.4.08.37"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("RC.1.12"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("RP.1.07"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("RC.4"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
            'tagbal(entityref("RP.4"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          ])
          .then(function (soldeCycles) {
            soldeCycles = numbersFR(soldeCycles);
            const arrayLabels = [
              "cycle1solde1",
              "cycle1solde2",
              "cycle1solde3",
              "cycle1solde4",
              "cycle2solde1",
              "cycle2solde2",
              "cycle2solde3",
              "cycle2solde4",
              "cycle3solde1",
              "cycle3solde2",
              "cycle3solde3",
              "cycle4solde1",
              "cycle4solde2",
              "cycle4solde3",
              "cycle5solde1",
              "cycle5solde2",
              "cycle5solde3",
              "cycle5solde4",
              "cycle6solde1",
              "cycle6solde2",
              "cycle7solde2",
              "cycle8solde2",
              "cycle9solde2",
              "cycle10solde1",
              "cycle10solde2",
              "cycle10solde3",
              "cycle11solde1",
              "cycle11solde2",
              "cycle11solde3",
              "cycle11solde4",
              "cycle13solde1",
              "cycle13solde2",
            ];
            for (let i = 0; i < arrayLabels.length; i++) {
              field(arrayLabels[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(soldeCycles[i] / 1000)
              );
            }
          });
      }),
      calcUtils.onFormLoad("PDM", function (calcUtils, field) {
        return wpw.tax.getRiskAssessments().then(function (Assessments) {
          const idCycles = {
            KjGW3VgwSROG93d0hSdOdA: 0,
            "GyST1tF-SZuzx0L53xp4hw": 1,
            "HiAP7WmlT1qBh3Oy2kio-w": 2,
            "9oJBBr2cS0WyOnZ2hPmIVQ": 3,
            CgLtGsAQRRG56wSDkQZWyQ: 4,
            "87mSz9F_SuOcwheEbKt1Vw": 5,
            RZEcgL2AQTWHWChnZtDnnQ: 6,
            "4aYCQPP2Qs27uvr1KiAybg": 7,
            hNKd_vUCR7u5lFqxZPH2nA: 8,
            "2EiHzoAzQ3e9zn10AMNu3Q": 9,
            "srII9Wn-SYq5I_ocgZH26g": 10,
            "eB-EFGK1SJylz7wgBbaugw": 11,
            LHQn2RHUTS2MGitqM3ZrKQ: 12,
          };

          const numsAssertionsEinhérent = [
            "cycle8Einhérent",
            "cycle5Einhérent",
            "cycle10Einhérent",
            "cycle4Einhérent",
            "cycle13Einhérent",
            "cycle12Einhérent",
            "cycle1Einhérent",
            "cycle9Einhérent",
            "cycle3Einhérent",
            "cycle7Einhérent",
            "cycle2Einhérent",
            "cycle11Einhérent",
            "cycle6Einhérent",
          ];

          const numsAssertionsEcontrole = [
            "cycle8Econtrole",
            "cycle5Econtrole",
            "cycle10Econtrole",
            "cycle4Econtrole",
            "cycle13Econtrole",
            "cycle12Econtrole",
            "cycle1Econtrole",
            "cycle9Econtrole",
            "cycle3Econtrole",
            "cycle7Econtrole",
            "cycle2Econtrole",
            "cycle11Econtrole",
            "cycle6Econtrole",
          ];

          const numsAssertionsEras = [
            "cycle8ERAS",
            "cycle5ERAS",
            "cycle10ERAS",
            "cycle4ERAS",
            "cycle13ERAS",
            "cycle12ERAS",
            "cycle1ERAS",
            "cycle9ERAS",
            "cycle3ERAS",
            "cycle7ERAS",
            "cycle2ERAS",
            "cycle11ERAS",
            "cycle6ERAS",
          ];

          const numsAssertionsRinhérent = [
            "cycle8Rinhérent",
            "cycle5Rinhérent",
            "cycle10Rinhérent",
            "cycle4Rinhérent",
            "cycle13Rinhérent",
            "cycle12Rinhérent",
            "cycle1Rinhérent",
            "cycle9Rinhérent",
            "cycle3Rinhérent",
            "cycle7Rinhérent",
            "cycle2Rinhérent",
            "cycle11Rinhérent",
            "cycle6Rinhérent",
          ];

          const numsAssertionsRcontrole = [
            "cycle8Rcontrole",
            "cycle5Rcontrole",
            "cycle10Rcontrole",
            "cycle4Rcontrole",
            "cycle13Rcontrole",
            "cycle12Rcontrole",
            "cycle1Rcontrole",
            "cycle9Rcontrole",
            "cycle3Rcontrole",
            "cycle7Rcontrole",
            "cycle2Rcontrole",
            "cycle11Rcontrole",
            "cycle6Rcontrole",
          ];

          const numsAssertionsRras = [
            "cycle8RRAS",
            "cycle5RRAS",
            "cycle10RRAS",
            "cycle4RRAS",
            "cycle13RRAS",
            "cycle12RRAS",
            "cycle1RRAS",
            "cycle9RRAS",
            "cycle3RRAS",
            "cycle7RRAS",
            "cycle2RRAS",
            "cycle11RRAS",
            "cycle6RRAS",
          ];

          const numsAssertionsCinhérent = [
            "cycle8Cinhérent",
            "cycle5Cinhérent",
            "cycle10Cinhérent",
            "cycle4Cinhérent",
            "cycle13Cinhérent",
            "cycle12Cinhérent",
            "cycle1Cinhérent",
            "cycle9Cinhérent",
            "cycle3Cinhérent",
            "cycle7Cinhérent",
            "cycle2Cinhérent",
            "cycle11Cinhérent",
            "cycle6Cinhérent",
          ];

          const numsAssertionsCcontrole = [
            "cycle8Ccontrole",
            "cycle5Ccontrole",
            "cycle10Ccontrole",
            "cycle4Ccontrole",
            "cycle13Ccontrole",
            "cycle12Ccontrole",
            "cycle1Ccontrole",
            "cycle9Ccontrole",
            "cycle3Ccontrole",
            "cycle7Ccontrole",
            "cycle2Ccontrole",
            "cycle11Ccontrole",
            "cycle6Ccontrole",
          ];

          const numsAssertionsCras = [
            "cycle8CRAS",
            "cycle5CRAS",
            "cycle10CRAS",
            "cycle4CRAS",
            "cycle13CRAS",
            "cycle12CRAS",
            "cycle1CRAS",
            "cycle9CRAS",
            "cycle3CRAS",
            "cycle7CRAS",
            "cycle2CRAS",
            "cycle11CRAS",
            "cycle6CRAS",
          ];

          const numsAssertionsMinhérent = [
            "cycle8Minhérent",
            "cycle5Minhérent",
            "cycle10Minhérent",
            "cycle4Minhérent",
            "cycle13Minhérent",
            "cycle12Minhérent",
            "cycle1Minhérent",
            "cycle9Minhérent",
            "cycle3Minhérent",
            "cycle7Minhérent",
            "cycle2Minhérent",
            "cycle11Minhérent",
            "cycle6Minhérent",
          ];

          const numsAssertionsMcontrole = [
            "cycle8Mcontrole",
            "cycle5Mcontrole",
            "cycle10Mcontrole",
            "cycle4Mcontrole",
            "cycle13Mcontrole",
            "cycle12Mcontrole",
            "cycle1Mcontrole",
            "cycle9Mcontrole",
            "cycle3Mcontrole",
            "cycle7Mcontrole",
            "cycle2Mcontrole",
            "cycle11Mcontrole",
            "cycle6Mcontrole",
          ];

          const numsAssertionsMras = [
            "cycle8MRAS",
            "cycle5MRAS",
            "cycle10MRAS",
            "cycle4MRAS",
            "cycle13MRAS",
            "cycle12MRAS",
            "cycle1MRAS",
            "cycle9MRAS",
            "cycle3MRAS",
            "cycle7MRAS",
            "cycle2MRAS",
            "cycle11MRAS",
            "cycle6MRAS",
          ];

          const numsAssertionsDinhérent = [
            "cycle8Dinhérent",
            "cycle5Dinhérent",
            "cycle10Dinhérent",
            "cycle4Dinhérent",
            "cycle13Dinhérent",
            "cycle12Dinhérent",
            "cycle1Dinhérent",
            "cycle9Dinhérent",
            "cycle3Dinhérent",
            "cycle7Dinhérent",
            "cycle2Dinhérent",
            "cycle11Dinhérent",
            "cycle6Dinhérent",
          ];

          const numsAssertionsDcontrole = [
            "cycle8Dcontrole",
            "cycle5Dcontrole",
            "cycle10Dcontrole",
            "cycle4Dcontrole",
            "cycle13Dcontrole",
            "cycle12Dcontrole",
            "cycle1Dcontrole",
            "cycle9Dcontrole",
            "cycle3Dcontrole",
            "cycle7Dcontrole",
            "cycle2Dcontrole",
            "cycle11Dcontrole",
            "cycle6Dcontrole",
          ];

          const numsAssertionsDras = [
            "cycle8DRAS",
            "cycle5DRAS",
            "cycle10DRAS",
            "cycle4DRAS",
            "cycle13DRAS",
            "cycle12DRAS",
            "cycle1DRAS",
            "cycle9DRAS",
            "cycle3DRAS",
            "cycle7DRAS",
            "cycle2DRAS",
            "cycle11DRAS",
            "cycle6DRAS",
          ];

          const numsAssertionsSinhérent = [
            "cycle8Sinhérent",
            "cycle5Sinhérent",
            "cycle10Sinhérent",
            "cycle4Sinhérent",
            "cycle13Sinhérent",
            "cycle12Sinhérent",
            "cycle1Sinhérent",
            "cycle9Sinhérent",
            "cycle3Sinhérent",
            "cycle7Sinhérent",
            "cycle2Sinhérent",
            "cycle11Sinhérent",
            "cycle6Sinhérent",
          ];

          const numsAssertionsScontrole = [
            "cycle8Scontrole",
            "cycle5Scontrole",
            "cycle10Scontrole",
            "cycle4Scontrole",
            "cycle13Scontrole",
            "cycle12Scontrole",
            "cycle1Scontrole",
            "cycle9Scontrole",
            "cycle3Scontrole",
            "cycle7Scontrole",
            "cycle2Scontrole",
            "cycle11Scontrole",
            "cycle6Scontrole",
          ];

          const numsAssertionsSras = [
            "cycle8SRAS",
            "cycle5SRAS",
            "cycle10SRAS",
            "cycle4SRAS",
            "cycle13SRAS",
            "cycle12SRAS",
            "cycle1SRAS",
            "cycle9SRAS",
            "cycle3SRAS",
            "cycle7SRAS",
            "cycle2SRAS",
            "cycle11SRAS",
            "cycle6SRAS",
          ];

          const numsCheckboxEBase = [
            "cycle8EBase",
            "cycle5EBase",
            "cycle10EBase",
            "cycle4EBase",
            "cycle13EBase",
            "cycle12EBase",
            "cycle1EBase",
            "cycle9EBase",
            "cycle3EBase",
            "cycle7EBase",
            "cycle2EBase",
            "cycle11EBase",
            "cycle6EBase",
          ];

          const numsCheckboxEAna = [
            "cycle8EAna",
            "cycle5EAna",
            "cycle10EAna",
            "cycle4EAna",
            "cycle13EAna",
            "cycle12EAna",
            "cycle1EAna",
            "cycle9EAna",
            "cycle3EAna",
            "cycle7EAna",
            "cycle2EAna",
            "cycle11EAna",
            "cycle6EAna",
          ];

          const numsCheckboxEDétails = [
            "cycle8EDétails",
            "cycle5EDétails",
            "cycle10EDétails",
            "cycle4EDétails",
            "cycle13EDétails",
            "cycle12EDétails",
            "cycle1EDétails",
            "cycle9EDétails",
            "cycle3EDétails",
            "cycle7EDétails",
            "cycle2EDétails",
            "cycle11EDétails",
            "cycle6EDétails",
          ];

          const numsCheckboxRBase = [
            "cycle8RBase",
            "cycle5RBase",
            "cycle10RBase",
            "cycle4RBase",
            "cycle13RBase",
            "cycle12RBase",
            "cycle1RBase",
            "cycle9RBase",
            "cycle3RBase",
            "cycle7RBase",
            "cycle2RBase",
            "cycle11RBase",
            "cycle6RBase",
          ];

          const numsCheckboxRAna = [
            "cycle8RAna",
            "cycle5RAna",
            "cycle10RAna",
            "cycle4RAna",
            "cycle13RAna",
            "cycle12RAna",
            "cycle1RAna",
            "cycle9RAna",
            "cycle3RAna",
            "cycle7RAna",
            "cycle2RAna",
            "cycle11RAna",
            "cycle6RAna",
          ];

          const numsCheckboxRDétails = [
            "cycle8RDétails",
            "cycle5RDétails",
            "cycle10RDétails",
            "cycle4RDétails",
            "cycle13RDétails",
            "cycle12RDétails",
            "cycle1RDétails",
            "cycle9RDétails",
            "cycle3RDétails",
            "cycle7RDétails",
            "cycle2RDétails",
            "cycle11RDétails",
            "cycle6RDétails",
          ];

          const numsCheckboxCBase = [
            "cycle8CBase",
            "cycle5CBase",
            "cycle10CBase",
            "cycle4CBase",
            "cycle13CBase",
            "cycle12CBase",
            "cycle1CBase",
            "cycle9CBase",
            "cycle3CBase",
            "cycle7CBase",
            "cycle2CBase",
            "cycle11CBase",
            "cycle6CBase",
          ];

          const numsCheckboxCAna = [
            "cycle8CAna",
            "cycle5CAna",
            "cycle10CAna",
            "cycle4CAna",
            "cycle13CAna",
            "cycle12CAna",
            "cycle1CAna",
            "cycle9CAna",
            "cycle3CAna",
            "cycle7CAna",
            "cycle2CAna",
            "cycle11CAna",
            "cycle6CAna",
          ];

          const numsCheckboxCDétails = [
            "cycle8CDétails",
            "cycle5CDétails",
            "cycle10CDétails",
            "cycle4CDétails",
            "cycle13CDétails",
            "cycle12CDétails",
            "cycle1CDétails",
            "cycle9CDétails",
            "cycle3CDétails",
            "cycle7CDétails",
            "cycle2CDétails",
            "cycle11CDétails",
            "cycle6CDétails",
          ];

          const numsCheckboxMBase = [
            "cycle8MBase",
            "cycle5MBase",
            "cycle10MBase",
            "cycle4MBase",
            "cycle13MBase",
            "cycle12MBase",
            "cycle1MBase",
            "cycle9MBase",
            "cycle3MBase",
            "cycle7MBase",
            "cycle2MBase",
            "cycle11MBase",
            "cycle6MBase",
          ];

          const numsCheckboxMAna = [
            "cycle8MAna",
            "cycle5MAna",
            "cycle10MAna",
            "cycle4MAna",
            "cycle13MAna",
            "cycle12MAna",
            "cycle1MAna",
            "cycle9MAna",
            "cycle3MAna",
            "cycle7MAna",
            "cycle2MAna",
            "cycle11MAna",
            "cycle6MAna",
          ];

          const numsCheckboxMDétails = [
            "cycle8MDétails",
            "cycle5MDétails",
            "cycle10MDétails",
            "cycle4MDétails",
            "cycle13MDétails",
            "cycle12MDétails",
            "cycle1MDétails",
            "cycle9MDétails",
            "cycle3MDétails",
            "cycle7MDétails",
            "cycle2MDétails",
            "cycle11MDétails",
            "cycle6MDétails",
          ];

          const numsCheckboxDBase = [
            "cycle8DBase",
            "cycle5DBase",
            "cycle10DBase",
            "cycle4DBase",
            "cycle13DBase",
            "cycle12DBase",
            "cycle1DBase",
            "cycle9DBase",
            "cycle3DBase",
            "cycle7DBase",
            "cycle2DBase",
            "cycle11DBase",
            "cycle6DBase",
          ];

          const numsCheckboxDAna = [
            "cycle8DAna",
            "cycle5DAna",
            "cycle10DAna",
            "cycle4DAna",
            "cycle13DAna",
            "cycle12DAna",
            "cycle1DAna",
            "cycle9DAna",
            "cycle3DAna",
            "cycle7DAna",
            "cycle2DAna",
            "cycle11DAna",
            "cycle6DAna",
          ];

          const numsCheckboxDDétails = [
            "cycle8DDétails",
            "cycle5DDétails",
            "cycle10DDétails",
            "cycle4DDétails",
            "cycle13DDétails",
            "cycle12DDétails",
            "cycle1DDétails",
            "cycle9DDétails",
            "cycle3DDétails",
            "cycle7DDétails",
            "cycle2DDétails",
            "cycle11DDétails",
            "cycle6DDétails",
          ];

          const numsCheckboxSBase = [
            "cycle8SBase",
            "cycle5SBase",
            "cycle10SBase",
            "cycle4SBase",
            "cycle13SBase",
            "cycle12SBase",
            "cycle1SBase",
            "cycle9SBase",
            "cycle3SBase",
            "cycle7SBase",
            "cycle2SBase",
            "cycle11SBase",
            "cycle6SBase",
          ];

          const numsCheckboxSAna = [
            "cycle8SAna",
            "cycle5SAna",
            "cycle10SAna",
            "cycle4SAna",
            "cycle13SAna",
            "cycle12SAna",
            "cycle1SAna",
            "cycle9SAna",
            "cycle3SAna",
            "cycle7SAna",
            "cycle2SAna",
            "cycle11SAna",
            "cycle6SAna",
          ];

          const numsCheckboxSDétails = [
            "cycle8SDétails",
            "cycle5SDétails",
            "cycle10SDétails",
            "cycle4SDétails",
            "cycle13SDétails",
            "cycle12SDétails",
            "cycle1SDétails",
            "cycle9SDétails",
            "cycle3SDétails",
            "cycle7SDétails",
            "cycle2SDétails",
            "cycle11SDétails",
            "cycle6SDétails",
          ];

          const allNumsAssertions = numsAssertionsEinhérent.concat(
            numsAssertionsEcontrole,
            numsAssertionsEras,
            numsAssertionsRinhérent,
            numsAssertionsRcontrole,
            numsAssertionsRras,
            numsAssertionsCinhérent,
            numsAssertionsCcontrole,
            numsAssertionsCras,
            numsAssertionsMinhérent,
            numsAssertionsMcontrole,
            numsAssertionsMras,
            numsAssertionsDinhérent,
            numsAssertionsDcontrole,
            numsAssertionsDras,
            numsAssertionsSinhérent,
            numsAssertionsScontrole,
            numsAssertionsSras
          );
          for (let i = 0; i < allNumsAssertions.length; i++) {
            field(allNumsAssertions[i]).set(" ");
          }

          const allNumsCheckbox = numsCheckboxEBase.concat(
            numsCheckboxEAna,
            numsCheckboxEDétails,
            numsCheckboxRBase,
            numsCheckboxRAna,
            numsCheckboxRDétails,
            numsCheckboxCBase,
            numsCheckboxCAna,
            numsCheckboxCDétails,
            numsCheckboxMBase,
            numsCheckboxMAna,
            numsCheckboxMDétails,
            numsCheckboxDBase,
            numsCheckboxDAna,
            numsCheckboxDDétails,
            numsCheckboxSBase,
            numsCheckboxSAna,
            numsCheckboxSDétails
          );
          for (let i = 0; i < allNumsCheckbox.length; i++) {
            field(allNumsCheckbox[i]).set(false);
          }
          for (let i = 0; i < Assessments.length; i++) {
            const id = idCycles[Assessments[i].id];

            if (Assessments[i].children.length !== 0) {
              Assessments[i].children[3].inherentRisk === "low"
                ? field(numsAssertionsEinhérent[id]).set("Faible")
                : Assessments[i].children[3].inherentRisk === "medium"
                ? field(numsAssertionsEinhérent[id]).set("Moyen")
                : Assessments[i].children[3].inherentRisk === "high"
                ? field(numsAssertionsEinhérent[id]).set("Elevé")
                : field(numsAssertionsEinhérent[id]).set(" ");

              Assessments[i].children[3].controlRisk === "low"
                ? field(numsAssertionsEcontrole[id]).set("Faible")
                : Assessments[i].children[3].controlRisk === "medium"
                ? field(numsAssertionsEcontrole[id]).set("Moyen")
                : Assessments[i].children[3].controlRisk === "high"
                ? field(numsAssertionsEcontrole[id]).set("Elevé")
                : field(numsAssertionsEcontrole[id]).set(" ");

              Assessments[i].children[3].rmmRank === "low"
                ? field(numsAssertionsEras[id]).set("Faible")
                : Assessments[i].children[3].rmmRank === "medium"
                ? field(numsAssertionsEras[id]).set("Moyen")
                : Assessments[i].children[3].rmmRank === "high"
                ? field(numsAssertionsEras[id]).set("Elevé")
                : field(numsAssertionsEras[id]).set(" ");

              if (Assessments[i].children[3].rmmRank === "low") {
                field(numsCheckboxEBase[id]).set(true);
              }
              if (Assessments[i].children[3].rmmRank === "medium") {
                field(numsCheckboxEBase[id]).set(true);
                field(numsCheckboxEAna[id]).set(true);
              }
              if (Assessments[i].children[3].rmmRank === "high") {
                field(numsCheckboxEBase[id]).set(true);
                field(numsCheckboxEAna[id]).set(true);
                field(numsCheckboxEDétails[id]).set(true);
              }

              Assessments[i].children[4].inherentRisk === "low"
                ? field(numsAssertionsRinhérent[id]).set("Faible")
                : Assessments[i].children[4].inherentRisk === "medium"
                ? field(numsAssertionsRinhérent[id]).set("Moyen")
                : Assessments[i].children[4].inherentRisk === "high"
                ? field(numsAssertionsRinhérent[id]).set("Elevé")
                : field(numsAssertionsRinhérent[id]).set(" ");

              Assessments[i].children[4].controlRisk === "low"
                ? field(numsAssertionsRcontrole[id]).set("Faible")
                : Assessments[i].children[4].controlRisk === "medium"
                ? field(numsAssertionsRcontrole[id]).set("Moyen")
                : Assessments[i].children[4].controlRisk === "high"
                ? field(numsAssertionsRcontrole[id]).set("Elevé")
                : field(numsAssertionsRcontrole[id]).set(" ");

              Assessments[i].children[4].rmmRank === "low"
                ? field(numsAssertionsRras[id]).set("Faible")
                : Assessments[i].children[4].rmmRank === "medium"
                ? field(numsAssertionsRras[id]).set("Moyen")
                : Assessments[i].children[4].rmmRank === "high"
                ? field(numsAssertionsRras[id]).set("Elevé")
                : field(numsAssertionsRras[id]).set(" ");
              if (Assessments[i].children[4].rmmRank === "low") {
                field(numsCheckboxRBase[id]).set(true);
              }
              if (Assessments[i].children[4].rmmRank === "medium") {
                field(numsCheckboxRBase[id]).set(true);
                field(numsCheckboxRAna[id]).set(true);
              }
              if (Assessments[i].children[4].rmmRank === "high") {
                field(numsCheckboxRBase[id]).set(true);
                field(numsCheckboxRAna[id]).set(true);
                field(numsCheckboxRDétails[id]).set(true);
              }

              Assessments[i].children[0].inherentRisk === "low"
                ? field(numsAssertionsCinhérent[id]).set("Faible")
                : Assessments[i].children[0].inherentRisk === "medium"
                ? field(numsAssertionsCinhérent[id]).set("Moyen")
                : Assessments[i].children[0].inherentRisk === "high"
                ? field(numsAssertionsCinhérent[id]).set("Elevé")
                : field(numsAssertionsCinhérent[id]).set(" ");

              Assessments[i].children[0].controlRisk === "low"
                ? field(numsAssertionsCcontrole[id]).set("Faible")
                : Assessments[i].children[0].controlRisk === "medium"
                ? field(numsAssertionsCcontrole[id]).set("Moyen")
                : Assessments[i].children[0].controlRisk === "high"
                ? field(numsAssertionsCcontrole[id]).set("Elevé")
                : field(numsAssertionsCcontrole[id]).set(" ");

              Assessments[i].children[0].rmmRank === "low"
                ? field(numsAssertionsCras[id]).set("Faible")
                : Assessments[i].children[0].rmmRank === "medium"
                ? field(numsAssertionsCras[id]).set("Moyen")
                : Assessments[i].children[0].rmmRank === "high"
                ? field(numsAssertionsCras[id]).set("Elevé")
                : field(numsAssertionsCras[id]).set(" ");
              if (Assessments[i].children[0].rmmRank === "low") {
                field(numsCheckboxCBase[id]).set(true);
              }
              if (Assessments[i].children[0].rmmRank === "medium") {
                field(numsCheckboxCBase[id]).set(true);
                field(numsCheckboxCAna[id]).set(true);
              }
              if (Assessments[i].children[0].rmmRank === "high") {
                field(numsCheckboxCBase[id]).set(true);
                field(numsCheckboxCAna[id]).set(true);
                field(numsCheckboxCDétails[id]).set(true);
              }

              Assessments[i].children[1].inherentRisk === "low"
                ? field(numsAssertionsMinhérent[id]).set("Faible")
                : Assessments[i].children[1].inherentRisk === "medium"
                ? field(numsAssertionsMinhérent[id]).set("Moyen")
                : Assessments[i].children[1].inherentRisk === "high"
                ? field(numsAssertionsMinhérent[id]).set("Elevé")
                : field(numsAssertionsMinhérent[id]).set(" ");

              Assessments[i].children[1].controlRisk === "low"
                ? field(numsAssertionsMcontrole[id]).set("Faible")
                : Assessments[i].children[1].controlRisk === "medium"
                ? field(numsAssertionsMcontrole[id]).set("Moyen")
                : Assessments[i].children[1].controlRisk === "high"
                ? field(numsAssertionsMcontrole[id]).set("Elevé")
                : field(numsAssertionsMcontrole[id]).set(" ");

              Assessments[i].children[1].rmmRank === "low"
                ? field(numsAssertionsMras[id]).set("Faible")
                : Assessments[i].children[1].rmmRank === "medium"
                ? field(numsAssertionsMras[id]).set("Moyen")
                : Assessments[i].children[1].rmmRank === "high"
                ? field(numsAssertionsMras[id]).set("Elevé")
                : field(numsAssertionsMras[id]).set(" ");
              if (Assessments[i].children[1].rmmRank === "low") {
                field(numsCheckboxMBase[id]).set(true);
              }
              if (Assessments[i].children[1].rmmRank === "medium") {
                field(numsCheckboxMBase[id]).set(true);
                field(numsCheckboxMAna[id]).set(true);
              }
              if (Assessments[i].children[1].rmmRank === "high") {
                field(numsCheckboxMBase[id]).set(true);
                field(numsCheckboxMAna[id]).set(true);
                field(numsCheckboxMDétails[id]).set(true);
              }

              Assessments[i].children[2].inherentRisk === "low"
                ? field(numsAssertionsDinhérent[id]).set("Faible")
                : Assessments[i].children[2].inherentRisk === "medium"
                ? field(numsAssertionsDinhérent[id]).set("Moyen")
                : Assessments[i].children[2].inherentRisk === "high"
                ? field(numsAssertionsDinhérent[id]).set("Elevé")
                : field(numsAssertionsDinhérent[id]).set(" ");

              Assessments[i].children[2].controlRisk === "low"
                ? field(numsAssertionsDcontrole[id]).set("Faible")
                : Assessments[i].children[2].controlRisk === "medium"
                ? field(numsAssertionsDcontrole[id]).set("Moyen")
                : Assessments[i].children[2].controlRisk === "high"
                ? field(numsAssertionsDcontrole[id]).set("Elevé")
                : field(numsAssertionsDcontrole[id]).set(" ");

              Assessments[i].children[2].rmmRank === "low"
                ? field(numsAssertionsDras[id]).set("Faible")
                : Assessments[i].children[2].rmmRank === "medium"
                ? field(numsAssertionsDras[id]).set("Moyen")
                : Assessments[i].children[2].rmmRank === "high"
                ? field(numsAssertionsDras[id]).set("Elevé")
                : field(numsAssertionsDras[id]).set(" ");
              if (Assessments[i].children[2].rmmRank === "low") {
                field(numsCheckboxDBase[id]).set(true);
              }
              if (Assessments[i].children[2].rmmRank === "medium") {
                field(numsCheckboxDBase[id]).set(true);
                field(numsCheckboxDAna[id]).set(true);
              }
              if (Assessments[i].children[2].rmmRank === "high") {
                field(numsCheckboxDBase[id]).set(true);
                field(numsCheckboxDAna[id]).set(true);
                field(numsCheckboxDDétails[id]).set(true);
              }

              Assessments[i].children[5].inherentRisk === "low"
                ? field(numsAssertionsSinhérent[id]).set("Faible")
                : Assessments[i].children[5].inherentRisk === "medium"
                ? field(numsAssertionsSinhérent[id]).set("Moyen")
                : Assessments[i].children[5].inherentRisk === "high"
                ? field(numsAssertionsSinhérent[id]).set("Elevé")
                : field(numsAssertionsSinhérent[id]).set(" ");

              Assessments[i].children[5].controlRisk === "low"
                ? field(numsAssertionsScontrole[id]).set("Faible")
                : Assessments[i].children[5].controlRisk === "medium"
                ? field(numsAssertionsScontrole[id]).set("Moyen")
                : Assessments[i].children[5].controlRisk === "high"
                ? field(numsAssertionsScontrole[id]).set("Elevé")
                : field(numsAssertionsScontrole[id]).set(" ");

              Assessments[i].children[5].rmmRank === "low"
                ? field(numsAssertionsSras[id]).set("Faible")
                : Assessments[i].children[5].rmmRank === "medium"
                ? field(numsAssertionsSras[id]).set("Moyen")
                : Assessments[i].children[5].rmmRank === "high"
                ? field(numsAssertionsSras[id]).set("Elevé")
                : field(numsAssertionsSras[id]).set(" ");
              if (Assessments[i].children[5].rmmRank === "low") {
                field(numsCheckboxSBase[id]).set(true);
              }
              if (Assessments[i].children[5].rmmRank === "medium") {
                field(numsCheckboxSBase[id]).set(true);
                field(numsCheckboxSAna[id]).set(true);
              }
              if (Assessments[i].children[5].rmmRank === "high") {
                field(numsCheckboxSBase[id]).set(true);
                field(numsCheckboxSAna[id]).set(true);
                field(numsCheckboxSDétails[id]).set(true);
              }
            }
          }
        });
      }),
      calcUtils.onFormLoad("PDM", function (calcUtils, field) {
        wpw.tax.getIssues().then(function (issueProperties) {
          let myTable = field("taches93");
          let datetext = "";
          const statut = {
            closed: "Fermé",
            open: "Ouvert",
            resolved: "Résolu",
          };

          let tableLength = myTable.getRows().length;
          for (let i = 0; i < tableLength; i++) {
            calcUtils.removeTableRow("PDM", "taches93", 0);
          }

          for (let i = 0; i < issueProperties.length; i++) {
            if (issueProperties[i].type !== "consideration") {
              issueProperties.splice(i, 1);
              i--;
            }
          }

          for (let i = 0; i < issueProperties.length; i++) {
            const arrayResponses = Object.values(issueProperties[i].responses);
            const description = arrayResponses[0].text
              .replace(RegExp(/<\/p>/gi), "\n")
              .replaceAll("&#160;", "\n")
              .replaceAll("&#38;", "&")
              .replace(RegExp(/<[^>]+>/gi), "");

            for (let j = 0; j < arrayResponses.length; j++) {
              const dateEU = arrayResponses[j].date.slice(0, 10).split("-");
              arrayResponses[j].status !== "open" &&
              issueProperties[i].status !== "open"
                ? (datetext = `${dateEU[2]}-${dateEU[1]}-${dateEU[0]}`)
                : (datetext = "En cours");
            }

            calcUtils.addTableRow("PDM", "taches93", i);
            myTable.cell(i, 0).assign("Point d'audit");
            myTable.cell(i, 2).assign(description);
            myTable.cell(i, 4).assign(statut[issueProperties[i].status]);
            myTable.cell(i, 6).assign(datetext);
          }
        });
      });

    calcUtils.onFormLoad("PDM", function (calcUtils, field) {
      wpw.tax.getRisks().then(function (risks) {
        const myTable = field("risque921");
        let repAudit = [];
        let assArray = [];
        let tagsFR = [];
        const tags = {
          MzUhZfaFT4KKspLoTre0_Q: "Fournisseurs Charges",
          XPKxJGCASXa90axSzrBPbQ: "Clients Ventes",
          "UZjt3V2rTM6-H_mEC_uyRw": "Stocks",
          Og9vyd9ORL6aF908dO4dWA: "Personnel",
          zNBtfv8vRDuKQ2jjCVynSA: "Tresorerie Financement",
          yxKIZSE4RtOcXR7gZzFIoQ: "Immobilisations incorporelle et corporelles",
          "DAtZRISTSvas2QsCHnJ-Dw": "immobilisations financières",
          OKD_V6k0SaeFI3OuJRuA3Q: "Capitaux propres",
          "glZIDE-tRrKaFl_88-BGmA": "Provisions pour risques et c",
          quTgDJw4SmStWvAq0WzKaA: "Impots et taxes",
          xI88oRMGQSmD0aBkx4p7dA: "Autres creances et dettes",
          "ZVHH8sj2SJm-P4NHDIjpUg": "Resultat exceptionnel",
        };

        let tableLength = myTable.getRows().length;
        for (let i = 0; i < tableLength; i++) {
          calcUtils.removeTableRow("PDM", "risque921", 0);
        }

        for (let i = 0; i < risks.length; i++) {
          for (let j = 0; j < Object.keys(risks[i].tags).length; j++) {
            tagsFR[j] = tags[risks[i].tags[j]];
          }
          for (let k = 0; k < Object.keys(risks[i].scopedTags).length; k++) {
            const tag = risks[i].tags[k];
            assArray[k] = [];
            for (
              let j = 0;
              j < Object.keys(risks[i].scopedTags[tag]).length;
              j++
            ) {
              const scoppedTag = risks[i].scopedTags[tag];
              switch (scoppedTag[j]) {
                case "k3O5OkcyMvquVyVAvgnewQ":
                  assArray[k][0] = "E";
                  break;
                case "rFNNk6YKOuG4FtE_-h0RWg":
                  break;
                case "koLe5cFPNOaoAtl-dnkoRQ":
                  assArray[k][1] = "R";
                  break;
                case "PWy-RwCTMX2GjcYPBPfGXQ":
                  break;
                case "D5YN3Ka6P9-nEobiLBjYXA":
                  assArray[k][2] = "C";
                  break;
                case "lHbZ4O3lMXesKcNQMc9Brw":
                  assArray[k][3] = "M";
                  break;
                case "qsj9cmePO5OI-8WOhCdpzg":
                  break;
                case "9uf1Z4dmNAC9SwWVG7E_Mw":
                  assArray[k][4] = "D";
                  break;
                case "4CqDDKzgO7uHFpLns6v_bA":
                  assArray[k][5] = "S";
                  break;
              }
            }
            assArray[k] = assArray[k].join("");
          }

          let cycles = [];
          for (let j = 0; j < tagsFR.length; j++) {
            cycles[j] = `${tagsFR[j]} / ${assArray[j]}`;
          }

          let documentsLink = [];
          Object.keys(risks[i]["attachables"]).forEach((id) => {
            documentsLink.push(risks[i]["attachables"][id].link);
          });
          let procedureLink = [];
          Object.keys(risks[i]["checklistProcedures"]).forEach((id) => {
            procedureLink.unshift(risks[i]["checklistProcedures"][id]);
          });
          calcUtils.addTableRow("PDM", "risque921", i);
          myTable.cell(i, 0).setCellLabel(risks[i].name);
          myTable.cell(i, 1).setCellLabel(cycles.join("\n"));
          risks[i].fraudRisk
            ? myTable.cell(i, 2).setCellLabel("Fraude")
            : risks[i].significantRisk
            ? myTable.cell(i, 2).setCellLabel("Significatif")
            : myTable.cell(i, 2).setCellLabel("Normal");
          wpw.tax.getControls().then(function (controls) {
            let controlsName = [];
            for (let j = 0; j < controls.length; j++) {
              for (let k = 0; k < risks[i].controls.length; k++) {
                if (risks[i].controls[k] === controls[j].id) {
                  controlsName[k] = controls[j].name;
                }
              }
            }
            myTable.cell(i, 3).setCellLabel(controlsName.join("\n"));
          });

          wpw.tax.getDocuments().then((docs) => {
            let docName = [];
            let docContent = [];

            for (let j = 0; j < docs.length; j++) {
              if (documentsLink.indexOf(docs[j].id) > -1) {
                docName.push(docs[j].name);
                docContent.push(docs[j].content);
              }
            }
            for (let j = 0; j < docContent.length; j++) {
              wpw.tax.getChecklists([docContent[j]]).then((checklists) => {
                let procedureName = [];
                for (let i = 0; i < checklists[0]["procedures"].length; i++) {
                  if (
                    procedureLink[j].indexOf(
                      checklists[0]["procedures"][i].id
                    ) > -1
                  ) {
                    procedureName.push(checklists[0]["procedures"][i].summary);
                  }
                }
                repAudit[j] = `${docName[j]} | ${procedureName[0]}`;
                myTable.cell(i, 4).setCellLabel(
                  repAudit
                    .filter((el) => {
                      return el != null && el != "";
                    })
                    .join("\n")
                );
              });
            }
          });

          assArray = [];
          tagsFR = [];
        }
      });
    });

    calcUtils.calc(function (calcUtils, field) {
      wpw.tax.getControls().then(function (controls) {
        const typeControls = {
          w7Nd3gjjO4q_Ms4_zG3GDA: "Entité",
          "8qybickhMKSmMAipaUlXzA": "SI",
          S7lipDe7PpaE4Aib2UiHbQ: "Etat financier",
        };
        const freqControls = {
          quarterly: "trimestrielle",
          weekly: "hebdomadaire",
          semi_annually: "semestrielle",
          annually: "tous les ans",
          monthly: "Mensuelle",
          daily: "quotidient",
          semi_monthly: "Bimensuel",
          multiple_daily: "Plusieurs fois par jour",
          multiple_weekly: "plusieurs fois par semaine",
        };
        const key = {
          true: "Oui",
          false: "Non",
        };
        const exec = {
          automated: "Automatisé",
          it_dependent: "Dépendant des TI",
          manual: "Manuel",
        };
        const classification = {
          all: "Les deux",
          preventative: "Préventif",
          detective: "Détective",
        };
        const assertions = {
          k3O5OkcyMvquVyVAvgnewQ: "E",
          "koLe5cFPNOaoAtl-dnkoRQ": "R",
          "D5YN3Ka6P9-nEobiLBjYXA": "C",
          lHbZ4O3lMXesKcNQMc9Brw: "M",
          "9uf1Z4dmNAC9SwWVG7E_Mw": "D",
          "4CqDDKzgO7uHFpLns6v_bA": "S",
        };
        const cycles = {
          MzUhZfaFT4KKspLoTre0_Q: "Fournisseurs Charges",
          XPKxJGCASXa90axSzrBPbQ: "Clients Ventes",
          "UZjt3V2rTM6-H_mEC_uyRw": "Stocks",
          Og9vyd9ORL6aF908dO4dWA: "Personnel",
          zNBtfv8vRDuKQ2jjCVynSA: "Tresorerie Financement",
          yxKIZSE4RtOcXR7gZzFIoQ: "Immobilisatiobs incorporelle et corporelles",
          "DAtZRISTSvas2QsCHnJ-Dw": "immobilisations financières",
          OKD_V6k0SaeFI3OuJRuA3Q: "Capitaux propres",
          "glZIDE-tRrKaFl_88-BGmA": "Provisions pour risques et c",
          quTgDJw4SmStWvAq0WzKaA: "Impots et taxes",
          xI88oRMGQSmD0aBkx4p7dA: "Autres creances et dettes",
          "ZVHH8sj2SJm-P4NHDIjpUg": "Resultat exceptionnel",
        };
        let cyclesFR = [];
        let assertionsFR = [];
        let cyclesAsser = [];

        for (let i = 0; i < field("controlsPDM").getRows().length; i++) {
          calcUtils.removeTableRow("PDM", "controlsPDM", 0);
        }

        for (let i = 0; i < controls.length; i++) {
          calcUtils.addTableRow("PDM", "controlsPDM", i);
          field("controlsPDM").cell(i, 0).setCellLabel(controls[i].name);
          field("controlsPDM")
            .cell(i, 1)
            .setCellLabel(typeControls[controls[i].tags[0]]);

          for (let j = 0; j < controls[i].tags.length; j++) {
            cyclesFR[j] = cycles[controls[i].tags[j]];
            cyclesFR = cyclesFR.filter(Boolean);
          }

          for (let j = 0; j < Object.keys(controls[i].scopedTags).length; j++) {
            assertionsFR[j] = [];
            assertionsFR[j] =
              controls[i].scopedTags[Object.keys(controls[i].scopedTags)[j]];
            for (let k = 0; k < assertionsFR[j].length; k++) {
              assertionsFR[j][k] = assertions[assertionsFR[j][k]];
            }
            assertionsFR[j] = assertionsFR[j].join("");
          }

          for (let i = 0; i < cyclesFR.length; i++) {
            cyclesAsser[i] = `${cyclesFR[i]}/${assertionsFR[i]}`;
          }

          field("controlsPDM").cell(i, 2).setCellLabel(cyclesAsser.join("\n"));

          wpw.tax.getRisks().then(function (risks) {
            let risksName = [];
            for (let j = 0; j < risks.length; j++) {
              for (let k = 0; k < controls[i].risks.length; k++) {
                if (controls[i].risks[k] === risks[j].id) {
                  risksName[k] = risks[j].name;
                }
              }
            }
            field("controlsPDM").cell(i, 3).setCellLabel(risksName.join("\n"));
          });

          field("controlsPDM").cell(i, 4).setCellLabel(key[controls[i].key]);
          field("controlsPDM")
            .cell(i, 5)
            .setCellLabel(exec[controls[i].execution]);
          field("controlsPDM")
            .cell(i, 6)
            .setCellLabel(classification[controls[i].classification]);
          field("controlsPDM")
            .cell(i, 7)
            .setCellLabel(freqControls[controls[i].frequency]);
        }
      });
    });
  });
})(wpw);
