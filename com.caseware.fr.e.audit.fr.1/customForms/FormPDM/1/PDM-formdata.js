(function () {
  "use strict";

  wpw.tax.create.formData("PDM", {
    formInfo: {
      abbreviation: "PDM",
      dynamicFormWidth: true,
      title: "Plan de mission",
      highlightFieldsets: false,
      hideheader: true,
      headerImage: "cw",
      css: "se-builder-custom.css",
      category: "Start Here",
      showDots: false,
      documentMap: true,
      enableTabs: true,
      tabLimit: 8,
    },

    sections: [
      {
        header: "1. Définition de la mission",
        headerClass: "font-header",
        tabConfig: {
          tabText: "Mission",
        },
        rows: [
          {
            header: "1.1 Synthèse",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "synt1",
              },
            ],
          },
          {
            header: "1.2 Informations générales",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label: "Type de mission",
                num: "tdm1",
                inputClass: "fullLength",
                type: "dropdown",
                init: "1",
                options: [
                  {
                    option: "Audit légal",
                    value: 1,
                  },
                  {
                    option: "Audit contractuel",
                    value: 2,
                  },
                ],
              },
              {
                label: "Nature des comptes",
                num: "ndc1",
                inputClass: "fullLength",
                type: "dropdown",
                init: "1",
                options: [
                  {
                    option: "Individuels",
                    value: 1,
                  },
                  {
                    option: "Consolidés",
                    value: 2,
                  },
                ],
              },
              {
                type: "table",
                num: "infosGTable",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header:
              "1.3 Techniques d'audit retenues pour la collecte d'éléments probants",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "table",
                num: "techaudit",
                procedureControls: "true",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
        ],
      },
      {
        header: "2. Présentation de l'entité",
        tabConfig: {
          tabText: "Entité",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "2.1 Synthèse",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "synt2",
              },
            ],
          },
          {
            header: "2.2 Information administrative",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                label: "Raison sociale",
                inputClass: "fullLength",
                num: "rs2",
              },
              {
                type: "textArea",
                label: "RCS (N°/lieu)",
                inputClass: "fullLength",
                num: "rcsN2",
              },
              {
                type: "textArea",
                label: "Forme juridique",
                inputClass: "fullLength",
                num: "fj2",
              },
              {
                type: "textArea",
                label: "Dirigeant",
                inputClass: "fullLength",
                num: "dirig2",
              },
              {
                type: "textArea",
                label: "Répartition de l'actionnariat",
                inputClass: "fullLength",
                num: "rda2",
              },
              {
                type: "textArea",
                label: "Activité principale",
                inputClass: "fullLength",
                num: "ap2",
              },
              {
                type: "textArea",
                label: "Siège social",
                inputClass: "fullLength",
                num: "ss2",
              },
              {
                type: "textArea",
                label: "Lieu d'exercice",
                inputClass: "fullLength",
                num: "le2",
              },
              {
                type: "text",
                label: "Effectif total",
                inputClass: "fullLength",
                num: "et2",
              },
            ],
          },
          {
            header: "2.3 Prise de connaissance des objectifs stratégiques",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "table",
                num: "pdcdos",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header: "2.4 Particularités juridiques",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "syntpj2",
              },
            ],
          },
          {
            header: "2.5 Particularités économiques",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "syntpe2",
              },
            ],
          },
          {
            header: "2.6 Particularités sociales et fiscales",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "syntpsf2",
              },
            ],
          },
          {
            header: "2.7 Particularités réglementaire",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "syntpr2",
              },
            ],
          },
          {
            header: "2.8 Particularités comptables / Changement de méthode",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "syntpcc2",
              },
            ],
          },
          {
            header: "2.9 Faits marquants intervenus au cours de l'exercice",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label: " ",
              },
              {
                type: "textArea",
                num: "syntfmi2",
              },
            ],
          },
          {
            header: "2.10 Conjonctures et perspectives d’avenir",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "syntcpa2",
              },
            ],
          },
          {
            header: "2.11 Liste des parties liées",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "table",
                num: "pariesliées",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header: "2.12 Filiales et participations",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "table",
                num: "filiales",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header: "2.13 Liste des principaux contrats",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "table",
                num: "contrats",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
        ],
      },
      {
        header: "3. INFORMATIONS COMPTABLES ET FINANCIERES",
        tabConfig: {
          tabText: "Informations",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "3.1 Synthèse",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "synt3",
              },
            ],
          },
          {
            header: "3.2 Bilan",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label: "Paramètres du tableau",
                type: "splitInputs",
                inputType: "checkbox",
                divisions: "3",
                num: "500",
                showValues: "false",
                items: [
                  { label: "K €", value: "1" },
                  { label: "Masquer les lignes vides", value: "2" },
                ],
              },
              {
                label: "Actif K€",
                labelClass: "bold",
              },
              {
                type: "table",
                num: "actifHeader",
              },
              {
                labelClass: "fullLength",
              },
              {
                type: "table",
                num: "actif",
                procedureControls: "true",
              },
              {
                labelClass: "fullLength",
              },
              {
                type: "text",
                inputType: "none",
                label: "Commentaire sur l'actif :",
              },
              {
                type: "textArea",
                num: "commentaireactif32",
              },
              {
                label: "Passif K€",
                labelClass: "bold",
              },
              {
                type: "table",
                num: "passifHeader",
              },
              {
                labelClass: "fullLength",
              },
              {
                type: "table",
                num: "passif",
                procedureControls: "true",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header: "3.3 Comptes de résultat",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "table",
                num: "compteresultatHeader",
              },
              {
                labelClass: "fullLength",
              },
              {
                type: "table",
                num: "compteresultat",
              },
              {
                labelClass: "fullLength",
              },
              {
                label: "Commentaire sur les produits : ",
              },
              {
                type: "textArea",
                num: "commentaireproduits33",
              },
              {
                label: "Commentaire sur les charges :",
              },
              {
                type: "textArea",
                num: "commentairecharges33",
              },
            ],
          },
          {
            header:
              "3.4 Points marquants résultant de l'analyse des chiffres significatifs",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label: "Commentaire sur le bilan:",
              },
              {
                type: "textArea",
                num: "commentairebilan",
              },
              {
                label: "Commentaire sur le résultat:",
              },
              {
                type: "textArea",
                num: "commentaireresultat",
              },
              {
                label: "Commentaire sur le SIG",
              },
              {
                type: "textArea",
                num: "commentairesig",
              },
              {
                label: "Commentaire sur le tableau de financement",
              },
              {
                type: "textArea",
                num: "commentairetableaufinancement",
              },
              {
                label: "Commentaire sur le tableau des flux de trésorerie",
              },
              {
                type: "textArea",
                num: "commentairefluxtresorerie",
              },
              {
                type: "table",
                num: "taches34",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header: "3.5 Rappel des Seuils",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "table",
                num: "rappeldesseuils",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header: "3.6 Suivi des anomalies relevées",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label: "Liste des Points d'Audit de l'exercice précédent",
              },
              {
                type: "textArea",
                num: "synt36",
              },
            ],
          },
        ],
      },
      {
        header: "4. ENVIRONNEMENT DE L'ENTITE",
        tabConfig: {
          tabText: "Environnement",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "4.1 Synthèse",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "synt4",
              },
            ],
          },
          {
            header: "4.2 Environnement de l'entité",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/N7h6R_WFRpaoZD4iiCPQXA" target="_blank">2.RI.01 Connaissance de l\'entité</a>',
              },
              {
                type: "textArea",
                num: "conclusion42",
                disabled: true,
              },
            ],
          },
        ],
      },
      {
        header: "5. RISQUES ET CONTROLE INTERNE",
        tabConfig: {
          tabText: "Ctrl. Interne",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "5.1 Synthèse",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "synt5",
              },
            ],
          },
          {
            header:
              "5.2 Appréciation du CI au niveau des Systèmes d'Information",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "syntadci5",
              },
            ],
          },
          {
            header: "5.3 Appréciation du CI au niveau des cycles",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "table",
                num: "controlsPDM",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
        ],
      },
      {
        header: "6. FRAUDE",
        tabConfig: {
          tabText: "Fraude",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "6.1 Synthèse",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "synt6",
              },
            ],
          },
          {
            header: "6.2. Risque de fraude",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/EDEHjsNkQmGCp49ENZWgCQ" target="_blank">2.RI.06 Risque de Fraude</a>',
              },
              {
                type: "textArea",
                num: "conclusion62",
                disabled: true,
              },
            ],
          },
          {
            header: "6.3. Lutte contre le blanchiment ",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/9aNbhn3uRUKSPJ_Wt8DItQ" target="_blank">2.RI.05 Risque de blanchiment et financement du terrorisme</a>',
              },
              {
                type: "textArea",
                num: "conclusion63",
                disabled: true,
              },
            ],
          },
        ],
      },
      {
        header: "7. SERVICES COMPTABLES ET FINANCIERS",
        tabConfig: {
          tabText: "Sces compta. et Fin.",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "7.1 Synthèse",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "synt7",
              },
            ],
          },
          {
            header: "7.2 Description des services",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "synt72",
              },
            ],
          },
          {
            header: "7.3 Conseils de l'entité",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "table",
                num: "cdle",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            label: " ",
          },
          {
            label:
              'Voir <a href="./index.jsp#/checklist/F51Q88LgTmaHhANENb_Xww" target="_blank">1.2 Fiche signalétique </a>',
            labelClass: "title",
          },
        ],
      },
      {
        header: "8. SYSTEME INFORMATIQUE",
        tabConfig: {
          tabText: "Syst. Info.",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "8.1 Synthèse",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "synt8",
              },
            ],
          },
          {
            header: "8.2 Liste des logiciels utilisés",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "table",
                num: "logiciels",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
        ],
      },
      {
        header: "9. STRATEGIE D'AUDIT",
        tabConfig: {
          tabText: "Stratégie d'audit",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "9.1 Synthèse",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "synt9",
              },
            ],
          },
          {
            header: "9.2 Liste des risques / Sections à risques",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "table",
                num: "risque921",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header: "9.3 Approche d'audit",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "table",
                num: "taches93",
              },
              {
                labelClass: "fullLength",
              },
              {
                label: "Paramètres du tableau",
                type: "splitInputs",
                inputType: "checkbox",
                divisions: "3",
                num: "checkboxCycles",
                showValues: "false",
                items: [{ label: "Montant en K €", value: "1" }],
              },
              {
                label:
                  'source <a href="./index.jsp#/se-risks/assessment" target=_blank>Section à risques</a>',
              },
              {
                type: "table",
                num: "cycle1",
              },
              {
                labelClass: "fullLength",
              },
              {
                label:
                  'source <a href="./index.jsp#/se-risks/assessment" target=_blank>Section à risques</a>',
              },
              {
                type: "table",
                num: "cycle2",
              },
              {
                labelClass: "fullLength",
              },
              {
                label:
                  'source <a href="./index.jsp#/se-risks/assessment" target=_blank>Section à risques</a>',
              },
              {
                type: "table",
                num: "cycle3",
              },
              {
                labelClass: "fullLength",
              },
              {
                label:
                  'source <a href="./index.jsp#/se-risks/assessment" target=_blank>Section à risques</a>',
              },
              {
                type: "table",
                num: "cycle4",
              },
              {
                labelClass: "fullLength",
              },
              {
                label:
                  'source <a href="./index.jsp#/se-risks/assessment" target=_blank>Section à risques</a>',
              },
              {
                type: "table",
                num: "cycle5",
              },
              {
                labelClass: "fullLength",
              },
              {
                label:
                  'source <a href="./index.jsp#/se-risks/assessment" target=_blank>Section à risques</a>',
              },
              {
                type: "table",
                num: "cycle6",
              },
              {
                labelClass: "fullLength",
              },
              {
                label:
                  'source <a href="./index.jsp#/se-risks/assessment" target=_blank>Section à risques</a>',
              },
              {
                type: "table",
                num: "cycle7",
              },
              {
                labelClass: "fullLength",
              },
              {
                label:
                  'source <a href="./index.jsp#/se-risks/assessment" target=_blank>Section à risques</a>',
              },
              {
                type: "table",
                num: "cycle8",
              },
              {
                labelClass: "fullLength",
              },
              {
                label:
                  'source <a href="./index.jsp#/se-risks/assessment" target=_blank>Section à risques</a>',
              },
              {
                type: "table",
                num: "cycle9",
              },
              {
                labelClass: "fullLength",
              },
              {
                label:
                  'source <a href="./index.jsp#/se-risks/assessment" target=_blank>Section à risques</a>',
              },
              {
                type: "table",
                num: "cycle10",
              },
              {
                labelClass: "fullLength",
              },
              {
                label:
                  'source <a href="./index.jsp#/se-risks/assessment" target=_blank>Section à risques</a>',
              },
              {
                type: "table",
                num: "cycle11",
              },
              {
                labelClass: "fullLength",
              },
              {
                label:
                  'source <a href="./index.jsp#/se-risks/assessment" target=_blank>Section à risques</a>',
              },
              {
                type: "table",
                num: "cycle12",
              },
              {
                labelClass: "fullLength",
              },
              {
                label:
                  'source <a href="./index.jsp#/se-risks/assessment" target=_blank>Section à risques</a>',
              },
              {
                type: "table",
                num: "cycle13",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
        ],
      },
      {
        header: "10. PLANIFICATION DE LA MISSION",
        tabConfig: {
          tabText: "Equipe & budget",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "10.1 Synthèse",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "synt10",
              },
            ],
          },
          {
            header: "10.2 Budget et composition équipe",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "input102",
              },
              {
                label:
                  '<a href="./index.jsp#/checklist/5qevD3eQTXCnRQUK0lMXEA" target="_blank">Voir 1.AM.05 Charges et budget </a>',
                labelClass: "title",
              },
            ],
          },
        ],
      },
      {
        header: "11. LIVRABLES",
        tabConfig: {
          tabText: "Livrables",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "11.1 Synthèse",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "synt11",
              },
            ],
          },
          {
            header: "11.2 Deliverables",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "table",
                num: "delivrables",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
        ],
      },
    ],
  });
})();
