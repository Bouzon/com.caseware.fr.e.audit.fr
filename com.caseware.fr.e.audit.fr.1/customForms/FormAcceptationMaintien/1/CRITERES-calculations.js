wpw.tax.create.calcBlocks("CRITERES", function (calcUtils) {
  calcUtils.calc(function (calcUtils, field) {
    const activitesEntite = {
      1: "Autre - Faible",
      20: "Voyages, hôtels, restaurants et hospitalité - Moyen",
      21: "Construction - Moyen",
      22: "Immobilier - Moyen",
      23: "Bijouterie - Moyen",
      30: "Antiquités, arts, Commerce de métaux précieux ou de diamants - élevé",
      31: "Jeux et paris - élevé",
      32: "Stupéfiants - élevé",
      33: "Pornographie - élevé",
      24: "Clubs sportifs, Agents sportifs - Moyen",
    };
    const localisationActivites = {
      1: "Autre - Faible",
      30: "Afghanistan - Elevé - Liste UE - 20/09/2016",
      31: "Albanie - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
      32: "Bahamas - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
      33: "Barbade - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
      34: "Botswana - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
      35: "Cambodge - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
      36: "Corée du nord - Elevé - GAFI - 23/10/2020 et Liste UE - 20/09/2016",
      37: "Ghana - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
      38: "Irak - Elevé - Liste UE - 01/10/2020",
      39: "Iran - Elevé - GAFI - 23/10/2020 et Liste UE - 20/09/2016",
      "3a": "Jamaïque - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
      "3b": "Maurice - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
      "3c": "Mongolie - Elevé - Liste UE - 01/10/2020",
      "3d": "Myanmar - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
      "3e": "Nicaragua - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
      "3f": "Ouganda - Elevé - GAFI - 23/10/2020 et Liste UE - 20/09/2016",
      "3g": "Pakistan - Elevé - GAFI - 23/10/2020 et Liste UE - 02/10/2018",
      "3h": "Panama - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
      "3i": "Syrie - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
      "3j": "Trinité et Tobago - Elevé - Liste UE - 14/02/2018",
      "3k": "Vanuatu - Elevé - GAFI - Liste UE - 20/09/2016",
      "3l": "Yémen - Elevé - GAFI - 23/10/2020 et Liste UE - 20/09/2016",
      "3m": "Zimbabwe - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
    };
    const caracteristiqueEntite = {
      1: "Autre - Faible",
      30: "Secteur associatif - Elevé - intervenant dans un secteur sensible par ex culturel, cultuel, ou à objet humanitaire, ou encore financées par des fonds publics et entretenant un lien avec des élus locaux et/ou des collectivités locales",
      2: "Les autres associations -  Moyen",
      31: "Trusts (fiducies) - Elevé",
      32: "Syndicats/ partis politiques - Elevé",
      33: "Entités à structure d’actionnariat inhabituelle ou complexe - Elevé - (ex. impliquant 3 niveaux ou plus de structures d’actionnariat dans différentes juridictions, en particulier dans les paradis fiscaux ou les pays à haut risque)",
      34: "Entité/représentant légal/bénéficiaire effectif ou actionnaire concerné faisant l'objet de nouvelles médiatiques défavorables - Elevé",
      35: "Le client ou le bénéficiaire effectif est une Personne Exposée - Elevé",
    };
    const propositionCAC = {
      1: "Autre - Faible",
      20: "Conseil en gestion d’actifs - Moyen - (allocation des actifs et des capitaux)",
      21: "Services de conseil en matière de fusions et acquisitions - Moyen",
      22: "Services de conseil en gestion de projets immobiliers - Moyen",
      23: "Services de conseil en matière de fusions et acquisitions côté vente - Moyen",
      24: "Conseil aux entreprises en difficulté - Moyen",
      25: "Restructuration opérationnelle d’entreprises en difficulté - Moyen",
    };
    const autres = {
      1: "Autre - Faible",
    };

    let elements = [
      field("770").get(),
      field("771").get(),
      field("772").get(),
      field("773").get(),
      field("774").get(),
    ];
    let reponse1 = elements[0].split(",");
    let reponse2 = elements[1].split(",");
    let reponse3 = elements[2].split(",");
    let reponse4 = elements[3].split(",");
    let reponse5 = elements[4].split(",");

    let risks1 = elements[0].split(",");
    let risks2 = elements[1].split(",");
    let risks3 = elements[2].split(",");
    let risks4 = elements[3].split(",");
    let risks5 = elements[4].split(",");

    for (let i = 0; i < reponse1.length; i++) {
      reponse1[i] = activitesEntite[reponse1[i]];
    }
    field("reponse1").set(reponse1.join("\n"));

    for (let i = 0; i < reponse2.length; i++) {
      reponse2[i] = localisationActivites[reponse2[i]];
    }
    field("reponse2").set(reponse2.join());

    for (let i = 0; i < reponse3.length; i++) {
      reponse3[i] = caracteristiqueEntite[reponse3[i]];
    }
    field("reponse3").set(reponse3.join());

    for (let i = 0; i < reponse4.length; i++) {
      reponse4[i] = propositionCAC[reponse4[i]];
    }
    field("reponse4").set(reponse4.join());

    // permet de remplir cellule 5 de la colonne reponse
    for (let i = 0; i < reponse5.length; i++) {
      reponse5[i] = propositionCAC[reponse5[i]];
    }
    field("reponse5").set(reponse5.join());

    for (let i = 0; i < risks1.length; i++) {
      const str = risks1[i];
      if (str.length !== 1) {
        risks1[i] = str.slice(0, -1);
      }
    }
    if (risks1.indexOf("3") !== -1) {
      field("880").set(risks1[risks1.indexOf("3")]);
    } else if (risks1.indexOf("2") !== -1) {
      field("880").set(risks1[risks1.indexOf("2")]);
    } else if (risks1.indexOf("1") !== -1) {
      field("880").set(risks1[risks1.indexOf("1")]);
    }

    for (let i = 0; i < risks2.length; i++) {
      const str = risks2[i];
      if (str.length !== 1) {
        risks2[i] = str.slice(0, -1);
      }
    }
    if (risks2.indexOf("3") !== -1) {
      field("881").set(risks2[risks2.indexOf("3")]);
    } else if (risks2.indexOf("2") !== -1) {
      field("881").set(risks2[risks2.indexOf("2")]);
    } else if (risks2.indexOf("1") !== -1) {
      field("881").set(risks2[risks2.indexOf("1")]);
    }
    for (let i = 0; i < risks3.length; i++) {
      const str = risks3[i];
      if (str.length !== 1) {
        risks3[i] = str.slice(0, -1);
      }
    }
    if (risks3.indexOf("3") !== -1) {
      field("882").set(risks3[risks3.indexOf("3")]);
    } else if (risks3.indexOf("2") !== -1) {
      field("882").set(risks3[risks3.indexOf("2")]);
    } else if (risks3.indexOf("1") !== -1) {
      field("882").set(risks3[risks3.indexOf("1")]);
    }

    for (let i = 0; i < risks4.length; i++) {
      const str = risks4[i];
      if (str.length !== 1) {
        risks4[i] = str.slice(0, -1);
      }
    }
    if (risks4.indexOf("3") !== -1) {
      field("883").set(risks4[risks4.indexOf("3")]);
    } else if (risks4.indexOf("2") !== -1) {
      field("883").set(risks4[risks4.indexOf("2")]);
    } else if (risks4.indexOf("1") !== -1) {
      field("883").set(risks4[risks4.indexOf("1")]);
    }

    field("884").set(risks5[risks5.indexOf("1")]);

    let riskLevel = [
      field("880").get(),
      field("881").get(),
      field("882").get(),
      field("883").get(),
      field("884").get(),
    ];

    if (riskLevel.indexOf("3") !== -1) {
      field("660").set(riskLevel[riskLevel.indexOf("3")]);
    } else if (riskLevel.indexOf("2") !== -1) {
      field("660").set(riskLevel[riskLevel.indexOf("2")]);
    } else if (riskLevel.indexOf("1") !== -1) {
      field("660").set(riskLevel[riskLevel.indexOf("1")]);
    }
    field("661").set(field("660").get());
  });
});
