(function () {
  "use strict";

  wpw.tax.create.formData("CRITERES", {
    formInfo: {
      abbreviation: "CRITERES",
      title:
        "Lutte contre le blanchiment des capitaux et de financement du terrorisme",
      highlightFieldsets: false,
      headerImage: "cw",
      category: "Start Here",
      css: "style-cdn.css",
      showDots: false,
      documentMap: true,
      sectionData: {},
    },

    sections: [
      {
        header: "Critères",
        headerClass: "font-header",
        rows: [
          {
            header:
              "1. Lutte contre le blanchiment des capitaux et de financement du terrorisme",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "table",
                num: "criteres",
              },
              {
                labelClass: "fullLength",
              },
              {
                type: "table",
                num: "revue",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
        ],
      },
    ],
  });
})();
