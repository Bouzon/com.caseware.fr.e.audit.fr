(function () {
  wpw.tax.create.tables("CRITERES", {
    criteres: {
      fixedRows: true,
      staticTable: true,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Eléments",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Choix de réponse",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Réponse",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Risque",
        },
        {
          type: "text",
          cellClass: "alignCenter",
          header: "Commentaire",
        },
      ],
      cells: [
        {
          0: {
            label: "Activités de l'entité",
          },
          1: {
            type: "selector",
            num: "770",
            inputClass: "std-input-width",
            selectorOptions: {
              items: {
                1: "Autre - Faible",
                20: "Voyages, hôtels, restaurants et hospitalité - Moyen",
                21: "Construction -  Moyen",
                22: "Immobilier - Moyen",
                23: "Bijouterie - Moyen",
                30: "Antiquités, arts, Commerce de métaux précieux ou de diamants - Elevé",
                31: "Jeux et paris - Elevé",
                32: "Stupéfiants - Elevé",
                33: "Pornographie - Elevé",
                24: "Clubs sportifs, Agents sportifs - Moyen",
              },
              displayValue: true,
              multiple: true,
            },
          },
          2: {
            type: "textArea",
            disabled: true,
            num: "reponse1",
          },
          3: {
            type: "dropdown",
            init: "0",
            num: "880",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
        },
        {
          0: {
            label: "Localisation des activités",
          },
          1: {
            type: "selector",
            num: "771",
            inputClass: "std-input-width",
            selectorOptions: {
              items: {
                1: "Autre - Faible",
                30: "Afghanistan - Elevé - Liste UE - 20/09/2016",
                31: "Albanie - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
                32: "Bahamas - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
                33: "Barbade - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
                34: "Botswana - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
                35: "Cambodge - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
                36: "Corée du nord - Elevé - GAFI - 23/10/2020 et Liste UE - 20/09/2016",
                37: "Ghana - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
                38: "Irak - Elevé - Liste UE - 01/10/2020",
                39: "Iran - Elevé - GAFI - 23/10/2020 et Liste UE - 20/09/2016",
                "3a": "Jamaïque - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
                "3b": "Maurice - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
                "3c": "Mongolie - Elevé - Liste UE - 01/10/2020",
                "3d": "Myanmar - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
                "3e": "Nicaragua - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
                "3f": "Ouganda - Elevé - GAFI - 23/10/2020 et Liste UE - 20/09/2016",
                "3g": "Pakistan - Elevé - GAFI - 23/10/2020 et Liste UE - 02/10/2018",
                "3h": "Panama - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
                "3i": "Syrie - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
                "3j": "Trinité et Tobago - Elevé - Liste UE - 14/02/2018",
                "3k": "Vanuatu - Elevé - GAFI - Liste UE - 20/09/2016",
                "3l": "Yémen - Elevé - GAFI - 23/10/2020 et Liste UE - 20/09/2016",
                "3m": "Zimbabwe - Elevé - GAFI - 23/10/2020 et Liste UE - 01/10/2020",
              },
              displayValue: true,
              multiple: true,
            },
          },
          2: {
            type: "textArea",
            disabled: true,
            num: "reponse2",
          },
          3: {
            type: "dropdown",
            init: "0",
            num: "881",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
        },
        {
          0: {
            label: "Caratéristique de l'entité",
          },
          1: {
            type: "selector",
            num: "772",
            inputClass: "std-input-width",
            selectorOptions: {
              items: {
                1: "Autre - Faible",
                30: "Secteur associatif - Elevé - intervenant dans un secteur sensible par ex culturel, cultuel, ou à objet humanitaire, ou encore financées par des fonds publics et entretenant un lien avec des élus locaux et/ou des collectivités locales",
                2: "Les autres associations -  Moyen",
                31: "Trusts (fiducies) - Elevé",
                32: "Syndicats/ partis politiques - Elevé",
                33: "Entités à structure d’actionnariat inhabituelle ou complexe - Elevé - (ex. impliquant 3 niveaux ou plus de structures d’actionnariat dans différentes juridictions, en particulier dans les paradis fiscaux ou les pays à haut risque)",
                34: "Entité/représentant légal/bénéficiaire effectif ou actionnaire concerné faisant l'objet de nouvelles médiatiques défavorables - Elevé",
                35: "Le client ou le bénéficiaire effectif est une Personne Exposée - Elevé",
              },
              multiple: true,
              displayValue: true,
            },
          },
          2: {
            type: "textArea",
            disabled: true,
            num: "reponse3",
          },
          3: {
            type: "dropdown",
            init: "0",
            num: "882",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
        },
        {
          0: {
            label: "Missions ou services proposés par le CAC",
          },
          1: {
            type: "selector",
            num: "773",
            inputClass: "std-input-width",
            selectorOptions: {
              items: {
                1: "Autre - Faible",
                20: "Conseil en gestion d’actifs - Moyen - (allocation des actifs et des capitaux)",
                21: "Services de conseil en matière de fusions et acquisitions - Moyen",
                22: "Services de conseil en gestion de projets immobiliers - Moyen",
                23: "Services de conseil en matière de fusions et acquisitions côté vente - Moyen",
                24: "Conseil aux entreprises en difficulté - Moyen",
                25: "Restructuration opérationnelle d’entreprises en difficulté - Moyen",
              },
              multiple: true,
              displayValue: true,
            },
          },
          2: {
            type: "textArea",
            disabled: true,
            num: "reponse4",
          },
          3: {
            type: "dropdown",
            init: "0",
            num: "883",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
        },
        {
          0: {
            label: "Autres",
          },
          1: {
            type: "selector",
            num: "774",
            inputClass: "std-input-width",
            selectorOptions: {
              items: {
                1: "Autre - Faible",
              },
              displayValue: true,
            },
          },
          2: {
            type: "textArea",
            disabled: true,
            num: "reponse5",
          },
          3: {
            type: "dropdown",
            init: "0",
            num: "884",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
        },
      ],
    },
    revue: {
      fixedRows: true,
      staticTable: true,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Niveau de risque",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Niveau de vigilance",
        },
      ],
      cells: [
        {
          0: {
            type: "dropdown",
            num: "660",
            init: "0",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
          1: {
            type: "dropdown",
            init: "0",
            num: "661",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Simplifiée",
                value: 1,
              },
              {
                option: "Normale",
                value: 2,
              },
              {
                option: "Renforcée",
                value: 3,
              },
            ],
          },
        },
      ],
    },
  });
})();
