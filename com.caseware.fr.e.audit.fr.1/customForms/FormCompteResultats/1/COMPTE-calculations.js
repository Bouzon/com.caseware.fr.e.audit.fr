wpw.tax.create.calcBlocks("COMPTE", function (calcUtils) {
  let tablesParameters = "";
  function numbersFR(data) {
    let newData = "";
    for (let i = 0; i < data.length; i++) {
      newData = data[i];
      const newNumbers = newData.replace(",", ".").replace(".", "");
      data[i] = newNumbers;
    }
    return data;
  }

  calcUtils.calc(function (calcUtils, field) {
    tablesParameters = field("500").get();
  });
  // Calculation pour header tableau date de fin de mission année N
  calcUtils.calc(function (calcUtils, field) {
    wpw.tax
      .evaluate([
        'engprop("relativeperiodend", 0, 0, "longDate")',
        'formatnumeric(year(engprop("yearend", 0)))',
        'formatnumeric(year(engprop("yearend", 0)) - 1)',
      ])
      .then(function (dates) {
        const parsedDate = dates[0].split(" ");
        const yearNM1 = parseFloat(parsedDate[2] - 1);
        field("yearN").set(dates[0]);
        field("yearN-1").set(`${parsedDate[0]} ${parsedDate[1]} ${yearNM1}`);
        field("dates").set(dates[1] + " " + dates[2]);
      });
  });
  // libellé des tableaux
  calcUtils.calc(function (calcUtils, field) {
    wpw.tax
      .evaluate([
        'tagprop(entityref("RP.1"), "name")',
        'tagprop(entityref("RC.1"), "name")',
        'tagprop(entityref("RP.2.01"), "name")',
        'tagprop(entityref("RC.2.01"), "name")',
        'tagprop(entityref("RP.3"), "name")',
        'tagprop(entityref("RC.3"), "name")',
        'tagprop(entityref("RP.4"), "name")',
        'tagprop(entityref("RC.5"), "name")',
        'tagprop(entityref("RC.6"), "name")',
      ])
      .then(function (labels) {
        const arrayLabels = [
          "labelcompteresultat1",
          "labelcompteresultat2",
          "labelcompteresultat4",
          "labelcompteresultat5",
          "labelcompteresultat6",
          "labelcompteresultat7",
          "labelcompteresultat10",
          "labelcompteresultat11",
          "labelcompteresultat12",
        ];
        for (let i = 0; i < arrayLabels.length; i++) {
          field(arrayLabels[i]).set(labels[i]);
        }
      });
  });
  //
  //Début  tableau compte de résultat
  //
  calcUtils.calc(function (calcUtils, field) {
    wpw.tax
      .evaluate([
        'tagbal(entityref("RP.1"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.1"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.1"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.1"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.2.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.2.01"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.2.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.2.01"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.3"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.3"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.3"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.3"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.4"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.4"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.5"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.5"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.6"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.6"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
      ])
      .then(function (dataCR) {
        dataCR = numbersFR(dataCR);
        // prettier-ignore
        const numsCRN = ['peNK','ceNK','bptNK','pbtNK','pfNK','cfNK','pexcepNK','psrNK','ibNK',];
        // prettier-ignore
        const numsCRNPourcent = ['peNK%','ceNK%','reN%','bptN%','pbtN%','pfN%','cfN%','rfN%','rcaiN%','pexcepN%','psrN%','ibN%','rnN%',];
        // prettier-ignore
        const numsCRNM1 = ['peNM1K','ceNM1K','bptNM1K','pbtNM1K','pfNM1K','cfNM1K','pexcepNM1K','psrNM1K','ibNM1K',];
        // prettier-ignore
        const numsCRNM1Pourcent = ['peNM1%','ceNM1%','reNM1%','bptNM1%','pbtNM1%','pfNM1%','cfNM1%','rfNM1%','rcaiNM1%','pexcepNM1%','psrNM1%','ibNM1%','rnNM1%',];
        // prettier-ignore
        const numsCRVAR = ['peVAR','ceVAR','bptVAR','pbtVAR','pfVAR','cfVAR','pexcepVAR','psrVAR','ibVAR',];
        // prettier-ignore
        const dataResults = {
            reNK:parseFloat(dataCR[0]) + parseFloat(dataCR[2]),
            reNM1K:parseFloat(dataCR[1]) + parseFloat(dataCR[3]),
            rfNK:parseFloat(dataCR[8]) + parseFloat(dataCR[10]),
            rfNM1K:parseFloat(dataCR[9]) + parseFloat(dataCR[11]),
            rcaiNK:parseFloat(dataCR[0]) + parseFloat(dataCR[2]) + parseFloat(dataCR[4]) + parseFloat(dataCR[6]) + parseFloat(dataCR[8]) + parseFloat(dataCR[10]) + parseFloat(dataCR[12]) + parseFloat(dataCR[14]) + parseFloat(dataCR[16]),
            rcaiNM1K:parseFloat(dataCR[1]) + parseFloat(dataCR[3]) + parseFloat(dataCR[5]) + parseFloat(dataCR[7]) + parseFloat(dataCR[9]) + parseFloat(dataCR[11]) + parseFloat(dataCR[13]) + parseFloat(dataCR[15]) + parseFloat(dataCR[17]),
            rnNK:parseFloat(dataCR[0]) + parseFloat(dataCR[2]) + parseFloat(dataCR[4]) + parseFloat(dataCR[6]) + parseFloat(dataCR[8]) + parseFloat(dataCR[10]),
            rnNM1K:parseFloat(dataCR[1]) + parseFloat(dataCR[3]) + parseFloat(dataCR[5]) + parseFloat(dataCR[7]) + parseFloat(dataCR[9]) + parseFloat(dataCR[11]),
            reVAR:parseFloat(dataCR[0]) + parseFloat(dataCR[2]) - (parseFloat(dataCR[1]) + parseFloat(dataCR[3])),
            rfVAR:parseFloat(dataCR[8]) + parseFloat(dataCR[10]) - (parseFloat(dataCR[9]) + parseFloat(dataCR[11])),
            rcaiVAR:parseFloat(dataCR[0]) + parseFloat(dataCR[2]) + parseFloat(dataCR[4]) + parseFloat(dataCR[6]) + parseFloat(dataCR[8]) + parseFloat(dataCR[10]) + parseFloat(dataCR[12]) + parseFloat(dataCR[14]) + parseFloat(dataCR[16]) - parseFloat(dataCR[1]) + parseFloat(dataCR[3]) + parseFloat(dataCR[5]) + parseFloat(dataCR[7]) + parseFloat(dataCR[9]) + parseFloat(dataCR[11]) + parseFloat(dataCR[13]) + parseFloat(dataCR[15]) + parseFloat(dataCR[17]),
            rnVAR:parseFloat(dataCR[0]) + parseFloat(dataCR[2]) + parseFloat(dataCR[4]) + parseFloat(dataCR[6]) + parseFloat(dataCR[8]) + parseFloat(dataCR[10]) - parseFloat(dataCR[1]) + parseFloat(dataCR[3]) + parseFloat(dataCR[5]) + parseFloat(dataCR[7]) + parseFloat(dataCR[9]) + parseFloat(dataCR[11]),
          }
        // prettier-ignore
        const numsResults = ['reNK','reNM1K','rfNK','rfNM1K','rcaiNK','rcaiNM1K','rnNK','rnNM1K','reVAR','rfVAR','rcaiVAR','rnVAR']

        let countN = 0;
        for (let i = 0; i < numsCRN.length; i++) {
          if (dataCR[countN] === undefined) {
            field(numsCRN[i]).set("N/A");
          } else if (
            tablesParameters["2"] === true &&
            parseFloat(dataCR[countN]) == 0
          ) {
            field(numsCRN[i]).set("hide");
          } else if (tablesParameters["1"] === false || undefined) {
            field(numsCRN[i]).set(
              new Intl.NumberFormat("fr-FR", {
                style: "currency",
                currency: "EUR",
              }).format(dataCR[countN])
            );
          } else {
            field(numsCRN[i]).set(
              new Intl.NumberFormat("fr-FR", {
                style: "currency",
                currency: "EUR",
              }).format(dataCR[countN] / 1000)
            );
          }
          countN += 2;
        }

        let countM1 = 1;
        for (let i = 0; i < numsCRNM1.length; i++) {
          if (dataCR[countM1] === undefined) {
            field(numsCRNM1[i]).set("N/A");
          } else if (
            tablesParameters["2"] === true &&
            parseFloat(dataCR[countM1]) == 0
          ) {
            field(numsCRNM1[i]).set("hide");
          } else if (tablesParameters["1"] === false || undefined) {
            field(numsCRNM1[i]).set(
              new Intl.NumberFormat("fr-FR", {
                style: "currency",
                currency: "EUR",
              }).format(dataCR[countM1])
            );
          } else {
            field(numsCRNM1[i]).set(
              new Intl.NumberFormat("fr-FR", {
                style: "currency",
                currency: "EUR",
              }).format(dataCR[countM1] / 1000)
            );
          }
          countM1 += 2;
        }

        for (let i = 0; i < numsResults.length; i++) {
          if (dataResults[numsResults[i]] === undefined) {
            field(numsResults[i]).set("N/A");
          } else if (
            tablesParameters["2"] === true &&
            parseFloat(dataResults[numsResults[i]]) == 0
          ) {
            field(numsResults[i]).set("hide");
          } else if (tablesParameters["1"] === false || undefined) {
            field(numsResults[i]).set(
              new Intl.NumberFormat("fr-FR", {
                style: "currency",
                currency: "EUR",
              }).format(dataResults[numsResults[i]])
            );
          } else {
            field(numsResults[i]).set(
              new Intl.NumberFormat("fr-FR", {
                style: "currency",
                currency: "EUR",
              }).format(dataResults[numsResults[i]] / 1000)
            );
          }
        }

        let countVAR = 0;
        for (let i = 0; i < numsCRVAR.length; i++) {
          // prettier-ignore
          const decimalFR = parseFloat(dataCR[countVAR]) - parseFloat(dataCR[countVAR + 1]).toFixed(2)
          field(numsCRVAR[i]).set(
            new Intl.NumberFormat("fr-FR", {
              style: "currency",
              currency: "EUR",
            }).format(dataCR[countVAR] - dataCR[countVAR + 1])
          );
          countVAR += 2;
        }
      });
  });

  //
  //Fin tableau compte de resultat
  //
});
