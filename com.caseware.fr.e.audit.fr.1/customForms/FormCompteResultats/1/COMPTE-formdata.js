(function () {
  "use strict";

  wpw.tax.create.formData("COMPTE", {
    formInfo: {
      abbreviation: "Comptes de résultat",
      title: "Comptes de résultat",
      highlightFieldsets: false,
      headerImage: "cw",
      category: "Start Here",
      css: "style-cdn.css",
      showDots: false,
      documentMap: true,
      sectionData: {},
    },

    sections: [
      {
        header: "Comptes de résultat",
        headerClass: "font-header",
        rows: [
          {
            header: "1. Comptes de résultat",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label: "Paramètres du tableau",
                type: "splitInputs",
                inputType: "checkbox",
                divisions: "3",
                num: "500",
                showValues: "false",
                items: [
                  { label: "K €", value: "1" },
                  { label: "Masquer les lignes vides", value: "2" },
                ],
              },
              {
                type: "table",
                num: "compteresultatHeader",
              },
              {
                labelClass: "fullLength",
              },
              {
                type: "table",
                num: "compteresultat",
              },
              {
                labelClass: "fullLength",
              },
              {
                label: "Commentaire sur les produits : ",
              },
              {
                type: "textArea",
                num: "commentaireproduits33",
              },
              {
                label: "Commentaire sur les charges :",
              },
              {
                type: "textArea",
                num: "commentairecharges33",
              },
            ],
          },
        ],
      },
    ],
  });
})();
