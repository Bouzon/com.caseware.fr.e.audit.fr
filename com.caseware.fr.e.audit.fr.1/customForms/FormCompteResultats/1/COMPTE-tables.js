(function () {
  wpw.tax.create.tables("COMPTE", {
    compteresultatHeader: {
      fixedRows: true,
      staticTable: true,
      columns: [
        {
          width: "200px",
        },
        {
          width: "200px",
        },
        {
          width: "200px",
        },
        {
          width: "200px",
        },
        {
          width: "200px",
        },
      ],
      cells: [
        {
          0: { label: " ", labelClass: "center", type: "none" },
          1: {
            label: "Year N",
            labelClass: "center",
            type: "none",
            labelSource: {
              fieldId: "yearN",
            },
          },
          2: {
            label: "Year N -1",
            labelClass: "center",
            type: "none",
            labelSource: {
              fieldId: "yearN-1",
            },
          },
          3: {
            label: "dates",
            labelClass: "center",
            type: "none",
            labelSource: {
              fieldId: "dates",
            },
          },
          4: {
            label: "  ",
            labelClass: "center",
            type: "none",
          },
        },
      ],
    },
    compteresultat: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          width: "200px",
          header: "Compte de Résultat",
        },
        { type: "none", cellClass: "alignCenter", width: "100px", header: "K" },
        { type: "none", cellClass: "alignCenter", width: "100px", header: "%" },
        { type: "none", cellClass: "alignCenter", width: "100px", header: "K" },
        { type: "none", cellClass: "alignCenter", width: "100px", header: "%" },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "100px",
          header: "Variations",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "100px",
          header: "%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "200px",
          header: "Commentaire",
        },
      ],
      cells: [
        {
          0: {
            label: "label",
            labelSource: { fieldId: "labelcompteresultat1" },
          },
          1: { type: "text", num: "peNK" },
          2: { type: "text", num: "peNK%" },
          3: { type: "text", num: "peNM1K" },
          4: { type: "text", num: "peNM1%" },
          5: { type: "text", num: "peVAR" },
          6: { type: "text", num: "peVAR%" },
          7: { type: "textArea", num: "peCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "labelcompteresultat2" },
          },
          1: { type: "text", num: "ceNK" },
          2: { type: "text", num: "ceNK%" },
          3: { type: "text", num: "ceNM1K" },
          4: { type: "text", num: "ceNM1%" },
          5: { type: "text", num: "ceVAR" },
          6: { type: "text", num: "ceVAR%" },
          7: { type: "textArea", num: "ceCOM" },
        },
        {
          0: {
            label: "Resultat d’exploitation",
            labelSource: { fieldId: "labelcompteresultat3" },
            labelClass: "bold",
          },
          1: { type: "text", num: "reNK", cellClass: "bold" },
          2: { type: "text", num: "reN%", cellClass: "bold" },
          3: { type: "text", num: "reNM1K", cellClass: "bold" },
          4: { type: "text", num: "reNM1%", cellClass: "bold" },
          5: { type: "text", num: "reVAR", cellClass: "bold" },
          6: { type: "text", num: "reVAR%", cellClass: "bold" },
          7: { type: "textArea", num: "reCOM", cellClass: "bold" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "labelcompteresultat4" },
          },
          1: { type: "text", num: "bptNK" },
          2: { type: "text", num: "bptN%" },
          3: { type: "text", num: "bptNM1K" },
          4: { type: "text", num: "bptNM1%" },
          5: { type: "text", num: "bptVAR" },
          6: { type: "text", num: "bptVAR%" },
          7: { type: "textArea", num: "bptCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "labelcompteresultat5" },
          },
          1: { type: "text", num: "pbtNK" },
          2: { type: "text", num: "pbtN%" },
          3: { type: "text", num: "pbtNM1K" },
          4: { type: "text", num: "pbtNM1%" },
          5: { type: "text", num: "pbtVAR" },
          6: { type: "text", num: "pbtVAR%" },
          7: { type: "textArea", num: "pbtCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "labelcompteresultat6" },
          },
          1: { type: "text", num: "pfNK" },
          2: { type: "text", num: "pfN%" },
          3: { type: "text", num: "pfNM1K" },
          4: { type: "text", num: "pfNM1%" },
          5: { type: "text", num: "pfVAR" },
          6: { type: "text", num: "pfVAR%" },
          7: { type: "textArea", num: "pfCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "labelcompteresultat7" },
          },
          1: { type: "text", num: "cfNK" },
          2: { type: "text", num: "cfN%" },
          3: { type: "text", num: "cfNM1K" },
          4: { type: "text", num: "cfNM1%" },
          5: { type: "text", num: "cfVAR" },
          6: { type: "text", num: "cfVAR%" },
          7: { type: "textArea", num: "cfCOM" },
        },
        {
          0: {
            label: "Résultat financier",
            labelSource: { fieldId: "labelcompteresultat8" },
            labelClass: "bold",
          },
          1: { type: "text", num: "rfNK", cellClass: "bold" },
          2: { type: "text", num: "rfN%", cellClass: "bold" },
          3: { type: "text", num: "rfNM1K", cellClass: "bold" },
          4: { type: "text", num: "rfNM1%", cellClass: "bold" },
          5: { type: "text", num: "rfVAR", cellClass: "bold" },
          6: { type: "text", num: "rfVAR%", cellClass: "bold" },
          7: { type: "textArea", num: "rfCOM", cellClass: "bold" },
        },
        {
          0: {
            label: "Resultat courant avant impôt ",
            labelSource: { fieldId: "labelcompteresultat9" },
            labelClass: "bold",
          },
          1: { type: "text", num: "rcaiNK", cellClass: "bold" },
          2: { type: "text", num: "rcaiN%", cellClass: "bold" },
          3: { type: "text", num: "rcaiNM1K", cellClass: "bold" },
          4: { type: "text", num: "rcaiNM1%", cellClass: "bold" },
          5: { type: "text", num: "rcaiVAR", cellClass: "bold" },
          6: { type: "text", num: "rcaiVAR%", cellClass: "bold" },
          7: { type: "textArea", num: "rcaiCOM", cellClass: "bold" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "labelcompteresultat10" },
          },
          1: { type: "text", num: "pexcepNK" },
          2: { type: "text", num: "pexcepN%" },
          3: { type: "text", num: "pexcepNM1K" },
          4: { type: "text", num: "pexcepNM1%" },
          5: { type: "text", num: "pexcepVAR" },
          6: { type: "text", num: "pexcepVAR%" },
          7: { type: "textArea", num: "pexcepCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "labelcompteresultat11" },
          },
          1: { type: "text", num: "psrNK" },
          2: { type: "text", num: "psrN%" },
          3: { type: "text", num: "psrNM1K" },
          4: { type: "text", num: "psrNM1%" },
          5: { type: "text", num: "psrVAR" },
          6: { type: "text", num: "psrVAR%" },
          7: { type: "textArea", num: "psrCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "labelcompteresultat12" },
          },
          1: { type: "text", num: "ibNK" },
          2: { type: "text", num: "ibN%" },
          3: { type: "text", num: "ibNM1K" },
          4: { type: "text", num: "ibNM1%" },
          5: { type: "text", num: "ibVAR" },
          6: { type: "text", num: "ibVAR%" },
          7: { type: "textArea", num: "ibCOM" },
        },
        {
          0: {
            label: "Résultat Net",
            labelSource: { fieldId: "labelcompteresultat13" },
            labelClass: "bold",
          },
          1: { type: "text", num: "rnNK", cellClass: "bold" },
          2: { type: "text", num: "rnN%", cellClass: "bold" },
          3: { type: "text", num: "rnNM1K", cellClass: "bold" },
          4: { type: "text", num: "rnNM1%", cellClass: "bold" },
          5: { type: "text", num: "rnVAR", cellClass: "bold" },
          6: { type: "text", num: "rnVAR%", cellClass: "bold" },
          7: { type: "textArea", num: "rnCOM", cellClass: "bold" },
        },
      ],
    },
  });
})();
