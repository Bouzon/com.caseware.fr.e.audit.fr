(function () {
  "use strict";

  wpw.tax.create.formData("BILAN", {
    formInfo: {
      abbreviation: "BILAN",
      dynamicFormWidth: true,
      title: "Bilan Actif et passif",
      highlightFieldsets: false,
      hideheader: true,
      headerImage: "cw",
      css: "se-builder-custom.css",
      category: "Start Here",
      showDots: false,
      documentMap: true,
      sectionData: {},
    },

    sections: [
      {
        header: "Bilans",
        headerClass: "font-header",
        rows: [
          {
            header: "1. Bilan Actif et passif",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label: "Paramètres du tableau",
                type: "splitInputs",
                inputType: "checkbox",
                divisions: "3",
                num: "500",
                showValues: "false",
                items: [
                  { label: "K €", value: "1" },
                  { label: "Masquer les lignes vides", value: "2" },
                ],
              },
              {
                label: "Actif K€",
                labelClass: "bold",
              },
              {
                type: "table",
                num: "actifHeader",
              },
              {
                labelClass: "fullLength",
              },
              {
                type: "table",
                num: "actif",
              },
              {
                labelClass: "fullLength",
              },
              {
                type: "text",
                inputType: "none",
                label: "Commentaire sur l'actif :",
              },
              {
                type: "textArea",
                num: "commentaireactif32",
              },
              {
                label: "Passif K€",
                labelClass: "bold",
              },
              {
                type: "table",
                num: "passifHeader",
              },
              {
                labelClass: "fullLength",
              },
              {
                type: "table",
                num: "passif",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
        ],
      },
    ],
  });
})();
