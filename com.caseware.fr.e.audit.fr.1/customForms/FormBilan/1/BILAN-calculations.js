wpw.tax.create.calcBlocks("BILAN", function (calcUtils) {
  let tablesParameters = "";
  function numbersFR(data) {
    let newData = "";
    for (let i = 0; i < data.length; i++) {
      newData = data[i];
      const newNumbers = newData.replace(",", ".").replace(".", "");
      data[i] = newNumbers;
    }
    return data;
  }

  calcUtils.calc(function (calcUtils, field) {
    tablesParameters = field("500").get();
    tablesParameters[1] ? field("NK€").set("K €") : field("NK€").set("€");
    tablesParameters[1] ? field("NM1K€").set("K €") : field("NM1K€").set("€");
  });

  calcUtils.calc(function (calcUtils, field) {
    return wpw.tax
      .evaluate([
        'engprop("relativeperiodend", 0, 0, "longDate")',
        'formatnumeric(year(engprop("yearend", 0)))',
        'formatnumeric(year(engprop("yearend", 0)) - 1)',
      ])
      .then(function (dates) {
        const parsedDate = dates[0].split(" ");
        const yearNM1 = parseFloat(parsedDate[2] - 1);
        field("yearN").set(dates[0]);
        field("yearN-1").set(`${parsedDate[0]} ${parsedDate[1]} ${yearNM1}`);
        field("dates").set(dates[1] + " " + dates[2]);
      });
  });

  calcUtils.calc(function (calcUtils, field) {
    return wpw.tax
      .evaluate([
        'tagprop(entityref("BA.1"), "name")',
        'tagprop(entityref("BA.2.01"), "name")',
        'tagprop(entityref("BA.2.02"), "name")',
        'tagprop(entityref("BA.2.03"), "name")',
        'tagprop(entityref("BA.2"), "name")',
        'tagprop(entityref("BA.3.01"), "name")',
        'tagprop(entityref("BA.3.03"), "name")',
        'tagprop(entityref("BA.3.05"), "name")',
        'tagprop(entityref("BA.3.06"), "name")',
        'tagprop(entityref("BA.3.07"), "name")',
        'tagprop(entityref("BA.3.08"), "name")',
        'tagprop(entityref("BA.3.02"), "name")',
        'tagprop(entityref("BA.3"), "name")',
        'tagprop(entityref("BA.3.09"), "name")',
        'tagprop(entityref("BA.3.10"), "name")',
        'tagprop(entityref("BA.3.11"), "name")',
        'tagprop(entityref("BA"), "name")',
        'tagprop(entityref("BP.1.01"), "name")',
        'tagprop(entityref("BP.1.02"), "name")',
        'tagprop(entityref("BP.1.03"), "name")',
        'tagprop(entityref("BP.1.04"), "name")',
        'tagprop(entityref("BP.1.05"), "name")',
        'tagprop(entityref("BP.1.06"), "name")',
        'tagprop(entityref("BP.1.07"), "name")',
        'tagprop(entityref("BP.1.08"), "name")',
        'tagprop(entityref("BP.1.09"), "name")',
        'tagprop(entityref("BP.1"), "name")',
        'tagprop(entityref("BP.2"), "name")',
        'tagprop(entityref("BP.3"), "name")',
        'tagprop(entityref("BP.4.01"), "name")',
        'tagprop(entityref("BP.4.02"), "name")',
        'tagprop(entityref("BP.4.03"), "name")',
        'tagprop(entityref("BP.4.04"), "name")',
        'tagprop(entityref("BP.4.05"), "name")',
        'tagprop(entityref("BP.4.06"), "name")',
        'tagprop(entityref("BP.4.07"), "name")',
        'tagprop(entityref("BP.4.08"), "name")',
        'tagprop(entityref("BP.4.09"), "name")',
        'tagprop(entityref("BP.4.10"), "name")',
        'tagprop(entityref("BP.4.11"), "name")',
        'tagprop(entityref("BP.4"), "name")',
        'tagprop(entityref("BP"), "name")',
      ])
      .then(function (labels) {
        const arrayLabels = [
          "label1",
          "label2",
          "label3",
          "label4",
          "label5",
          "label6",
          "label7",
          "label8",
          "label9",
          "label10",
          "label11",
          "label12",
          "label13",
          "label14",
          "label15",
          "label16",
          "label17",
          "labelPassif1",
          "labelPassif2",
          "labelPassif3",
          "labelPassif4",
          "labelPassif5",
          "labelPassif6",
          "labelPassif7",
          "labelPassif8",
          "labelPassif9",
          "labelPassif10",
          "labelPassif11",
          "labelPassif12",
          "labelPassif13",
          "labelPassif14",
          "labelPassif15",
          "labelPassif16",
          "labelPassif17",
          "labelPassif18",
          "labelPassif19",
          "labelPassif20",
          "labelPassif21",
          "labelPassif22",
          "labelPassif23",
          "labelPassif24",
          "labelPassif25",
        ];
        for (let i = 0; i < arrayLabels.length; i++) {
          field(arrayLabels[i]).set(labels[i]);
        }
      });
  });

  calcUtils.calc(function (calcUtils, field) {
    return wpw.tax
      .evaluate([
        'tagbal(entityref("BA.1"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.1"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.2.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.2.01"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.2.02"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.2.02"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.2.03"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.2.03"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.2"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.2"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3.01"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3.03"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3.03"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3.05"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3.05"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3.06"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3.06"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3.07"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3.07"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3.08"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3.08"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3.02"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3.02"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3.09"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3.09"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3.10"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3.10"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3.11"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.3.11"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
      ])
      .then(function (dataBA) {
        dataBA = numbersFR(dataBA);

        const numsBAN = [
          "csnaNK",
          "iiNK",
          "icNK",
          "ifNK",
          "taiNK",
          "seecNK",
          "cNK",
          "vmdpNK",
          "instruNK",
          "dNK",
          "ccaNK",
          "aeavscNK",
          "tacecNK",
          "crspeNK",
          "preNK",
          "ecaNK",
          "tbaNK",
        ];

        const numsBANM1 = [
          "csnaNM1K",
          "iiNM1K",
          "icNM1K",
          "ifNM1K",
          "taiNM1K",
          "seecNM1K",
          "cNM1K",
          "vmdpNM1K",
          "instruNM1K",
          "dNM1K",
          "ccaNM1K",
          "aeavscNM1K",
          "tacecNM1K",
          "crspeNM1K",
          "preNM1K",
          "ecaNM1K",
          "tbaNM1K",
        ];

        const numsNPourcent = [
          "csnaN%",
          "iiN%",
          "icN%",
          "ifN%",
          "taiN%",
          "seecN%",
          "cN%",
          "vmdpN%",
          "instruN%",
          "dN%",
          "ccaN%",
          "aeavscN%",
          "tacecN%",
          "crspeN%",
          "preN%",
          "ecaN%",
          "tbaN%",
        ];

        const numsNM1Pourcent = [
          "csnaNM1%",
          "iiNM1%",
          "icNM1%",
          "ifNM1%",
          "taiNM1%",
          "seecNM1%",
          "cNM1%",
          "vmdpNM1%",
          "instruNM1%",
          "dNM1%",
          "ccaNM1%",
          "aeavscNM1%",
          "tacecNM1%",
          "crspeNM1%",
          "preNM1%",
          "ecaNM1%",
          "tbaNM1%",
        ];

        const numsVarPourCent = [
          "csnaVAR%",
          "iiVAR%",
          "icVAR%",
          "ifVAR%",
          "taiVAR%",
          "seecVAR%",
          "cVAR%",
          "vmdpVAR%",
          "instruVAR%",
          "dVAR%",
          "ccaVAR%",
          "aeavscVAR%",
          "tacecVAR%",
          "crspeVAR%",
          "preVAR%",
          "ecaVAR%",
          "tbaVAR%",
        ];

        const numsVar = [
          "csnaVAR",
          "iiVAR",
          "icVAR",
          "ifVAR",
          "taiVAR",
          "seecVAR",
          "cVAR",
          "vmdpVAR",
          "instruVAR",
          "dVAR",
          "ccaVAR",
          "aeavscVAR",
          "tacecVAR",
          "crspeVAR",
          "preVAR",
          "ecaVAR",
          "tbaVAR",
        ];

        let countN = 0;
        for (let i = 0; i < numsBAN.length; i++) {
          if (dataBA[countN] === undefined) {
            field(numsBAN[i]).set("N/A");
          } else if (
            tablesParameters["2"] === true &&
            parseFloat(dataBA[countN]) == 0
          ) {
            field(numsBAN[i]).set("hide");
          } else if (tablesParameters["1"] === false || undefined) {
            field(numsBAN[i]).set(
              new Intl.NumberFormat("fr-FR", {
                maximumFractionDigits: 0,
              }).format(dataBA[countN])
            );
          } else {
            field(numsBAN[i]).set(
              new Intl.NumberFormat("fr-FR", {
                maximumFractionDigits: 0,
              }).format(dataBA[countN] / 1000)
            );
          }
          countN += 2;
        }

        let countM1 = 1;
        for (let i = 0; i < numsBANM1.length; i++) {
          if (dataBA[countM1] === undefined) {
            field(numsBANM1[i]).set("N/A");
          } else if (
            tablesParameters["2"] === true &&
            parseFloat(dataBA[countM1]) == 0
          ) {
            field(numsBANM1[i]).set("hide");
          } else if (tablesParameters["1"] === false || undefined) {
            field(numsBANM1[i]).set(
              new Intl.NumberFormat("fr-FR", {
                maximumFractionDigits: 0,
              }).format(dataBA[countM1])
            );
          } else {
            field(numsBANM1[i]).set(
              new Intl.NumberFormat("fr-FR", {
                maximumFractionDigits: 0,
              }).format(dataBA[countM1] / 1000)
            );
          }
          countM1 += 2;
        }

        let countNPourcent = 0;
        for (let i = 0; i < numsNPourcent.length; i++) {
          const nPourcent =
            (parseFloat(dataBA[countNPourcent]) * 100) / parseFloat(dataBA[32]);
          Number.isNaN(nPourcent)
            ? field(numsNPourcent[i]).set("0")
            : field(numsNPourcent[i]).set(nPourcent.toFixed(0));
          countNPourcent += 2;
        }

        let countNM1Pourcent = 1;
        for (let i = 0; i < numsNM1Pourcent.length; i++) {
          const nm1Pourcent =
            (parseFloat(dataBA[countNM1Pourcent]) * 100) /
            parseFloat(dataBA[33]);
          Number.isNaN(nm1Pourcent)
            ? field(numsNM1Pourcent[i]).set("0")
            : field(numsNM1Pourcent[i]).set(nm1Pourcent.toFixed(0));
          countNM1Pourcent += 2;
        }

        let countVAR = 0;
        for (let i = 0; i < numsVar.length; i++) {
          field(numsVar[i]).set(
            new Intl.NumberFormat("fr-FR", { maximumFractionDigits: 0 }).format(
              dataBA[countVAR] - dataBA[countVAR + 1]
            )
          );
          countVAR += 2;
        }

        let countVARPourcent = 0;
        for (let i = 0; i < numsVarPourCent.length; i++) {
          const variationPourcent =
            ((parseFloat(dataBA[countVARPourcent]) -
              parseFloat(dataBA[countVARPourcent + 1])) /
              parseFloat(dataBA[countVARPourcent + 1])) *
            100;

          Number.isNaN(variationPourcent)
            ? field(numsVarPourCent[i]).set("0")
            : field(numsVarPourCent[i]).set(variationPourcent.toFixed(0));
          countVARPourcent += 2;
        }
      });
  });

  calcUtils.calc(function (calcUtils, field) {
    return wpw.tax
      .evaluate([
        'tagbal(entityref("BP.1.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1.01"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1.02"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1.02"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1.03"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1.03"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1.04"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1.04"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1.05"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1.05"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1.06"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1.06"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1.07"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1.07"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1.08"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1.08"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1.09"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1.09"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.2"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.2"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.3"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.3"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.01"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.02"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.02"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.03"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.03"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.04"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.04"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.05"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.05"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.06"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.06"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.07"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.07"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.08"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.08"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.09"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.09"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.10"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.10"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.11"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4.11"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.4"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
      ])
      .then(function (dataBP) {
        dataBP = numbersFR(dataBP);

        const numsBPN = [
          "capiPNK",
          "primesPNK",
          "ecartsreevalPNK",
          "ecartequivalencePNK",
          "reservesPNK",
          "reportnouveauPNK",
          "resultatexercicePNK",
          "subventionPNK",
          "provisionPNK",
          "capitauxPNK",
          "autresfondsPNK",
          "provisionsPNK",
          "eocPNK",
          "aeoPNK",
          "edecPNK",
          "edfdPNK",
          "dfcrPNK",
          "dfsPNK",
          "dicrPNK",
          "autresdettesPNK",
          "itPNK",
          "pcaPNK",
          "ecpPNK",
          "dettesPNK",
          "passifPNK",
        ];

        const numsBPNPourcent = [
          "capiPN%",
          "primesPN%",
          "ecartsreevalPN%",
          "ecartequivalencePN%",
          "reservesPN%",
          "reportnouveauPN%",
          "resultatexercicePN%",
          "subventionPN%",
          "provisionPN%",
          "capitauxPN%",
          "autresfondsPN%",
          "provisionsPN%",
          "eocPN%",
          "aeoPN%",
          "edecPN%",
          "edfdPN%",
          "dfcrPN%",
          "dfsPN%",
          "dicrPN%",
          "autresdettesPN%",
          "itPN%",
          "pcaPN%",
          "ecpPN%",
          "dettesPN%",
          "passifPN%",
        ];

        const numsBPNM1K = [
          "capiPNM1K",
          "primesPNM1K",
          "ecartsreevalPNM1K",
          "ecartequivalencePNM1K",
          "reservesPNM1K",
          "reportnouveauPNM1K",
          "resultatexercicePNM1K",
          "subventionPNM1K",
          "provisionPNM1K",
          "capitauxPNM1K",
          "autresfondsPNM1K",
          "provisionsPNM1K",
          "eocPNM1K",
          "aeoPNM1K",
          "edecPNM1K",
          "edfdPNM1K",
          "dfcrPNM1K",
          "dfsPNM1K",
          "dicrPNM1K",
          "autresdettesPNM1K",
          "itPNM1K",
          "pcaPNM1K",
          "ecpPNM1K",
          "dettesPNM1K",
          "passifPNM1K",
        ];

        const numsBPNM1Pourcent = [
          "capiPNM1%",
          "primesPNM1%",
          "ecartsreevalPNM1%",
          "ecartequivalencePNM1%",
          "reservesPNM1%",
          "reportnouveauPNM1%",
          "resultatexercicePNM1%",
          "subventionPNM1%",
          "provisionPNM1%",
          "capitauxPNM1%",
          "autresfondsPNM1%",
          "provisionsPNM1%",
          "eocPNM1%",
          "aeoPNM1%",
          "edecPNM1%",
          "edfdPNM1%",
          "dfcrPNM1%",
          "dfsPNM1%",
          "dicrPNM1%",
          "autresdettesPNM1%",
          "itPNM1%",
          "pcaPNM1%",
          "ecpPNM1%",
          "dettesPNM1%",
          "passifPNM1%",
        ];

        const numsBPVAR = [
          "capiVAR",
          "primesVAR",
          "ecartsreevalVAR",
          "ecartequivalenceVAR",
          "reservesVAR",
          "reportnouveauVAR",
          "resultatexerciceVAR",
          "subventionVAR",
          "provisionVAR",
          "capitauxVAR",
          "autresfondsVAR",
          "provisionsVAR",
          "eocVAR",
          "aeoVAR",
          "edecVAR",
          "edfdVAR",
          "dfcrVAR",
          "dfsVAR",
          "dicrVAR",
          "autresdettesVAR",
          "itVAR",
          "pcaVAR",
          "ecpVAR",
          "dettesVAR",
          "passifVAR",
        ];

        const numsBPVARPourcent = [
          "capiVAR%",
          "primesVAR%",
          "ecartsreevalVAR%",
          "ecartequivalenceVAR%",
          "reservesVAR%",
          "reportnouveauVAR%",
          "resultatexerciceVAR%",
          "subventionVAR%",
          "provisionVAR%",
          "capitauxVAR%",
          "autresfondsVAR%",
          "provisionsVAR%",
          "eocVAR%",
          "aeoVAR%",
          "edecVAR%",
          "edfdVAR%",
          "dfcrVAR%",
          "dfsVAR%",
          "dicrVAR%",
          "autresdettesVAR%",
          "itVAR%",
          "pcaVAR%",
          "ecpVAR%",
          "dettesVAR%",
          "passifVAR%",
        ];

        let countN = 0;
        for (let i = 0; i < numsBPN.length; i++) {
          if (dataBP[countN] === undefined) {
            field(numsBPN[i]).set("N/A");
          } else if (
            tablesParameters["2"] === true &&
            parseFloat(dataBP[countN]) == 0
          ) {
            field(numsBPN[i]).set("hide");
          } else if (tablesParameters["1"] === false || undefined) {
            field(numsBPN[i]).set(
              new Intl.NumberFormat("fr-FR", {
                maximumFractionDigits: 0,
              }).format(dataBP[countN])
            );
          } else {
            field(numsBPN[i]).set(
              new Intl.NumberFormat("fr-FR", {
                maximumFractionDigits: 0,
              }).format(dataBP[countN] / 1000)
            );
          }
          countN += 2;
        }

        let countM1 = 1;
        for (let i = 0; i < numsBPNM1K.length; i++) {
          if (dataBP[countM1] === undefined) {
            field(numsBPNM1K[i]).set("N/A");
          } else if (
            tablesParameters["2"] === true &&
            parseFloat(dataBP[countM1]) == 0
          ) {
            field(numsBPNM1K[i]).set("hide");
          } else if (tablesParameters["1"] === false || undefined) {
            field(numsBPNM1K[i]).set(
              new Intl.NumberFormat("fr-FR", {
                maximumFractionDigits: 0,
              }).format(dataBP[countM1])
            );
          } else {
            field(numsBPNM1K[i]).set(
              new Intl.NumberFormat("fr-FR", {
                maximumFractionDigits: 0,
              }).format(dataBP[countM1] / 1000)
            );
          }
          countM1 += 2;
        }

        let countNPourcent = 0;
        for (let i = 0; i < numsBPNPourcent.length; i++) {
          const nPourcent =
            (parseFloat(dataBP[countNPourcent]) * 100) / parseFloat(dataBP[48]);
          Number.isNaN(nPourcent)
            ? field(numsBPNPourcent[i]).set("0")
            : field(numsBPNPourcent[i]).set(nPourcent.toFixed(0));
          countNPourcent += 2;
        }

        let countNM1Pourcent = 1;
        for (let i = 0; i < numsBPNM1Pourcent.length; i++) {
          const nm1Pourcent =
            (parseFloat(dataBP[countNM1Pourcent]) * 100) /
            parseFloat(dataBP[49]);
          Number.isNaN(nm1Pourcent)
            ? field(numsBPNM1Pourcent[i]).set("0")
            : field(numsBPNM1Pourcent[i]).set(nm1Pourcent.toFixed(0));
          countNM1Pourcent += 2;
        }

        let countVAR = 0;
        for (let i = 0; i < numsBPVAR.length; i++) {
          field(numsBPVAR[i]).set(
            new Intl.NumberFormat("fr-FR", { maximumFractionDigits: 0 }).format(
              dataBP[countVAR] - dataBP[countVAR + 1]
            )
          );
          countVAR += 2;
        }

        let countVARPourcent = 0;
        for (let i = 0; i < numsBPVARPourcent.length; i++) {
          const variationPourcent =
            ((parseFloat(dataBP[countVARPourcent]) -
              parseFloat(dataBP[countVARPourcent + 1])) /
              parseFloat(dataBP[countVARPourcent + 1])) *
            100;

          Number.isNaN(variationPourcent)
            ? field(numsBPVARPourcent[i]).set("0")
            : field(numsBPVARPourcent[i]).set(variationPourcent.toFixed(0));
          countVARPourcent += 2;
        }
      });
  });
});
