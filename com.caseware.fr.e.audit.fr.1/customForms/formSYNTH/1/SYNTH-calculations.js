(function (wpw) {
  "use strict";

  wpw.tax.create.calcBlocks("SYNTH", function (calcUtils) {
    let tablesParameters = "";
    function numbersFR(data) {
      let newData = "";
      for (let i = 0; i < data.length; i++) {
        newData = data[i];
        const newNumbers = newData.replace(",", ".").replace(".", "");
        data[i] = newNumbers;
      }
      return data;
    }

    calcUtils.calc(function (calcUtils, field) {
      tablesParameters = field("500").get();
      tablesParameters[1] ? field("NK€").set("K €") : field("NK€").set("€");
      tablesParameters[1] ? field("NM1K€").set("K €") : field("NM1K€").set("€");
    });

    calcUtils.calc(function (calcUtils, field) {
      return wpw.tax
        .evaluate([
          'engprop("relativeperiodend", 0, 0, "longDate")',
          'formatnumeric(year(engprop("yearend", 0)))',
          'formatnumeric(year(engprop("yearend", 0)) - 1)',
        ])
        .then(function (dates) {
          const parsedDate = dates[0].split(" ");
          const yearNM1 = parseFloat(parsedDate[2] - 1);
          field("yearN").set(dates[0]);
          field("yearN-1").set(`${parsedDate[0]} ${parsedDate[1]} ${yearNM1}`);
          field("dates").set(dates[1] + " " + dates[2]);
        });
    });
    calcUtils.calc(function (calcUtils, field) {
      const pdmNums = [
        "input102",
        "commentairebilan",
        "commentaireresultat",
        "commentairesig",
        "commentairetableaufinancement",
        "commentairefluxtresorerie",
        "syntfmi2",
      ];
      const synthNums = [
        "compoéquipe",
        "commentairebilan",
        "commentaireresultat",
        "commentairesig",
        "commentairetableaufinancement",
        "commentairefluxtresorerie",
        "faitsmarquants",
      ];
      for (let i = 0; i < pdmNums.length; i++) {
        const f = calcUtils.form("PDM").field(pdmNums[i]);
        field(synthNums[i]).set(f.get());
        field(synthNums[i]).save();
      }
    });

    calcUtils.calc(function (calcUtils, field) {
      return wpw.tax
        .evaluate([
          'tagprop(entityref("BA.1"), "name")',
          'tagprop(entityref("BA.2.01"), "name")',
          'tagprop(entityref("BA.2.02"), "name")',
          'tagprop(entityref("BA.2.03"), "name")',
          'tagprop(entityref("BA.2"), "name")',
          'tagprop(entityref("BA.3.01"), "name")',
          'tagprop(entityref("BA.3.03"), "name")',
          'tagprop(entityref("BA.3.05"), "name")',
          'tagprop(entityref("BA.3.06"), "name")',
          'tagprop(entityref("BA.3.07"), "name")',
          'tagprop(entityref("BA.3.08"), "name")',
          'tagprop(entityref("BA.3.02"), "name")',
          'tagprop(entityref("BA.3"), "name")',
          'tagprop(entityref("BA.3.09"), "name")',
          'tagprop(entityref("BA.3.10"), "name")',
          'tagprop(entityref("BA.3.11"), "name")',
          'tagprop(entityref("BA"), "name")',
          'tagprop(entityref("BP.1.01"), "name")',
          'tagprop(entityref("BP.1.02"), "name")',
          'tagprop(entityref("BP.1.03"), "name")',
          'tagprop(entityref("BP.1.04"), "name")',
          'tagprop(entityref("BP.1.05"), "name")',
          'tagprop(entityref("BP.1.06"), "name")',
          'tagprop(entityref("BP.1.07"), "name")',
          'tagprop(entityref("BP.1.08"), "name")',
          'tagprop(entityref("BP.1.09"), "name")',
          'tagprop(entityref("BP.1"), "name")',
          'tagprop(entityref("BP.2"), "name")',
          'tagprop(entityref("BP.3"), "name")',
          'tagprop(entityref("BP.4.01"), "name")',
          'tagprop(entityref("BP.4.02"), "name")',
          'tagprop(entityref("BP.4.03"), "name")',
          'tagprop(entityref("BP.4.04"), "name")',
          'tagprop(entityref("BP.4.05"), "name")',
          'tagprop(entityref("BP.4.06"), "name")',
          'tagprop(entityref("BP.4.07"), "name")',
          'tagprop(entityref("BP.4.08"), "name")',
          'tagprop(entityref("BP.4.09"), "name")',
          'tagprop(entityref("BP.4.10"), "name")',
          'tagprop(entityref("BP.4.11"), "name")',
          'tagprop(entityref("BP.4"), "name")',
          'tagprop(entityref("BP"), "name")',
          'tagprop(entityref("RP.1"), "name")',
          'tagprop(entityref("RC.1"), "name")',
          'tagprop(entityref("RP.2.01"), "name")',
          'tagprop(entityref("RC.2.01"), "name")',
          'tagprop(entityref("RP.3"), "name")',
          'tagprop(entityref("RC.3"), "name")',
          'tagprop(entityref("RP.4"), "name")',
          'tagprop(entityref("RC.5"), "name")',
          'tagprop(entityref("RC.6"), "name")',
        ])
        .then(function (labels) {
          const arrayLabels = [
            "label1",
            "label2",
            "label3",
            "label4",
            "label5",
            "label6",
            "label7",
            "label8",
            "label9",
            "label10",
            "label11",
            "label12",
            "label13",
            "label14",
            "label15",
            "label16",
            "label17",
            "labelPassif1",
            "labelPassif2",
            "labelPassif3",
            "labelPassif4",
            "labelPassif5",
            "labelPassif6",
            "labelPassif7",
            "labelPassif8",
            "labelPassif9",
            "labelPassif10",
            "labelPassif11",
            "labelPassif12",
            "labelPassif13",
            "labelPassif14",
            "labelPassif15",
            "labelPassif16",
            "labelPassif17",
            "labelPassif18",
            "labelPassif19",
            "labelPassif20",
            "labelPassif21",
            "labelPassif22",
            "labelPassif23",
            "labelPassif24",
            "labelPassif25",
            "labelcompteresultat1",
            "labelcompteresultat2",
            "labelcompteresultat4",
            "labelcompteresultat5",
            "labelcompteresultat6",
            "labelcompteresultat7",
            "labelcompteresultat10",
            "labelcompteresultat11",
            "labelcompteresultat12",
          ];
          for (let i = 0; i < arrayLabels.length; i++) {
            field(arrayLabels[i]).set(labels[i]);
          }
        });
    });

    calcUtils.calc(function (calcUtils, field) {
      return wpw.tax
        .evaluate([
          'tagbal(entityref("BA.1"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.1"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.2.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.2.01"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.2.02"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.2.02"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.2.03"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.2.03"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.2"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.2"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.01"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.03"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.03"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.05"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.05"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.06"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.06"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.07"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.07"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.08"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.08"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.02"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.02"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.09"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.09"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.10"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.10"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.11"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA.3.11"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BA"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        ])
        .then(function (dataBA) {
          dataBA = numbersFR(dataBA);

          const numsBAN = [
            "csnaNK",
            "iiNK",
            "icNK",
            "ifNK",
            "taiNK",
            "seecNK",
            "cNK",
            "vmdpNK",
            "instruNK",
            "dNK",
            "ccaNK",
            "aeavscNK",
            "tacecNK",
            "crspeNK",
            "preNK",
            "ecaNK",
            "tbaNK",
          ];

          const numsBANM1 = [
            "csnaNM1K",
            "iiNM1K",
            "icNM1K",
            "ifNM1K",
            "taiNM1K",
            "seecNM1K",
            "cNM1K",
            "vmdpNM1K",
            "instruNM1K",
            "dNM1K",
            "ccaNM1K",
            "aeavscNM1K",
            "tacecNM1K",
            "crspeNM1K",
            "preNM1K",
            "ecaNM1K",
            "tbaNM1K",
          ];

          const numsNPourcent = [
            "csnaN%",
            "iiN%",
            "icN%",
            "ifN%",
            "taiN%",
            "seecN%",
            "cN%",
            "vmdpN%",
            "instruN%",
            "dN%",
            "ccaN%",
            "aeavscN%",
            "tacecN%",
            "crspeN%",
            "preN%",
            "ecaN%",
            "tbaN%",
          ];

          const numsNM1Pourcent = [
            "csnaNM1%",
            "iiNM1%",
            "icNM1%",
            "ifNM1%",
            "taiNM1%",
            "seecNM1%",
            "cNM1%",
            "vmdpNM1%",
            "instruNM1%",
            "dNM1%",
            "ccaNM1%",
            "aeavscNM1%",
            "tacecNM1%",
            "crspeNM1%",
            "preNM1%",
            "ecaNM1%",
            "tbaNM1%",
          ];

          const numsVarPourCent = [
            "csnaVAR%",
            "iiVAR%",
            "icVAR%",
            "ifVAR%",
            "taiVAR%",
            "seecVAR%",
            "cVAR%",
            "vmdpVAR%",
            "instruVAR%",
            "dVAR%",
            "ccaVAR%",
            "aeavscVAR%",
            "tacecVAR%",
            "crspeVAR%",
            "preVAR%",
            "ecaVAR%",
            "tbaVAR%",
          ];

          const numsVar = [
            "csnaVAR",
            "iiVAR",
            "icVAR",
            "ifVAR",
            "taiVAR",
            "seecVAR",
            "cVAR",
            "vmdpVAR",
            "instruVAR",
            "dVAR",
            "ccaVAR",
            "aeavscVAR",
            "tacecVAR",
            "crspeVAR",
            "preVAR",
            "ecaVAR",
            "tbaVAR",
          ];

          let countN = 0;
          for (let i = 0; i < numsBAN.length; i++) {
            if (dataBA[countN] === undefined) {
              field(numsBAN[i]).set("N/A");
            } else if (
              tablesParameters["2"] === true &&
              parseFloat(dataBA[countN]) == 0
            ) {
              field(numsBAN[i]).set("hide");
            } else if (tablesParameters["1"] === false || undefined) {
              field(numsBAN[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataBA[countN])
              );
            } else {
              field(numsBAN[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataBA[countN] / 1000)
              );
            }
            countN += 2;
          }

          let countM1 = 1;
          for (let i = 0; i < numsBANM1.length; i++) {
            if (dataBA[countM1] === undefined) {
              field(numsBANM1[i]).set("N/A");
            } else if (
              tablesParameters["2"] === true &&
              parseFloat(dataBA[countM1]) == 0
            ) {
              field(numsBANM1[i]).set("hide");
            } else if (tablesParameters["1"] === false || undefined) {
              field(numsBANM1[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataBA[countM1])
              );
            } else {
              field(numsBANM1[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataBA[countM1] / 1000)
              );
            }
            countM1 += 2;
          }

          let countNPourcent = 0;
          for (let i = 0; i < numsNPourcent.length; i++) {
            const nPourcent =
              (parseFloat(dataBA[countNPourcent]) * 100) /
              parseFloat(dataBA[32]);
            Number.isNaN(nPourcent)
              ? field(numsNPourcent[i]).set("0")
              : field(numsNPourcent[i]).set(nPourcent.toFixed(0));
            countNPourcent += 2;
          }

          let countNM1Pourcent = 1;
          for (let i = 0; i < numsNM1Pourcent.length; i++) {
            const nm1Pourcent =
              (parseFloat(dataBA[countNM1Pourcent]) * 100) /
              parseFloat(dataBA[33]);
            Number.isNaN(nm1Pourcent)
              ? field(numsNM1Pourcent[i]).set("0")
              : field(numsNM1Pourcent[i]).set(nm1Pourcent.toFixed(0));
            countNM1Pourcent += 2;
          }

          let countVAR = 0;
          for (let i = 0; i < numsVar.length; i++) {
            field(numsVar[i]).set(
              new Intl.NumberFormat("fr-FR", {
                maximumFractionDigits: 0,
              }).format(dataBA[countVAR] - dataBA[countVAR + 1])
            );
            countVAR += 2;
          }

          let countVARPourcent = 0;
          for (let i = 0; i < numsVarPourCent.length; i++) {
            const variationPourcent =
              ((parseFloat(dataBA[countVARPourcent]) -
                parseFloat(dataBA[countVARPourcent + 1])) /
                parseFloat(dataBA[countVARPourcent + 1])) *
              100;

            Number.isNaN(variationPourcent)
              ? field(numsVarPourCent[i]).set("0")
              : field(numsVarPourCent[i]).set(variationPourcent.toFixed(0));
            countVARPourcent += 2;
          }
        });
    });

    calcUtils.calc(function (calcUtils, field) {
      return wpw.tax
        .evaluate([
          'tagbal(entityref("BP.1.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.01"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.02"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.02"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.03"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.03"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.04"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.04"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.05"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.05"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.06"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.06"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.07"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.07"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.08"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.08"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.09"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1.09"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.1"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.2"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.2"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.3"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.3"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.01"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.02"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.02"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.03"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.03"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.04"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.04"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.05"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.05"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.06"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.06"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.07"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.07"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.08"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.08"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.09"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.09"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.10"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.10"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.11"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4.11"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP.4"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("BP"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        ])
        .then(function (dataBP) {
          dataBP = numbersFR(dataBP);

          const numsBPN = [
            "capiPNK",
            "primesPNK",
            "ecartsreevalPNK",
            "ecartequivalencePNK",
            "reservesPNK",
            "reportnouveauPNK",
            "resultatexercicePNK",
            "subventionPNK",
            "provisionPNK",
            "capitauxPNK",
            "autresfondsPNK",
            "provisionsPNK",
            "eocPNK",
            "aeoPNK",
            "edecPNK",
            "edfdPNK",
            "dfcrPNK",
            "dfsPNK",
            "dicrPNK",
            "autresdettesPNK",
            "itPNK",
            "pcaPNK",
            "ecpPNK",
            "dettesPNK",
            "passifPNK",
          ];

          const numsBPNPourcent = [
            "capiPN%",
            "primesPN%",
            "ecartsreevalPN%",
            "ecartequivalencePN%",
            "reservesPN%",
            "reportnouveauPN%",
            "resultatexercicePN%",
            "subventionPN%",
            "provisionPN%",
            "capitauxPN%",
            "autresfondsPN%",
            "provisionsPN%",
            "eocPN%",
            "aeoPN%",
            "edecPN%",
            "edfdPN%",
            "dfcrPN%",
            "dfsPN%",
            "dicrPN%",
            "autresdettesPN%",
            "itPN%",
            "pcaPN%",
            "ecpPN%",
            "dettesPN%",
            "passifPN%",
          ];

          const numsBPNM1K = [
            "capiPNM1K",
            "primesPNM1K",
            "ecartsreevalPNM1K",
            "ecartequivalencePNM1K",
            "reservesPNM1K",
            "reportnouveauPNM1K",
            "resultatexercicePNM1K",
            "subventionPNM1K",
            "provisionPNM1K",
            "capitauxPNM1K",
            "autresfondsPNM1K",
            "provisionsPNM1K",
            "eocPNM1K",
            "aeoPNM1K",
            "edecPNM1K",
            "edfdPNM1K",
            "dfcrPNM1K",
            "dfsPNM1K",
            "dicrPNM1K",
            "autresdettesPNM1K",
            "itPNM1K",
            "pcaPNM1K",
            "ecpPNM1K",
            "dettesPNM1K",
            "passifPNM1K",
          ];

          const numsBPNM1Pourcent = [
            "capiPNM1%",
            "primesPNM1%",
            "ecartsreevalPNM1%",
            "ecartequivalencePNM1%",
            "reservesPNM1%",
            "reportnouveauPNM1%",
            "resultatexercicePNM1%",
            "subventionPNM1%",
            "provisionPNM1%",
            "capitauxPNM1%",
            "autresfondsPNM1%",
            "provisionsPNM1%",
            "eocPNM1%",
            "aeoPNM1%",
            "edecPNM1%",
            "edfdPNM1%",
            "dfcrPNM1%",
            "dfsPNM1%",
            "dicrPNM1%",
            "autresdettesPNM1%",
            "itPNM1%",
            "pcaPNM1%",
            "ecpPNM1%",
            "dettesPNM1%",
            "passifPNM1%",
          ];

          const numsBPVAR = [
            "capiVAR",
            "primesVAR",
            "ecartsreevalVAR",
            "ecartequivalenceVAR",
            "reservesVAR",
            "reportnouveauVAR",
            "resultatexerciceVAR",
            "subventionVAR",
            "provisionVAR",
            "capitauxVAR",
            "autresfondsVAR",
            "provisionsVAR",
            "eocVAR",
            "aeoVAR",
            "edecVAR",
            "edfdVAR",
            "dfcrVAR",
            "dfsVAR",
            "dicrVAR",
            "autresdettesVAR",
            "itVAR",
            "pcaVAR",
            "ecpVAR",
            "dettesVAR",
            "passifVAR",
          ];

          const numsBPVARPourcent = [
            "capiVAR%",
            "primesVAR%",
            "ecartsreevalVAR%",
            "ecartequivalenceVAR%",
            "reservesVAR%",
            "reportnouveauVAR%",
            "resultatexerciceVAR%",
            "subventionVAR%",
            "provisionVAR%",
            "capitauxVAR%",
            "autresfondsVAR%",
            "provisionsVAR%",
            "eocVAR%",
            "aeoVAR%",
            "edecVAR%",
            "edfdVAR%",
            "dfcrVAR%",
            "dfsVAR%",
            "dicrVAR%",
            "autresdettesVAR%",
            "itVAR%",
            "pcaVAR%",
            "ecpVAR%",
            "dettesVAR%",
            "passifVAR%",
          ];

          let countN = 0;
          for (let i = 0; i < numsBPN.length; i++) {
            if (dataBP[countN] === undefined) {
              field(numsBPN[i]).set("N/A");
            } else if (
              tablesParameters["2"] === true &&
              parseFloat(dataBP[countN]) == 0
            ) {
              field(numsBPN[i]).set("hide");
            } else if (tablesParameters["1"] === false || undefined) {
              field(numsBPN[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataBP[countN])
              );
            } else {
              field(numsBPN[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataBP[countN] / 1000)
              );
            }
            countN += 2;
          }

          let countM1 = 1;
          for (let i = 0; i < numsBPNM1K.length; i++) {
            if (dataBP[countM1] === undefined) {
              field(numsBPNM1K[i]).set("N/A");
            } else if (
              tablesParameters["2"] === true &&
              parseFloat(dataBP[countM1]) == 0
            ) {
              field(numsBPNM1K[i]).set("hide");
            } else if (tablesParameters["1"] === false || undefined) {
              field(numsBPNM1K[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataBP[countM1])
              );
            } else {
              field(numsBPNM1K[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataBP[countM1] / 1000)
              );
            }
            countM1 += 2;
          }

          let countNPourcent = 0;
          for (let i = 0; i < numsBPNPourcent.length; i++) {
            const nPourcent =
              (parseFloat(dataBP[countNPourcent]) * 100) /
              parseFloat(dataBP[48]);
            Number.isNaN(nPourcent)
              ? field(numsBPNPourcent[i]).set("0")
              : field(numsBPNPourcent[i]).set(nPourcent.toFixed(0));
            countNPourcent += 2;
          }

          let countNM1Pourcent = 1;
          for (let i = 0; i < numsBPNM1Pourcent.length; i++) {
            const nm1Pourcent =
              (parseFloat(dataBP[countNM1Pourcent]) * 100) /
              parseFloat(dataBP[49]);
            Number.isNaN(nm1Pourcent)
              ? field(numsBPNM1Pourcent[i]).set("0")
              : field(numsBPNM1Pourcent[i]).set(nm1Pourcent.toFixed(0));
            countNM1Pourcent += 2;
          }

          let countVAR = 0;
          for (let i = 0; i < numsBPVAR.length; i++) {
            field(numsBPVAR[i]).set(
              new Intl.NumberFormat("fr-FR", {
                maximumFractionDigits: 0,
              }).format(dataBP[countVAR] - dataBP[countVAR + 1])
            );
            countVAR += 2;
          }

          let countVARPourcent = 0;
          for (let i = 0; i < numsBPVARPourcent.length; i++) {
            const variationPourcent =
              ((parseFloat(dataBP[countVARPourcent]) -
                parseFloat(dataBP[countVARPourcent + 1])) /
                parseFloat(dataBP[countVARPourcent + 1])) *
              100;

            Number.isNaN(variationPourcent)
              ? field(numsBPVARPourcent[i]).set("0")
              : field(numsBPVARPourcent[i]).set(variationPourcent.toFixed(0));
            countVARPourcent += 2;
          }
        });
    });

    calcUtils.calc(function (calcUtils, field) {
      return wpw.tax
        .evaluate([
          'tagbal(entityref("RP.1"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RP.1"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RC.1"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RC.1"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RP.2.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RP.2.01"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RC.2.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RC.2.01"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RP.3"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RP.3"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RC.3"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RC.3"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RP.4"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RP.4"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RC.5"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RC.5"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RC.6"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
          'tagbal(entityref("RC.6"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        ])
        .then(function (dataCR) {
          dataCR = numbersFR(dataCR);

          const numsCRN = [
            "peNK",
            "ceNK",
            "bptNK",
            "pbtNK",
            "pfNK",
            "cfNK",
            "pexcepNK",
            "psrNK",
            "ibNK",
          ];

          const numsCRNPourcent = [
            "peNK%",
            "ceNK%",
            "bptN%",
            "pbtN%",
            "pfN%",
            "cfN%",
            "pexcepN%",
            "psrN%",
            "ibN%",
          ];

          const numsCRNM1 = [
            "peNM1K",
            "ceNM1K",
            "bptNM1K",
            "pbtNM1K",
            "pfNM1K",
            "cfNM1K",
            "pexcepNM1K",
            "psrNM1K",
            "ibNM1K",
          ];

          const numsCRNM1Pourcent = [
            "peNM1%",
            "ceNM1%",
            "bptNM1%",
            "pbtNM1%",
            "pfNM1%",
            "cfNM1%",
            "pexcepNM1%",
            "psrNM1%",
            "ibNM1%",
          ];

          const numsCRVAR = [
            "peVAR",
            "ceVAR",
            "bptVAR",
            "pbtVAR",
            "pfVAR",
            "cfVAR",
            "pexcepVAR",
            "psrVAR",
            "ibVAR",
          ];

          const numsCRVARPourcent = [
            "peVAR%",
            "ceVAR%",
            "bptVAR%",
            "pbtVAR%",
            "pfVAR%",
            "cfVAR%",
            "pexcepVAR%",
            "psrVAR%",
            "ibVAR%",
          ];

          const dataResults = {
            reNK: parseFloat(dataCR[0]) + parseFloat(dataCR[2]),
            reNM1K: parseFloat(dataCR[1]) + parseFloat(dataCR[3]),
            rfNK: parseFloat(dataCR[8]) + parseFloat(dataCR[10]),
            rfNM1K: parseFloat(dataCR[9]) + parseFloat(dataCR[11]),
            rcaiNK:
              parseFloat(dataCR[0]) +
              parseFloat(dataCR[2]) +
              parseFloat(dataCR[4]) +
              parseFloat(dataCR[6]) +
              parseFloat(dataCR[8]) +
              parseFloat(dataCR[10]) +
              parseFloat(dataCR[12]) +
              parseFloat(dataCR[14]) +
              parseFloat(dataCR[16]),
            rcaiNM1K:
              parseFloat(dataCR[1]) +
              parseFloat(dataCR[3]) +
              parseFloat(dataCR[5]) +
              parseFloat(dataCR[7]) +
              parseFloat(dataCR[9]) +
              parseFloat(dataCR[11]) +
              parseFloat(dataCR[13]) +
              parseFloat(dataCR[15]) +
              parseFloat(dataCR[17]),
            rnNK:
              parseFloat(dataCR[0]) +
              parseFloat(dataCR[2]) +
              parseFloat(dataCR[4]) +
              parseFloat(dataCR[6]) +
              parseFloat(dataCR[8]) +
              parseFloat(dataCR[10]),
            rnNM1K:
              parseFloat(dataCR[1]) +
              parseFloat(dataCR[3]) +
              parseFloat(dataCR[5]) +
              parseFloat(dataCR[7]) +
              parseFloat(dataCR[9]) +
              parseFloat(dataCR[11]),
            reVAR:
              parseFloat(dataCR[0]) +
              parseFloat(dataCR[2]) -
              (parseFloat(dataCR[1]) + parseFloat(dataCR[3])),
            rfVAR:
              parseFloat(dataCR[8]) +
              parseFloat(dataCR[10]) -
              (parseFloat(dataCR[9]) + parseFloat(dataCR[11])),
            rcaiVAR:
              parseFloat(dataCR[0]) +
              parseFloat(dataCR[2]) +
              parseFloat(dataCR[4]) +
              parseFloat(dataCR[6]) +
              parseFloat(dataCR[8]) +
              parseFloat(dataCR[10]) +
              parseFloat(dataCR[12]) +
              parseFloat(dataCR[14]) +
              parseFloat(dataCR[16]) -
              (parseFloat(dataCR[1]) +
                parseFloat(dataCR[3]) +
                parseFloat(dataCR[5]) +
                parseFloat(dataCR[7]) +
                parseFloat(dataCR[9]) +
                parseFloat(dataCR[11]) +
                parseFloat(dataCR[13]) +
                parseFloat(dataCR[15]) +
                parseFloat(dataCR[17])),
            rnVAR:
              parseFloat(dataCR[0]) +
              parseFloat(dataCR[2]) +
              parseFloat(dataCR[4]) +
              parseFloat(dataCR[6]) +
              parseFloat(dataCR[8]) +
              parseFloat(dataCR[10]) -
              (parseFloat(dataCR[1]) +
                parseFloat(dataCR[3]) +
                parseFloat(dataCR[5]) +
                parseFloat(dataCR[7]) +
                parseFloat(dataCR[9]) +
                parseFloat(dataCR[11])),
          };

          const numsResults = [
            "reNK",
            "reNM1K",
            "rfNK",
            "rfNM1K",
            "rcaiNK",
            "rcaiNM1K",
            "rnNK",
            "rnNM1K",
            "reVAR",
            "rfVAR",
            "rcaiVAR",
            "rnVAR",
          ];
          const numsResultsNPourcent = ["reN%", "rfN%", "rcaiN%", "rnN%"];
          const numsResultsNM1Pourcent = [
            "reNM1%",
            "rfNM1%",
            "rcaiNM1%",
            "rnNM1%",
          ];
          const numsResultsVARPourcent = [
            "reVAR%",
            "rfVAR%",
            "rcaiVAR%",
            "rnVAR%",
          ];

          let countN = 0;
          for (let i = 0; i < numsCRN.length; i++) {
            if (dataCR[countN] === undefined) {
              field(numsCRN[i]).set("N/A");
            } else if (
              tablesParameters["2"] === true &&
              parseFloat(dataCR[countN]) == 0
            ) {
              field(numsCRN[i]).set("hide");
            } else if (tablesParameters["1"] === false || undefined) {
              field(numsCRN[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataCR[countN])
              );
            } else {
              field(numsCRN[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataCR[countN] / 1000)
              );
            }
            countN += 2;
          }

          let countM1 = 1;
          for (let i = 0; i < numsCRNM1.length; i++) {
            if (dataCR[countM1] === undefined) {
              field(numsCRNM1[i]).set("N/A");
            } else if (
              tablesParameters["2"] === true &&
              parseFloat(dataCR[countM1]) == 0
            ) {
              field(numsCRNM1[i]).set("hide");
            } else if (tablesParameters["1"] === false || undefined) {
              field(numsCRNM1[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataCR[countM1])
              );
            } else {
              field(numsCRNM1[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataCR[countM1] / 1000)
              );
            }
            countM1 += 2;
          }

          for (let i = 0; i < numsResults.length; i++) {
            if (dataResults[numsResults[i]] === undefined) {
              field(numsResults[i]).set("N/A");
            } else if (
              tablesParameters["2"] === true &&
              parseFloat(dataResults[numsResults[i]]) == 0
            ) {
              field(numsResults[i]).set("hide");
            } else if (tablesParameters["1"] === false || undefined) {
              field(numsResults[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataResults[numsResults[i]])
              );
            } else {
              field(numsResults[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(dataResults[numsResults[i]] / 1000)
              );
            }
          }

          let countVAR = 0;
          for (let i = 0; i < numsCRVAR.length; i++) {
            field(numsCRVAR[i]).set(
              new Intl.NumberFormat("fr-FR", {
                maximumFractionDigits: 0,
              }).format(dataCR[countVAR] - dataCR[countVAR + 1])
            );
            countVAR += 2;
          }

          let countNPourcent = 0;
          for (let i = 0; i < numsCRNPourcent.length; i++) {
            const nPourcent =
              (parseFloat(dataCR[countNPourcent]) * 100) / dataResults.rnNK;
            Number.isNaN(nPourcent)
              ? field(numsCRNPourcent[i]).set("0")
              : field(numsCRNPourcent[i]).set(nPourcent.toFixed(0));
            countNPourcent += 2;
          }

          let countNM1Pourcent = 1;
          for (let i = 0; i < numsCRNM1Pourcent.length; i++) {
            const nm1Pourcent =
              (parseFloat(dataCR[countNM1Pourcent]) * 100) / dataResults.rnNM1K;
            Number.isNaN(nm1Pourcent)
              ? field(numsCRNM1Pourcent[i]).set("0")
              : field(numsCRNM1Pourcent[i]).set(nm1Pourcent.toFixed(0));
            countNM1Pourcent += 2;
          }

          let N = 0;
          for (let i = 0; i < numsResultsNPourcent.length; i++) {
            const nResultsPourcent =
              (dataResults[numsResults[N]] * 100) / dataResults.rnNK;
            Number.isNaN(nResultsPourcent)
              ? field(numsResultsNPourcent[i]).set("0")
              : field(numsResultsNPourcent[i]).set(nResultsPourcent.toFixed(0));
            N += 2;
          }

          let NM = 1;
          for (let i = 0; i < numsResultsNM1Pourcent.length; i++) {
            const nm1ResultsPourcent =
              (dataResults[numsResults[NM]] * 100) / dataResults.rnNM1K;
            Number.isNaN(nm1ResultsPourcent)
              ? field(numsResultsNM1Pourcent[i]).set("0")
              : field(numsResultsNM1Pourcent[i]).set(
                  nm1ResultsPourcent.toFixed(0)
                );
            NM += 2;
          }

          let VARResults = 0;
          for (let i = 0; i < numsResultsVARPourcent.length; i++) {
            const varResultsPourcent =
              ((dataResults[numsResults[VARResults]] -
                dataResults[numsResults[VARResults + 1]]) /
                dataResults[numsResults[VARResults + 1]]) *
              100;
            Number.isNaN(varResultsPourcent)
              ? field(numsResultsVARPourcent[i]).set("0")
              : field(numsResultsVARPourcent[i]).set(
                  varResultsPourcent.toFixed(0)
                );
            VARResults += 2;
          }

          let VAR = 0;
          for (let i = 0; i < numsCRVARPourcent.length; i++) {
            const varResultsPourcent =
              ((dataCR[VAR] - dataCR[VAR + 1]) / dataCR[VAR + 1]) * 100;
            Number.isNaN(varResultsPourcent)
              ? field(numsCRVARPourcent[i]).set("0")
              : field(numsCRVARPourcent[i]).set(varResultsPourcent.toFixed(0));
            VAR += 2;
          }
        });
    });

    calcUtils.calc(function (calcUtils, field) {
      return wpw.tax.getMateriality().then(function (materialityProperties) {
        const parameterseuil = [
          "com.caseware.materiality;preliminary;-1",
          "com.caseware.materiality;preliminary;0",
          "com.caseware.materiality;report;0",
        ];
        const row1 = [
          "periodePrecedenteSYNTHSI",
          "periodePrecedenteSYNTHPlan",
          "periodePrecedenteSYNTHInsi",
        ];
        const row2 = ["preliSYNTHSI", "preliSYNTHPlan", "preliSYNTHInsi"];
        const row3 = ["FinalSYNTHSI", "FinalSYNTHPlan", "FinalSYNTHInsi"];

        for (let i = 0; i < row1.length; i++) {
          materialityProperties[i].balances[parameterseuil[0]] === undefined
            ? field(row1[i]).set("-")
            : field(row1[i]).set(
                materialityProperties[i].balances[parameterseuil[0]].amount
              );
        }

        for (let i = 0; i < row2.length; i++) {
          materialityProperties[i].balances[parameterseuil[1]] === undefined
            ? field(row2[i]).set("-")
            : field(row2[i]).set(
                materialityProperties[i].balances[parameterseuil[1]].amount
              );
        }

        for (let i = 0; i < row3.length; i++) {
          materialityProperties[i].balances[parameterseuil[2]] === undefined
            ? field(row3[i]).set("-")
            : field(row3[i]).set(
                materialityProperties[i].balances[parameterseuil[2]].amount
              );
        }
      });
    }),
      calcUtils.calc(function (calcUtils, field) {
        return wpw.tax
          .evaluate([
            'procedureprop("@rhN76tASQAyO1Ytppbna3Q", "responseAnswer", "@0dzgFS9AR-CGV6SKor1VMw", "@buebF31fRX2vby2gRgsm6w")',
            'procedureprop("@unLQHnVsRVai2UmW4ttzBQ", "responseAnswer", "@02tuY5qXTb-s1nDcQKrHtA", "@jRqdpwN_TPWfMPuOMgMxtw")',
            'procedureprop("@VMEhEs3jTOWC0BY-WVdHGw", "responseAnswer", "@6Sz2rTuFQ06Ec5Az3vtMLQ", "@KnSyKSwfRKKMpSlf6vgKYg")',
            'procedureprop("@fIHBFldARdSlKayuKDH6pA", "responseAnswer", "@dLIRMM46TsSDvWDL0i7R1A", "@RKk3g3dkSbu1-HrUGKOc8w")',
            'procedureprop("@J07MoS9kSh6gDovzY7GeEw", "responseAnswer", "@wAYTgoE-TjmetRLbVpCjDA", "@rD7oqLOwS3ivkK6gjoJTww")',
            'procedureprop("@fOIbug2CRR2DGcDI71_aUQ", "responseAnswer", "@t3Iv3JipRZqVn0kNJMFdOw", "@RBCwfz7LRWmQdToqHrp5BA")',
            'procedureprop("@cRaFLk16RV2zUlp1sohYPA", "responseAnswer", "@o8FENRl9SS2m39bAmfjH5w", "@ks3Xu3RES2yzEffl1XoxjA")',
            'procedureprop("@Jd10Ey0kQqCGkdov0WhIAw", "responseAnswer", "@crGqzEjTRzCLgJ-p0a8bbQ", "@qPnKVvNYRJyo4Hid1WkigA")',
            'procedureprop("@wiknIxHaRGqXHIbTrbGZUQ", "responseAnswer", "@RYjk-f2sQHqg-Ff6eJsW9A", "@vpQ2ZFf9Q1y7zM1QQhhadA")',
            'procedureprop("@YHkOxGwfRtiSnewpNrPrsw", "responseAnswer", "@SxgQnZCRQ_yZKbWWDyag6g", "@GsOeTpy2SKSUhpc5uG9lBQ")',
            'procedureprop("@ZcqcaB-sRm-u0R_evN23cw", "responseAnswer", "@4seGazx1Q5ezNjGkTU8fJA", "@8JYQndrITxSeBTTLcksSUA")',
            'procedureprop("@AKtuqf4lR_2aqxbZw2O3zw", "responseAnswer", "@L3CCcq0-RtC_j6eUygJoGA", "@Hkcn1UbySNS4ABdFfe0Qfg")',
            'procedureprop("@KZiiz3jFT4e_P2l-K_WEIw", "responseAnswer", "@BF4hf83ITp-aMg3DTv625g", "@c3Jkqj75TMSWBru0xGU4QQ")',
            'procedureprop("@_2JXI_z4ROafm6YJnJR8qQ", "responseAnswer", "@cQOlmVUBSViRVw1LZorGYg", "@V0ajEFlsQ1y3ozNKSAFRRw")',
            'procedureprop("@KEHaohN7QwCyfYSxUu-RPw", "responseAnswer", "@s6KuQhtJTAOYUPPsxqVg_g", "@vJL4H-FVTvW9H9x4O5mjGw")',
            'procedureprop("@fJmhIragTSmGq8GHUX290A", "responseAnswer", "@51sz8eoHTPGssZ2PJBEWjQ", "@8N5XBsk2SXCkqF-r4ckl3Q")',
            'procedureprop("@PKxALFB6RBy8mpGgYMisZQ", "responseAnswer", "@MkN5wnWcTT2g40ZVrlPmZg", "@VaQ0BM8_SPqXpIPmyhJtFA")',
            'procedureprop("@QhgJI0yyQm6KCf8cDj0EBw", "responseAnswer", "@GKeBTKlSRfuZb8bUxYRt3Q", "@0R1_jcfORRCTnZqgeCfW0Q")',
            'procedureprop("@UxykQ2OtTCm-R-m5T_J32Q", "responseAnswer", "@ym95ubPaR5-ACTQ5KxmanQ", "@IfDHYRINTqm1jxkalt07uQ")',
            'procedureprop("@hWb6t7DRRNOirfI2CdU7IQ", "responseAnswer", "@jMZXrFTxTMqR63FwZ4Ij5A", "@vLu2XAv1TzyAjDpRiYw5oA")',
          ])
          .then(function (allConclusions) {
            const conclusionNums = [
              "conclusion52",
              "conclusion53",
              "conclusion72",
              "conclusion83",
              "conclusion84",
              "conclusion85",
              "conclusion86",
              "conclusion87",
              "conclusion88",
              "conclusion89",
              "conclusion810",
              "conclusion811",
              "conclusion812",
              "conclusion813",
              "conclusion815",
              "conclusion122",
              "conclusion123",
              "conclusion124",
              "conclusion125",
              "conclusion126",
            ];
            for (let i = 0; i < allConclusions.length; i++) {
              field(conclusionNums[i]).set(
                allConclusions[i]
                  .replaceAll("&nbsp;", " ")
                  .replace(RegExp(/<[^>]+>/gi), "")
              );
            }
          });
      });

    calcUtils.onFormLoad("SYNTH", function (calcUtils, field) {
      wpw.tax.getRisks().then(function (risks) {
        const myTable = field("risque73");
        let repAudit = [];
        let assArray = [];
        let tagsFR = [];
        const tags = {
          MzUhZfaFT4KKspLoTre0_Q: "Fournisseurs Charges",
          XPKxJGCASXa90axSzrBPbQ: "Clients Ventes",
          "UZjt3V2rTM6-H_mEC_uyRw": "Stocks",
          Og9vyd9ORL6aF908dO4dWA: "Personnel",
          zNBtfv8vRDuKQ2jjCVynSA: "Tresorerie Financement",
          yxKIZSE4RtOcXR7gZzFIoQ: "Immobilisations incorporelle et corporelles",
          "DAtZRISTSvas2QsCHnJ-Dw": "immobilisations financières",
          OKD_V6k0SaeFI3OuJRuA3Q: "Capitaux propres",
          "glZIDE-tRrKaFl_88-BGmA": "Provisions pour risques et c",
          quTgDJw4SmStWvAq0WzKaA: "Impots et taxes",
          xI88oRMGQSmD0aBkx4p7dA: "Autres creances et dettes",
          "ZVHH8sj2SJm-P4NHDIjpUg": "Resultat exceptionnel",
        };

        let tableLength = myTable.getRows().length;
        for (let i = 0; i < tableLength; i++) {
          calcUtils.removeTableRow("SYNTH", "risque73", 0);
        }

        for (let i = 0; i < risks.length; i++) {
          for (let j = 0; j < Object.keys(risks[i].tags).length; j++) {
            tagsFR[j] = tags[risks[i].tags[j]];
          }
          for (let k = 0; k < Object.keys(risks[i].scopedTags).length; k++) {
            const tag = risks[i].tags[k];
            assArray[k] = [];
            for (
              let j = 0;
              j < Object.keys(risks[i].scopedTags[tag]).length;
              j++
            ) {
              const scoppedTag = risks[i].scopedTags[tag];
              switch (scoppedTag[j]) {
                case "k3O5OkcyMvquVyVAvgnewQ":
                  assArray[k][0] = "E";
                  break;
                case "rFNNk6YKOuG4FtE_-h0RWg":
                  break;
                case "koLe5cFPNOaoAtl-dnkoRQ":
                  assArray[k][1] = "R";
                  break;
                case "PWy-RwCTMX2GjcYPBPfGXQ":
                  break;
                case "D5YN3Ka6P9-nEobiLBjYXA":
                  assArray[k][2] = "C";
                  break;
                case "lHbZ4O3lMXesKcNQMc9Brw":
                  assArray[k][3] = "M";
                  break;
                case "qsj9cmePO5OI-8WOhCdpzg":
                  break;
                case "9uf1Z4dmNAC9SwWVG7E_Mw":
                  assArray[k][4] = "D";
                  break;
                case "4CqDDKzgO7uHFpLns6v_bA":
                  assArray[k][5] = "S";
                  break;
              }
            }
            assArray[k] = assArray[k].join("");
          }

          let cycles = [];
          for (let j = 0; j < tagsFR.length; j++) {
            cycles[j] = `${tagsFR[j]} / ${assArray[j]}`;
          }

          let documentsLink = [];
          Object.keys(risks[i]["attachables"]).forEach((id) => {
            documentsLink.push(risks[i]["attachables"][id].link);
          });
          let procedureLink = [];
          Object.keys(risks[i]["checklistProcedures"]).forEach((id) => {
            procedureLink.unshift(risks[i]["checklistProcedures"][id]);
          });
          calcUtils.addTableRow("SYNTH", "risque73", i);
          field("risque73").cell(i, 0).setCellLabel(risks[i].name);
          field("risque73").cell(i, 1).setCellLabel(cycles.join("\n"));
          risks[i].fraudRisk
            ? field("risque73").cell(i, 2).setCellLabel("Fraude")
            : risks[i].significantRisk
            ? field("risque73").cell(i, 2).setCellLabel("Significatif")
            : field("risque73").cell(i, 2).setCellLabel("Normal");
          wpw.tax.getControls().then(function (controls) {
            let controlsName = [];
            for (let j = 0; j < controls.length; j++) {
              for (let k = 0; k < risks[i].controls.length; k++) {
                if (risks[i].controls[k] === controls[j].id) {
                  controlsName[k] = controls[j].name;
                }
              }
            }
            field("risque73").cell(i, 3).setCellLabel(controlsName.join("\n"));
          });

          wpw.tax.getDocuments().then((docs) => {
            let docName = [];
            let docContent = [];

            for (let j = 0; j < docs.length; j++) {
              if (documentsLink.indexOf(docs[j].id) > -1) {
                docName.push(docs[j].name);
                docContent.push(docs[j].content);
              }
            }
            for (let j = 0; j < docContent.length; j++) {
              wpw.tax.getChecklists([docContent[j]]).then((checklists) => {
                let procedureName = [];
                for (let i = 0; i < checklists[0]["procedures"].length; i++) {
                  if (
                    procedureLink[j].indexOf(
                      checklists[0]["procedures"][i].id
                    ) > -1
                  ) {
                    procedureName.push(checklists[0]["procedures"][i].summary);
                  }
                }
                repAudit[j] = `${docName[j]} | ${procedureName[0]}`;
                myTable.cell(i, 4).setCellLabel(
                  repAudit
                    .filter((el) => {
                      return el != null && el != "";
                    })
                    .join("\n")
                );
              });
            }
          });

          assArray = [];
          tagsFR = [];
        }
      });
    });

    calcUtils.onFormLoad("SYNTH", function (calcUtils, field) {
      wpw.tax.getIssues().then(function (issueProperties) {
        let myTable = field("taches93");
        let datetext = "";
        const statut = {
          closed: "Fermé",
          open: "Ouvert",
          resolved: "Résolu",
        };

        let tableLength = myTable.getRows().length;
        for (let i = 0; i < tableLength; i++) {
          calcUtils.removeTableRow("SYNTH", "taches93", 0);
        }

        for (let i = 0; i < issueProperties.length; i++) {
          if (issueProperties[i].type !== "consideration") {
            issueProperties.splice(i, 1);
            i--;
          }
        }

        for (let i = 0; i < issueProperties.length; i++) {
          const arrayResponses = Object.values(issueProperties[i].responses);
          const description = arrayResponses[0].text
            .replace(RegExp(/<\/p>/gi), "\n")
            .replaceAll("&#160;", "\n")
            .replaceAll("&#38;", "&")
            .replace(RegExp(/<[^>]+>/gi), "");

          for (let j = 0; j < arrayResponses.length; j++) {
            const dateEU = arrayResponses[j].date.slice(0, 10).split("-");
            arrayResponses[j].status !== "open" &&
            issueProperties[i].status !== "open"
              ? (datetext = `${dateEU[2]}-${dateEU[1]}-${dateEU[0]}`)
              : (datetext = "En cours");
          }

          calcUtils.addTableRow("SYNTH", "taches93", i);
          myTable.cell(i, 0).setCellLabel("Point d'audit");
          myTable.cell(i, 1).setCellLabel(description);
          myTable.cell(i, 2).setCellLabel(statut[issueProperties[i].status]);
          myTable.cell(i, 3).setCellLabel(datetext);
        }
      });
    });

    calcUtils.onFormLoad("SYNTH", function (calcUtils, field) {
      wpw.tax.getIssues().then(function (issueProperties) {
        let myTable = field("alltaches");
        let datetext = "";
        const statut = {
          closed: "Fermé",
          open: "Ouvert",
          resolved: "Résolu",
        };
        const type = {
          error: "Erreur",
          letter: "Lettre d'affirmation",
          consideration_for_next_year: "Point N+1",
          resolved: "Résolue",
          todo: "A faire",
          consideration: "Point d'audit",
          review: "Notes de revues",
          note: "Note",
          deficiency: "Insuffisance",
        };

        let tableLength = myTable.getRows().length;
        for (let i = 0; i < tableLength; i++) {
          calcUtils.removeTableRow("SYNTH", "alltaches", 0);
        }

        for (let i = 0; i < issueProperties.length; i++) {
          const arrayResponses = Object.values(issueProperties[i].responses);
          const description = arrayResponses[0].text
            .replace(RegExp(/<\/p>/gi), "\n")
            .replaceAll("&#160;", "\n")
            .replaceAll("&#38;", "&")
            .replace(RegExp(/<[^>]+>/gi), "");

          for (let j = 0; j < arrayResponses.length; j++) {
            const dateEU = arrayResponses[j].date.slice(0, 10).split("-");
            arrayResponses[j].status !== "open" &&
            issueProperties[i].status !== "open"
              ? (datetext = `${dateEU[2]}-${dateEU[1]}-${dateEU[0]}`)
              : (datetext = "En cours");
          }

          calcUtils.addTableRow("SYNTH", "alltaches", i);
          myTable.cell(i, 0).setCellLabel(type[issueProperties[i].type]);
          myTable.cell(i, 0).config("class", "labelLeft");
          myTable.cell(i, 1).setCellLabel(description);
          myTable.cell(i, 1).config("class", "labelLeft");
          myTable.cell(i, 2).setCellLabel(statut[issueProperties[i].status]);
          myTable.cell(i, 2).config("class", "labelLeft");
          myTable.cell(i, 3).setCellLabel(datetext);
          myTable.cell(i, 3).config("class", "labelLeft");
        }
      });
    });

    calcUtils.calc(function (calcUtils, field) {
      wpw.tax.getControls().then(function (controls) {
        const typeControls = {
          w7Nd3gjjO4q_Ms4_zG3GDA: "Entité",
          "8qybickhMKSmMAipaUlXzA": "SI",
          S7lipDe7PpaE4Aib2UiHbQ: "Etat financier",
        };
        const freqControls = {
          quarterly: "trimestrielle",
          weekly: "hebdomadaire",
          semi_annually: "semestrielle",
          annually: "tous les ans",
          monthly: "Mensuelle",
          daily: "quotidient",
          semi_monthly: "Bimensuel",
          multiple_daily: "Plusieurs fois par jour",
          multiple_weekly: "plusieurs fois par semaine",
        };
        const key = {
          true: "Oui",
          false: "Non",
        };
        const exec = {
          automated: "Automatisé",
          it_dependent: "Dépendant des TI",
          manual: "Manuel",
        };
        const classification = {
          all: "Les deux",
          preventative: "Préventif",
          detective: "Détective",
        };
        const assertions = {
          k3O5OkcyMvquVyVAvgnewQ: "E",
          "koLe5cFPNOaoAtl-dnkoRQ": "R",
          "D5YN3Ka6P9-nEobiLBjYXA": "C",
          lHbZ4O3lMXesKcNQMc9Brw: "M",
          "9uf1Z4dmNAC9SwWVG7E_Mw": "D",
          "4CqDDKzgO7uHFpLns6v_bA": "S",
        };
        const cycles = {
          MzUhZfaFT4KKspLoTre0_Q: "Fournisseurs Charges",
          XPKxJGCASXa90axSzrBPbQ: "Clients Ventes",
          "UZjt3V2rTM6-H_mEC_uyRw": "Stocks",
          Og9vyd9ORL6aF908dO4dWA: "Personnel",
          zNBtfv8vRDuKQ2jjCVynSA: "Tresorerie Financement",
          yxKIZSE4RtOcXR7gZzFIoQ: "Immobilisatiobs incorporelle et corporelles",
          "DAtZRISTSvas2QsCHnJ-Dw": "immobilisations financières",
          OKD_V6k0SaeFI3OuJRuA3Q: "Capitaux propres",
          "glZIDE-tRrKaFl_88-BGmA": "Provisions pour risques et c",
          quTgDJw4SmStWvAq0WzKaA: "Impots et taxes",
          xI88oRMGQSmD0aBkx4p7dA: "Autres creances et dettes",
          "ZVHH8sj2SJm-P4NHDIjpUg": "Resultat exceptionnel",
        };
        let cyclesFR = [];
        let assertionsFR = [];
        let cyclesAsser = [];

        for (let i = 0; i < field("controlsSynth").getRows().length; i++) {
          calcUtils.removeTableRow("SYNTH", "controlsSynth", 0);
        }

        for (let i = 0; i < controls.length; i++) {
          calcUtils.addTableRow("SYNTH", "controlsSynth", i);
          field("controlsSynth").cell(i, 0).setCellLabel(controls[i].name);
          field("controlsSynth")
            .cell(i, 1)
            .setCellLabel(typeControls[controls[i].tags[0]]);

          for (let j = 0; j < controls[i].tags.length; j++) {
            cyclesFR[j] = cycles[controls[i].tags[j]];
            cyclesFR = cyclesFR.filter(Boolean);
          }

          for (let j = 0; j < Object.keys(controls[i].scopedTags).length; j++) {
            assertionsFR[j] = [];
            assertionsFR[j] =
              controls[i].scopedTags[Object.keys(controls[i].scopedTags)[j]];
            for (let k = 0; k < assertionsFR[j].length; k++) {
              assertionsFR[j][k] = assertions[assertionsFR[j][k]];
            }
            assertionsFR[j] = assertionsFR[j].join("");
          }

          for (let i = 0; i < cyclesFR.length; i++) {
            cyclesAsser[i] = `${cyclesFR[i]}/${assertionsFR[i]}`;
          }
          field("controlsSynth")
            .cell(i, 2)
            .setCellLabel(cyclesAsser.join("\n"));

          wpw.tax.getRisks().then(function (risks) {
            let risksName = [];
            for (let j = 0; j < risks.length; j++) {
              for (let k = 0; k < controls[i].risks.length; k++) {
                if (controls[i].risks[k] === risks[j].id) {
                  risksName[k] = risks[j].name;
                }
              }
            }
            field("controlsSynth")
              .cell(i, 3)
              .setCellLabel(risksName.join("\n"));
          });

          field("controlsSynth").cell(i, 4).setCellLabel(key[controls[i].key]);
          field("controlsSynth")
            .cell(i, 5)
            .setCellLabel(exec[controls[i].execution]);
          field("controlsSynth")
            .cell(i, 6)
            .setCellLabel(classification[controls[i].classification]);
          field("controlsSynth")
            .cell(i, 7)
            .setCellLabel(freqControls[controls[i].frequency]);
        }
      });
    });
  });
})(wpw);
