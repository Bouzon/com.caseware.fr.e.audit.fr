(function () {
  "use strict";
  wpw.tax.create.formData("SYNTH", {
    formInfo: {
      abbreviation: "SYNTH",
      dynamicFormWidth: true,
      title: "Synthèse générale",
      highlightFieldsets: false,
      hideheader: true,
      headerImage: "cw",
      css: "se-builder-custom.css",
      category: "Start Here",
      showDots: false,
      documentMap: true,
      enableTabs: true,
      tabLimit: 7,
    },

    sections: [
      {
        header: "1. DEROULEMENT DE LA MISSION",
        tabConfig: {
          tabText: "Mission",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "1.1. Résumé",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "resume",
              },
            ],
          },
          {
            header: "1.2. Etendue de la mission",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label: "Nature de la mission",
                num: "tdm1",
                inputClass: "fullLength",
                type: "dropdown",
                init: "1",
                options: [
                  {
                    option: "Audit légal",
                    value: 1,
                  },
                  {
                    option: "Audit contractuel",
                    value: 2,
                  },
                ],
              },
              {
                label: "Nature des comptes",
                num: "ndc1",
                inputClass: "fullLength",
                type: "dropdown",
                init: "1",
                options: [
                  {
                    option: "Individuels",
                    value: 1,
                  },
                  {
                    option: "Consolidés",
                    value: 2,
                  },
                ],
              },
              {
                type: "table",
                num: "edlmTable",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header: "1.3. Déroulement de la mission",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label: "Composition de l'équipe",
                labelClass: "bold",
              },
              {
                label:
                  'Données venant de la cellule 10.2 dans <a href="./index.jsp#/document/37RIdHtxRvy_dwt1ctOxmw" target="_blank">plan de mission</a>',
              },
              {
                label:
                  'Voir dans <a href="./index.jsp#/checklist/5qevD3eQTXCnRQUK0lMXEA" target="_blank">1.AM.05 Charges et budget</a>',
              },
              {
                type: "textArea",
                num: "compoéquipe",
              },
            ],
          },
          {
            header: "1.4. Difficultés rencontrées",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "input14",
              },
            ],
          },
        ],
      },
      {
        header: "2. EVENEMENTS SIGNIFICATIFS DE L'EXERCICE",
        tabConfig: {
          tabText: "Evé. Sign.",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "2.1. Commentaire",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "commentaire2",
              },
            ],
          },
          {
            header: "2.2. Faits marquants intervenus au cours de l'exercice",
            headerClass: "font-subhaeader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "faitsmarquants",
              },
            ],
          },
        ],
      },
      {
        header: "3. SITUATION FINANCIERE DE L’ENTREPRISE",
        tabConfig: {
          tabText: "Situ. Fin.",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "3.1. Commentaire",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "commentaire3",
              },
            ],
          },
          {
            header: "3.2. Bilan",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label: "Paramètres du tableau",
                type: "splitInputs",
                inputType: "checkbox",
                divisions: "3",
                num: "500",
                showValues: "false",
                items: [
                  { label: "K €", value: "1" },
                  { label: "Masquer les lignes vides", value: "2" },
                ],
              },
              {
                label: "Actif K€",
                labelClass: "bold",
              },
              {
                type: "table",
                num: "actifHeader",
                labelClass: "headerToTable",
              },
              {
                labelClass: "fullLength headerToTable",
              },
              {
                type: "table",
                num: "actif",
                procedureControls: "true",
              },
              {
                labelClass: "fullLength",
              },
              {
                type: "text",
                inputType: "none",
                label: "Commentaire sur l'actif :",
              },
              {
                type: "textArea",
                num: "commentaireactif32",
              },
              {
                label: "Passif K€",
                labelClass: "bold",
              },
              {
                type: "table",
                num: "passifHeader",
              },
              {
                labelClass: "fullLength",
              },
              {
                type: "table",
                num: "passif",
                procedureControls: "true",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header: "3.3. Comptes de résultat",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "table",
                num: "compteresultatHeader",
              },
              {
                labelClass: "fullLength",
              },
              {
                type: "table",
                num: "compteresultat",
                procedureControls: "true",
              },
              {
                labelClass: "fullLength",
              },
              {
                label: "Commentaire sur les produits : ",
              },
              {
                type: "textArea",
                num: "commentaireproduits33",
              },
              {
                label: "Commentaire sur les charges :",
              },
              {
                type: "textArea",
                num: "commentairecharges33",
              },
            ],
          },
          {
            header:
              "3.4. Points marquants résultant de l'analyse des chiffres significatifs",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label: "Commentaire sur le bilan:",
              },
              {
                type: "textArea",
                num: "commentairebilan",
              },
              {
                label: "Commentaire sur le résultat:",
              },
              {
                type: "textArea",
                num: "commentaireresultat",
              },
              {
                label: "Commentaire sur le SIG",
              },
              {
                type: "textArea",
                num: "commentairesig",
              },
              {
                label: "Commentaire sur le tableau de financement",
              },
              {
                type: "textArea",
                num: "commentairetableaufinancement",
              },
              {
                label: "Commentaire sur le tableau des flux de trésorerie",
              },
              {
                type: "textArea",
                num: "commentairefluxtresorerie",
              },
            ],
          },
          {
            header:
              "3.5. Rappel des seuils de Signification et de Planification",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/document/wMgdLJ0JQxW1qx--pfJq_Q" target="_blank">2.SE Seuils</a>',
              },
              {
                type: "table",
                num: "rappeldesseuilsSYNTH",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
        ],
      },
      {
        header: "Ctrl. Int.",
        headerClass: "font-header",
        rows: [
          {
            header: "4.1. Commentaire",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "commentaire4",
              },
            ],
          },
          {
            header: "4.2. Liste des contrôles",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "table",
                num: "controlsSynth",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
        ],
      },
      {
        header: "5. FRAUDE",
        tabConfig: {
          tabText: "Fraude",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "5.1. Commentaire",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "commentaire5",
              },
            ],
          },
          {
            header: "5.2. Risque de fraude",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/EDEHjsNkQmGCp49ENZWgCQ" target="_blank">2.RI.06 Risque de Fraude</a>',
              },
              {
                type: "textArea",
                num: "conclusion52",
                disabled: true,
              },
            ],
          },
          {
            header:
              "5.3. Lutte contre le blanchiment et le financement du terrorisme",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/9aNbhn3uRUKSPJ_Wt8DItQ" target="_blank">2.RI.05 Risque de blanchiment et financement du terrorisme</a>',
              },
              {
                type: "textArea",
                num: "conclusion53",
                disabled: true,
              },
            ],
          },
        ],
      },
      {
        header: "6. CHANGEMENT DE METHODE COMPTABLE",
        tabConfig: {
          tabText: "Chgt. Met.",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "6.1. Commentaire",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "commentaire6",
              },
            ],
          },
          {
            header: "6.2. Particularités Comptables",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
              },
            ],
          },
          {
            header: "6.3. Changement de méthode",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
              },
            ],
          },
        ],
      },
      {
        header: "7. RISQUES",
        tabConfig: {
          tabText: "Risques",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "7.1. Commentaire",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "commentaire7",
              },
            ],
          },
          {
            header: "7.2. Environnement de l'entité",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/N7h6R_WFRpaoZD4iiCPQXA" target="_blank">2.RI.01 Connaissance de l\'entité</a>',
              },
              {
                type: "textArea",
                num: "conclusion72",
                disabled: true,
              },
            ],
          },
          {
            header: "7.3. Liste des risques identifiés",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "table",
                num: "risque73",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
        ],
      },
      {
        header: "8. TRAVAUX PAR CYCLES",
        tabConfig: {
          tabText: "Cycles",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "8.1. Commentaire",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "commentaire8",
              },
            ],
          },
          {
            header: "8.2. Général et revue du CO-CAC",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label: "Conclusion du programme de travail général",
                labelClass: "bold",
              },
              {
                type: "textArea",
                num: "conclusion821",
                disabled: true,
              },
              {
                label: "Conclusion de la revue des travaux du confrère",
                labelClass: "bold",
              },
              {
                type: "textArea",
                num: "conclusion822",
                disabled: true,
              },
            ],
          },
          {
            header: "8.3. A - Achats / Fournisseurs",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/YwrhpTj9Rz2LwPcvL5-RbQ" target="_blank">3.A.00 PT Achats / Fournisseurs</a>',
              },
              {
                label: "Conclusion",
                labelClass: "bold",
              },
              {
                type: "textArea",
                num: "conclusion83",
                disabled: true,
              },
            ],
          },
          {
            header: "8.4. B - Ventes / Clients",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/CG7Y4WCJTrG6xxncKMpLdA" target="_blank">3.B.00 PT Ventes / Clients</a>',
              },
              {
                label: "Conclusion",
                labelClass: "bold",
              },
              {
                type: "textArea",
                num: "conclusion84",
                disabled: true,
              },
            ],
          },
          {
            header: "8.5. C - Stocks",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/VBvXlytsQ0m18QBZ8Zo_4g" target="_blank">3.C.00 PT Stocks</a>',
              },
              {
                label: "Conclusion",
                labelClass: "bold",
              },
              {
                type: "textArea",
                num: "conclusion85",
                disabled: true,
              },
            ],
          },
          {
            header: "8.6. D - Personnel",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/IFmR3eHVR_OrlD1WCuOWng" target="_blank">3.D.00 PT Personnel</a>',
              },
              {
                label: "Conclusion",
                labelClass: "bold",
              },
              {
                type: "textArea",
                num: "conclusion86",
                disabled: true,
              },
            ],
          },
          {
            header: "8.7. E - Banque et Financement",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/mbJGvhVFTzaW8dpxoiUcIg" target="_blank">3.E.00 PT Trésorerie / Financement</a>',
              },
              {
                label: "Conclusion",
                labelClass: "bold",
              },
              {
                type: "textArea",
                num: "conclusion87",
                disabled: true,
              },
            ],
          },
          {
            header: "8.8. F – Immobilisations corporelles et incorporelles",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/Wk_weK-CQUOhBPbv0Z8uAw" target="_blank">3.F.00 PT Immobilisations</a>',
              },
              {
                label: "Conclusion",
                labelClass: "bold",
              },
              {
                type: "textArea",
                num: "conclusion88",
                disabled: true,
              },
            ],
          },
          {
            header: "8.9. G – Immobilisations financières",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/hLOPax4JSA-Gxgy_estr1Q" target="_blank">3.G.00 PT Immo financières</a>',
              },
              {
                label: "Conclusion",
                labelClass: "bold",
              },
              {
                type: "textArea",
                num: "conclusion89",
                disabled: true,
              },
            ],
          },
          {
            header: "8.10. H – Capitaux propres",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/A0mxBcdnSjeXQfCwYeVNEw" target="_blank">3.H.00 PT Capitaux propres</a>',
              },
              {
                label: "Conclusion",
                labelClass: "bold",
              },
              {
                type: "textArea",
                num: "conclusion810",
                disabled: true,
              },
            ],
          },
          {
            header: "8.11. I – Provisions pour risques et charges",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/poUaG80dQh2SmrVhgZ_qtQ" target="_blank">3.I.00 PT Provisions pour RC</a>',
              },
              {
                label: "Conclusion",
                labelClass: "bold",
              },
              {
                type: "textArea",
                num: "conclusion811",
                disabled: true,
              },
            ],
          },
          {
            header: "8.12. J – Impôts et taxes",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/4Q46ZlXQT2WE5M5yu08B3w" target="_blank">3.J.00 PT Impôts et taxes</a>',
              },
              {
                label: "Conclusion",
                labelClass: "bold",
              },
              {
                type: "textArea",
                num: "conclusion812",
                disabled: true,
              },
            ],
          },
          {
            header: "8.13 K – autres créances et autres dettes",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/qd9tI8rmSJuoqKp5TPJiHg" target="_blank">3.K.00 PT Autres créances et dettes</a>',
              },
              {
                label: "Conclusion",
                labelClass: "bold",
              },
              {
                type: "textArea",
                num: "conclusion813",
                disabled: true,
              },
            ],
          },
          {
            header: "8.14 L – Hors bilan",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/h95gyaH9R8OISrZmTV78uw" target="_blank">3.L.00 PT Hors Bilan</a>',
              },
              {
                label: "Conclusion",
                labelClass: "bold",
              },
              {
                type: "textArea",
                num: "conclusion814",
                disabled: true,
              },
            ],
          },
          {
            header: "8.15 M – Résultat Exceptionnel",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/6GKDMgvBRrKtVIKdc1mdvw" target="_blank">3.M.00 PT Résultat Exceptionnel</a>',
              },
              {
                label: "Conclusion",
                labelClass: "bold",
              },
              {
                type: "textArea",
                num: "conclusion815",
                disabled: true,
              },
            ],
          },
        ],
      },
      {
        header: "9. POINTS D'AUDIT",
        tabConfig: {
          tabText: "Pts Audit",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "9.1. Commentaire",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "commentaire9",
              },
            ],
          },
          {
            header: "9.2. Points d'Audit N",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "table",
                num: "taches93",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
        ],
      },
      {
        header: "10. AJUSTEMENTS",
        tabConfig: {
          tabText: "Ajust.",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "10.1. Commentaire",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "commentaire10",
              },
            ],
          },
          {
            header: "10.2. Synthèse des Ajustements",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  '<a href="./index.jsp#/document/SuseGg0MS6eOf9YP0LmIUw" target="_blank">Voir 4.SR.02 Synthèse des ajustements</a>',
                labelClass: "title",
              },
            ],
          },
          {
            header: "10.3. Liste des Ajustements",
            headerClass: "font-subheader font-blue",
            subsection: [],
          },
          {
            header: "10.4. Suivi des Ajustements N-1",
            headerClass: "font-subheader font-blue",
            subsection: [],
          },
        ],
      },
      {
        header: "11. POINTS EN SUSPENS ET A SUIVRE",
        tabConfig: {
          tabText: "Suspens",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "11.1. Commentaire",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "commentaire11",
              },
            ],
          },
          {
            header: "11.2. Points en Suspens",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "table",
                num: "alltaches",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header: "11.3. Points à Suivre",
            headerClass: "font-subheader font-blue",
            subsection: [],
          },
        ],
      },
      {
        header: "12. VERIFICATIONS SPECIFIQUES",
        tabConfig: {
          tabText: "Spécif.",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "12.1. Commentaire",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "commentaire12",
              },
            ],
          },
          {
            header: "12.2. Evènements postérieurs",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/ShOxTCstQo6eZys7Dg0gAQ" target="_blank">4.VS.04 Événements postérieurs</a>',
              },
              {
                type: "textArea",
                num: "conclusion122",
                disabled: true,
              },
            ],
          },
          {
            header: "12.3. Vérification de l'annexe",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/Zm9jc8ZWQEuPhno-FIDjGg" target="_blank">4.VS.01 Contrôle de l\'annexe</a>',
              },
              {
                type: "textArea",
                num: "conclusion123",
                disabled: true,
              },
            ],
          },
          {
            header: "12.4. Continuité d'exploitation",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/HPV8QOpHTAqt-VsrwRpqGQ" target="_blank">4.VS.05 Continuité de l\'exploitation</a>',
              },
              {
                type: "textArea",
                num: "conclusion124",
                disabled: true,
              },
            ],
          },
          {
            header: "12.5. Contrôle du rapport de gestion",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/_CBiFsU_RUeJiTam3WF66w" target="_blank">4.VS.02 Rapport de gestion</a>',
              },
              {
                type: "textArea",
                num: "conclusion125",
                disabled: true,
              },
            ],
          },
          {
            header: "12.6. Conventions règlementées",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/checklist/yn7oTX1aTAuoA_wMlnQPEg" target="_blank">4.VS.06 Conventions reglementées</a>',
              },
              {
                type: "textArea",
                num: "conclusion126",
                disabled: true,
              },
            ],
          },
        ],
      },
      {
        header: "13. OPINION",
        tabConfig: {
          tabText: "Opinion",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "13.1. Commentaire",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "commentaire13",
              },
            ],
          },
          {
            header: "13.2. Opinion",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "input132",
              },
            ],
          },
        ],
      },
      {
        header: "14. AUTRES DILIGENCES",
        tabConfig: {
          tabText: "Autres",
        },
        headerClass: "font-header",
        rows: [
          {
            header: "14.1. Commentaire",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "commentaire14",
              },
            ],
          },
          {
            header: "14.2. Autres Diligences",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                type: "textArea",
                num: "input142",
              },
            ],
          },
        ],
      },
    ],
  });
})();
