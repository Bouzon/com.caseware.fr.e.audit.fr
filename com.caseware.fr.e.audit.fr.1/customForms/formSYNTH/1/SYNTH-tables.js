(function () {
  wpw.tax.create.tables("SYNTH", {
    edlmTable: {
      fixedRows: true,
      infoTable: false,
      staticTable: true,
      columns: [
        {
          type: "text",
          colClass: "alignCenter",
          header: "Nominations",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "text",
          colClass: "alignCenter",
          header: "Titulaire",
          headerClass: "tableHeaderCells",
        },
        {
          type: "text",
          colClass: "alignCenter",
          header: "Signataire",
          headerClass: "tableHeaderCells",
        },
        {
          type: "date",
          colClass: "alignCenter",
          header: "Date nomination",
          headerClass: "tableHeaderCells",
        },
        {
          type: "date",
          colClass: "alignCenter",
          header: "Date cloture dernier exercice contrôlé",
          headerClass: "tableHeaderCells",
        },
        {
          type: "text",
          colClass: "alignCenter",
          header: "Réf",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [],
    },
    actifHeader: {
      fixedRows: true,
      staticTable: true,
      infoTable: true,
      columns: [
        {
          width: "35%",
        },
        {
          width: "10%",
        },
        {
          width: "10%",
        },
        {
          width: "10%",
        },
        {
          width: "35%",
        },
      ],
      cells: [
        {
          0: { label: " ", labelClass: "center", type: "none" },
          1: {
            label: "N",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells leftCornerRounded",
            type: "none",
            labelSource: {
              fieldId: "yearN",
            },
          },
          2: {
            label: "N-1",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells",
            type: "none",
            labelSource: {
              fieldId: "yearN-1",
            },
          },
          3: {
            label: "Variations",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells rightCornerRounded",
            type: "none",
            labelSource: {
              fieldId: "dates",
            },
          },
          4: {
            label: "  ",
            labelClass: "center",
            type: "none",
          },
        },
      ],
    },
    actif: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          width: "35%",
          header: "Bilan Actif",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "K",
          headerSource: { fieldId: "NK€" },
          headerClass: "tableHeaderCells",
          width: "5%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "%",
          headerClass: "tableHeaderCells",
          width: "5%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "K",
          headerSource: { fieldId: "NM1K€" },
          headerClass: "tableHeaderCells",
          width: "5%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "%",
          headerClass: "tableHeaderCells",
          width: "5%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "5%",
          header: "Montant",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "5%",
          header: "%",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "35%",
          header: "Commentaire",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: "label",
            colClass: "testCell",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label1" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "csnaNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "csnaN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "csnaNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "csnaNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "csnaVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "csnaVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "csnaCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label2" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "iiNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "iiN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "iiNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "iiNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "iiVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "iiVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "iiCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label3" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "icNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "icN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "icNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "icNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "icVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "icVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "icCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label4" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "ifNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "ifN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "ifNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "ifNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "ifVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "ifVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "ifCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "label5" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "taiNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "taiN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "taiNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "taiNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "taiVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "taiVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "taiCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label6" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "seecNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "seecN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "seecNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "seecNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "seecVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "seecVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "seecCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label7" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "cNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "cN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "cNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "cNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "cVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "cVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "cCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label8" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "vmdpNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "vmdpN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "vmdpNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "vmdpNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "vmdpVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "vmdpVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "vmdpCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label9" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "instruNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "instruN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "instruNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "instruNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "instruVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "instruVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "instruCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label10" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "dNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "dN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "dNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "dNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "dVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "dVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "dCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label11" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "ccaNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "ccaN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "ccaNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "ccaNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "ccaVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "ccaVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "ccaCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label12" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "aeavscNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "aeavscN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "aeavscNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "aeavscNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "aeavscVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "aeavscVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "aeavscCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "label13" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "tacecNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "tacecN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "tacecNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "tacecNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "tacecVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "tacecVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "tacecCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label14" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "crspeNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "crspeN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "crspeNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "crspeNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "crspeVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "crspeVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "crspeCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label15" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "preNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "preN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "preNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "preNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "preVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "preVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "preCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "label16" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "ecaNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "ecaN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "ecaNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "ecaNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "ecaVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "ecaVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "ecaCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "label17" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "tbaNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "tbaN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "tbaNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "tbaNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "tbaVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "tbaVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "tbaCOM" },
        },
      ],
    },
    passifHeader: {
      fixedRows: true,
      staticTable: true,
      infoTable: true,
      columns: [
        {
          width: "35%",
        },
        {
          width: "10%",
        },
        {
          width: "10%",
        },
        {
          width: "10%",
        },
        {
          width: "35%",
        },
      ],
      cells: [
        {
          0: { label: " ", labelClass: "center", type: "none" },
          1: {
            label: "N",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells leftCornerRounded",
            type: "none",
            labelSource: {
              fieldId: "yearN",
            },
          },
          2: {
            label: "N-1",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells",
            type: "none",
            labelSource: {
              fieldId: "yearN-1",
            },
          },
          3: {
            label: "Variations",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells rightCornerRounded",
            type: "none",
            labelSource: {
              fieldId: "dates",
            },
          },
          4: {
            label: "  ",
            labelClass: "center",
            type: "none",
          },
        },
      ],
    },
    passif: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          width: "35%",
          header: "Bilan Passif",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "5%",
          header: "K",
          headerSource: { fieldId: "NK€" },
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "5%",
          header: "%",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "5%",
          header: "K",
          headerSource: { fieldId: "NM1K€" },
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "5%",
          header: "%",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "5%",
          header: "Montant",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "5%",
          header: "%",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "35%",
          header: "Commentaire",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif1" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "capiPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "capiPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "capiPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "capiPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "capiVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "capiVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "capiCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif2" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "primesPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "primesPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "primesPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "primesPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "primesVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "primesVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "primesCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif3" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "ecartsreevalPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "ecartsreevalPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "ecartsreevalPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "ecartsreevalPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "ecartsreevalVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "ecartsreevalVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "ecartsreevalCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif4" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "ecartequivalencePNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "ecartequivalencePN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "ecartequivalencePNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "ecartequivalencePNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "ecartequivalenceVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "ecartequivalenceVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "ecartequivalenceCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif5" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "reservesPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "reservesPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "reservesPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "reservesPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "reservesVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "reservesVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "reservesCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif6" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "reportnouveauPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "reportnouveauPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "reportnouveauPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "reportnouveauPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "reportnouveauVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "reportnouveauVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "reportnouveauCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif7" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "resultatexercicePNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "resultatexercicePN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "resultatexercicePNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "resultatexercicePNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "resultatexerciceVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "resultatexerciceVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "resultatexerciceCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif8" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "subventionPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "subventionPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "subventionPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "subventionPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "subventionVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "subventionVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "subventionCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif9" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "provisionPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "provisionPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "provisionPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "provisionPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "provisionVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "provisionVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "provisionCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "labelPassif10" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "capitauxPNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "capitauxPN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "capitauxPNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "capitauxPNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "capitauxVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "capitauxVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "capitauxCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "labelPassif11" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "autresfondsPNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "autresfondsPN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "autresfondsPNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "autresfondsPNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "autresfondsVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "autresfondsVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "autresfondsCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "labelPassif12" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "provisionsPNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "provisionsPN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "provisionsPNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "provisionsPNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "provisionsVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "provisionsVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "provisionsCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif13" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "eocPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "eocPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "eocPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "eocPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "eocVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "eocVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "eocCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif14" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "aeoPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "aeoPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "aeoPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "aeoPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "aeoVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "aeoVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "aeoCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif15" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "edecPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "edecPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "edecPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "edecPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "edecVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "edecVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "edecCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif16" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "edfdPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "edfdPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "edfdPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "edfdPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "edfdVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "edfdVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "edfdCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif17" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "dfcrPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "dfcrPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "dfcrPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "dfcrPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "dfcrVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "dfcrVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "dfcrCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif18" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "dfsPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "dfsPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "dfsPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "dfsPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "dfsVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "dfsVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "dfsCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif19" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "dicrPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "dicrPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "dicrPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "dicrPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "dicrVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "dicrVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "dicrCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif20" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "autresdettesPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "autresdettesPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "autresdettesPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "autresdettesPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "autresdettesVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "autresdettesVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "autresdettesCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif21" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "itPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "itPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "itPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "itPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "itVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "itVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "itCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif22" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "pcaPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "pcaPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "pcaPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "pcaPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "pcaVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "pcaVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "pcaCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelPassif23" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "ecpPNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "ecpPN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "ecpPNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "ecpPNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "ecpVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "ecpVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "ecpCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "labelPassif24" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "dettesPNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "dettesPN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "dettesPNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "dettesPNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "dettesVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "dettesVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "dettesCOM" },
        },
        {
          0: {
            label: "label",
            labelSource: { fieldId: "labelPassif25" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "passifPNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "passifPN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "passifPNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "passifPNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "passifVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "passifVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "passifCOM" },
        },
      ],
    },
    controlsSynth: {
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          type: "none",
          colClass: "std-spacing-width",
          header: "Titre",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          colClass: "std-spacing-width",
          header: "Type",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          colClass: "std-spacing-width",
          header: "Cycles/assertions",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          colClass: "std-spacing-width",
          header: "Risques associés",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          colClass: "std-spacing-width",
          header: "Contrôle clé",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          colClass: "std-spacing-width",
          header: "Exécution",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          colClass: "std-spacing-width",
          header: "Classification",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          colClass: "std-spacing-width",
          header: "Fréquence",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [],
    },
    compteresultatHeader: {
      fixedRows: true,
      staticTable: true,
      infoTable: true,
      columns: [
        {
          width: "35%",
        },
        {
          width: "10%",
        },
        {
          width: "10%",
        },
        {
          width: "10%",
        },
        {
          width: "35%",
        },
      ],
      cells: [
        {
          0: { label: " ", labelClass: "center", type: "none" },
          1: {
            label: "N",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells leftCornerRounded",
            type: "none",
            labelSource: {
              fieldId: "yearN",
            },
          },
          2: {
            label: "N-1",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells",
            type: "none",
            labelSource: {
              fieldId: "yearN-1",
            },
          },
          3: {
            label: "Variations",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells rightCornerRounded",
            type: "none",
            labelSource: {
              fieldId: "dates",
            },
          },
          4: {
            label: "  ",
            labelClass: "center",
            type: "none",
          },
        },
      ],
    },
    compteresultat: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          width: "35%",
          header: "Compte de Résultat",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "5%",
          header: "K",
          headerSource: { fieldId: "NK€" },
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "5%",
          header: "%",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "5%",
          header: "K",
          headerSource: { fieldId: "NK€" },
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "5%",
          header: "%",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "5%",
          header: "Montant",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "5%",
          header: "%",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          width: "35%",
          header: "Commentaire",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelcompteresultat1" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "peNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "peNK%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "peNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "peNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "peVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "peVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "peCOM", cellClass: "textArea" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelcompteresultat2" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "ceNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "ceNK%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "ceNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "ceNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "ceVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "ceVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "ceCOM" },
        },
        {
          0: {
            label: "Resultat d’exploitation",
            labelSource: { fieldId: "labelcompteresultat3" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "reNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "reN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "reNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "reNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "reVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "reVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "reCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelcompteresultat4" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "bptNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "bptN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "bptNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "bptNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "bptVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "bptVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "bptCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelcompteresultat5" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "pbtNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "pbtN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "pbtNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "pbtNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "pbtVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "pbtVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "pbtCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelcompteresultat6" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "pfNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "pfN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "pfNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "pfNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "pfVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "pfVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "pfCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelcompteresultat7" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "cfNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "cfN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "cfNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "cfNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "cfVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "cfVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "cfCOM" },
        },
        {
          0: {
            label: "Résultat financier",
            labelSource: { fieldId: "labelcompteresultat8" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "rfNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "rfN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "rfNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "rfNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "rfVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "rfVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "rfCOM" },
        },
        {
          0: {
            label: "Resultat courant avant impôt ",
            labelSource: { fieldId: "labelcompteresultat9" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "rcaiNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "rcaiN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "rcaiNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "rcaiNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "rcaiVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "rcaiVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "rcaiCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelcompteresultat10" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "pexcepNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "pexcepN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "pexcepNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "pexcepNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "pexcepVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "pexcepVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "pexcepCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelcompteresultat11" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "psrNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "psrN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "psrNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "psrNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "psrVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "psrVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "psrCOM" },
        },
        {
          0: {
            label: "label",
            cellClass: "labelLeft",
            labelSource: { fieldId: "labelcompteresultat12" },
          },
          1: {
            label: " ",
            labelSource: { fieldId: "ibNK" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "ibN%" },
            cellClass: "labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "ibNM1K" },
            cellClass: "labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "ibNM1%" },
            cellClass: "labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "ibVAR" },
            cellClass: "labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "ibVAR%" },
            cellClass: "labelRight italicLabel",
          },
          7: { type: "textArea", num: "ibCOM" },
        },
        {
          0: {
            label: "Résultat Net",
            labelSource: { fieldId: "labelcompteresultat13" },
            cellClass: "boldTextBackground labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "rnNK" },
            cellClass: "boldTextBackground labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "rnN%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "rnNM1K" },
            cellClass: "boldTextBackground labelRight",
          },
          4: {
            label: " ",
            labelSource: { fieldId: "rnNM1%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          5: {
            label: " ",
            labelSource: { fieldId: "rnVAR" },
            cellClass: "boldTextBackground labelRight",
          },
          6: {
            label: " ",
            labelSource: { fieldId: "rnVAR%" },
            cellClass: "boldTextBackground labelRight italicLabel",
          },
          7: { type: "textArea", num: "rnCOM" },
        },
      ],
    },
    rappeldesseuilsSYNTH: {
      fixedRows: true,
      infoTable: false,
      staticTable: true,
      rowDeleteConfirmation: true,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Période",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Seuils de Signification",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Seuils de Planification",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Anomalies insignifiantes",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: "Période précédente",
            cellClass: "labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "periodePrecedenteSYNTHSI" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "periodePrecedenteSYNTHPlan" },
            cellClass: "labelRight",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "periodePrecedenteSYNTHInsi" },
            cellClass: "labelRight",
          },
        },
        {
          0: {
            label: "Préliminaire",
            cellClass: "labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "preliSYNTHSI" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "preliSYNTHPlan" },
            cellClass: "labelRight",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "preliSYNTHInsi" },
            cellClass: "labelRight",
          },
        },
        {
          0: {
            label: "Final",
            cellClass: "labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "FinalSYNTHSI" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "FinalSYNTHPlan" },
            cellClass: "labelRight",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "FinalSYNTHInsi" },
            cellClass: "labelRight",
          },
        },
      ],
    },
    risque73: {
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          type: "none",
          header: "Libellé",
          headerClass: "tableHeaderCells leftCornerRounded",
          colClass: "alignCenter",
        },
        {
          type: "none",
          header: "Cycles/assertions",
          headerClass: "tableHeaderCells",
          colClass: "alignCenter",
          width: "350px",
        },
        {
          type: "none",
          header: "Type",
          headerClass: "tableHeaderCells",
          colClass: "alignCenter",
          width: "80px",
        },
        {
          type: "none",
          header: "Réponse entité",
          headerClass: "tableHeaderCells",
          colClass: "alignCenter",
        },
        {
          type: "none",
          header: "Rep Audit",
          headerClass: "tableHeaderCells rightCornerRounded",
          colClass: "alignCenter",
        },
      ],
      cells: [],
    },
    taches93: {
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          header: "Libellé",
          headerClass: "tableHeaderCells leftCornerRounded",
          type: "text",
          width: "10%",
          disabled: true,
        },
        {
          header: "Description / resolution",
          headerClass: "tableHeaderCells",
          type: "textArea",
          width: "70%",
          disabled: true,
        },
        {
          header: "Statut",
          headerClass: "tableHeaderCells",
          type: "text",
          width: "10%",
          disabled: true,
        },
        {
          header: "Date de fin",
          headerClass: "tableHeaderCells rightCornerRounded",
          type: "text",
          width: "10%",
          disabled: true,
        },
      ],
      cells: [],
    },
    alltaches: {
      fixedRows: true,
      infoTable: false,
      columns: [
        {
          header: "Libellé",
          headerClass: "tableHeaderCells leftCornerRounded",
          type: "none",
          width: "10%",
          disabled: true,
        },
        {
          header: "Description / resolution",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "70%",
          disabled: true,
        },
        {
          header: "Statut",
          headerClass: "tableHeaderCells",
          type: "none",
          width: "10%",
          disabled: true,
        },
        {
          header: "Date de fin",
          headerClass: "tableHeaderCells rightCornerRounded",
          type: "none",
          width: "10%",
          disabled: true,
        },
      ],
      cells: [],
    },
  });
})();
