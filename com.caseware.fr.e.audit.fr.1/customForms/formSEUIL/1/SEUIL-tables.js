(function () {
  wpw.tax.create.tables("SEUIL", {
    résuméseuils: {
      staticTable: true,
      fixedRows: true,
      infoTable: false,
      rowDeleteConfirmation: true,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Période",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Seuils de Signification",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Seuils de Planification",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Anomalies insignifiantes",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: "Période précédente",
            cellClass: "labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "periodePrecedenteSEUILSI" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "periodePrecedenteSEUILPlan" },
            cellClass: "labelRight",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "periodePrecedenteSEUILInsi" },
            cellClass: "labelRight",
          },
        },
        {
          0: {
            label: "Préliminaire",
            cellClass: "labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "preliSEUILSI" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "preliSEUILPlan" },
            cellClass: "labelRight",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "preliSEUILInsi" },
            cellClass: "labelRight",
          },
        },
        {
          0: {
            label: "Final",
            cellClass: "labelLeft",
          },
          1: {
            label: " ",
            labelSource: { fieldId: "FinalSEUILSI" },
            cellClass: "labelRight",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "FinalSEUILPlan" },
            cellClass: "labelRight",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "FinalSEUILInsi" },
            cellClass: "labelRight",
          },
        },
      ],
    },
    utilisateursetatfinancier: {
      fixedRows: true,
      infoTable: false,
      staticTable: true,
      rowDeleteConfirmation: true,
      columns: [
        {
          type: "none",
          header: "#",
          headerClass: "tableHeaderCells leftCornerRounded",
          width: "2%",
        },
        {
          type: "text",
          header: "Utilisateurs",
          headerClass: "tableHeaderCells",
        },
        {
          type: "textArea",
          header: "Facteurs qui pourraient influencer la prise de décision",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: "1",
            cellClass: "labelLeft",
          },
        },
        {
          0: {
            label: "2",
            cellClass: "labelLeft",
          },
        },
        {
          0: {
            label: "3",
            cellClass: "labelLeft",
          },
        },
        {
          0: {
            label: "4",
            cellClass: "labelLeft",
          },
        },
        {
          0: {
            label: "5",
            cellClass: "labelLeft",
          },
        },
      ],
    },
    facteursinfluence: {
      fixedRows: true,
      staticTable: true,
      columns: [
        {
          type: "none",
          header: "Facteurs d'influence",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          header: "Commentaire",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          header: "Niveau de risque",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: "Risque global de la mission",
            labelClass: "boldText",
          },
          1: {
            type: "textArea",
            num: "facteursinfluenceCOM0",
          },
          2: {
            type: "dropdown",
            init: "0",
            num: "lvlrisk0",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
        },
        {
          0: {
            label: "Structure des comptes de l'entité",
            labelClass: "boldText",
          },
          1: {
            type: "textArea",
            num: "facteursinfluenceCOM1",
          },
          2: {
            type: "dropdown",
            init: "0",
            num: "lvlrisk1",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
        },
        {
          0: {
            label: "Secteur d'activité de l'entité",
            labelClass: "boldText",
          },
          1: {
            type: "textArea",
            num: "facteursinfluenceCOM2",
          },
          2: {
            type: "dropdown",
            init: "0",
            num: "lvlrisk2",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
        },
        {
          0: {
            label: "Structure de l'actionnariat de l'entité",
            labelClass: "boldText",
          },
          1: {
            type: "textArea",
            num: "facteursinfluenceCOM3",
          },
          2: {
            type: "dropdown",
            init: "0",
            num: "lvlrisk3",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
        },
        {
          0: {
            label: "Structure du financement de l'entité",
            labelClass: "boldText",
          },
          1: {
            type: "textArea",
            num: "facteursinfluenceCOM4",
          },
          2: {
            type: "dropdown",
            init: "0",
            num: "lvlrisk4",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
        },
        {
          0: {
            label: "Résultat faible ou déficit",
            labelClass: "boldText",
          },
          1: {
            type: "textArea",
            num: "facteursinfluenceCOM5",
          },
          2: {
            type: "dropdown",
            init: "0",
            num: "lvlrisk5",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
        },
        {
          0: {
            label: "Variabilité de ces critères dans le temps",
            labelClass: "boldText",
          },
          1: {
            type: "textArea",
            num: "facteursinfluenceCOM6",
          },
          2: {
            type: "dropdown",
            init: "0",
            num: "lvlrisk6",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
        },
        {
          0: {
            label: "Entreprise susceptible d'être cédée",
            labelClass: "boldText",
          },
          1: {
            type: "textArea",
            num: "facteursinfluenceCOM7",
          },
          2: {
            type: "dropdown",
            init: "0",
            num: "lvlrisk7",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
        },
        {
          0: {
            label: "Capitaux propres proches des 50% du capital",
            labelClass: "boldText",
          },
          1: {
            type: "textArea",
            num: "facteursinfluenceCOM8",
          },
          2: {
            type: "dropdown",
            init: "0",
            num: "lvlrisk8",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
        },
        {
          0: {
            label: "Entreprise en difficulté",
            labelClass: "boldText",
          },
          1: {
            type: "textArea",
            num: "facteursinfluenceCOM9",
          },
          2: {
            type: "dropdown",
            init: "0",
            num: "lvlrisk9",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
        },
      ],
    },
    facteursinfluenceResult: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      columns: [
        {
          type: "none",
          header: "Moyenne de niveau de risque",
          headerClass: "tableHeaderCells ",
        },
        {
          type: "none",
          header: "Taux à appliquer",
          headerClass: "tableHeaderCells",
        },
      ],
      cells: [
        {
          0: {
            label: " ",
            labelClass: "center",
            type: "none",
            labelSource: {
              fieldId: "MoyenneRisque",
            },
          },
          1: {
            type: "dropdown",
            init: "0",
            num: "661",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Taux risque très faible",
                value: 1,
              },
              {
                option: "Taux risque faible",
                value: 2,
              },
              {
                option: "Taux risque moyen",
                value: 3,
              },
              {
                option: "Taux risque élevé",
                value: 4,
              },
            ],
          },
        },
      ],
    },
    tauxretenuheader: {
      fixedRows: true,
      infoTable: false,
      rowDeleteConfirmation: true,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Critères",
          headerClass: "tableHeaderCells leftCornerRounded",
          width: "20.5%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Soldes en K€",
          headerClass: "tableHeaderCells",
          width: "22.2%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: " ",
          headerClass: "tableHeaderCells",
          width: "6.6%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Variabilité",
          headerClass: "tableHeaderCells",
          width: "13%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Seuil en K€",
          headerClass: "tableHeaderCells",
          width: "22.3%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: " ",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [],
    },
    tauxretenu: {
      fixedRows: true,
      infoTable: false,
      staticTable: true,
      rowDeleteConfirmation: true,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Description critère",
          headerClass: "tableHeaderCells",
          width: "15.4%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Taux",
          headerClass: "tableHeaderCells",
          width: "5%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "N",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "N-1",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "N-2",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Critère paramétré",
          headerClass: "tableHeaderCells",
          width: "6.5%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Taux stable",
          headerClass: "tableHeaderCells",
          width: "6.5%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Critère à retenir",
          headerClass: "tableHeaderCells",
          width: "6.5%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "N",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "N-1",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "N-2",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Com.",
          headerClass: "tableHeaderCells",
          width: "15.4%",
        },
      ],
      cells: [
        {
          0: {
            label: "Résultat de l'ex.",
          },
          1: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "tauxAppliqué1" },
          },
          2: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNResutEX" },
          },
          3: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNM1ResutEX" },
          },
          4: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNM2ResutEX" },
          },
          5: {
            type: "checkbox",
          },
          6: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "tauxStableRetenu1" },
          },
          7: {
            type: "checkbox",
            num: "critèreàretenir1",
          },
          8: {
            label: "-",
            labelSource: { fieldId: "seuilNResutEX" },
            cellClass: "labelRight italicLabel",
          },
          9: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNM1ResutEX" },
          },
          10: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNM2ResutEX" },
          },
          11: {
            type: "textArea",
          },
        },
        {
          0: {
            label: "Résultat courant avant impôt",
          },
          1: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "tauxAppliqué2" },
          },
          2: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNRcai" },
          },
          3: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNM1Rcai" },
          },
          4: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNM2Rcai" },
          },
          5: {
            type: "checkbox",
          },
          6: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "tauxStableRetenu2" },
          },
          7: {
            type: "checkbox",
            num: "critèreàretenir2",
          },
          8: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNRcai" },
          },
          9: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNM1Rcai" },
          },
          10: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNM2Rcai" },
          },
          11: {
            type: "textArea",
          },
        },
        {
          0: {
            label: "Chiffre d'affaires",
          },
          1: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "tauxAppliqué3" },
          },
          2: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNCA" },
          },
          3: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNM1CA" },
          },
          4: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNM2CA" },
          },
          5: {
            type: "checkbox",
            num: "critèreparamétréCA",
          },
          6: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "tauxStableRetenu3" },
          },
          7: {
            type: "checkbox",
            num: "critèreàretenir3",
          },
          8: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNCA" },
          },
          9: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNM1CA" },
          },
          10: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNM2CA" },
          },
          11: {
            type: "textArea",
          },
        },
        {
          0: {
            label: "Capitaux propres",
          },
          1: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "tauxAppliqué4" },
          },
          2: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNCP" },
          },
          3: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNM1CP" },
          },
          4: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNM2CP" },
          },
          5: {
            type: "checkbox",
          },
          6: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "tauxStableRetenu4" },
          },
          7: {
            type: "checkbox",
            num: "critèreàretenir4",
          },
          8: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNCP" },
          },
          9: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNM1CP" },
          },
          10: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNM2CP" },
          },
          11: {
            type: "textArea",
          },
        },
        {
          0: {
            label: "Total Bilan",
          },
          1: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "tauxAppliqué6" },
          },
          2: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNTB" },
          },
          3: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNM1TB" },
          },
          4: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNM2TB" },
          },
          5: {
            type: "checkbox",
          },
          6: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "tauxStableRetenu6" },
          },
          7: {
            type: "checkbox",
            num: "critèreàretenir6",
          },
          8: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNTB" },
          },
          9: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNM1TB" },
          },
          10: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNM2TB" },
          },
          11: {
            type: "textArea",
          },
        },
        {
          0: {
            label: "Charges d'exploitation",
          },
          1: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "tauxAppliqué8" },
          },
          2: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNCExploit" },
          },
          3: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNM1CExploit" },
          },
          4: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNM2CExploit" },
          },
          5: {
            type: "checkbox",
          },
          6: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "tauxStableRetenu8" },
          },
          7: {
            type: "checkbox",
            num: "critèreàretenir8",
          },
          8: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNCExploit" },
          },
          9: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNM1CExploit" },
          },
          10: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNM2CExploit" },
          },
          11: {
            type: "textArea",
          },
        },
        {
          0: {
            label: "Total Participations et créances rattachées",
          },
          1: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "tauxAppliqué9" },
          },
          2: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNTPCR" },
          },
          3: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNM1TPCR" },
          },
          4: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNM2TPCR" },
          },
          5: {
            type: "checkbox",
          },
          6: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "tauxStableRetenu9" },
          },
          7: {
            type: "checkbox",
            num: "critèreàretenir9",
          },
          8: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNTPCR" },
          },
          9: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNM1TPCR" },
          },
          10: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNM2TPCR" },
          },
          11: {
            type: "textArea",
          },
        },
        {
          0: {
            label: "Frais de recherche et de développement",
          },
          1: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "tauxAppliqué10" },
          },
          2: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNFRD" },
          },
          3: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNM1FRD" },
          },
          4: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "soldeNM2FRD" },
          },
          5: {
            type: "checkbox",
          },
          6: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "tauxStableRetenu10" },
          },
          7: {
            type: "checkbox",
            num: "critèreàretenir10",
          },
          8: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNFRD" },
          },
          9: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNM1FRD" },
          },
          10: {
            label: "-",
            cellClass: "labelRight italicLabel",
            labelSource: { fieldId: "seuilNM2FRD" },
          },
          11: {
            type: "textArea",
          },
        },
      ],
    },
    tauxretenuresults: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      rowDeleteConfirmation: true,
      columns: [
        {
          width: "35.5%",
        },
        {
          width: "21.5%",
        },
        {
          width: "7.1%",
        },
        {
          width: "7.1%",
        },
        {
          width: "7.1%",
        },
        {
          width: "14.2%",
        },
      ],
      cells: [
        {
          0: {
            type: "none",
            label: "Taux appliqué",
            labelSource: { fieldId: "tauxApply" },
            cellClass: "tableHeaderCells",
            labelClass: "labelWhite labelLeft",
          },
          1: {
            type: "none",
            label: "seuil:",
            cellClass: "tableHeaderCells labelRight",
            labelClass: "labelWhite",
          },
          2: {
            type: "none",
            label: "-",
            labelSource: { fieldId: "moyenneSeuilN" },
            cellClass: "tableHeaderCells labelRight italicLabel",
            labelClass: "labelWhite",
          },
          3: {
            type: "none",
            label: "-",
            labelSource: { fieldId: "moyenneSeuilNM1" },
            cellClass: "tableHeaderCells labelRight italicLabel",
            labelClass: "labelWhite",
          },
          4: {
            type: "none",
            label: "-",
            labelSource: { fieldId: "moyenneSeuilNM2" },
            cellClass: "tableHeaderCells labelRight italicLabel",
            labelClass: "labelWhite",
          },
          5: {
            type: "textArea",
            cellClass: "tableHeaderCells",
          },
        },
      ],
    },
    seuilsignification: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      rowDeleteConfirmation: true,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Période",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Valeur K€",
          headerClass: "tableHeaderCells",
        },
        {
          type: "textArea",
          cellClass: "alignCenter",
          header: "Commentaire",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: "Période précédente",
            cellClass: "labelLeft",
          },
          1: {
            type: "text",
            num: "périodePréSignification",
          },
        },
        {
          0: {
            label: "Préliminaire",
            cellClass: "labelLeft",
          },
          1: {
            type: "text",
            num: "préliminaireSignification",
          },
        },
        {
          0: {
            label: "Final",
            cellClass: "labelLeft",
          },
          1: {
            type: "text",
            num: "finaleSignification",
          },
        },
      ],
    },
    seuilmontantinferieur: {
      fixedRows: true,
      infoTable: false,
      rowDeleteConfirmation: true,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Type",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Final K€",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Préliminaire K€",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Période précédente",
          headerClass: "tableHeaderCells",
        },
        {
          type: "textArea",
          cellClass: "alignCenter",
          header: "Argummentation",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: "Flux d'opérations",
            cellClass: "labelLeft",
          },
          1: {
            type: "text",
          },
          2: {
            type: "text",
          },
          3: {
            type: "text",
          },
        },
        {
          0: {
            label: "Soldes de comptes",
            cellClass: "labelLeft",
          },
          1: {
            type: "text",
          },
          2: {
            type: "text",
          },
          3: {
            type: "text",
          },
        },
        {
          0: {
            label: "Informations à fournir",
            cellClass: "labelLeft",
          },
          1: {
            type: "text",
          },
          2: {
            type: "text",
          },
          3: {
            type: "text",
          },
        },
        {
          0: {
            label: "Autre",
            cellClass: "labelLeft",
          },
          1: {
            type: "text",
          },
          2: {
            type: "text",
          },
          3: {
            type: "text",
          },
        },
      ],
    },
    percenttoapply: {
      fixedRows: true,
      staticTable: true,
      columns: [
        {
          type: "none",
          header: "Critères",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          header: "Evaluation",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          header: "commentaire",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: "Premier exercice audité",
            labelClass: "boldText labelLeft",
          },
          1: {
            type: "dropdown",
            init: "0",
            num: "percentapply0",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
          2: {
            type: "textArea",
            num: "percentapplyCOM0",
          },
        },
        {
          0: {
            label: "Fréquence des anomalies récurentes",
            labelClass: "boldText labelLeft",
          },
          1: {
            type: "dropdown",
            init: "0",
            num: "percentapply1",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
          2: {
            type: "textArea",
            num: "percentapplyCOM1",
          },
        },
        {
          0: {
            label: "Présence d'un Expert-Comptable",
            labelClass: "boldText labelLeft",
          },
          1: {
            type: "dropdown",
            init: "0",
            num: "percentapply2",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
          2: {
            type: "textArea",
            num: "percentapplyCOM2",
          },
        },
        {
          0: {
            label: "Faiblesse du contrôle interne",
            labelClass: "boldText labelLeft",
          },
          1: {
            type: "dropdown",
            init: "0",
            num: "percentapply3",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
          2: {
            type: "textArea",
            num: "percentapplyCOM3",
          },
        },
      ],
    },
    percenttoapplyresult: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      columns: [
        {
          type: "none",
          header: "Moyenne de niveau de risque",
          headerClass: "tableHeaderCells ",
        },
        {
          type: "none",
          header: "pourcentage à appliquer",
          headerClass: "tableHeaderCells",
        },
      ],
      cells: [
        {
          0: {
            label: " ",
            labelClass: "center",
            type: "none",
            labelSource: {
              fieldId: "MoyenneRisquePercentapply",
            },
          },
          1: {
            label: "",
            labelSource: { fieldId: "percent" },
            type: "dropdown",
            init: "0",
            num: "percentresult",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Taux risque faible",
                value: 1,
              },
              {
                option: "Taux risque moyen",
                value: 2,
              },
              {
                option: "Taux risque élevé",
                value: 3,
              },
            ],
          },
        },
      ],
    },
    seuilplanification: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      rowDeleteConfirmation: true,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Période",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Valeur K€",
          headerClass: "tableHeaderCells",
        },
        {
          type: "textArea",
          cellClass: "alignCenter",
          header: "Commentaire",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: "Période précédente",
            cellClass: "labelLeft",
          },
          1: {
            type: "text",
            num: "périodePréPlanification",
          },
        },
        {
          0: {
            label: "Préliminaire",
            cellClass: "labelLeft",
          },
          1: {
            type: "text",
            num: "préliminairePlanification",
          },
        },
        {
          0: {
            label: "Final",
            cellClass: "labelLeft",
          },
          1: {
            type: "text",
            num: "finalPlanification",
          },
        },
      ],
    },
    planmontantinférieur: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      rowDeleteConfirmation: true,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Section à risque ou informations",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Finale K€",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Préliminaire K€",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Période précédente",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Explication",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "% du seuil Planification",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            type: "selector",
            num: "selector1SEUILPLAN",
            inputClass: "std-input-width",
            selectorOptions: {
              items: {
                0: "Autre",
                1: "IS et participation",
                2: "Déb. et créd. divers  Passif",
                3: "Produits Financiers",
                4: "Autres charges",
                5: "Immo Corp. Incorp.",
                6: "Charges sur Immo. Corp. Incorp.",
                7: "Produits sur immo. corp. Incorp.",
                8: "Déb. et créd. divers  Actif",
                9: "Capitaux Propres, provisions, associés",
                10: "Charges et Prov. sur Stocks",
                11: "Autres Produits",
                12: "Groupes et Associés",
                13: "IS et Participation",
                14: "Charges sur Immo. Financières",
                15: "Impots et taxes",
                16: "Disponibilités",
                17: "Dettes sociales",
                18: "Part. et autres immo. financières",
                19: "Produits Exceptionnels",
                20: "Charges Exceptionnelles",
                21: "Dettes sur immo. et cptes rattachés",
                22: "Charges de personnel",
                23: "Produits & Rep. prov (stocks)",
                24: "Dettes fiscales",
                25: "Immo. Corp. Incorp.",
                26: "Charges sur CP et dotation PRC",
                27: "Emprunts et dettes financières divers",
                28: "Consolidation",
                29: "Personnel et organismes sociaux",
                30: "Produits sur Immo. Financières",
                31: "Hors Bilan",
                32: "Stocks",
                33: "Charges financières",
                34: "Rep. sur provision",
                35: "Impôts, taxes et Vers. assimilés",
                36: "Créances Fournisseurs",
                37: "Dettes fournisseurs",
                38: "Achats et Charges externes",
                39: "Produits Fournisseurs",
                40: "Comptes Clients",
                41: "Avances / Dettes cptes clients",
                42: "Charges sur comptes Clients",
                43: "Ventes march., biens, services",
              },
              displayValue: true,
              multiple: false,
            },
          },
          1: {
            type: "text",
            num: "finalSEUILPLAN1",
          },
          2: {
            type: "text",
            num: "preliSEUILPLAN1",
          },
          3: {
            type: "text",
            num: "periodepreSEUILPLAN1",
          },
          4: {
            type: "text",
            num: "explicationSEUILPLAN1",
          },
          5: {
            type: "text",
            num: "percentseuilSEUILPLAN1",
          },
        },
        {
          0: {
            type: "selector",
            num: "selector2SEUILPLAN",
            inputClass: "std-input-width",
            selectorOptions: {
              items: {
                0: "Autre",
                1: "IS et participation",
                2: "Déb. et créd. divers  Passif",
                3: "Produits Financiers",
                4: "Autres charges",
                5: "Immo Corp. Incorp.",
                6: "Charges sur Immo. Corp. Incorp.",
                7: "Produits sur immo. corp. Incorp.",
                8: "Déb. et créd. divers  Actif",
                9: "Capitaux Propres, provisions, associés",
                10: "Charges et Prov. sur Stocks",
                11: "Autres Produits",
                12: "Groupes et Associés",
                13: "IS et Participation",
                14: "Charges sur Immo. Financières",
                15: "Impots et taxes",
                16: "Disponibilités",
                17: "Dettes sociales",
                18: "Part. et autres immo. financières",
                19: "Produits Exceptionnels",
                20: "Charges Exceptionnelles",
                21: "Dettes sur immo. et cptes rattachés",
                22: "Charges de personnel",
                23: "Produits & Rep. prov (stocks)",
                24: "Dettes fiscales",
                25: "Immo. Corp. Incorp.",
                26: "Charges sur CP et dotation PRC",
                27: "Emprunts et dettes financières divers",
                28: "Consolidation",
                29: "Personnel et organismes sociaux",
                30: "Produits sur Immo. Financières",
                31: "Hors Bilan",
                32: "Stocks",
                33: "Charges financières",
                34: "Rep. sur provision",
                35: "Impôts, taxes et Vers. assimilés",
                36: "Créances Fournisseurs",
                37: "Dettes fournisseurs",
                38: "Achats et Charges externes",
                39: "Produits Fournisseurs",
                40: "Comptes Clients",
                41: "Avances / Dettes cptes clients",
                42: "Charges sur comptes Clients",
                43: "Ventes march., biens, services",
              },
              displayValue: true,
              multiple: false,
            },
          },
          1: {
            type: "text",
            num: "finalSEUILPLAN2",
          },
          2: {
            type: "text",
            num: "preliSEUILPLAN2",
          },
          3: {
            type: "text",
            num: "periodepreSEUILPLAN2",
          },
          4: {
            type: "text",
            num: "explicationSEUILPLAN2",
          },
          5: {
            type: "text",
            num: "percentseuilSEUILPLAN2",
          },
        },
        {
          0: {
            type: "selector",
            num: "selector3SEUILPLAN",
            inputClass: "std-input-width",
            selectorOptions: {
              items: {
                0: "Autre",
                1: "IS et participation",
                2: "Déb. et créd. divers  Passif",
                3: "Produits Financiers",
                4: "Autres charges",
                5: "Immo Corp. Incorp.",
                6: "Charges sur Immo. Corp. Incorp.",
                7: "Produits sur immo. corp. Incorp.",
                8: "Déb. et créd. divers  Actif",
                9: "Capitaux Propres, provisions, associés",
                10: "Charges et Prov. sur Stocks",
                11: "Autres Produits",
                12: "Groupes et Associés",
                13: "IS et Participation",
                14: "Charges sur Immo. Financières",
                15: "Impots et taxes",
                16: "Disponibilités",
                17: "Dettes sociales",
                18: "Part. et autres immo. financières",
                19: "Produits Exceptionnels",
                20: "Charges Exceptionnelles",
                21: "Dettes sur immo. et cptes rattachés",
                22: "Charges de personnel",
                23: "Produits & Rep. prov (stocks)",
                24: "Dettes fiscales",
                25: "Immo. Corp. Incorp.",
                26: "Charges sur CP et dotation PRC",
                27: "Emprunts et dettes financières divers",
                28: "Consolidation",
                29: "Personnel et organismes sociaux",
                30: "Produits sur Immo. Financières",
                31: "Hors Bilan",
                32: "Stocks",
                33: "Charges financières",
                34: "Rep. sur provision",
                35: "Impôts, taxes et Vers. assimilés",
                36: "Créances Fournisseurs",
                37: "Dettes fournisseurs",
                38: "Achats et Charges externes",
                39: "Produits Fournisseurs",
                40: "Comptes Clients",
                41: "Avances / Dettes cptes clients",
                42: "Charges sur comptes Clients",
                43: "Ventes march., biens, services",
              },
              displayValue: true,
              multiple: false,
            },
          },
          1: {
            type: "text",
            num: "finalSEUILPLAN3",
          },
          2: {
            type: "text",
            num: "preliSEUILPLAN3",
          },
          3: {
            type: "text",
            num: "periodepreSEUILPLAN3",
          },
          4: {
            type: "text",
            num: "explicationSEUILPLAN3",
          },
          5: {
            type: "text",
            num: "",
          },
        },
        {
          0: {
            type: "selector",
            num: "selector4SEUILPLAN",
            inputClass: "std-input-width",
            selectorOptions: {
              items: {
                0: "Autre",
                1: "IS et participation",
                2: "Déb. et créd. divers  Passif",
                3: "Produits Financiers",
                4: "Autres charges",
                5: "Immo Corp. Incorp.",
                6: "Charges sur Immo. Corp. Incorp.",
                7: "Produits sur immo. corp. Incorp.",
                8: "Déb. et créd. divers  Actif",
                9: "Capitaux Propres, provisions, associés",
                10: "Charges et Prov. sur Stocks",
                11: "Autres Produits",
                12: "Groupes et Associés",
                13: "IS et Participation",
                14: "Charges sur Immo. Financières",
                15: "Impots et taxes",
                16: "Disponibilités",
                17: "Dettes sociales",
                18: "Part. et autres immo. financières",
                19: "Produits Exceptionnels",
                20: "Charges Exceptionnelles",
                21: "Dettes sur immo. et cptes rattachés",
                22: "Charges de personnel",
                23: "Produits & Rep. prov (stocks)",
                24: "Dettes fiscales",
                25: "Immo. Corp. Incorp.",
                26: "Charges sur CP et dotation PRC",
                27: "Emprunts et dettes financières divers",
                28: "Consolidation",
                29: "Personnel et organismes sociaux",
                30: "Produits sur Immo. Financières",
                31: "Hors Bilan",
                32: "Stocks",
                33: "Charges financières",
                34: "Rep. sur provision",
                35: "Impôts, taxes et Vers. assimilés",
                36: "Créances Fournisseurs",
                37: "Dettes fournisseurs",
                38: "Achats et Charges externes",
                39: "Produits Fournisseurs",
                40: "Comptes Clients",
                41: "Avances / Dettes cptes clients",
                42: "Charges sur comptes Clients",
                43: "Ventes march., biens, services",
              },
              displayValue: true,
              multiple: false,
            },
          },
          1: {
            type: "text",
            num: "finalSEUILPLAN4",
          },
          2: {
            type: "text",
            num: "preliSEUILPLAN4",
          },
          3: {
            type: "text",
            num: "periodepreSEUILPLAN4",
          },
          4: {
            type: "text",
            num: "explicationSEUILPLAN4",
          },
          5: {
            type: "text",
            num: "percentseuilSEUILPLAN4",
          },
        },
        {
          0: {
            type: "selector",
            num: "selector5SEUILPLAN",
            inputClass: "std-input-width",
            selectorOptions: {
              items: {
                0: "Autre",
                1: "IS et participation",
                2: "Déb. et créd. divers  Passif",
                3: "Produits Financiers",
                4: "Autres charges",
                5: "Immo Corp. Incorp.",
                6: "Charges sur Immo. Corp. Incorp.",
                7: "Produits sur immo. corp. Incorp.",
                8: "Déb. et créd. divers  Actif",
                9: "Capitaux Propres, provisions, associés",
                10: "Charges et Prov. sur Stocks",
                11: "Autres Produits",
                12: "Groupes et Associés",
                13: "IS et Participation",
                14: "Charges sur Immo. Financières",
                15: "Impots et taxes",
                16: "Disponibilités",
                17: "Dettes sociales",
                18: "Part. et autres immo. financières",
                19: "Produits Exceptionnels",
                20: "Charges Exceptionnelles",
                21: "Dettes sur immo. et cptes rattachés",
                22: "Charges de personnel",
                23: "Produits & Rep. prov (stocks)",
                24: "Dettes fiscales",
                25: "Immo. Corp. Incorp.",
                26: "Charges sur CP et dotation PRC",
                27: "Emprunts et dettes financières divers",
                28: "Consolidation",
                29: "Personnel et organismes sociaux",
                30: "Produits sur Immo. Financières",
                31: "Hors Bilan",
                32: "Stocks",
                33: "Charges financières",
                34: "Rep. sur provision",
                35: "Impôts, taxes et Vers. assimilés",
                36: "Créances Fournisseurs",
                37: "Dettes fournisseurs",
                38: "Achats et Charges externes",
                39: "Produits Fournisseurs",
                40: "Comptes Clients",
                41: "Avances / Dettes cptes clients",
                42: "Charges sur comptes Clients",
                43: "Ventes march., biens, services",
              },
              displayValue: true,
              multiple: false,
            },
          },
          1: {
            type: "text",
            num: "finalSEUILPLAN5",
          },
          2: {
            type: "text",
            num: "preliSEUILPLAN5",
          },
          3: {
            type: "text",
            num: "periodepreSEUILPLAN5",
          },
          4: {
            type: "text",
            num: "explicationSEUILPLAN5",
          },
          5: {
            type: "text",
            num: "percentseuilSEUILPLAN5",
          },
        },
        {
          0: {
            type: "selector",
            num: "selector6SEUILPLAN",
            inputClass: "std-input-width",
            selectorOptions: {
              items: {
                0: "Autre",
                1: "IS et participation",
                2: "Déb. et créd. divers  Passif",
                3: "Produits Financiers",
                4: "Autres charges",
                5: "Immo Corp. Incorp.",
                6: "Charges sur Immo. Corp. Incorp.",
                7: "Produits sur immo. corp. Incorp.",
                8: "Déb. et créd. divers  Actif",
                9: "Capitaux Propres, provisions, associés",
                10: "Charges et Prov. sur Stocks",
                11: "Autres Produits",
                12: "Groupes et Associés",
                13: "IS et Participation",
                14: "Charges sur Immo. Financières",
                15: "Impots et taxes",
                16: "Disponibilités",
                17: "Dettes sociales",
                18: "Part. et autres immo. financières",
                19: "Produits Exceptionnels",
                20: "Charges Exceptionnelles",
                21: "Dettes sur immo. et cptes rattachés",
                22: "Charges de personnel",
                23: "Produits & Rep. prov (stocks)",
                24: "Dettes fiscales",
                25: "Immo. Corp. Incorp.",
                26: "Charges sur CP et dotation PRC",
                27: "Emprunts et dettes financières divers",
                28: "Consolidation",
                29: "Personnel et organismes sociaux",
                30: "Produits sur Immo. Financières",
                31: "Hors Bilan",
                32: "Stocks",
                33: "Charges financières",
                34: "Rep. sur provision",
                35: "Impôts, taxes et Vers. assimilés",
                36: "Créances Fournisseurs",
                37: "Dettes fournisseurs",
                38: "Achats et Charges externes",
                39: "Produits Fournisseurs",
                40: "Comptes Clients",
                41: "Avances / Dettes cptes clients",
                42: "Charges sur comptes Clients",
                43: "Ventes march., biens, services",
              },
              displayValue: true,
              multiple: false,
            },
          },
          1: {
            type: "text",
            num: "finalSEUILPLAN6",
          },
          2: {
            type: "text",
            num: "preliSEUILPLAN6",
          },
          3: {
            type: "text",
            num: "periodepreSEUILPLAN6",
          },
          4: {
            type: "text",
            num: "explicationSEUILPLAN6",
          },
          5: {
            type: "text",
            num: "percentseuilSEUILPLAN6",
          },
        },
        {
          0: {
            type: "selector",
            num: "selector7SEUILPLAN",
            inputClass: "std-input-width",
            selectorOptions: {
              items: {
                0: "Autre",
                1: "IS et participation",
                2: "Déb. et créd. divers  Passif",
                3: "Produits Financiers",
                4: "Autres charges",
                5: "Immo Corp. Incorp.",
                6: "Charges sur Immo. Corp. Incorp.",
                7: "Produits sur immo. corp. Incorp.",
                8: "Déb. et créd. divers  Actif",
                9: "Capitaux Propres, provisions, associés",
                10: "Charges et Prov. sur Stocks",
                11: "Autres Produits",
                12: "Groupes et Associés",
                13: "IS et Participation",
                14: "Charges sur Immo. Financières",
                15: "Impots et taxes",
                16: "Disponibilités",
                17: "Dettes sociales",
                18: "Part. et autres immo. financières",
                19: "Produits Exceptionnels",
                20: "Charges Exceptionnelles",
                21: "Dettes sur immo. et cptes rattachés",
                22: "Charges de personnel",
                23: "Produits & Rep. prov (stocks)",
                24: "Dettes fiscales",
                25: "Immo. Corp. Incorp.",
                26: "Charges sur CP et dotation PRC",
                27: "Emprunts et dettes financières divers",
                28: "Consolidation",
                29: "Personnel et organismes sociaux",
                30: "Produits sur Immo. Financières",
                31: "Hors Bilan",
                32: "Stocks",
                33: "Charges financières",
                34: "Rep. sur provision",
                35: "Impôts, taxes et Vers. assimilés",
                36: "Créances Fournisseurs",
                37: "Dettes fournisseurs",
                38: "Achats et Charges externes",
                39: "Produits Fournisseurs",
                40: "Comptes Clients",
                41: "Avances / Dettes cptes clients",
                42: "Charges sur comptes Clients",
                43: "Ventes march., biens, services",
              },
              displayValue: true,
              multiple: false,
            },
          },
          1: {
            type: "text",
            num: "finalSEUILPLAN7",
          },
          2: {
            type: "text",
            num: "preliSEUILPLAN7",
          },
          3: {
            type: "text",
            num: "periodepreSEUILPLAN7",
          },
          4: {
            type: "text",
            num: "explicationSEUILPLAN7",
          },
          5: {
            type: "text",
            num: "percentseuilSEUILPLAN7",
          },
        },
        {
          0: {
            type: "selector",
            num: "selector8SEUILPLAN",
            inputClass: "std-input-width",
            selectorOptions: {
              items: {
                0: "Autre",
                1: "IS et participation",
                2: "Déb. et créd. divers  Passif",
                3: "Produits Financiers",
                4: "Autres charges",
                5: "Immo Corp. Incorp.",
                6: "Charges sur Immo. Corp. Incorp.",
                7: "Produits sur immo. corp. Incorp.",
                8: "Déb. et créd. divers  Actif",
                9: "Capitaux Propres, provisions, associés",
                10: "Charges et Prov. sur Stocks",
                11: "Autres Produits",
                12: "Groupes et Associés",
                13: "IS et Participation",
                14: "Charges sur Immo. Financières",
                15: "Impots et taxes",
                16: "Disponibilités",
                17: "Dettes sociales",
                18: "Part. et autres immo. financières",
                19: "Produits Exceptionnels",
                20: "Charges Exceptionnelles",
                21: "Dettes sur immo. et cptes rattachés",
                22: "Charges de personnel",
                23: "Produits & Rep. prov (stocks)",
                24: "Dettes fiscales",
                25: "Immo. Corp. Incorp.",
                26: "Charges sur CP et dotation PRC",
                27: "Emprunts et dettes financières divers",
                28: "Consolidation",
                29: "Personnel et organismes sociaux",
                30: "Produits sur Immo. Financières",
                31: "Hors Bilan",
                32: "Stocks",
                33: "Charges financières",
                34: "Rep. sur provision",
                35: "Impôts, taxes et Vers. assimilés",
                36: "Créances Fournisseurs",
                37: "Dettes fournisseurs",
                38: "Achats et Charges externes",
                39: "Produits Fournisseurs",
                40: "Comptes Clients",
                41: "Avances / Dettes cptes clients",
                42: "Charges sur comptes Clients",
                43: "Ventes march., biens, services",
              },
              displayValue: true,
              multiple: false,
            },
          },
          1: {
            type: "text",
            num: "finalSEUILPLAN8",
          },
          2: {
            type: "text",
            num: "preliSEUILPLAN8",
          },
          3: {
            type: "text",
            num: "periodepreSEUILPLAN8",
          },
          4: {
            type: "text",
            num: "explicationSEUILPLAN8",
          },
          5: {
            type: "text",
            num: "percentseuilSEUILPLAN8",
          },
        },
        {
          0: {
            type: "selector",
            num: "selector9SEUILPLAN",
            inputClass: "std-input-width",
            selectorOptions: {
              items: {
                0: "Autre",
                1: "IS et participation",
                2: "Déb. et créd. divers  Passif",
                3: "Produits Financiers",
                4: "Autres charges",
                5: "Immo Corp. Incorp.",
                6: "Charges sur Immo. Corp. Incorp.",
                7: "Produits sur immo. corp. Incorp.",
                8: "Déb. et créd. divers  Actif",
                9: "Capitaux Propres, provisions, associés",
                10: "Charges et Prov. sur Stocks",
                11: "Autres Produits",
                12: "Groupes et Associés",
                13: "IS et Participation",
                14: "Charges sur Immo. Financières",
                15: "Impots et taxes",
                16: "Disponibilités",
                17: "Dettes sociales",
                18: "Part. et autres immo. financières",
                19: "Produits Exceptionnels",
                20: "Charges Exceptionnelles",
                21: "Dettes sur immo. et cptes rattachés",
                22: "Charges de personnel",
                23: "Produits & Rep. prov (stocks)",
                24: "Dettes fiscales",
                25: "Immo. Corp. Incorp.",
                26: "Charges sur CP et dotation PRC",
                27: "Emprunts et dettes financières divers",
                28: "Consolidation",
                29: "Personnel et organismes sociaux",
                30: "Produits sur Immo. Financières",
                31: "Hors Bilan",
                32: "Stocks",
                33: "Charges financières",
                34: "Rep. sur provision",
                35: "Impôts, taxes et Vers. assimilés",
                36: "Créances Fournisseurs",
                37: "Dettes fournisseurs",
                38: "Achats et Charges externes",
                39: "Produits Fournisseurs",
                40: "Comptes Clients",
                41: "Avances / Dettes cptes clients",
                42: "Charges sur comptes Clients",
                43: "Ventes march., biens, services",
              },
              displayValue: true,
              multiple: false,
            },
          },
          1: {
            type: "text",
            num: "finalSEUILPLAN9",
          },
          2: {
            type: "text",
            num: "preliSEUILPLAN9",
          },
          3: {
            type: "text",
            num: "periodepreSEUILPLAN9",
          },
          4: {
            type: "text",
            num: "explicationSEUILPLAN9",
          },
          5: {
            type: "text",
            num: "percentseuilSEUILPLAN9",
          },
        },
        {
          0: {
            type: "selector",
            num: "selector10SEUILPLAN",
            inputClass: "std-input-width",
            selectorOptions: {
              items: {
                0: "Autre",
                1: "IS et participation",
                2: "Déb. et créd. divers  Passif",
                3: "Produits Financiers",
                4: "Autres charges",
                5: "Immo Corp. Incorp.",
                6: "Charges sur Immo. Corp. Incorp.",
                7: "Produits sur immo. corp. Incorp.",
                8: "Déb. et créd. divers  Actif",
                9: "Capitaux Propres, provisions, associés",
                10: "Charges et Prov. sur Stocks",
                11: "Autres Produits",
                12: "Groupes et Associés",
                13: "IS et Participation",
                14: "Charges sur Immo. Financières",
                15: "Impots et taxes",
                16: "Disponibilités",
                17: "Dettes sociales",
                18: "Part. et autres immo. financières",
                19: "Produits Exceptionnels",
                20: "Charges Exceptionnelles",
                21: "Dettes sur immo. et cptes rattachés",
                22: "Charges de personnel",
                23: "Produits & Rep. prov (stocks)",
                24: "Dettes fiscales",
                25: "Immo. Corp. Incorp.",
                26: "Charges sur CP et dotation PRC",
                27: "Emprunts et dettes financières divers",
                28: "Consolidation",
                29: "Personnel et organismes sociaux",
                30: "Produits sur Immo. Financières",
                31: "Hors Bilan",
                32: "Stocks",
                33: "Charges financières",
                34: "Rep. sur provision",
                35: "Impôts, taxes et Vers. assimilés",
                36: "Créances Fournisseurs",
                37: "Dettes fournisseurs",
                38: "Achats et Charges externes",
                39: "Produits Fournisseurs",
                40: "Comptes Clients",
                41: "Avances / Dettes cptes clients",
                42: "Charges sur comptes Clients",
                43: "Ventes march., biens, services",
              },
              displayValue: true,
              multiple: false,
            },
          },
          1: {
            type: "text",
            num: "finalSEUILPLAN10",
          },
          2: {
            type: "text",
            num: "preliSEUILPLAN10",
          },
          3: {
            type: "text",
            num: "periodepreSEUILPLAN10",
          },
          4: {
            type: "text",
            num: "explicationSEUILPLAN10",
          },
          5: {
            type: "text",
            num: "percentseuilSEUILPLAN10",
          },
        },
        {
          0: {
            type: "selector",
            num: "selector11SEUILPLAN",
            inputClass: "std-input-width",
            selectorOptions: {
              items: {
                0: "Autre",
                1: "IS et participation",
                2: "Déb. et créd. divers  Passif",
                3: "Produits Financiers",
                4: "Autres charges",
                5: "Immo Corp. Incorp.",
                6: "Charges sur Immo. Corp. Incorp.",
                7: "Produits sur immo. corp. Incorp.",
                8: "Déb. et créd. divers  Actif",
                9: "Capitaux Propres, provisions, associés",
                10: "Charges et Prov. sur Stocks",
                11: "Autres Produits",
                12: "Groupes et Associés",
                13: "IS et Participation",
                14: "Charges sur Immo. Financières",
                15: "Impots et taxes",
                16: "Disponibilités",
                17: "Dettes sociales",
                18: "Part. et autres immo. financières",
                19: "Produits Exceptionnels",
                20: "Charges Exceptionnelles",
                21: "Dettes sur immo. et cptes rattachés",
                22: "Charges de personnel",
                23: "Produits & Rep. prov (stocks)",
                24: "Dettes fiscales",
                25: "Immo. Corp. Incorp.",
                26: "Charges sur CP et dotation PRC",
                27: "Emprunts et dettes financières divers",
                28: "Consolidation",
                29: "Personnel et organismes sociaux",
                30: "Produits sur Immo. Financières",
                31: "Hors Bilan",
                32: "Stocks",
                33: "Charges financières",
                34: "Rep. sur provision",
                35: "Impôts, taxes et Vers. assimilés",
                36: "Créances Fournisseurs",
                37: "Dettes fournisseurs",
                38: "Achats et Charges externes",
                39: "Produits Fournisseurs",
                40: "Comptes Clients",
                41: "Avances / Dettes cptes clients",
                42: "Charges sur comptes Clients",
                43: "Ventes march., biens, services",
              },
              displayValue: true,
              multiple: false,
            },
          },
          1: {
            type: "text",
            num: "finalSEUILPLAN11",
          },
          2: {
            type: "text",
            num: "preliSEUILPLAN11",
          },
          3: {
            type: "text",
            num: "periodepreSEUILPLAN11",
          },
          4: {
            type: "text",
            num: "explicationSEUILPLAN11",
          },
          5: {
            type: "text",
            num: "percentseuilSEUILPLAN11",
          },
        },
      ],
    },
    percenttoapplyANO: {
      staticTable: true,
      fixedRows: true,
      columns: [
        {
          type: "none",
          header: "Critères",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          header: "Evaluation",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          header: "commentaire",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: "Premier exercice audité",
            labelClass: "boldText labelLeft",
          },
          1: {
            type: "dropdown",
            init: "0",
            num: "percentapplyANO0",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
          2: {
            type: "textArea",
            num: "percentapplyCOMANO0",
          },
        },
        {
          0: {
            label: "Fréquence des anomalies récurentes",
            labelClass: "boldText labelLeft",
          },
          1: {
            type: "dropdown",
            init: "0",
            num: "percentapplyANO1",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Faible",
                value: 1,
              },
              {
                option: "Moyen",
                value: 2,
              },
              {
                option: "Elevé",
                value: 3,
              },
            ],
          },
          2: {
            type: "textArea",
            num: "percentapplyCOMANO1",
          },
        },
      ],
    },
    percenttoapplyresultANO: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      columns: [
        {
          type: "none",
          header: "Moyenne de niveau de risque",
          headerClass: "tableHeaderCells ",
        },
        {
          type: "none",
          header: "pourcentage à appliquer",
          headerClass: "tableHeaderCells",
        },
      ],
      cells: [
        {
          0: {
            label: " ",
            labelClass: "center",
            type: "none",
            labelSource: {
              fieldId: "MoyenneRisquePercentapplyANO",
            },
          },
          1: {
            type: "dropdown",
            init: "0",
            num: "percentresultANO",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Taux risque faible",
                value: 1,
              },
              {
                option: "Taux risque moyen",
                value: 2,
              },
              {
                option: "Taux risque élevé",
                value: 3,
              },
            ],
          },
        },
      ],
    },
    seuilanomalie: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      rowDeleteConfirmation: true,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Période",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Valeur K€",
          headerClass: "tableHeaderCells",
        },
        {
          type: "textArea",
          cellClass: "alignCenter",
          header: "Commentaire",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: "Période précédente",
            cellClass: "labelLeft",
          },
          1: {
            type: "text",
          },
        },
        {
          0: {
            label: "Préliminaire",
            cellClass: "labelLeft",
          },
          1: {
            type: "text",
            num: "préliminaireAnomalie",
          },
        },
        {
          0: {
            label: "Final",
            cellClass: "labelLeft",
          },
          1: {
            type: "text",
          },
        },
      ],
    },
    justifications: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      rowDeleteConfirmation: true,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          header: " ",
          headerClass: "tableHeaderCells leftCornerRounded",
          width: "2%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Modifications des seuils",
          headerClass: "tableHeaderCells",
          width: "63%",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Oui/Non",
          headerClass: "tableHeaderCells",
          width: "10%",
        },
        {
          type: "textArea",
          cellClass: "alignCenter",
          header: "commentaire",
          headerClass: "tableHeaderCells rightCornerRounded",
          width: "25%",
        },
      ],
      cells: [
        {
          0: {
            label: "1",
            cellClass: "labelLeft",
          },
          1: {
            label:
              "Les éventuelles modifications apportées aux seuils de signification et de planification ont-ils été justifiés et formalisés dans les tableaux ci-dessus ? Prendre en compte les modifications apportés aux résultats et opérations de l'entité, les informations obtenues et les anomalies identifiées au cours de l'audit.",
            cellClass: "labelLeft",
          },
          2: {
            type: "dropdown",
            init: "0",
            num: "yesno1",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Oui",
                value: 1,
              },
              {
                option: "Non",
                value: 2,
              },
            ],
          },
        },
        {
          0: {
            label: "2",
            cellClass: "labelLeft",
          },
          1: {
            label:
              "L'impact des modifications de ces seuils sur l'évaluation des risques, la nature et l'étendue des procédures d'audit complémentaires et les délais ont-ils été pris en compte ?",
            cellClass: "labelLeft",
          },
          2: {
            type: "dropdown",
            init: "0",
            num: "yesno2",
            options: [
              {
                option: " ",
                value: 0,
              },
              {
                option: "Oui",
                value: 1,
              },
              {
                option: "Non",
                value: 2,
              },
            ],
          },
        },
      ],
    },
    evalPRELI: {
      fixedRows: true,
      infoTable: false,
      staticTable: true,
      rowDeleteConfirmation: true,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          header: " ",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Nom",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Date",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: "Préparé par",
            cellClass: "labelLeft",
          },
        },
        {
          0: {
            label: "Revu par",
            cellClass: "labelLeft",
          },
        },
      ],
    },
    evalFINAL: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      rowDeleteConfirmation: true,
      columns: [
        {
          type: "none",
          cellClass: "alignCenter",
          header: " ",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Nom",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          cellClass: "alignCenter",
          header: "Date",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: "Préparé par",
            cellClass: "labelLeft",
          },
        },
        {
          0: {
            label: "Revu par",
            cellClass: "labelLeft",
          },
        },
      ],
    },
    paramDefTaux: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      columns: [
        {
          type: "none",
          header: "ㅤ",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          header: " ",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          header: " ",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          header: " ",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          header: " ",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: " ",
          },
          1: {
            type: "text",
            init: "Taux risques très faible",
            num: "paramLabelTaux0",
          },
          2: {
            type: "text",
            init: "Taux risques faible",
            num: "paramLabelTaux1",
          },
          3: {
            type: "text",
            init: "Taux risques moyen",
            num: "paramLabelTaux2",
          },
          4: {
            type: "text",
            init: "Taux risques élevé",
            num: "paramLabelTaux3",
          },
        },
        {
          0: {
            label: "Moyenne de l'évaluation des risques",
          },
          1: {
            label: "Inférieur ou égale à",
            type: "text",
            init: "1.2",
            num: "paramTauxLabel0",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "paramTauxLabel11" },
            type: "text",
            init: "1.5",
            num: "paramTauxLabel12",
          },
          3: {
            label: " ",
            labelSource: { fieldId: "paramTauxLabel21" },
            type: "text",
            init: "2.0 ",
            num: "paramTauxLabel22",
          },
          4: {
            label: "Supérieur à",
            type: "text",
            init: "2.0",
            num: "paramTauxLabel3",
          },
        },
      ],
    },
    paramCritèresPrédéterHeader: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      columns: [
        {
          width: "10%",
        },
        {
          width: "10%",
        },
        {
          width: "10%",
        },
        {
          width: "10%",
        },
        {
          width: "10%",
        },
        {
          width: "10%",
        },
        {
          width: "10%",
        },
      ],
      cells: [
        {
          0: {
            type: "none",
            label: "Description Critères",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells leftCornerRounded",
          },
          1: {
            type: "none",
            label: "Taux de stabilité",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells",
          },
          2: {
            type: "none",
            label: "Taux risques très faible",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells",
          },
          3: {
            type: "none",
            label: "Taux risque faible",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells",
          },
          4: {
            type: "none",
            label: "Taux risque moyen",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells",
          },
          5: {
            type: "none",
            label: "Taux risque élevé",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells",
          },
          6: {
            type: "none",
            label: "Activité",
            labelClass: "center labelWhite",
            cellClass: "tableHeaderCells rightCornerRounded",
          },
        },
      ],
    },
    paramCritèresPrédéter: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      columns: [
        {
          width: "15%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
        {
          width: "5%",
        },
      ],
      cells: [
        {
          0: {
            type: "none",
          },
          1: {
            type: "none",
          },
          2: {
            type: "none",
          },
          3: {
            type: "none",
          },
          4: {
            type: "none",
          },
          5: {
            type: "none",
          },
          6: {
            type: "none",
            label: "Par défaut",
          },
          7: {
            type: "none",
            label: "10.",
          },
          8: {
            type: "none",
            label: "14",
          },
          9: {
            type: "none",
            label: "21",
          },
          10: {
            type: "none",
            label: "29",
          },
          11: {
            type: "none",
            label: "35",
          },
          12: {
            type: "none",
            label: "41",
          },
          13: {
            type: "none",
            label: "46",
          },
          14: {
            type: "none",
            label: "47",
          },
          15: {
            type: "none",
            label: "55",
          },
          16: {
            type: "none",
            label: "56",
          },
          17: {
            type: "none",
            label: "61",
          },
          18: {
            type: "none",
            label: "79",
          },
          19: {
            type: "none",
            label: "86",
          },
          20: {
            type: "none",
            label: "Startup",
          },
          21: {
            type: "none",
            label: "Holding",
          },
          22: {
            type: "none",
            label: "Difficulté",
          },
        },
        {
          0: {
            type: "none",
            label: "Résultat de l'ex.",
          },
          1: {
            type: "text",
            num: "tauxstable1",
            init: "20%",
          },
          2: {
            type: "text",
            num: "tauxRiskTFaible1",
            init: "8%",
          },
          3: {
            type: "text",
            num: "tauxRiskFaible1",
            init: "7%",
          },
          4: {
            type: "text",
            num: "tauxRiskMoyen1",
            init: "5%",
          },
          5: {
            type: "text",
            num: "tauxRiskEleve1",
            init: "3%",
          },
          6: {
            type: "checkbox",
            num: "resultexDefaultCheckbox",
          },
          7: {
            type: "checkbox",
          },
          8: {
            type: "checkbox",
          },
          9: {
            type: "checkbox",
          },
          10: {
            type: "checkbox",
          },
          11: {
            type: "checkbox",
          },
          12: {
            type: "checkbox",
          },
          13: {
            type: "checkbox",
          },
          14: {
            type: "checkbox",
          },
          15: {
            type: "checkbox",
          },
          16: {
            type: "checkbox",
          },
          17: {
            type: "checkbox",
          },
          18: {
            type: "checkbox",
          },
          19: {
            type: "checkbox",
          },
          20: {
            type: "checkbox",
          },
          21: {
            type: "checkbox",
          },
          22: {
            type: "checkbox",
          },
        },
        {
          0: {
            type: "none",
            label: "Résultat Courant Avant Impôt",
          },
          1: {
            type: "text",
            num: "tauxstable2",
            init: "20%",
          },
          2: {
            type: "text",
            num: "tauxRiskTFaible2",
            init: "11%",
          },
          3: {
            type: "text",
            num: "tauxRiskFaible2",
            init: "10%",
          },
          4: {
            type: "text",
            num: "tauxRiskMoyen2",
            init: "7%",
          },
          5: {
            type: "text",
            num: "tauxRiskEleve2",
            init: "5%",
          },
          6: {
            type: "checkbox",
            num: "rcaiDefautCheckbox",
          },
          7: {
            type: "checkbox",
          },
          8: {
            type: "checkbox",
          },
          9: {
            type: "checkbox",
          },
          10: {
            type: "checkbox",
          },
          11: {
            type: "checkbox",
          },
          12: {
            type: "checkbox",
          },
          13: {
            type: "checkbox",
          },
          14: {
            type: "checkbox",
          },
          15: {
            type: "checkbox",
          },
          16: {
            type: "checkbox",
          },
          17: {
            type: "checkbox",
          },
          18: {
            type: "checkbox",
          },
          19: {
            type: "checkbox",
          },
          20: {
            type: "checkbox",
          },
          21: {
            type: "checkbox",
          },
          22: {
            type: "checkbox",
          },
        },
        {
          0: {
            type: "none",
            label: "Chiffre d'Affaires",
          },
          1: {
            type: "text",
            num: "tauxstable3",
            init: "20%",
          },
          2: {
            type: "text",
            num: "tauxRiskTFaible3",
            init: "3%",
          },
          3: {
            type: "text",
            num: "tauxRiskFaible3",
            init: "2%",
          },
          4: {
            type: "text",
            num: "tauxRiskMoyen3",
            init: "1%",
          },
          5: {
            type: "text",
            num: "tauxRiskEleve3",
            init: "0.5%",
          },
          6: {
            type: "checkbox",
            num: "caDefaultCheckbox",
          },
          7: {
            type: "checkbox",
          },
          8: {
            type: "checkbox",
          },
          9: {
            type: "checkbox",
          },
          10: {
            type: "checkbox",
          },
          11: {
            type: "checkbox",
          },
          12: {
            type: "checkbox",
          },
          13: {
            type: "checkbox",
          },
          14: {
            type: "checkbox",
          },
          15: {
            type: "checkbox",
          },
          16: {
            type: "checkbox",
          },
          17: {
            type: "checkbox",
          },
          18: {
            type: "checkbox",
          },
          19: {
            type: "checkbox",
          },
          20: {
            type: "checkbox",
          },
          21: {
            type: "checkbox",
          },
          22: {
            type: "checkbox",
          },
        },
        {
          0: {
            type: "none",
            label: "Capitaux Propres",
          },
          1: {
            type: "text",
            num: "tauxstable4",
            init: "5%",
          },
          2: {
            type: "text",
            num: "tauxRiskTFaible4",
            init: "6%",
          },
          3: {
            type: "text",
            num: "tauxRiskFaible4",
            init: "5%",
          },
          4: {
            type: "text",
            num: "tauxRiskMoyen4",
            init: "3%",
          },
          5: {
            type: "text",
            num: "tauxRiskEleve4",
            init: "1%",
          },
          6: {
            type: "checkbox",
            num: "cpDefaultCheckbox",
          },
          7: {
            type: "checkbox",
          },
          8: {
            type: "checkbox",
          },
          9: {
            type: "checkbox",
          },
          10: {
            type: "checkbox",
          },
          11: {
            type: "checkbox",
          },
          12: {
            type: "checkbox",
          },
          13: {
            type: "checkbox",
          },
          14: {
            type: "checkbox",
          },
          15: {
            type: "checkbox",
          },
          16: {
            type: "checkbox",
          },
          17: {
            type: "checkbox",
          },
          18: {
            type: "checkbox",
          },
          19: {
            type: "checkbox",
          },
          20: {
            type: "checkbox",
          },
          21: {
            type: "checkbox",
          },
          22: {
            type: "checkbox",
          },
        },
        {
          0: {
            type: "none",
            label: "Endettement net",
          },
          1: {
            type: "text",
            num: "tauxstable5",
            init: "10%",
          },
          2: {
            type: "text",
            num: "tauxRiskTFaible5",
            init: "2.5%",
          },
          3: {
            type: "text",
            num: "tauxRiskFaible5",
            init: "2%",
          },
          4: {
            type: "text",
            num: "tauxRiskMoyen5",
            init: "1.5%",
          },
          5: {
            type: "text",
            num: "tauxRiskEleve5",
            init: "1%",
          },
          6: {
            type: "checkbox",
          },
          7: {
            type: "checkbox",
          },
          8: {
            type: "checkbox",
          },
          9: {
            type: "checkbox",
          },
          10: {
            type: "checkbox",
          },
          11: {
            type: "checkbox",
          },
          12: {
            type: "checkbox",
          },
          13: {
            type: "checkbox",
          },
          14: {
            type: "checkbox",
          },
          15: {
            type: "checkbox",
          },
          16: {
            type: "checkbox",
          },
          17: {
            type: "checkbox",
          },
          18: {
            type: "checkbox",
          },
          19: {
            type: "checkbox",
          },
          20: {
            type: "checkbox",
          },
          21: {
            type: "checkbox",
          },
          22: {
            type: "checkbox",
          },
        },
        {
          0: {
            type: "none",
            label: "Total Bilan",
          },
          1: {
            type: "text",
            num: "tauxstable6",
            init: "10%",
          },
          2: {
            type: "text",
            num: "tauxRiskTFaible6",
            init: "2%",
          },
          3: {
            type: "text",
            num: "tauxRiskFaible6",
            init: "1.5%",
          },
          4: {
            type: "text",
            num: "tauxRiskMoyen6",
            init: "1%",
          },
          5: {
            type: "text",
            num: "tauxRiskEleve6",
            init: "0.5%",
          },
          6: {
            type: "checkbox",
          },
          7: {
            type: "checkbox",
          },
          8: {
            type: "checkbox",
          },
          9: {
            type: "checkbox",
          },
          10: {
            type: "checkbox",
          },
          11: {
            type: "checkbox",
          },
          12: {
            type: "checkbox",
          },
          13: {
            type: "checkbox",
          },
          14: {
            type: "checkbox",
          },
          15: {
            type: "checkbox",
          },
          16: {
            type: "checkbox",
          },
          17: {
            type: "checkbox",
          },
          18: {
            type: "checkbox",
          },
          19: {
            type: "checkbox",
          },
          20: {
            type: "checkbox",
          },
          21: {
            type: "checkbox",
          },
          22: {
            type: "checkbox",
          },
        },
        {
          0: {
            type: "none",
            label: "Charges d'exploitation",
          },
          1: {
            type: "text",
            num: "tauxstable8",
            init: "20%",
          },
          2: {
            type: "text",
            num: "tauxRiskTFaible8",
            init: "6%",
          },
          3: {
            type: "text",
            num: "tauxRiskFaible8",
            init: "5%",
          },
          4: {
            type: "text",
            num: "tauxRiskMoyen8",
            init: "3%",
          },
          5: {
            type: "text",
            num: "tauxRiskEleve8",
            init: "1%",
          },
          6: {
            type: "checkbox",
          },
          7: {
            type: "checkbox",
          },
          8: {
            type: "checkbox",
          },
          9: {
            type: "checkbox",
          },
          10: {
            type: "checkbox",
          },
          11: {
            type: "checkbox",
          },
          12: {
            type: "checkbox",
          },
          13: {
            type: "checkbox",
          },
          14: {
            type: "checkbox",
          },
          15: {
            type: "checkbox",
          },
          16: {
            type: "checkbox",
          },
          17: {
            type: "checkbox",
          },
          18: {
            type: "checkbox",
          },
          19: {
            type: "checkbox",
          },
          20: {
            type: "checkbox",
          },
          21: {
            type: "checkbox",
          },
          22: {
            type: "checkbox",
          },
        },
        {
          0: {
            type: "none",
            label: "RTotal Participations et créances rattachées",
          },
          1: {
            type: "text",
            num: "tauxstable9",
            init: "10%",
          },
          2: {
            type: "text",
            num: "tauxRiskTFaible9",
            init: "6%",
          },
          3: {
            type: "text",
            num: "tauxRiskFaible9",
            init: "5%",
          },
          4: {
            type: "text",
            num: "tauxRiskMoyen9",
            init: "3%",
          },
          5: {
            type: "text",
            num: "tauxRiskEleve9",
            init: "1%",
          },
          6: {
            type: "checkbox",
          },
          7: {
            type: "checkbox",
          },
          8: {
            type: "checkbox",
          },
          9: {
            type: "checkbox",
          },
          10: {
            type: "checkbox",
          },
          11: {
            type: "checkbox",
          },
          12: {
            type: "checkbox",
          },
          13: {
            type: "checkbox",
          },
          14: {
            type: "checkbox",
          },
          15: {
            type: "checkbox",
          },
          16: {
            type: "checkbox",
          },
          17: {
            type: "checkbox",
          },
          18: {
            type: "checkbox",
          },
          19: {
            type: "checkbox",
          },
          20: {
            type: "checkbox",
          },
          21: {
            type: "checkbox",
          },
          22: {
            type: "checkbox",
          },
        },
        {
          0: {
            type: "none",
            label: "Frais de recherche et de développement",
          },
          1: {
            type: "text",
            num: "tauxstable10",
            init: "10%",
          },
          2: {
            type: "text",
            num: "tauxRiskTFaible10",
            init: "6%",
          },
          3: {
            type: "text",
            num: "tauxRiskFaible10",
            init: "5%",
          },
          4: {
            type: "text",
            num: "tauxRiskMoyen10",
            init: "3%",
          },
          5: {
            type: "text",
            num: "tauxRiskEleve10",
            init: "1%",
          },
          6: {
            type: "checkbox",
          },
          7: {
            type: "checkbox",
          },
          8: {
            type: "checkbox",
          },
          9: {
            type: "checkbox",
          },
          10: {
            type: "checkbox",
          },
          11: {
            type: "checkbox",
          },
          12: {
            type: "checkbox",
          },
          13: {
            type: "checkbox",
          },
          14: {
            type: "checkbox",
          },
          15: {
            type: "checkbox",
          },
          16: {
            type: "checkbox",
          },
          17: {
            type: "checkbox",
          },
          18: {
            type: "checkbox",
          },
          19: {
            type: "checkbox",
          },
          20: {
            type: "checkbox",
          },
          21: {
            type: "checkbox",
          },
          22: {
            type: "checkbox",
          },
        },
      ],
    },
    paramDefTauxPLAN: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      columns: [
        {
          type: "none",
          header: "ㅤ",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          header: " ",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          header: " ",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          header: " ",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: " ",
          },
          1: {
            type: "text",
            init: "Taux risques faible",
            num: "paramPLANLabelTaux1",
          },
          2: {
            type: "text",
            init: "Taux risques moyen",
            num: "paramPLANLabelTaux2",
          },
          3: {
            type: "text",
            init: "Taux risques élevé",
            num: "paramPLANLabelTaux3",
          },
        },
        {
          0: {
            label: "Moyenne de l'évaluation des risques",
          },
          1: {
            label: "Inférieur ou égale à",
            type: "text",
            init: "1.2",
            num: "paramPLANTauxLabel1",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "paramPLANTauxLabel11" },
            type: "text",
            init: "1.5",
            num: "paramPLANTauxLabel2",
          },
          3: {
            label: "Supérieur à",
            labelSource: { fieldId: "paramPLANTauxLabel21" },
            type: "text",
            init: "2.0 ",
            num: "paramPLANTauxLabel3",
          },
        },
        {
          0: {
            label: "Valeur du pourcentage",
          },
          1: {
            type: "text",
            num: "valuepercent1",
            init: "70%",
          },
          2: {
            type: "text",
            num: "valuepercent2",
            init: "60%",
          },
          3: {
            type: "text",
            num: "valuepercent3",
            init: "50%",
          },
        },
      ],
    },
    paramDefTauxANO: {
      fixedRows: true,
      staticTable: true,
      infoTable: false,
      columns: [
        {
          type: "none",
          header: "ㅤ",
          headerClass: "tableHeaderCells leftCornerRounded",
        },
        {
          type: "none",
          header: " ",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          header: " ",
          headerClass: "tableHeaderCells",
        },
        {
          type: "none",
          header: " ",
          headerClass: "tableHeaderCells rightCornerRounded",
        },
      ],
      cells: [
        {
          0: {
            label: " ",
          },
          1: {
            type: "text",
            init: "Taux risques faible",
            num: "paramANOLabelTaux1",
          },
          2: {
            type: "text",
            init: "Taux risques moyen",
            num: "paramANOLabelTaux2",
          },
          3: {
            type: "text",
            init: "Taux risques élevé",
            num: "paramANOLabelTaux3",
          },
        },
        {
          0: {
            label: "Moyenne de l'évaluation des risques",
          },
          1: {
            label: "Inférieur ou égale à",
            type: "text",
            init: "1.2",
            num: "paramANOTauxLabel0",
          },
          2: {
            label: " ",
            labelSource: { fieldId: "paramANOTauxLabel11" },
            type: "text",
            init: "1.5",
            num: "paramANOTauxLabel12",
          },
          3: {
            label: "Supérieur",
            labelSource: { fieldId: "paramANOTauxLabel21" },
            type: "text",
            init: "2.0 ",
            num: "paramANOTauxLabel22",
          },
        },
        {
          0: {
            label: "Valeur du pourcentage signification",
          },
          1: {
            type: "text",
            init: "5%",
          },
          2: {
            type: "text",
            init: "5%",
          },
          3: {
            type: "text",
            init: "2.5%",
          },
        },
        {
          0: {
            label: "Valeur du pourcentage planification",
          },
          1: {
            type: "text",
            init: "10%",
          },
          2: {
            type: "text",
            init: "10%",
          },
          3: {
            type: "text",
            init: "5%",
          },
        },
      ],
    },
  });
})();
