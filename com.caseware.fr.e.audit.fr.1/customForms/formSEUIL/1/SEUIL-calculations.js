wpw.tax.create.calcBlocks("SEUIL", function (calcUtils) {
  function numbersFR(data) {
    let newData = "";
    for (let i = 0; i < data.length; i++) {
      newData = data[i];
      const newNumbers = newData.replace(",", ".").replace(".", "");
      data[i] = newNumbers;
    }
    return data;
  }
  calcUtils.calc(function (calcUtils, field) {
    return wpw.tax.getMateriality().then(function (materialityProperties) {
      const parameterseuil = [
        "com.caseware.materiality;preliminary;-1",
        "com.caseware.materiality;preliminary;0",
        "com.caseware.materiality;report;0",
      ];

      const row1 = [
        "periodePrecedenteSEUILSI",
        "periodePrecedenteSEUILPlan",
        "periodePrecedenteSEUILInsi",
      ];
      const row2 = ["preliSEUILSI", "preliSEUILPlan", "preliSEUILInsi"];
      const row3 = ["FinalSEUILSI", "FinalSEUILPlan", "FinalSEUILInsi"];
      for (let i = 0; i < row1.length; i++) {
        materialityProperties[i].balances[parameterseuil[0]] === undefined
          ? field(row1[i]).set("-")
          : field(row1[i]).set(
              materialityProperties[i].balances[parameterseuil[0]].amount
            );
      }
      for (let i = 0; i < row2.length; i++) {
        materialityProperties[i].balances[parameterseuil[1]] === undefined
          ? field(row2[i]).set("-")
          : field(row2[i]).set(
              materialityProperties[i].balances[parameterseuil[1]].amount
            );
      }
      for (let i = 0; i < row3.length; i++) {
        materialityProperties[i].balances[parameterseuil[2]] === undefined
          ? field(row3[i]).set("-")
          : field(row3[i]).set(
              materialityProperties[i].balances[parameterseuil[2]].amount
            );
      }
    });
  }),
    calcUtils.calc(function (calcUtils, field) {
      let totalrisk = [
        parseFloat(field("lvlrisk0").get()),
        parseFloat(field("lvlrisk1").get()),
        parseFloat(field("lvlrisk2").get()),
        parseFloat(field("lvlrisk3").get()),
        parseFloat(field("lvlrisk4").get()),
        parseFloat(field("lvlrisk5").get()),
        parseFloat(field("lvlrisk6").get()),
        parseFloat(field("lvlrisk7").get()),
        parseFloat(field("lvlrisk8").get()),
        parseFloat(field("lvlrisk9").get()),
      ];
      totalrisk = totalrisk.filter((total) => total > 0);
      let moyenne = 0;
      for (let i = 0; i < totalrisk.length; i++) {
        moyenne += totalrisk[i];
      }
      moyenne = moyenne / totalrisk.length;

      const parampercent = [
        field("paramTauxLabel0").get(),
        field("paramTauxLabel12").get(),
        field("paramTauxLabel22").get(),
        field("paramTauxLabel3").get(),
      ];

      if (moyenne < 1) {
        field("661").set("0");
        field("MoyenneRisque").set(" ");
      }

      if (moyenne >= 1 && moyenne <= parampercent[0]) {
        field("661").set("1");
        field("MoyenneRisque").set("Très faible");
      }

      if (moyenne > parampercent[0] && moyenne <= parampercent[1]) {
        field("661").set("2");
        field("MoyenneRisque").set("Faible");
      }

      if (moyenne > parampercent[1] && moyenne <= parampercent[2]) {
        field("661").set("3");
        field("MoyenneRisque").set("Moyen");
      }

      if (moyenne > parampercent[3]) {
        field("661").set("4");
        field("MoyenneRisque").set("Elevé");
      }
    });

  calcUtils.calc(function (calcUtils, field) {
    field("MoyenneRisque").get() === undefined
      ? field("tauxApply").set(" ")
      : field("tauxApply").set(
          `Taux appliqué : <b>${field("MoyenneRisque").get()}</b>`
        );
  });

  calcUtils.calc(function (calcUtils, field) {
    return wpw.tax
      .evaluate(['collaborate("primaryIndustryCode")'])
      .then(function (code) {
        const selectedcriteria = field("critèreretenu").get();
        const codeNace = code[0];
        const codeActivity = codeNace.split(".");
        const criteres = {
          0: " ",
          1: "Code NACE",
          2: "Startup",
          3: "Entité en difficulté financière",
          4: "Holding",
        };
        if (selectedcriteria === 1) {
          console.log("select 1");
          field("codecritère").set(`Code critère retenu : <b>${codeNace}</b>`);
          field("paramsecteur").set(
            `Paramétrage secteur retenu : <b>${codeActivity[0]}</b>`
          );
        } else {
          field("codecritère").set(
            `Code critère retenu : <b>${criteres[selectedcriteria]}</b>`
          );
          field("paramsecteur").set(
            `Paramétrage secteur retenu : <b>${criteres[selectedcriteria]}</b>`
          );
        }
        const buisnessCodeActivity = parseFloat(codeNace.split("."));
        const codesActivity = [
          10, 14, 21, 29, 35, 41, 46, 47, 55, 56, 61, 79, 86,
        ];
        if (codesActivity.indexOf(buisnessCodeActivity) === -1) {
          field("caDefaultCheckbox").set(true);
          field("critèreparamétréCA").set(true);
        }
      });
  });
  calcUtils.calc(function (calcUtils, field) {
    const tauxStable = [
      field("tauxstable1").get(),
      field("tauxstable2").get(),
      field("tauxstable3").get(),
      field("tauxstable4").get(),
      field("tauxstable6").get(),
      field("tauxstable8").get(),
      field("tauxstable9").get(),
      field("tauxstable10").get(),
    ];
    const numsStableretenu = [
      "tauxStableRetenu1",
      "tauxStableRetenu2",
      "tauxStableRetenu3",
      "tauxStableRetenu4",
      "tauxStableRetenu6",
      "tauxStableRetenu8",
      "tauxStableRetenu9",
      "tauxStableRetenu10",
    ];
    const numstauxapplique = [
      "tauxAppliqué1",
      "tauxAppliqué2",
      "tauxAppliqué3",
      "tauxAppliqué4",
      "tauxAppliqué6",
      "tauxAppliqué8",
      "tauxAppliqué9",
      "tauxAppliqué10",
    ];
    const tauxTFaible = [
      field("tauxRiskTFaible1").get(),
      field("tauxRiskTFaible2").get(),
      field("tauxRiskTFaible3").get(),
      field("tauxRiskTFaible4").get(),
      field("tauxRiskTFaible6").get(),
      field("tauxRiskTFaible8").get(),
      field("tauxRiskTFaible9").get(),
      field("tauxRiskTFaible10").get(),
    ];
    const tauxFaible = [
      field("tauxRiskFaible1").get(),
      field("tauxRiskFaible2").get(),
      field("tauxRiskFaible3").get(),
      field("tauxRiskFaible4").get(),
      field("tauxRiskFaible6").get(),
      field("tauxRiskFaible8").get(),
      field("tauxRiskFaible9").get(),
      field("tauxRiskFaible10").get(),
    ];
    const tauxMoyen = [
      field("tauxRiskMoyen1").get(),
      field("tauxRiskMoyen2").get(),
      field("tauxRiskMoyen3").get(),
      field("tauxRiskMoyen4").get(),
      field("tauxRiskMoyen6").get(),
      field("tauxRiskMoyen8").get(),
      field("tauxRiskMoyen9").get(),
      field("tauxRiskMoyen10").get(),
    ];
    const tauxEleve = [
      field("tauxRiskEleve1").get(),
      field("tauxRiskEleve2").get(),
      field("tauxRiskEleve3").get(),
      field("tauxRiskEleve4").get(),
      field("tauxRiskEleve6").get(),
      field("tauxRiskEleve8").get(),
      field("tauxRiskEleve9").get(),
      field("tauxRiskEleve10").get(),
    ];

    for (let i = 0; i < numsStableretenu.length; i++) {
      field(numsStableretenu[i]).set(tauxStable[i]);
    }

    if (field("661").get() === "1") {
      for (let i = 0; i < numstauxapplique.length; i++) {
        field(numstauxapplique[i]).set(tauxTFaible[i]);
      }
    }

    if (field("661").get() == "2") {
      for (let i = 0; i < numstauxapplique.length; i++) {
        field(numstauxapplique[i]).set(tauxFaible[i]);
      }
    }

    if (field("661").get() == "3") {
      for (let i = 0; i < numstauxapplique.length; i++) {
        field(numstauxapplique[i]).set(tauxMoyen[i]);
      }
    }

    if (field("661").get() == "4") {
      for (let i = 0; i < numstauxapplique.length; i++) {
        field(numstauxapplique[i]).set(tauxEleve[i]);
      }
    }
  });

  calcUtils.calc(function (calcUtils, field, form) {
    field("préliminaireSignification").get().length !== 0
      ? wpw.tax.saveMateriality(
          "overall",
          parseFloat(field("préliminaireSignification").get()),
          false
        )
      : wpw.tax.saveMateriality("overall", 0, false);
  });

  calcUtils.calc(function (calcUtils, field, form) {
    field("préliminairePlanification").get().length !== 0
      ? wpw.tax.saveMateriality(
          "performance",
          parseFloat(field("préliminairePlanification").get()),
          false
        )
      : wpw.tax.saveMateriality("performance", 0, false);
  });

  calcUtils.calc(function (calcUtils, field, form) {
    field("préliminaireAnomalie").get().length !== 0
      ? wpw.tax.saveMateriality(
          "trivial",
          parseFloat(field("préliminaireAnomalie").get()),
          false
        )
      : wpw.tax.saveMateriality("trivial", 0, false);
  });
  let soldesByLabels = "";
  calcUtils.calc(function (calcUtils, field) {
    return wpw.tax
      .evaluate([
        'tagbal(entityref("RC.1"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.2"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.3"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.4"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.5"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.6"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.1"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.2"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.3"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.4"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.1.01"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.1.02"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.2.03"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.2.01.04"), perspec(0), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.1"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.2"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.3"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.4"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.5"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.6"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.1"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.2"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.3"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.4"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.1.01"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.1.02"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.2.03"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.2.01.04"), perspec(-1), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.1"), perspec(-2), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.2"), perspec(-2), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.3"), perspec(-2), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.4"), perspec(-2), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.5"), perspec(-2), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RC.6"), perspec(-2), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.1"), perspec(-2), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.2"), perspec(-2), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.3"), perspec(-2), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.4"), perspec(-2), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.1.01"), perspec(-2), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("RP.1.02"), perspec(-2), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BP.1"), perspec(-2), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA"), perspec(-2), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.2.03"), perspec(-2), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
        'tagbal(entityref("BA.2.01.04"), perspec(-2), "BR", NaN, NaN, NaN, currencyoptions(false, true, false))',
      ])
      .then(function (Soldes) {
        Soldes = numbersFR(Soldes);
        const arrayLabels = [
          "soldeNResutEX",
          "soldeNRcai",
          "soldeNCA",
          "soldeNCP",
          "soldeNTB",
          "soldeNCExploit",
          "soldeNTPCR",
          "soldeNFRD",
          "soldeNM1ResutEX",
          "soldeNM1Rcai",
          "soldeNM1CA",
          "soldeNM1CP",
          "soldeNM1TB",
          "soldeNM1CExploit",
          "soldeNM1TPCR",
          "soldeNM1FRD",
          "soldeNM2ResutEX",
          "soldeNM2Rcai",
          "soldeNM2CA",
          "soldeNM2CP",
          "soldeNM2TB",
          "soldeNM2CExploit",
          "soldeNM2TPCR",
          "soldeNM2FRD",
        ];
        soldesByLabels = {
          soldeNResutEX:
            parseFloat(Soldes[0]) +
            parseFloat(Soldes[1]) +
            parseFloat(Soldes[2]) +
            parseFloat(Soldes[3]) +
            parseFloat(Soldes[4]) +
            parseFloat(Soldes[5]) +
            parseFloat(Soldes[6]) +
            parseFloat(Soldes[7]) +
            parseFloat(Soldes[8]) +
            parseFloat(Soldes[9]),
          soldeNRcai:
            parseFloat(Soldes[0]) +
            parseFloat(Soldes[1]) +
            parseFloat(Soldes[2]) +
            parseFloat(Soldes[3]) +
            parseFloat(Soldes[6]) +
            parseFloat(Soldes[7]) +
            parseFloat(Soldes[8]) +
            parseFloat(Soldes[9]),
          soldeNCA: parseFloat(Soldes[10]) + parseFloat(Soldes[11]),
          soldeNCP: parseFloat(Soldes[12]),
          soldeNTB: parseFloat(Soldes[13]),
          soldeNCExploit: parseFloat(Soldes[0]),
          soldeNTPCR: parseFloat(Soldes[14]),
          soldeNFRD: parseFloat(Soldes[15]),
          soldeNM1ResutEX:
            parseFloat(Soldes[16]) +
            parseFloat(Soldes[17]) +
            parseFloat(Soldes[18]) +
            parseFloat(Soldes[19]) +
            parseFloat(Soldes[20]) +
            parseFloat(Soldes[21]) +
            parseFloat(Soldes[22]) +
            parseFloat(Soldes[23]) +
            parseFloat(Soldes[24]) +
            parseFloat(Soldes[25]),
          soldeNM1Rcai:
            parseFloat(Soldes[16]) +
            parseFloat(Soldes[17]) +
            parseFloat(Soldes[18]) +
            parseFloat(Soldes[19]) +
            parseFloat(Soldes[22]) +
            parseFloat(Soldes[23]) +
            parseFloat(Soldes[24]) +
            parseFloat(Soldes[25]),
          soldeNM1CA: parseFloat(Soldes[26]) + parseFloat(Soldes[27]),
          soldeNM1CP: parseFloat(Soldes[28]),
          soldeNM1TB: parseFloat(Soldes[29]),
          soldeNM1CExploit: parseFloat(Soldes[16]),
          soldeNM1TPCR: parseFloat(Soldes[30]),
          soldeNM1FRD: parseFloat(Soldes[31]),
          soldeNM2ResutEX:
            parseFloat(Soldes[32]) +
            parseFloat(Soldes[33]) +
            parseFloat(Soldes[34]) +
            parseFloat(Soldes[35]) +
            parseFloat(Soldes[36]) +
            parseFloat(Soldes[37]) +
            parseFloat(Soldes[38]) +
            parseFloat(Soldes[39]) +
            parseFloat(Soldes[40]) +
            parseFloat(Soldes[41]),
          soldeNM2Rcai:
            parseFloat(Soldes[32]) +
            parseFloat(Soldes[33]) +
            parseFloat(Soldes[34]) +
            parseFloat(Soldes[35]) +
            parseFloat(Soldes[38]) +
            parseFloat(Soldes[39]) +
            parseFloat(Soldes[40]) +
            parseFloat(Soldes[41]),
          soldeNM2CA: parseFloat(Soldes[42]) + parseFloat(Soldes[43]),
          soldeNM2CP: parseFloat(Soldes[44]),
          soldeNM2TB: parseFloat(Soldes[45]),
          soldeNM2CExploit: parseFloat(Soldes[32]),
          soldeNM2TPCR: parseFloat(Soldes[46]),
          soldeNM2FRD: parseFloat(Soldes[47]),
        };
        for (let i = 0; i < arrayLabels.length; i++) {
          Number.isNaN(soldesByLabels[arrayLabels[i]])
            ? field(arrayLabels[i]).set("0")
            : field(arrayLabels[i]).set(
                new Intl.NumberFormat("fr-FR", {
                  maximumFractionDigits: 0,
                }).format(soldesByLabels[arrayLabels[i]] / 1000)
              );
        }
      });
  }),
    calcUtils.calc(function (calcUtils, field) {
      const soldes = [
        parseFloat(field("soldeNResutEX").get()),
        parseFloat(field("soldeNRcai").get()),
        parseFloat(field("soldeNCA").get()),
        parseFloat(field("soldeNCP").get()),
        parseFloat(field("soldeNTB").get()),
        parseFloat(field("soldeNCExploit").get()),
        parseFloat(field("soldeNTPCR").get()),
        parseFloat(field("soldeNFRD").get()),
        parseFloat(field("soldeNM1ResutEX").get()),
        parseFloat(field("soldeNM1Rcai").get()),
        parseFloat(field("soldeNM1CA").get()),
        parseFloat(field("soldeNM1CP").get()),
        parseFloat(field("soldeNM1TB").get()),
        parseFloat(field("soldeNM1CExploit").get()),
        parseFloat(field("soldeNM1TPCR").get()),
        parseFloat(field("soldeNM1FRD").get()),
        parseFloat(field("soldeNM2ResutEX").get()),
        parseFloat(field("soldeNM2Rcai").get()),
        parseFloat(field("soldeNM2CA").get()),
        parseFloat(field("soldeNM2CP").get()),
        parseFloat(field("soldeNM2TB").get()),
        parseFloat(field("soldeNM2CExploit").get()),
        parseFloat(field("soldeNM2TPCR").get()),
        parseFloat(field("soldeNM2FRD").get()),
      ];
      const seuilsLabels = [
        "seuilNResutEX",
        "seuilNRcai",
        "seuilNCA",
        "seuilNCP",
        "seuilNTB",
        "seuilNCExploit",
        "seuilNTPCR",
        "seuilNFRD",
        "seuilNM1ResutEX",
        "seuilNM1Rcai",
        "seuilNM1CA",
        "seuilNM1CP",
        "seuilNM1TB",
        "seuilNM1CExploit",
        "seuilNM1TPCR",
        "seuilNM1FRD",
        "seuilNM2ResutEX",
        "seuilNM2Rcai",
        "seuilNM2CA",
        "seuilNM2CP",
        "seuilNM2TB",
        "seuilNM2CExploit",
        "seuilNM2TPCR",
        "seuilNM2FRD",
      ];
      const tauxStable = [
        parseFloat(field("tauxAppliqué1").get()),
        parseFloat(field("tauxAppliqué2").get()),
        parseFloat(field("tauxAppliqué3").get()),
        parseFloat(field("tauxAppliqué4").get()),
        parseFloat(field("tauxAppliqué6").get()),
        parseFloat(field("tauxAppliqué8").get()),
        parseFloat(field("tauxAppliqué9").get()),
        parseFloat(field("tauxAppliqué10").get()),
      ];
      let j = 0;
      for (let i = 0; i < seuilsLabels.length; i++) {
        if (j === tauxStable.length) {
          j = 0;
        }
        Number.isNaN((soldes[i] * tauxStable[j]) / 100)
          ? field(seuilsLabels[i]).set("0")
          : (soldes[i] * tauxStable[j]) / 100 === 0
          ? field(seuilsLabels[i]).set("0")
          : field(seuilsLabels[i]).set((soldes[i] * tauxStable[j]) / 100);
        j++;
      }
    }),
    calcUtils.calc(function (calcUtils, field) {
      const seuilsN = [
        field("seuilNResutEX").get(),
        field("seuilNRcai").get(),
        field("seuilNCA").get(),
        field("seuilNCP").get(),
        field("seuilNTB").get(),
        field("seuilNCExploit").get(),
        field("seuilNTPCR").get(),
        field("seuilNFRD").get(),
      ];
      const seuilsNM1 = [
        field("seuilNM1ResutEX").get(),
        field("seuilNM1Rcai").get(),
        field("seuilNM1CA").get(),
        field("seuilNM1CP").get(),
        field("seuilNM1TB").get(),
        field("seuilNM1CExploit").get(),
        field("seuilNM1TPCR").get(),
        field("seuilNM1FRD").get(),
      ];
      const seuilsNM2 = [
        field("seuilNM2ResutEX").get(),
        field("seuilNM2Rcai").get(),
        field("seuilNM2CA").get(),
        field("seuilNM2CP").get(),
        field("seuilNM2TB").get(),
        field("seuilNM2CExploit").get(),
        field("seuilNM2TPCR").get(),
        field("seuilNM2FRD").get(),
      ];
      const checkbox = [
        field("critèreàretenir1").get(),
        field("critèreàretenir2").get(),
        field("critèreàretenir3").get(),
        field("critèreàretenir4").get(),
        field("critèreàretenir6").get(),
        field("critèreàretenir8").get(),
        field("critèreàretenir9").get(),
        field("critèreàretenir10").get(),
      ];
      let indentation = [];
      for (let i = 0; i < checkbox.length; i++) {
        if (checkbox.indexOf(true, i) !== -1) {
          indentation.push(checkbox.indexOf(true, i));
          i = checkbox.indexOf(true, i);
        }
      }
      const retenu = checkbox.filter((criteres) => criteres === true);
      let totalN = 0;
      let totalNM1 = 0;
      let totalNM2 = 0;
      for (let i = 0; i < retenu.length; i++) {
        totalN += seuilsN[indentation[i]];
        totalNM1 += seuilsNM1[indentation[i]];
        totalNM2 += seuilsNM2[indentation[i]];
      }
      Number.isNaN(totalN / retenu.length)
        ? field("moyenneSeuilN").set("-")
        : field("moyenneSeuilN").set((totalN / retenu.length).toFixed(2));
      Number.isNaN(totalNM1 / retenu.length)
        ? field("moyenneSeuilNM1").set("-")
        : field("moyenneSeuilNM1").set((totalNM1 / retenu.length).toFixed(2));
      Number.isNaN(totalNM2 / retenu.length)
        ? field("moyenneSeuilNM2").set("-")
        : field("moyenneSeuilNM2").set((totalNM2 / retenu.length).toFixed(2));
    });

  calcUtils.calc(function (calcUtils, field) {
    let evaluation = [
      parseFloat(field("percentapply0").get()),
      parseFloat(field("percentapply1").get()),
      parseFloat(field("percentapply2").get()),
      parseFloat(field("percentapply3").get()),
    ];
    evaluation = evaluation.filter((total) => total > 0);
    let moyenne = 0;
    for (let i = 0; i < evaluation.length; i++) {
      moyenne += evaluation[i];
    }
    moyenne = moyenne / evaluation.length;
    const parampercent = [
      field("paramPLANTauxLabel1").get(),
      field("paramPLANTauxLabel2").get(),
      field("paramPLANTauxLabel3").get(),
    ];
    const valuepercent = [
      field("valuepercent1").get(),
      field("valuepercent2").get(),
      field("valuepercent3").get(),
    ];
    if (moyenne < 1) {
      field("MoyenneRisquePercentapply").set(" ");
      field("percentresult").set("0");
    }

    if (moyenne >= 1 && moyenne <= parampercent[0]) {
      field("MoyenneRisquePercentapply").set("Taux risques faible");
      field("percentresult").set("1");
      field("percent").set(valuepercent[0]);
    }

    if (moyenne > parampercent[0] && moyenne <= parampercent[1]) {
      field("MoyenneRisquePercentapply").set("Taux risques moyen");
      field("percentresult").set("2");
      field("percent").set(valuepercent[1]);
    }

    if (moyenne > parampercent[2]) {
      field("MoyenneRisquePercentapply").set("Taux risques élevé");
      field("percentresult").set("3");
      field("percent").set(valuepercent[2]);
    }
  });

  calcUtils.calc(function (calcUtils, field) {
    let evaluation = [
      parseFloat(field("percentapplyANO0").get()),
      parseFloat(field("percentapplyANO1").get()),
    ];
    evaluation = evaluation.filter((total) => total > 0);
    let moyenne = 0;
    for (let i = 0; i < evaluation.length; i++) {
      moyenne += evaluation[i];
    }
    moyenne = moyenne / evaluation.length;
    const parampercent = [
      field("paramANOTauxLabel0").get(),
      field("paramANOTauxLabel12").get(),
      field("paramANOTauxLabel22").get(),
    ];

    if (moyenne < 1) {
      field("MoyenneRisquePercentapplyANO").set(" ");
      field("percentresultANO").set("0");
    }

    if (moyenne >= 1 && moyenne <= parampercent[0]) {
      field("MoyenneRisquePercentapplyANO").set("Taux risques faible");
      field("percentresultANO").set("1");
    }

    if (moyenne > parampercent[0] && moyenne <= parampercent[1]) {
      field("MoyenneRisquePercentapplyANO").set("Taux risques moyen");
      field("percentresultANO").set("2");
    }

    if (moyenne > parampercent[2]) {
      field("MoyenneRisquePercentapplyANO").set("Taux risques élevé");
      field("percentresultANO").set("3");
    }
  });

  calcUtils.calc(function (calcUtils, field) {
    field("paramTauxLabel11").set(
      `Supérieur à <b>${field(
        "paramTauxLabel0"
      ).get()}</b> et Inférieure ou égale à`
    );
    field("paramTauxLabel21").set(
      `Supérieur à <b>${field(
        "paramTauxLabel12"
      ).get()}</b> et Inférieure ou égale à`
    );
    field("paramTauxLabel3").set(field("paramTauxLabel22").get());

    field("paramPLANTauxLabel11").set(
      `Supérieur à <b>${field(
        "paramPLANTauxLabel1"
      ).get()}</b> et Inférieure ou égale à`
    );
  });
});
