(function () {
  "use strict";
  wpw.tax.create.formData("SEUIL", {
    formInfo: {
      abbreviation: "SEUIL",
      dynamicFormWidth: true,
      title: "Seuils de Signification et de Planification",
      highlightFieldsets: false,
      hideheader: true,
      headerImage: "cw",
      css: "se-builder-custom.css",
      category: "Start Here",
      showDots: false,
      documentMap: true,
      enableTabs: true,
      tabLimit: 7,
    },

    sections: [
      {
        header: "Résumé",
        headerClass: "font-header",
        tabConfig: {
          tabText: "Résumé",
        },
        rows: [
          {
            header: "1. Résumé des seuils",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  'Source : <a href="./index.jsp#/document/wMgdLJ0JQxW1qx--pfJq_Q" target="_blank">2.SE Seuils</a>',
              },
              {
                type: "table",
                num: "résuméseuils",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
        ],
      },
      {
        header: "Utilisateurs",
        headerClass: "font-header",
        tabConfig: {
          tabText: "Utilisateurs",
        },
        rows: [
          {
            header: "2. Utilisateurs des Etats Financier",
            headerClass: "font-subheader font-blue",
            subsection: [
              {
                label:
                  "Lister dans le tableau suivant les principaux utilisateurs des états financiers et les facteurs qui pourraient influencer une décision sur les Etats Financiers.",
              },
              {
                type: "table",
                num: "utilisateursetatfinancier",
                procedureControls: "true",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
        ],
      },
      {
        header: "3. Seuil(s) de Signification",
        headerClass: "font-header",
        tabConfig: {
          tabText: "S. de signification",
        },
        rows: [
          {
            label:
              "Suivant les entités, certains comptes sont significatifs indépendamment de la notion de seuil. Il est alors nécessaire de définir un seuil de montant inférieur ou de définir manuellement le cycle en significatif si les automatismes de CaseWare ne permettent pas de les sélectionner.",
          },
          {
            header:
              "a - Facteurs susceptibles d'influencer le choix des critères",
            headerClass: "font-subheader font-blue title",
            subsection: [
              {
                label:
                  "Pour la détermination des critères, le commissaire aux comptes doit faire appel à son jugement professionnel et utiliser des agrégats financiers adaptés à l'entité ainsi qu'à son contenu. En fonction de l'évaluation des différents facteurs, le taux à appliquer aux différents critères sera modifié.",
              },
              {
                type: "table",
                num: "facteursinfluence",
              },
              {
                labelClass: "fullLength",
              },
              {
                type: "table",
                num: "facteursinfluenceResult",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header: "b - Critères pertinents",
            headerClass: "font-subheader font-blue title",
            subsection: [
              {
                label:
                  "Les éléments influençant le choix de l'un ou de l'autre de ces critères sont précisés au <b>paragraphe 17 de la NEP-320</b>.",
              },
              {
                label:
                  "Il est possible de prédéterminer les critères pertinents pour le calcul du seuil en fonction du secteur d'activité ou la catégorie (Start up, entité en difficulté…) et leur stabilité dans le temps. Pour cela :",
              },
              {
                label:
                  "<ol><li>Définir un critère catégorie: Code Nace, Start Up, Entreprise en difficulté ou Autre. La catégorie Code NACE est celle choisie par défaut. Pour toute modification à apporter aux paramétrages par défaut, se référer à l'onglet Paramétrage.</li><li>Valider la stabilité du critère: Un critère est dit stable lorsque celui-ci a une variation entre deux exercices inférieure au taux de stabilité renseigné dans la colonne dédiée du tableau ci-dessous</li></ol>",
              },
              {
                label: "Type critère à retenir",
                num: "critèreretenu",
                inputClass: "fullLength",
                type: "dropdown",
                init: "0",
                options: [
                  {
                    option: " ",
                    value: 0,
                  },
                  {
                    option: "Code NACE",
                    value: 1,
                  },
                  {
                    option: "Startup",
                    value: 2,
                  },
                  {
                    option: "Entité en difficulté financière",
                    value: 3,
                  },
                  {
                    option: "Holding",
                    value: 4,
                  },
                ],
              },
              {
                label: " ",
                labelSource: { fieldId: "codecritère" },
              },
              {
                label: " ",
                labelSource: { fieldId: "paramsecteur" },
              },
              {
                type: "table",
                num: "tauxretenuheader",
              },
              {
                labelClass: "fullLength",
              },
              {
                type: "table",
                num: "tauxretenu",
              },
              {
                labelClass: "fullLength",
              },
              {
                type: "table",
                num: "tauxretenuresults",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header: "c - Valeur du Seuil de signification",
            headerClass: "font-subheader font-blue title",
            subsection: [
              {
                label:
                  'Lors de la planification de son audit, le commissaire aux comptes détermine un Seuil de signification au niveau des comptes pris dans leur ensemble. La valeur "préliminaire" de ce seuil est reprise automatiquement dans le document <a href="./index.jsp#/se-risks/assessment" target="_blank">SAR - Sections A Risque</a> et permet d\'identifier les sections au-delà du seuil de signification.',
              },
              {
                type: "table",
                num: "seuilsignification",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header: "d - Seuil(s) de signification de montant(s) inférieur(s)",
            headerClass: "font-subheader font-blue title",
            subsection: [
              {
                label:
                  "Pour apprécier si des seuils de signification d'un montant moins élevé que le seuil de signification retenu au niveau des comptes pris dans leur ensemble sont nécessaires pour certaines catégories d'opérations, certains soldes comptables ou certaines informations fournies dans l'annexe, le commissaire aux comptes prend notamment en compte <b>(paragraphe 16 de la NEP-320)</b> :",
              },
              {
                label:
                  "<ul><li>les informations sensibles des comptes en fonction du secteur d'activité de l'entité</li><li>l'existence de règles comptables ou de textes légaux ou réglementaires spécifiques à l'entité ou à son secteur</li><li>la réalisation d'opérations particulières au cours de l'exercice.</li></ul>",
              },
              {
                type: "table",
                num: "seuilmontantinferieur",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
        ],
      },
      {
        header: "4. Seuil(s) de Planification",
        headerClass: "font-header",
        tabConfig: {
          tabText: "S. de planification",
        },
        rows: [
          {
            label:
              "Lors de la planification de son audit, le commissaire aux comptes détermine un Seuil de planification de la mission, utilisé pour définir la nature et l'étendue des travaux. Ce seuil est fixé à un montant tel qu'il permet de réduire à un niveau acceptable le risque que le montant des anomalies relevées non corrigées et des anomalies non détectées excède le Seuil de signification.",
          },
          {
            label:
              'La valeur "préliminaire" de ce seuil est reprise automatiquement en entête du document <a href="./index.jsp#/se-risks/assessment" target="_blank">SAR - Sections A Risque</a>',
          },
          {
            header: "a - Pourcentage à appliquer",
            headerClass: "font-subheader font-blue title",
            subsection: [
              {
                label:
                  "Evaluer le niveau de risque des critères listés dans le tableau pour conclure sur un pourcentage à appliquer au seuil de signification et ainsi déterminer le seuil de planification.",
              },
              {
                type: "table",
                num: "percenttoapply",
              },
              {
                labelClass: "fullLength",
              },
              {
                type: "table",
                num: "percenttoapplyresult",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header: "b - Valeur du Seuil de Planification",
            headerClass: "font-subheader font-blue title",
            subsection: [
              {
                label: " ",
                labelSource: { fieldId: "percentseuilSI" },
              },
              {
                type: "table",
                num: "seuilplanification",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header: "c - Seuil(s) de planification de montant(s) inférieur(s)",
            headerClass: "font-subheader font-blue title",
            subsection: [
              {
                label:
                  "Si le commissaire aux comptes a estimé nécessaire de fixer un ou des Seuils de signification de montants inférieurs pour certains flux d'opérations, soldes de comptes ou informations, il détermine pour ce ou chacun de ces seuils de signification un seuil de planification <b>(paragraphe 21 de la NEP-320)</b>.",
              },
              {
                label:
                  'Les valeurs "finales" de ces seuils sont reprises automatiquement dans le document <a href="./index.jsp#/se-risks/assessment" target="_blank">SAR - Sections A Risque</a> et permettent d\'identifier les sections au-delà des seuils de planification définis ci-dessous.',
              },
              {
                type: "table",
                num: "planmontantinférieur",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
        ],
      },
      {
        header: "5. Anomalies manifestement insignifiantes",
        headerClass: "font-header",
        tabConfig: {
          tabText: "Ano. insignifiantes",
        },
        rows: [
          {
            label:
              "Le commissaire aux comptes doit par ailleurs définir un seuil en deçà duquel les anomalies relevées sont considérées comme insignifiantes et qui ne feront donc pas l'objet d'une communication avec les organes mentionnés à l'article L. 823-16 du code de commerce <b>( NEP-450 )</b>.",
          },
          {
            header: "a - Pourcentage à appliquer",
            headerClass: "font-subheader font-blue title",
            subsection: [
              {
                label:
                  "Le Seuil d'anomalies manifestement insignifiante généralement déterminé en appliquant un pourcentage au seuil de planification ou au seuil de signification. Ce pourcentage peut etre déterminé en fonction d'un niveau de risque basé sur plusieurs critères : ",
              },
              {
                type: "table",
                num: "percenttoapplyANO",
              },
              {
                labelClass: "fullLength",
              },
              {
                type: "table",
                num: "percenttoapplyresultANO",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header:
              "b - Valeur du seuil d'anomalies manifestement insignifiantes",
            headerClass: "font-subheader font-blue title",
            subsection: [
              {
                label: " ",
                labelSource: { fieldId: "percentseuilANO" },
              },
              {
                type: "table",
                num: "seuilanomalie",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
        ],
      },
      {
        header: "6. Modification des seuils au cours de la mission",
        headerClass: "font-header",
        tabConfig: {
          tabText: "Modifications",
        },
        rows: [
          {
            label:
              "Cet onglet permet de justifier des modifications du ou des seuil(s) au cours de la mission.Le suivi des évaluations permet de formalisation de la validation des seuils de versions préliminaires ainsi que ceux de versions finales.",
          },
          {
            header: "a - Justification des modifications",
            headerClass: "font-subheader font-blue title",
            subsection: [
              {
                label:
                  "Au cours de la mission, le commissaire aux comptes reconsidère le seuil ou les seuils de signification s'il a connaissance de faits nouveaux ou d'évolution de l'entité qui remettent en cause l'évaluation préliminaire de ces seuils.",
              },
              {
                label:
                  "Si le commissaire aux comptes conclut que la fixation d'un ou de seuils de signification moins élevé(s) que celui ou ceux initialement fixé(s) est appropriée, il détermine s'il est nécessaire de modifier le ou les seuils de planification, et si la nature et l'étendue des procédures d'audit complémentaires qu'il a définies restent appropriées",
              },
              {
                type: "table",
                num: "justifications",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header: "b - Suivi des évaluations",
            headerClass: "font-subheader font-blue title",
            subsection: [
              {
                type: "table",
                num: "evalPRELI",
              },
              {
                labelClass: "fullLength",
              },
              {
                type: "table",
                num: "evalFINAL",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
        ],
      },
      {
        header: "7. Paramétrage global du document",
        headerClass: "font-header",
        tabConfig: {
          tabText: "Paramétrages",
        },
        rows: [
          {
            label:
              'Il est préférable de modifier les paramétrages au niveau modèle cabinet.Pour toute question, envoyer une demande depuis le centre d\'aide : <a href="https://cwfsupport.zendesk.com/hc/fr" target="_blank">support.caseware.fr</a>',
          },
          {
            header: "a - Définition des taux pour le seuil de signification",
            headerClass: "font-subheader font-blue title",
            subsection: [
              {
                label:
                  "Cette partie permet de déterminer les différents taux applicables (et leurs modalités d'applications) aux critères permettant de définir le seuil de signification de l'entité.",
              },
              {
                type: "table",
                num: "paramDefTaux",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header: "b - Critères pré-déterminés selon le secteur d'activité",
            headerClass: "font-subheader font-blue title",
            subsection: [
              {
                label:
                  "Cette section permet de définir des critères automatiquement selon un code (Par exemple : le code NACE)",
              },
              {
                label:
                  "Un critère est dit stable lorsque celui-ci a une variation entre deux exercices inférieures au taux de stabilité renseigné dans la colonne dédiée du tableau ci-dessous. En fonction des paramétrages définis dans le document, si le critère pertinent de la catégorie (Secteur d’activité, Startup, Entité en difficulté …) choisie pour l’entité auditée est stable alors il sera sélectionné pour la détermination du seuil.",
              },
              {
                type: "table",
                num: "paramCritèresPrédéterHeader",
              },
              {
                labelClass: "fullLength",
              },
              {
                type: "table",
                num: "paramCritèresPrédéter",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header:
              "c - Définition du pourcentage pour le seuil de planification",
            headerClass: "font-subheader font-blue title",
            subsection: [
              {
                label:
                  "Cette partie permet de déterminer les différents taux applicables (et leurs modalités d'applications) aux critères permettant de définir le seuil de planification de l'entité",
              },
              {
                type: "table",
                num: "paramDefTauxPLAN",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
          {
            header:
              "d - Définition des pourcentage pour le seuil des anomalies manifestement insignifiantes",
            headerClass: "font-subheader font-blue title",
            subsection: [
              {
                label:
                  "Cette partie permet de déterminer les différents taux applicables (et leurs modalités d'applications) aux critères permettant de définir le seuil des anomalies manifestement insignifiantes de l'entité.",
              },
              {
                label:
                  "Il faut aussi déterminer à quel seuil ce poucentage va être appliqué : ",
                num: "deterSeuil",
                inputClass: "fullLength",
                type: "dropdown",
                init: "1",
                options: [
                  {
                    option: "Seuil de planification",
                    value: 1,
                  },
                  {
                    option: "Seuil de signification",
                    value: 2,
                  },
                ],
              },
              {
                type: "table",
                num: "paramDefTauxANO",
              },
              {
                labelClass: "fullLength",
              },
            ],
          },
        ],
      },
    ],
  });
})();
