(function (wpw) {
  wpw.taxMain.config([
    'taxConfigProvider',
    function (conf) {
      //Invoke config functions here
      conf.setCssRoot('prod/com.caseware.fr.e.audit.fr.1/');
      conf.setCssFilesLocal(['se-builder-custom.css']);
      conf.enableTabs();
      conf.setDocumentMap(true);
    },
  ]);
})(wpw);
