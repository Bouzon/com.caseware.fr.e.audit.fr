(function (wpw) {
    'use strict';

    if (!wpw.injectedAnalysisDefinitions)
        wpw.injectedAnalysisDefinitions = [];

    var COMPONENT_TYPES = wpw.analysis.ComponentType;
    var ComponentDefinition = wpw.analysis.ComponentDefinition;

    var analysisDef = new wpw.analysis.AnalysisDefinition();
    analysisDef.id = 'jaqQPTPrTTKGGVfRk-drmw';
    analysisDef.name = 'Revana';
    
     //Fournisseurs
    var componentFournBil = new ComponentDefinition('dDK6h-wjQuS3T_LklozowQ', 'Achats/Frs - Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentFournBil.breakdownGroupNumbers = ['BA.3.02', 'BA.3.08.01', 'BP.4.05.01', 'BP.4.05.04', 'BP.4.05.09', 'BP.4.05.11', 'BP.4.05.12', 'BP.4.05.13', 'BP.4.05.14', 'BA.3.03.02.01'];
    componentFournBil.allowEdit = false;
    analysisDef.componentDefinitions.push(componentFournBil);

    //Achats et charges externes Compte de resultat
    var componentFournCdR = new ComponentDefinition('xvkBV3fIRXugTTDnVHD-VQ', 'Achats/Frs - CdR', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentFournCdR.breakdownGroupNumbers = ['RC.1.01.01', 'RC.1.01.02', 'RC.1.01.03', 'RC.1.01.04', 'RC.1.03.01', 'RC.1.03.02', 'RC.1.03.03', 'RC.1.03.04', 'RC.1.05.01', 'RC.1.05.02', 'RC.1.05.03', 'RC.1.05.04', 'RC.1.05.05', 'RC.1.05.06', 'RC.1.05.07', 'RC.1.05.08', 'RC.1.05.09', 'RC.1.05.10', 'RC.1.05.11', 'RC.1.05.12', 'RC.1.05.13', 'RC.1.05.14', 'RC.1.05.15', 'RC.1.05.16', 'RC.1.05.17', 'RC.1.05.18', 'RC.1.05.19', 'RC.1.05.20', 'RC.1.05.21', 'RC.1.05.22', 'RC.1.05.23', 'RC.1.05.24', 'RC.1.05.25', 'RP.3.03.05'];
    componentFournCdR.allowEdit = false;
    analysisDef.componentDefinitions.push(componentFournCdR);
    
    // Client Bilan
    var componentClientBil = new ComponentDefinition('TqBkY80UT22QW_uapccakQ', 'Ventes/Clts - Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentClientBil.breakdownGroupNumbers = ['BA.3.03.01.01', 'BA.3.03.01.02', 'BA.3.03.01.03', 'BA.3.03.01.04', 'BA.3.03.01.05', 'BP.4.08.22', 'BP.4.10.01', 'BP.4.08.28', 'BP.4.08.31', 'BP.4.08.32', 'BP.4.08.33', 'BP.4.08.24'];
    componentClientBil.allowEdit = false;
    analysisDef.componentDefinitions.push(componentClientBil);

    // Clients Compte de résultat
    var componentClientCdR = new ComponentDefinition('cxtWhDj6Q3ipeV0khUhYDw', 'Ventes/Clts - CdR', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentClientCdR.breakdownGroupNumbers = ['RC.1.08.07.01', 'RC.1.08.07.03', 'RC.1.10.03', 'RC.3.02.09', 'RP.1.01', 'RP.1.02.01', 'RP.1.02.02', 'RP.1.02.03', 'RP.1.02.04', 'RP.1.02.05', 'RP.1.02.06', 'RP.1.02.07', 'RP.1.02.08', 'RP.1.06.06', 'RP.3.03.02'];
    componentClientCdR.allowEdit = false;
    analysisDef.componentDefinitions.push(componentClientCdR);
    
    // Stocks Bilan
    var componentStockBil = new ComponentDefinition('8DfGMM-uT-eGxQzwpA_aag', 'Stocks - Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentStockBil.breakdownGroupNumbers = ['BA.3.01.01.02', 'BA.3.01.01.03', 'BA.3.01.01.04', 'BA.3.01.01.05', 'BA.3.01.01.06', 'BA.3.01.01.07', 'BA.3.01.01.08', 'BA.3.01.01.09', 'BA.3.01.01.10', 'BA.3.01.01.11', 'BA.3.01.02.01', 'BA.3.01.02.02', 'BA.3.01.02.03', 'BA.3.01.02.04', 'BA.3.01.02.05', 'BA.3.01.02.06', 'BA.3.01.02.07', 'BA.3.01.02.08', 'BA.3.01.03', 'BA.3.01.03.01', 'BA.3.01.03.02', 'BA.3.01.03.03', 'BA.3.01.04', 'BA.3.01.04.01'];
    componentStockBil.allowEdit = false;
    analysisDef.componentDefinitions.push(componentStockBil);

    // Stocks Compte de résultat
    var componentStockCdR = new ComponentDefinition('2u9va3LHTsmOyTLpvwD-Dg', 'Stocks - CdR', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentStockCdR.breakdownGroupNumbers = ['RC.1.02', 'RC.1.04.01', 'RC.1.04.02', 'RC.1.08.07.02', 'RC.4.03.03', 'RP.1.03', 'RP.1.06.05', 'RP.4.03.05'];
    componentStockCdR.allowEdit = false;
    analysisDef.componentDefinitions.push(componentStockCdR);
    
    // Personnel Bilan
    var componentPersBil = new ComponentDefinition('-Kee3OSqQ2CtxqQuNSFSfA', 'Personnel - Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentPersBil.breakdownGroupNumbers = ['BA.3.03.02.07', 'BA.3.03.02.08', 'BA.3.03.02.09', 'BA.3.03.02.10', 'BA.3.03.02.11', 'BA.3.03.02.12', 'BA.3.03.02.13', 'BA.3.03.02.14', 'BA.3.03.02.15', 'BA.3.03.02.16', 'BP.4.06.02', 'BP.4.06.03', 'BP.4.06.04', 'BP.4.06.07', 'BP.4.04.17', 'BP.4.06.08', 'BP.4.06.09', 'BP.4.06.14', 'BP.4.06.15', 'BP.4.06.16'];
    componentPersBil.allowEdit = false;
    analysisDef.componentDefinitions.push(componentPersBil);

    // Personnel Compte de résultat
    var componentPersCdR = new ComponentDefinition('66kxTca0SIeReO470Xbe8A', 'Personnel - CdR', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentPersCdR.breakdownGroupNumbers = ['RC.1.07.01', 'RC.1.07.02', 'RC.1.08.01', 'RC.1.08.02', 'RC.1.08.03', 'RC.1.08.04', 'RC.1.08.05'];
    componentPersCdR.allowEdit = false;
    analysisDef.componentDefinitions.push(componentPersCdR);
    
    // Trésorerie Financement Bilan
    var componentTresoBil = new ComponentDefinition('-5RGo7i7Q1KzIgS44Gxeqw', 'Tréso/Financement - Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentTresoBil.breakdownGroupNumbers = ['BP.4.01.01','BP.4.01.02','BP.4.02.01','BP.4.02.02','BP.4.02.03','BP.4.03.01','BP.4.03.02','BP.4.04.01','BP.4.04.02','BP.4.04.03','BP.4.04.04','BP.4.04.05','BP.4.04.06','BP.4.04.07','BP.4.04.08','BP.4.04.09','BP.4.04.10','BP.4.04.11','BA.3.05.01.01','BA.3.05.01.02','BA.3.05.01.02','BA.3.05.01.03','BA.3.05.02.01','BA.3.05.02.01','BA.3.05.02.02','BA.3.05.02.03','BA.3.05.02.04','BA.3.05.02.05','BA.3.05.02.06','BA.3.05.02.07','BA.3.05.02.08','BA.3.05.02.09','BA.3.05.02.10','BA.3.06','BA.3.07.01','BA.3.07.01','BA.3.07.02','BA.3.07.02','BA.3.07.02','BA.3.07.03','BA.3.07.04','BA.3.07.05','BA.3.07.06','BA.3.07.07','BA.3.07.07','BA.3.07.07','BA.3.07.08','BA.3.07.08','BA.3.07.09','BA.3.07.10','BA.3.07.11','BA.3.07.12'];
    componentTresoBil.allowEdit = false;
    analysisDef.componentDefinitions.push(componentTresoBil);

   // Trésorerie Financement Compte de resultat
   var componentTresoCdR = new ComponentDefinition('_j2zc3HWTKy-RbbW72zjOQ', 'Tréso/Financement - CDR', COMPONENT_TYPES.ACCOUNTS_TABLE);
   componentTresoCdR.breakdownGroupNumbers = ['RC.3.01','RC.3.02.01','RC.3.02.02','RC.3.02.03','RC.3.02.05','RC.3.02.06','RC.3.02.07','RC.3.02.08','RC.3.02.10','RC.3.04','RP.3.03.01','RP.3.03.03','RP.3.03.04','RP.3.03.06','RP.3.04.01','RP.3.04.02','RP.3.04.03','RP.3.04.05','RP.3.06'];
   componentTresoCdR.allowEdit = false;
   analysisDef.componentDefinitions.push(componentTresoCdR);
    
    // Immobilisation Bilan
    var componentImmoBil = new ComponentDefinition('CLmMZc62REOXIhIPNzMWKw', 'Immobilisations - Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentImmoBil.breakdownGroupNumbers = ['BA.2.01.01', 'BA.2.01.02', 'BA.2.01.03.01', 'BA.2.01.03.02', 'BA.2.01.03.03', 'BA.2.01.04.01', 'BA.2.01.04.02', 'BA.2.01.05.01', 'BA.2.01.05.02', 'BA.2.01.06.01', 'BA.2.01.06.02', 'BA.2.01.06.03', 'BA.2.01.06.04', 'BA.2.01.06.05', 'BA.2.01.07.01', 'BA.2.01.07.02', 'BA.2.01.07.03', 'BA.2.01.08', 'BA.2.01.09', 'BA.2.02', 'BA.2.02.01', 'BA.2.02.02.01', 'BA.2.02.02.02', 'BA.2.02.02.03', 'BA.2.02.02.04', 'BA.2.02.02.05', 'BA.2.02.02.06', 'BA.2.02.03.01', 'BA.2.02.03.02', 'BA.2.02.03.03', 'BA.2.02.03.04', 'BA.2.02.03.05', 'BA.2.02.04.01', 'BA.2.02.04.02', 'BA.2.02.04.03', 'BA.2.02.05.01', 'BA.2.02.05.02', 'BA.2.02.05.03', 'BA.2.02.05.04', 'BA.2.02.05.05', 'BA.2.02.05.06', 'BA.2.02.06', 'BA.2.02.06.01', 'BA.2.02.06.02', 'BA.2.02.06.03', 'BA.2.02.07', 'BA.2.03.01.01', 'BA.2.03.01.02', 'BA.2.03.01.03', 'BA.2.03.01.04', 'BA.2.03.01.05', 'BA.2.03.01.06', 'BA.2.03.01.07', 'BA.2.03.01.08', 'BA.2.03.01.09', 'BA.2.03.02.06', 'BA.2.03.02.07', 'BA.2.03.02.08', 'BA.2.03.03', 'BA.2.03.04.01', 'BA.2.03.04.02', 'BA.2.03.04.03', 'BA.2.03.04.04', 'BA.2.03.04.05', 'BA.2.03.04.06', 'BA.2.03.05.01', 'BA.2.03.05.02', 'BA.2.03.06.01', 'BA.2.03.06.02', 'BA.2.03.06.03', 'BA.2.03.06.04', 'BA.2.03.06.05', 'BA.2.03.06.06', 'BA.2.03.06.07', 'BA.2.03.06.08', 'BA.2.03.06.09', 'BA.2.03.06.10', 'BA.2.03.06.11', 'BA.2.03.06.12', 'BA.3.01.01.01', 'BA.3.03.02.35'];
    componentImmoBil.allowEdit = false;
    analysisDef.componentDefinitions.push(componentImmoBil);

    // Immobilisation Compte de résultat
    var componentImmoCdR = new ComponentDefinition('t4oF8yhLSwKNqITwFxxD8g', 'Immobilisations - CdR', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentImmoCdR.breakdownGroupNumbers = ['RC.1.09.01', 'RC.1.09.02', 'RC.1.09.03', 'RC.4.02.02', 'RC.4.02.03', 'RC.4.02.04', 'RC.4.03.01', 'RC.4.03.02', 'RP.1.04.01', 'RP.1.04.02', 'RP.1.04.03', 'RP.1.06.01', 'RP.1.06.03', 'RP.1.06.04', 'RP.3.01', 'RP.3.02', 'RP.3.04.04', 'RP.4.02.02', 'RP.4.02.03', 'RP.4.02.04', 'RP.4.03.01', 'RP.4.03.02'];
    componentImmoCdR.allowEdit = false;
    analysisDef.componentDefinitions.push(componentImmoCdR);
    
     // Immobilisation Financières Bilan
    var componentImmoFinBil = new ComponentDefinition('AGwU7mQ5SFmMgl9DucqfSA', 'Immos Fi - Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentImmoFinBil.breakdownGroupNumbers = ['BA.2.03.01','BA.2.03.01.01','BA.2.03.01.02','BA.2.03.01.03','BA.2.03.01.04','BA.2.03.01.05','BA.2.03.01.06','BA.2.03.01.07','BA.2.03.01.08','BA.2.03.01.09','BA.2.03.02','BA.2.03.02.01','BA.2.03.02.02','BA.2.03.02.03','BA.2.03.02.04','BA.2.03.02.05','BA.2.03.02.06','BA.2.03.02.07','BA.2.03.02.08','BA.2.03.03','BA.2.03.04','BA.2.03.05','BA.2.03.06','BA.2.03.06.01','BA.2.03.06.02','BA.2.03.06.03','BA.2.03.06.04','BA.2.03.06.05','BA.2.03.06.06','BA.2.03.06.07','BA.2.03.06.08','BA.2.03.06.09','BA.2.03.06.10','BA.2.03.06.11','BA.2.03.06.12','BA.2.03.06.13'];
    componentImmoFinBil.allowEdit = false;
    analysisDef.componentDefinitions.push(componentImmoFinBil);

    // Immobilisation Financières Compte de résultat
    var componentImmoFinCdR = new ComponentDefinition('fLZElm9KSbqDqctMYbUchg', 'Immos Fi - CdR', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentImmoFinCdR.breakdownGroupNumbers = ['RP.3.01','RP.3.02'];
    componentImmoFinCdR.allowEdit = false;
    analysisDef.componentDefinitions.push(componentImmoFinCdR);

    // Capitaux propres Bilan
    var componentCapiBil = new ComponentDefinition('F_pKhWaoSrygRVKr9CsqyQ', 'Capitaux Propres - Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentCapiBil.breakdownGroupNumbers = ['BA.1', 'BA.3.03.01.06', 'BA.3.03.02.26', 'BA.3.03.02.27', 'BA.3.03.02.28', 'BA.3.03.02.29', 'BA.3.03.02.30', 'BA.3.03.02.31', 'BA.3.03.02.32', 'BA.3.03.02.33', 'BA.3.03.02.34', 'BA.3.04', 'BP.1.01.01', 'BP.1.01.02', 'BP.1.02.01', 'BP.1.02.02', 'BP.1.03', 'BP.1.04', 'BP.1.05.01', 'BP.1.05.02', 'BP.1.05.03.01', 'BP.1.05.03.02', 'BP.1.05.04', 'BP.1.06.01', 'BP.1.06.02', 'BP.1.07.01', 'BP.1.07.02', 'BP.1.08.01', 'BP.1.08.02', 'BP.1.08.03', 'BP.1.09.01', 'BP.1.09.02', 'BP.1.09.03', 'BP.1.09.04', 'BP.1.09.06', 'BP.1.09.07', 'BP.1.09.08', 'BP.3.01.01', 'BP.3.01.02', 'BP.3.01.03', 'BP.3.01.04', 'BP.3.01.05', 'BP.3.01.06', 'BP.3.01.07', 'BP.4.06.01'];
    componentCapiBil.allowEdit = false;
    analysisDef.componentDefinitions.push(componentCapiBil);

    // Capitaux propres Compte de résultat
    var componentCapiCdR = new ComponentDefinition('2iWXNUhsTS6mLwThKAZpRw', 'Capitaux Propres - CdR', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentCapiCdR.breakdownGroupNumbers = ['RC.1.06.01', 'RC.1.09', 'RC.3.02.04', 'RC.4.03.05', 'RP.1.06.02', 'RP.4.03.03', 'RP.4.03.07'];
    componentCapiCdR.allowEdit = false;
    analysisDef.componentDefinitions.push(componentCapiCdR);
    
    // Provisions Bilan
    var componentProvBil = new ComponentDefinition('f5w5QrHSRcyRTVsye1Wfdg', 'PRC - Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentProvBil.breakdownGroupNumbers = ['BP.3.01','BP.3.02.01','BP.3.02.02','BP.3.02.03','BP.3.02.04','BP.3.02.05','BP.3.02.06'];
    componentProvBil.allowEdit = false;
    analysisDef.componentDefinitions.push(componentProvBil);

    // Provisions CdR
    var componentProvCdR = new ComponentDefinition('UhwdmnwwRAWuJheLYoj0XA', 'PRC- CdR', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentProvCdR.breakdownGroupNumbers = ['RC.1.11','RP.1.06.02','RP.3.04.01','RP.3.04.02','RP.4.03.07'];
    componentProvCdR.allowEdit = false;
    analysisDef.componentDefinitions.push(componentProvCdR);
    
    // Impôts et taxes Bilan
    var componentEtatBil = new ComponentDefinition('3c9KbpE-Q76WpgWFd2mVwg', 'Impots/taxes - Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentEtatBil.breakdownGroupNumbers = ['BA.3.03.02.17', 'BA.3.03.02.18', 'BA.3.03.02.19', 'BA.3.03.02.20', 'BA.3.03.02.21', 'BA.3.03.02.22', 'BA.3.03.02.23', 'BA.3.03.02.24', 'BA.3.03.02.25', 'BP.4.06.20', 'BP.4.06.22', 'BP.4.06.23', 'BP.4.06.24', 'BP.4.06.27', 'BP.4.06.28', 'BP.4.06.34', 'BP.4.06.35', 'BP.4.06.36', 'BP.4.06.40'];
    componentEtatBil.allowEdit = false;
    analysisDef.componentDefinitions.push(componentEtatBil);

    // Impôts et taxes Compte de résultat
    var componentEtatCdR = new ComponentDefinition('9rftezRsRhGCABA5C_LyYw', 'Impots/taxes - CdR', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentEtatCdR.breakdownGroupNumbers = ['RC.1.06.02', 'RC.1.06.03', 'RC.1.06.04', 'RC.1.06.05', 'RC.5', 'RC.6.01', 'RC.6.02', 'RC.6.03', 'RC.6.04'];
    componentEtatCdR.allowEdit = false;
    analysisDef.componentDefinitions.push(componentEtatCdR);

    // Autres Comptes Bilan
    var componentAutresBil = new ComponentDefinition('lbSrx_RPQYuBx-lWNiTXWA', 'Autres C & D - Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentAutresBil.breakdownGroupNumbers = ['BA.3.03.01.10', 'BA.3.03.02.36', 'BA.3.03.02.37', 'BA.3.03.02.38', 'BA.3.03.02.38', 'BA.3.03.02.38', 'BA.3.03.02.39', 'BA.3.03.02.40', 'BA.3.03.02.41', 'BA.3.03.02.42', 'BA.3.03.02.43', 'BA.3.03.02.44', 'BA.3.03.02.45', 'BA.3.08.02', 'BA.3.09', 'BA.3.11', 'BP.4.08.23', 'BP.4.11'];
    componentAutresBil.allowEdit = false;
    analysisDef.componentDefinitions.push(componentAutresBil);

    // Autres Comptes Compte de résultat
    var componentAutresCdR = new ComponentDefinition('9uSyWPI7QJm-V0q9-QcEnA', 'Autres C & D - CdR', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentAutresCdR.breakdownGroupNumbers = ['RC.1.08.06.02', 'RC.1.10.01', 'RC.1.10.02', 'RC.1.10.04', 'RC.2.01', 'RC.3.03', 'RC.4.03.04', 'RC.4.03.06', 'RP.1.05', 'RP.1.06.07', 'RP.1.07.01', 'RP.1.07.02', 'RP.1.07.03', 'RP.1.07.04', 'RP.1.07.05', 'RP.2.01', 'RP.3.04.06', 'RP.3.05', 'RP.4.03.04', 'RP.4.03.06', 'RP.4.03.08', 'RP.4.03.09'];
    componentAutresCdR.allowEdit = false;
    analysisDef.componentDefinitions.push(componentAutresCdR);

    // Résultat exceptionnel
    var componentResE = new ComponentDefinition('m61Ypa3STzK9GGwqFI-XOw', 'Résultat exceptionnel', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentResE.breakdownGroupNumbers = ['RC.4.01.01', 'RC.4.01.02', 'RC.4.01.03', 'RC.4.01.04', 'RC.4.01.05', 'RC.4.02.01', 'RC.4.02.05', 'RC.4.02.06', 'RP.4.01.01', 'RP.4.01.02', 'RP.4.01.03', 'RP.4.02.01', 'RP.4.02.05', 'RP.4.02.06', 'RP.4.02.07'];
    componentResE.allowEdit = false;
    analysisDef.componentDefinitions.push(componentResE);
    wpw.injectedAnalysisDefinitions.push(analysisDef);

})(wpw);