(function(wpw) {
    'use strict';
  
    if (!wpw.injectedAnalysisDefinitions)
      wpw.injectedAnalysisDefinitions = [];
  
    var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  
    var DATA_TYPE = wpw.analysis.DataType;
    var COMPONENT_TYPES = wpw.analysis.ComponentType;
    var CHART_TYPES = wpw.analysis.ChartType;
    var KPIModel = wpw.analysis.KPIModel;
    var ComponentDefinition = wpw.analysis.ComponentDefinition;

    /********************* Formulas ***************************/
    var CAPITAL_ET_RESERVES_FORMULE = TAG('BP.1.01');
    var REPORT_A_NOUVEAU_FORMULE = TAG('BP.1.06');
    var SUBVENTIONS_D_INVESTISSEMENT_FORMULE = TAG('BP.1.08');
    var PROVISIONS_RÉGLEMENTÉES_FORMULE = TAG('BP.1.09');
    var GROUPE_ET_ASSOCIÉS_DEBITEUR_FORMULE = TAG('4.5');
    var GROUPE_ET_ASSOCIÉS_CREDITEUR_FORMULE = TAG();
	var DEP_DES_COMPTES_DU_GROUPE_FORMULE = TAG('4.9.5');
    /********************* Formulas ends **********************/

    var analysisDef = new wpw.analysis.AnalysisDefinition();
    analysisDef.id = 'RpPmKv-NRu2CXp749_lbHA';
    analysisDef.name = 'Capitaux propres et comptes courants';

    // Capitaux propres Bilan
    var componentBil = new ComponentDefinition('0GlkgKt4Q0CjYGbFgTp7Gw', 'Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentBil.breakdownGroupNumbers = ['BA.1','BA.3.03.01.06','BA.3.03.02.26','BA.3.03.02.27','BA.3.03.02.28','BA.3.03.02.29','BA.3.03.02.30','BA.3.03.02.31','BA.3.03.02.32','BA.3.03.02.33','BA.3.03.02.34','BA.3.04','BP.1.01.01','BP.1.01.02','BP.1.02.01','BP.1.02.02','BP.1.03','BP.1.04','BP.1.05.01','BP.1.05.02','BP.1.05.03.01','BP.1.05.03.02','BP.1.05.04','BP.1.06.01','BP.1.06.02','BP.1.07.01','BP.1.07.02','BP.1.08.01','BP.1.08.02','BP.1.08.03','BP.1.09.01','BP.1.09.02','BP.1.09.03','BP.1.09.04','BP.1.09.06','BP.1.09.07','BP.1.09.08','BP.4.06.01'];
    componentBil.allowEdit = false;
    analysisDef.componentDefinitions.push(componentBil);


    //Graphique Bilan
    var CAPITAL_ET_RESERVES = new KPIModel('CAPITAL_ET_RESERVES', 'Capital et réserves', DATA_TYPE.NUMBER, true, null, CAPITAL_ET_RESERVES_FORMULE);
    var REPORT_A_NOUVEAU = new KPIModel('REPORT_A_NOUVEAU', 'Report à nouveau', DATA_TYPE.NUMBER, true, null, REPORT_A_NOUVEAU_FORMULE);
    var SUBVENTIONS_D_INVESTISSEMENT = new KPIModel('SUBVENTIONS_D_INVESTISSEMENT', 'Subventions d\'investissement', DATA_TYPE.NUMBER, true, null, SUBVENTIONS_D_INVESTISSEMENT_FORMULE);
    var PROVISIONS_RÉGLEMENTÉES = new KPIModel('PROVISIONS_RÉGLEMENTÉES', 'Provisions réglementées', DATA_TYPE.NUMBER, true, null, PROVISIONS_RÉGLEMENTÉES_FORMULE);
    var GROUPE_ET_ASSOCIÉS_DEBITEUR = new KPIModel('GROUPE_ET_ASSOCIÉS', 'Groupe et associés', DATA_TYPE.NUMBER, true, null, GROUPE_ET_ASSOCIÉS_DEBITEUR_FORMULE);
	var GROUPE_ET_ASSOCIÉS_CREDITEUR = new KPIModel('GROUPE_ET_ASSOCIÉS_CREDITEUR', 'Groupe et associés', DATA_TYPE.NUMBER, true, null, GROUPE_ET_ASSOCIÉS_CREDITEUR_FORMULE);
    var DEP_DES_COMPTES_DU_GROUPE = new KPIModel('DEP_DES_COMPTES_DU_GROUPE', 'Dep. des comptes du groupe', DATA_TYPE.NUMBER, true, null, DEP_DES_COMPTES_DU_GROUPE_FORMULE);

    var componentBilGraph = new ComponentDefinition('04Jcut8IRBys5uCNIRAuoQ', 'Graphique Bilan', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [CAPITAL_ET_RESERVES.key,REPORT_A_NOUVEAU.key,SUBVENTIONS_D_INVESTISSEMENT.key,PROVISIONS_RÉGLEMENTÉES.key,GROUPE_ET_ASSOCIÉS_DEBITEUR.key,GROUPE_ET_ASSOCIÉS_CREDITEUR.key,DEP_DES_COMPTES_DU_GROUPE.key]);

    analysisDef.addUniqueKpiModels([CAPITAL_ET_RESERVES,REPORT_A_NOUVEAU,SUBVENTIONS_D_INVESTISSEMENT,PROVISIONS_RÉGLEMENTÉES,GROUPE_ET_ASSOCIÉS_DEBITEUR,GROUPE_ET_ASSOCIÉS_CREDITEUR,DEP_DES_COMPTES_DU_GROUPE]);
    analysisDef.componentDefinitions.push(componentBilGraph);
    
    wpw.injectedAnalysisDefinitions.push(analysisDef);
})(wpw);