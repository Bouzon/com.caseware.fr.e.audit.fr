(function(wpw) {
    'use strict';
  
    if (!wpw.injectedAnalysisDefinitions)
      wpw.injectedAnalysisDefinitions = [];
  
    var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  
    var DATA_TYPE = wpw.analysis.DataType;
    var COMPONENT_TYPES = wpw.analysis.ComponentType;
    var CHART_TYPES = wpw.analysis.ChartType;
    var KPIModel = wpw.analysis.KPIModel;
    var ComponentDefinition = wpw.analysis.ComponentDefinition;

    /********************* Formulas ***************************/
    var DÉBITEURS_DIVERS_ET_CRÉDITEURS_DIVERS_FORMULE = TAG('BA.3.03.02.37');
    var COMPTES_TRANSITOIRES_OU_D_ATTENTE_FORMULE = TAG('BA.3.03.02.39');
    var DIVERS_CAP_PAR_FORMULE = TAG('BA.3.03.02.38');
    var CHARGES_A_REP_EXO_FORMULE = TAG('BA.3.09');
    var AUTRES_CHARGES_DE_GESTION_COURANTE_FORMULE = TAG('RC.1.12');
    var SUBVENTIONS_D_EXPLOITATION_FORMULE = TAG('RP.1.05');
    var AUTRES_PRODUITS_DE_GESTION_COURANTE_FORMULE = TAG('RP.1.07');
    var TRANSFERTS_DE_CHARGES_FORMULE = TAG('RP.1.06.07');
    /********************* Formulas ends **********************/

    var analysisDef = new wpw.analysis.AnalysisDefinition();
    analysisDef.id = 'PAFkEV7wQXmzxTg7TupBIA';
    analysisDef.name = 'Autres comptes';

    // Autres Comptes Bilan
    var componentBilan = new ComponentDefinition('lbSrx_RPQYuBx-lWNiTXWA', 'Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentBilan.breakdownGroupNumbers = ['BA.3.03.01.10','BA.3.03.02.36','BA.3.03.02.37','BA.3.03.02.38','BA.3.03.02.38','BA.3.03.02.38','BA.3.03.02.39','BA.3.03.02.40','BA.3.03.02.41','BA.3.03.02.42','BA.3.03.02.43','BA.3.03.02.44','BA.3.03.02.45','BA.3.08.02','BA.3.09','BA.3.11','BP.4.08.23','BP.4.11'];
    componentBilan.allowEdit = false;
    analysisDef.componentDefinitions.push(componentBilan);

    // Autres Comptes Compte de résultat
    var componentCdR = new ComponentDefinition('9uSyWPI7QJm-V0q9-QcEnA', 'Compte de Résultat', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentCdR.breakdownGroupNumbers = ['RC.1.08.06.02','RC.1.10.01','RC.1.10.02','RC.1.10.04','RC.2.01','RC.3.03','RC.4.03.04','RC.4.03.06','RP.1.05','RP.1.06.07','RP.1.07.01','RP.1.07.02','RP.1.07.03','RP.1.07.04','RP.1.07.05','RP.2.01','RP.3.04.06','RP.3.05','RP.4.03.04','RP.4.03.06','RP.4.03.08','RP.4.03.09'];
    componentCdR.allowEdit = false;
    analysisDef.componentDefinitions.push(componentCdR);

    //Graphique Bilan
    var DÉBITEURS_DIVERS_ET_CRÉDITEURS_DIVERS = new KPIModel('DÉBITEURS_DIVERS_ET_CRÉDITEURS_DIVERS', 'Débiteurs divers et créditeurs divers', DATA_TYPE.NUMBER, true, null, DÉBITEURS_DIVERS_ET_CRÉDITEURS_DIVERS_FORMULE);
    var COMPTES_TRANSITOIRES_OU_D_ATTENTE = new KPIModel('COMPTES_TRANSITOIRES_OU_D_ATTENTE', 'Comptes transistoires ou d\'attente', DATA_TYPE.NUMBER, true, null, COMPTES_TRANSITOIRES_OU_D_ATTENTE_FORMULE);
    var DIVERS_CAP_PAR = new KPIModel('DIVERS_CAP_PAR', 'Comptes de régularisation', DATA_TYPE.NUMBER, true, null, DIVERS_CAP_PAR_FORMULE);
    var CHARGES_A_REP_EXO = new KPIModel('CHARGES_A_REP_EXO', 'Dep. des comptes de créditeurs divers', DATA_TYPE.NUMBER, true, null, CHARGES_A_REP_EXO_FORMULE);

    var componentBilanGraph = new ComponentDefinition('5IGahwpURQ6VInHL3EDEew', 'Bilan', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [DÉBITEURS_DIVERS_ET_CRÉDITEURS_DIVERS.key,COMPTES_TRANSITOIRES_OU_D_ATTENTE.key,DIVERS_CAP_PAR.key,CHARGES_A_REP_EXO.key]);

    analysisDef.addUniqueKpiModels([DÉBITEURS_DIVERS_ET_CRÉDITEURS_DIVERS,COMPTES_TRANSITOIRES_OU_D_ATTENTE,DIVERS_CAP_PAR,CHARGES_A_REP_EXO]);
    analysisDef.componentDefinitions.push(componentBilanGraph);

    //Graphique Compte de résultat
    var AUTRES_CHARGES_DE_GESTION_COURANTE = new KPIModel('AUTRES_CHARGES_DE_GESTION_COURANTE', 'Autres charges de gestion courante', DATA_TYPE.NUMBER, true, null, AUTRES_CHARGES_DE_GESTION_COURANTE_FORMULE);
    var SUBVENTIONS_D_EXPLOITATION = new KPIModel('SUBVENTIONS_D_EXPLOITATION', 'Subventions d\'exploitation', DATA_TYPE.NUMBER, true, null, SUBVENTIONS_D_EXPLOITATION_FORMULE);
    var AUTRES_PRODUITS_DE_GESTION_COURANTE = new KPIModel('AUTRES_PRODUITS_DE_GESTION_COURANTE', 'Autres produits de gestion courante', DATA_TYPE.NUMBER, true, null, AUTRES_PRODUITS_DE_GESTION_COURANTE_FORMULE);
    var TRANSFERTS_DE_CHARGES = new KPIModel('TRANSFERTS_DE_CHARGES', 'Transfert de charges', DATA_TYPE.NUMBER, true, null, TRANSFERTS_DE_CHARGES_FORMULE);

    var componentCdRGraph = new ComponentDefinition('9CorthvVTv6q7XEbzs4Z_A', 'Compte de Résultat', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [AUTRES_CHARGES_DE_GESTION_COURANTE.key,SUBVENTIONS_D_EXPLOITATION.key,AUTRES_PRODUITS_DE_GESTION_COURANTE.key,TRANSFERTS_DE_CHARGES.key]);
    
    analysisDef.addUniqueKpiModels([AUTRES_CHARGES_DE_GESTION_COURANTE,SUBVENTIONS_D_EXPLOITATION,AUTRES_PRODUITS_DE_GESTION_COURANTE,TRANSFERTS_DE_CHARGES]);
    analysisDef.componentDefinitions.push(componentCdRGraph);
    
    wpw.injectedAnalysisDefinitions.push(analysisDef);
})(wpw);