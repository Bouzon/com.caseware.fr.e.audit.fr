(function(wpw) {
  'use strict';

  if (!wpw.injectedAnalysisDefinitions)
    wpw.injectedAnalysisDefinitions = [];

  var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;

  var DATA_TYPE = wpw.analysis.DataType;
  var COMPONENT_TYPES = wpw.analysis.ComponentType;
  var CHART_TYPES = wpw.analysis.ChartType;
  var KPIModel = wpw.analysis.KPIModel;
  var ComponentDefinition = wpw.analysis.ComponentDefinition;

  /********************* Formulas ***************************/
  var EMPRUNTS_OBLIGATAIRES_CONVERTIBLES_FORMULE = TAG('BP.4.01');
  var AUTRES_EMPRUNTS_OBLIGATAIRES_FORMULE = TAG('BP.4.02');
  var EMPRUNTS_DETTES_ETABCRED_FORMULE = TAG('BP.4.03')
  var EMPRUNTS_DETTES_DIVERS_FORMULE = TAG('BP.4.04');
  var VALEURS_MOBILIÈRES_DE_PLACEMENT_FORMULE = TAG('BA.3.05');
  var BANQUE_FORMULE = TAG('BA.3.07');
  var INSTRUMENTS_DE_TRÉSORERIE_FORMULE = TAG('BA.3.06');
  var CHARGES_FORMULE = TAG('RC.3');
  var PRODUITS_FORMULE = TAG('RP.3');
  /********************* Formulas ends **********************/

  var analysisDef = new wpw.analysis.AnalysisDefinition();
  analysisDef.id = 'RmdsNWnRR4qZk0s27XG8Yw';
  analysisDef.name = 'Trésorerie et Financement';

  // Trésorerie Financement Bilan
  var componentBil = new ComponentDefinition('-5RGo7i7Q1KzIgS44Gxeqw', 'Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
  componentBil.breakdownGroupNumbers = ['BP.4.01.01','BP.4.01.02','BP.4.02.01','BP.4.02.02','BP.4.02.03','BP.4.03.01','BP.4.03.02','BP.4.04.01','BP.4.04.02','BP.4.04.03','BP.4.04.04','BP.4.04.05','BP.4.04.06','BP.4.04.07','BP.4.04.08','BP.4.04.09','BP.4.04.10','BP.4.04.11','BA.3.05.01.01','BA.3.05.01.02','BA.3.05.01.02','BA.3.05.01.03','BA.3.05.02.01','BA.3.05.02.01','BA.3.05.02.02','BA.3.05.02.03','BA.3.05.02.04','BA.3.05.02.05','BA.3.05.02.06','BA.3.05.02.07','BA.3.05.02.08','BA.3.05.02.09','BA.3.05.02.10','BA.3.06','BA.3.07.01','BA.3.07.01','BA.3.07.02','BA.3.07.02','BA.3.07.02','BA.3.07.03','BA.3.07.04','BA.3.07.05','BA.3.07.06','BA.3.07.07','BA.3.07.07','BA.3.07.07','BA.3.07.08','BA.3.07.08','BA.3.07.09','BA.3.07.10','BA.3.07.11','BA.3.07.12'];
  componentBil.allowEdit = false;
  analysisDef.componentDefinitions.push(componentBil);

  // Trésorerie Financement Compte de resultat
  var componentCdR = new ComponentDefinition('_j2zc3HWTKy-RbbW72zjOQ', 'Compte de resultat', COMPONENT_TYPES.ACCOUNTS_TABLE);
  componentCdR.breakdownGroupNumbers = ['RC.3.01','RC.3.02.01','RC.3.02.02','RC.3.02.03','RC.3.02.05','RC.3.02.06','RC.3.02.07','RC.3.02.08','RC.3.02.10','RC.3.04','RP.3.03.01','RP.3.03.03','RP.3.03.04','RP.3.03.06','RP.3.04.01','RP.3.04.02','RP.3.04.03','RP.3.04.05','RP.3.06'];
  componentCdR.allowEdit = false;
  analysisDef.componentDefinitions.push(componentCdR);

  //Graphique Bilan 
  var EMPRUNTS_OBLIGATAIRES_CONVERTIBLES = new KPIModel('EMPRUNTS_OBLIGATAIRES_CONVERTIBLES', 'Emprunts obligataires convertibles', DATA_TYPE.NUMBER, true, null, EMPRUNTS_OBLIGATAIRES_CONVERTIBLES_FORMULE);
  var AUTRES_EMPRUNTS_OBLIGATAIRES = new KPIModel('AUTRES_EMPRUNTS_OBLIGATAIRES', 'Autres emprunts obligataires', DATA_TYPE.NUMBER, true, null, AUTRES_EMPRUNTS_OBLIGATAIRES_FORMULE);
  var VALEURS_MOBILIÈRES_DE_PLACEMENT = new KPIModel('VALEURS_MOBILIÈRES_DE_PLACEMENT', 'Valeurs mobilières de placement', DATA_TYPE.NUMBER, true, null, VALEURS_MOBILIÈRES_DE_PLACEMENT_FORMULE);
  var BANQUES = new KPIModel('BANQUES', 'Banque et établissement financiers', DATA_TYPE.NUMBER, true, null, BANQUE_FORMULE);
  var INSTRUMENTS_DE_TRÉSORERIE = new KPIModel('INSTRUMENTS_DE_TRÉSORERIE', 'Instruments de trésorerie', DATA_TYPE.NUMBER, true, null, INSTRUMENTS_DE_TRÉSORERIE_FORMULE);
  var EMPRUNTS_DETTES_ETABCRED = new KPIModel('EMPRUNTS_DETTES_ETABCRED', 'Emprunts et dettes auprès des étab. de crédit', DATA_TYPE.NUMBER, true, null, EMPRUNTS_DETTES_ETABCRED_FORMULE);
  var EMPRUNTS_DETTES_DIVERS = new KPIModel('EMPRUNTS_DETTES_DIVERS', 'Emprunts et dettes financières divers', DATA_TYPE.NUMBER, true, null, EMPRUNTS_DETTES_DIVERS_FORMULE);

  var componentBilGraph = new ComponentDefinition('C9nrmi4SSJeRHfj2_Qbsxg', 'Bilan', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [EMPRUNTS_OBLIGATAIRES_CONVERTIBLES.key,AUTRES_EMPRUNTS_OBLIGATAIRES.key,EMPRUNTS_DETTES_ETABCRED.key,EMPRUNTS_DETTES_DIVERS.key,VALEURS_MOBILIÈRES_DE_PLACEMENT.key,BANQUES.key,INSTRUMENTS_DE_TRÉSORERIE.key]);
  analysisDef.addUniqueKpiModels([EMPRUNTS_OBLIGATAIRES_CONVERTIBLES,AUTRES_EMPRUNTS_OBLIGATAIRES,EMPRUNTS_DETTES_ETABCRED,EMPRUNTS_DETTES_DIVERS,VALEURS_MOBILIÈRES_DE_PLACEMENT,BANQUES,INSTRUMENTS_DE_TRÉSORERIE]);
  analysisDef.componentDefinitions.push(componentBilGraph);

  //Graphique CdR
  var CHARGES = new KPIModel('CHARGES', 'Charges financières', DATA_TYPE.NUMBER, true, null, CHARGES_FORMULE);
  var PRODUITS = new KPIModel('PRODUITS', 'Produits financiers', DATA_TYPE.NUMBER, true, null, PRODUITS_FORMULE);

  var componentCdRGraph = new ComponentDefinition('Hn6lVUWzTrOfZEXQNlWSaQ', 'Compte de resultat', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [CHARGES.key, PRODUITS.key]);
  analysisDef.addUniqueKpiModels([CHARGES,PRODUITS]);
  analysisDef.componentDefinitions.push(componentCdRGraph);

  wpw.injectedAnalysisDefinitions.push(analysisDef);
})(wpw);