(function(wpw) {
    'use strict';
  
    if (!wpw.injectedAnalysisDefinitions)
      wpw.injectedAnalysisDefinitions = [];
  
    var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  
    var DATA_TYPE = wpw.analysis.DataType;
    var COMPONENT_TYPES = wpw.analysis.ComponentType;
    var CHART_TYPES = wpw.analysis.ChartType;
    var KPIModel = wpw.analysis.KPIModel;
    var ComponentDefinition = wpw.analysis.ComponentDefinition;

    /********************* Formulas ***************************/
    var PARTICIPATIONS_ET_CREANCES_RATTACHEES_FORMULE = TAG('BA.2.03');
    var PRODUITS_FINANCIERS_FORMULE = TAG('RP.3.01');
    var PRODUITS_FINANCIERS2_FORMULE = TAG('RP.3.02');
    /********************* Formulas ends **********************/

    var analysisDef = new wpw.analysis.AnalysisDefinition();
    analysisDef.id = 'Xno53uolQ6GYq-CCk7zwyQ';
    analysisDef.name = 'Immobilisations financières';

    // Immobilisation Financières Bilan
    var component = new ComponentDefinition('AGwU7mQ5SFmMgl9DucqfSA', 'Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    component.breakdownGroupNumbers = ['BA.2.03.01','BA.2.03.01.01','BA.2.03.01.02','BA.2.03.01.03','BA.2.03.01.04','BA.2.03.01.05','BA.2.03.01.06','BA.2.03.01.07','BA.2.03.01.08','BA.2.03.01.09','BA.2.03.02','BA.2.03.02.01','BA.2.03.02.02','BA.2.03.02.03','BA.2.03.02.04','BA.2.03.02.05','BA.2.03.02.06','BA.2.03.02.07','BA.2.03.02.08','BA.2.03.03','BA.2.03.04','BA.2.03.05','BA.2.03.06','BA.2.03.06.01','BA.2.03.06.02','BA.2.03.06.03','BA.2.03.06.04','BA.2.03.06.05','BA.2.03.06.06','BA.2.03.06.07','BA.2.03.06.08','BA.2.03.06.09','BA.2.03.06.10','BA.2.03.06.11','BA.2.03.06.12','BA.2.03.06.13'];
    component.allowEdit = false;
    analysisDef.componentDefinitions.push(component);

    // Immobilisation Financières Compte de résultat
    component = new ComponentDefinition('fLZElm9KSbqDqctMYbUchg', 'Compte de résultat', COMPONENT_TYPES.ACCOUNTS_TABLE);
    component.breakdownGroupNumbers = ['RP.3.01','RP.3.02'];
    component.allowEdit = false;
    analysisDef.componentDefinitions.push(component);

    //Graphique Bilan
    var PARTICIPATIONS_ET_CREANCES_RATTACHEES = new KPIModel('PARTICIPATIONS_ET_CREANCES_RATTACHEES', 'Participations et créances rattachées', DATA_TYPE.NUMBER, true, null, PARTICIPATIONS_ET_CREANCES_RATTACHEES_FORMULE);
    
    component = new ComponentDefinition('oMmx4hkER1ewvVhFrg7ZmQ', 'Bilan', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [PARTICIPATIONS_ET_CREANCES_RATTACHEES.key]);
    
    analysisDef.addUniqueKpiModels([PARTICIPATIONS_ET_CREANCES_RATTACHEES]);
    analysisDef.componentDefinitions.push(component);
    
    //Graphique Compte de résultat
    var PRODUITS_FINANCIERS = new KPIModel('PRODUITS_FINANCIERS', 'Produits financiers', DATA_TYPE.NUMBER, true, null, PRODUITS_FINANCIERS_FORMULE);
	var PRODUITS_FINANCIERS2 = new KPIModel('PRODUITS_FINANCIERS', 'Produits financiers', DATA_TYPE.NUMBER, true, null, PRODUITS_FINANCIERS2_FORMULE);

    component = new ComponentDefinition('srGyFPPeReiv6OEf_WcymQ', 'Compte de résultat', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [PRODUITS_FINANCIERS.key,PRODUITS_FINANCIERS2.key]);

    analysisDef.addUniqueKpiModels([PRODUITS_FINANCIERS,PRODUITS_FINANCIERS2]);
    analysisDef.componentDefinitions.push(component);

    wpw.injectedAnalysisDefinitions.push(analysisDef);
})(wpw);