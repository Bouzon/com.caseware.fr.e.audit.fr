(function(wpw) {
    'use strict';
  
    if (!wpw.injectedAnalysisDefinitions)
      wpw.injectedAnalysisDefinitions = [];
  
    var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  
    var DATA_TYPE = wpw.analysis.DataType;
    var COMPONENT_TYPES = wpw.analysis.ComponentType;
    var CHART_TYPES = wpw.analysis.ChartType;
    var KPIModel = wpw.analysis.KPIModel;
    var ComponentDefinition = wpw.analysis.ComponentDefinition;

    /********************* Formulas ***************************/
    var FOURNISSEURS_FORMULE = TAG('BP.4.05.01')
	var FOURNISSEURS_ANORMAUX_FORMULE = TAG('BA.3.03.02.01');
	var FACTURES_NP_FORMULE = TAG('BP.4.05.09');
    var ACHATS_MARCHANDISES_FORMULE = TAG('RC.1.01');
    var ACHATS_MP_FORMULE = TAG('RC.1.03');
	var AUTRES_ACHATS_CE_FORMULE = TAG('RC.1.05');
    /********************* Formulas ends **********************/

    var analysisDef = new wpw.analysis.AnalysisDefinition();
    analysisDef.id = 'xQSm2gRGRM6QskvsjsG67g';
    analysisDef.name = 'Fournisseurs, achats et charges externes';

    //Fournisseurs
    var componentBil = new ComponentDefinition('dDK6h-wjQuS3T_LklozowQ', 'Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentBil.breakdownGroupNumbers = ['BA.3.02','BA.3.08.01','BP.4.05.01','BP.4.05.04','BP.4.05.09','BP.4.05.11','BP.4.05.12','BP.4.05.13','BP.4.05.14','BA.3.03.02.01'];
    componentBil.allowEdit = false;
    analysisDef.componentDefinitions.push(componentBil);

    //Achats et charges externes Compte de resultat
    var componentCdR = new ComponentDefinition('xvkBV3fIRXugTTDnVHD-VQ', 'Compte de résultat', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentCdR.breakdownGroupNumbers = ['RC.1.01.01','RC.1.01.02','RC.1.01.03','RC.1.01.04','RC.1.03.01','RC.1.03.02','RC.1.03.03','RC.1.03.04','RC.1.05.01','RC.1.05.02','RC.1.05.03','RC.1.05.04','RC.1.05.05','RC.1.05.06','RC.1.05.07','RC.1.05.08','RC.1.05.09','RC.1.05.10','RC.1.05.11','RC.1.05.12','RC.1.05.13','RC.1.05.14','RC.1.05.15','RC.1.05.16','RC.1.05.17','RC.1.05.18','RC.1.05.19','RC.1.05.20','RC.1.05.21','RC.1.05.22','RC.1.05.23','RC.1.05.24','RC.1.05.25','RP.3.03.05'];
    componentCdR.allowEdit = false;
    analysisDef.componentDefinitions.push(componentCdR);

    //Graphique Bilan
    var FOURNISSEURS = new KPIModel('FOURNISSEURS', 'Fournisseurs', DATA_TYPE.NUMBER, true, null, FOURNISSEURS_FORMULE);
    var FOURNISSEURS_ANORMAUX = new KPIModel('FOURNISSEURS_ANORMAUX', 'Fournisseurs débiteurs', DATA_TYPE.NUMBER, true, null, FOURNISSEURS_ANORMAUX_FORMULE);
	var FACTURES_NP = new KPIModel('FACTURES_NP_NP', 'Factures non parvenues', DATA_TYPE.NUMBER, true, null, FACTURES_NP_FORMULE);
	var componentBilGraph = new ComponentDefinition('VYq1aajtR2Sj0XJ9_ZsjtQ', 'Bilan', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [FOURNISSEURS.key,FOURNISSEURS_ANORMAUX.key,FACTURES_NP.key]);
    analysisDef.addUniqueKpiModels([FOURNISSEURS,FOURNISSEURS_ANORMAUX,FACTURES_NP]);
    analysisDef.componentDefinitions.push(componentBilGraph);

    //Graphique Compte de résultat
    var ACHATS_MARCHANDISES = new KPIModel('ACHATS', 'Achats de marchandises', DATA_TYPE.NUMBER, true, null, ACHATS_MARCHANDISES_FORMULE);
    var ACHATS_MP = new KPIModel('SACHATS_MP', 'Achats de matières premières et autres approvisionnement ', DATA_TYPE.NUMBER, true, null, ACHATS_MP_FORMULE);
    var AUTRES_ACHATS_CE = new KPIModel('AUTRES_ACHATS_CE', 'Autres achats et charges externes', DATA_TYPE.NUMBER, true, null, AUTRES_ACHATS_CE_FORMULE);
	var componentCdRGraph = new ComponentDefinition('LjeKsXUpQT6dD9yNitSQ9Q', 'Compte de Résultat', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [ACHATS_MARCHANDISES.key, ACHATS_MP.key, AUTRES_ACHATS_CE.key]);
    analysisDef.addUniqueKpiModels([ACHATS_MARCHANDISES, ACHATS_MP, AUTRES_ACHATS_CE]);
    analysisDef.componentDefinitions.push(componentCdRGraph);


    wpw.injectedAnalysisDefinitions.push(analysisDef);
})(wpw);