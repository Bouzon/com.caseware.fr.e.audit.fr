(function(wpw) {
    'use strict';
  
    if (!wpw.injectedAnalysisDefinitions)
      wpw.injectedAnalysisDefinitions = [];
  
    var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  
    var DATA_TYPE = wpw.analysis.DataType;
    var COMPONENT_TYPES = wpw.analysis.ComponentType;
    var CHART_TYPES = wpw.analysis.ChartType;
    var KPIModel = wpw.analysis.KPIModel;
    var ComponentDefinition = wpw.analysis.ComponentDefinition;

    /********************* Formulas ***************************/
    var PROVISIONS_RISQUES_FORMULE = TAG('BP.3.01');
	var PROVISIONS_CHARGES_FORMULE = TAG('BP.3.02');
	var DOT_PROVISIONS_FORMULE = TAG('RC.1.11');
	var REP_PROVISIONS_EXPLOIT_FORMULE = TAG('RP.1.06.02');
	var REP_PROVISIONS_FINANCIER_PDT_FORMULE = TAG('RP.3.04.01');
	var REP_PROVISIONS_FINANCIER_CHG_FORMULE = TAG('RP.3.04.02');
	var REP_PROVISIONS_EXCEPT_FORMULE = TAG('RP.4.03.07');
    /********************* Formulas ends **********************/

    var analysisDef = new wpw.analysis.AnalysisDefinition();
    analysisDef.id = 'lbFs5Y0RTeqcwvkseBXMxA';
    analysisDef.name = 'Provisions pour Risques et Charges';

    // Capitaux propres Bilan
    var component = new ComponentDefinition('F_pKhWaoSrygRVKr9CsqyQ', 'Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    component.breakdownGroupNumbers = ['BP.3.01','BP.3.02.01','BP.3.02.02','BP.3.02.03','BP.3.02.04','BP.3.02.05','BP.3.02.06'];
    component.allowEdit = false;
    analysisDef.componentDefinitions.push(component);

    // Capitaux propres Compte de rÃ©sultat
    component = new ComponentDefinition('2iWXNUhsTS6mLwThKAZpRw', 'CdR', COMPONENT_TYPES.ACCOUNTS_TABLE);
    component.breakdownGroupNumbers = ['RC.1.11','RP.1.06.02','RP.3.04.01','RP.3.04.02','RP.4.03.07'];
    component.allowEdit = false;
    analysisDef.componentDefinitions.push(component);

    //Graphique Bilan
    var PROVISIONS_RISQUES = new KPIModel('PROVISIONS_RISQUES', 'Provisions sur risques', DATA_TYPE.NUMBER, true, null, PROVISIONS_RISQUES_FORMULE);
    var PROVISIONS_CHARGES = new KPIModel('PROVISIONS_CHARGES', 'Provisions sur charges', DATA_TYPE.NUMBER, true, null, PROVISIONS_CHARGES_FORMULE);

    component = new ComponentDefinition('KN1nbfvDS4iqjI3BSBTRMw', 'Graphique Bilan', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [PROVISIONS_RISQUES.key,PROVISIONS_CHARGES.key]);

    analysisDef.addUniqueKpiModels([PROVISIONS_RISQUES,PROVISIONS_CHARGES]);
    analysisDef.componentDefinitions.push(component);

    //Graphique Compte de rÃ©sultat
    var DOT_PROVISIONS = new KPIModel('DOT_PROVISIONS', 'Dot. sur provisions', DATA_TYPE.NUMBER, true, null, DOT_PROVISIONS_FORMULE);
    var REP_PROVISIONS_EXPLOIT = new KPIModel('REP_PROVISIONS_EXPLOIT', 'Rep. sur provisions ch. exploitation', DATA_TYPE.NUMBER, true, null, REP_PROVISIONS_EXPLOIT_FORMULE);
    var REP_PROVISIONS_FINANCIER_PDT = new KPIModel('REP_PROVISIONS_FINANCIER_PDT', 'Rep. sur provisions pdt. financiers', DATA_TYPE.NUMBER, true, null, REP_PROVISIONS_FINANCIER_PDT_FORMULE);
   var REP_PROVISIONS_FINANCIER_CHG = new KPIModel('REP_PROVISIONS_FINANCIER_CHG', 'Rep. sur provisions ch. financières', DATA_TYPE.NUMBER, true, null, REP_PROVISIONS_FINANCIER_CHG_FORMULE);
   var REP_PROVISIONS_EXCEPT = new KPIModel('REP_PROVISIONS_EXCEPT', 'Rep. sur provisions exceptionnelles', DATA_TYPE.NUMBER, true, null, REP_PROVISIONS_EXCEPT_FORMULE);
    component = new ComponentDefinition('RZ56WOvhQImiJ_oFEL5Xhg', 'Graphique CdR', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [DOT_PROVISIONS.key,REP_PROVISIONS_EXPLOIT.key,	REP_PROVISIONS_FINANCIER_PDT.key, REP_PROVISIONS_FINANCIER_CHG.key, REP_PROVISIONS_EXCEPT.key]);
    
    analysisDef.addUniqueKpiModels([DOT_PROVISIONS,REP_PROVISIONS_EXPLOIT,REP_PROVISIONS_FINANCIER_PDT,REP_PROVISIONS_FINANCIER_CHG,REP_PROVISIONS_EXCEPT]);
    analysisDef.componentDefinitions.push(component);
    
    wpw.injectedAnalysisDefinitions.push(analysisDef);
})(wpw);