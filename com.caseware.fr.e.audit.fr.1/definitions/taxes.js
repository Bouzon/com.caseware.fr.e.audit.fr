(function(wpw) {
    'use strict';
  
    if (!wpw.injectedAnalysisDefinitions)
      wpw.injectedAnalysisDefinitions = [];
  
    var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  
    var DATA_TYPE = wpw.analysis.DataType;
    var COMPONENT_TYPES = wpw.analysis.ComponentType;
    var CHART_TYPES = wpw.analysis.ChartType;
    var KPIModel = wpw.analysis.KPIModel;
    var ComponentDefinition = wpw.analysis.ComponentDefinition;

    /********************* Formulas ***************************/
    var SUBVENTIONAR_FORMULE = TAG('BA.3.03.02.17');
    var IMPOTS_RECOUVRABLE_FORMULE = TAG('BA.3.03.02.18');
	var IMPOTS_RECOUVRABLE_PA_FORMULE = TAG('BP.4.06.21');
	var IMPOTS_IS_FORMULE = TAG('BA.3.03.02.20');
	var IMPOTS_IS_PA_FORMULE = TAG('BP.4.06.27');
	var IMPOTS_CA_FORMULE = TAG('BA.3.03.02.21');
	var IMPOTS_CA_PA_FORMULE = TAG('BP.4.06.28');
	var IMPOTS_CAPPAR_FORMULE = TAG('BA.3.03.02.24');
	var IMPOTS_CAPPAR_PA_FORMULE = TAG('BP.4.06.36');
	var IMPOTS_TAXES_ET_VERSEMENTS_ASSIMILES_FORMULE = TAG('RC.1.06');
    var PART_DES_SALARIES_IS_ET_ASSIMILES_FORMULE = TAG('RC.6');
    /********************* Formulas ends **********************/

    var analysisDef = new wpw.analysis.AnalysisDefinition();
    analysisDef.id = 'RSSdfu8hSDybpsTthU-xNQ';
    analysisDef.name = 'Etat';

    // Etat Bilan
    var componentBil = new ComponentDefinition('3c9KbpE-Q76WpgWFd2mVwg', 'Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentBil.breakdownGroupNumbers = ['BA.3.03.02.17','BA.3.03.02.18','BA.3.03.02.19','BA.3.03.02.20','BA.3.03.02.21','BA.3.03.02.22','BA.3.03.02.23','BA.3.03.02.24','BA.3.03.02.25','BP.4.06.20','BP.4.06.22','BP.4.06.23','BP.4.06.24','BP.4.06.27','BP.4.06.28','BP.4.06.34','BP.4.06.35','BP.4.06.36','BP.4.06.40'];
    componentBil.allowEdit = false;
    analysisDef.componentDefinitions.push(componentBil);

    // Etat Compte de résultat
    var componentCdR = new ComponentDefinition('9rftezRsRhGCABA5C_LyYw', 'Compte de Résultat', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentCdR.breakdownGroupNumbers = ['RC.1.06.02','RC.1.06.03','RC.1.06.04','RC.1.06.05','RC.5','RC.6.01','RC.6.02','RC.6.03','RC.6.04'];
    componentCdR.allowEdit = false;
    analysisDef.componentDefinitions.push(componentCdR);

    //Graphique Bilan
    var SUBVENTIONAR = new KPIModel('SUBVENTIONAR', 'État - Subventions à recevoir', DATA_TYPE.NUMBER, true, null, SUBVENTIONAR_FORMULE);
	var IMPOTS_RECOUVRABLE = new KPIModel('IMPOTS_RECOUVRABLE', 'Etat - Impôts et taxes recouvrables sur des tiers ', DATA_TYPE.NUMBER, true, null, IMPOTS_RECOUVRABLE_FORMULE);
	var IMPOTS_RECOUVRABLE_PA = new KPIModel('IMPOTS_RECOUVRABLE_PA', 'Etat - Impôts et taxes recouvrables sur des tiers ', DATA_TYPE.NUMBER, true, null, IMPOTS_RECOUVRABLE_PA_FORMULE);
	var IMPOTS_IS = new KPIModel('IMPOTS_IS', 'Etat - Impôts sur les bénéfices', DATA_TYPE.NUMBER, true, null, IMPOTS_IS_FORMULE);
	var IMPOTS_IS_PA = new KPIModel('IMPOTS_IS_PA', 'Etat - Impôts sur les bénéfices ', DATA_TYPE.NUMBER, true, null, IMPOTS_IS_PA_FORMULE);
	var IMPOTS_CA = new KPIModel('IMPOTS_CA', 'Etat - Taxes sur le chiffre d\'affaires', DATA_TYPE.NUMBER, true, null, IMPOTS_CA_FORMULE);
	var IMPOTS_CA_PA = new KPIModel('IMPOTS_CA_PA', 'Etat - Taxes sur le chiffre d\'affaires', DATA_TYPE.NUMBER, true, null, IMPOTS_CA_PA_FORMULE);
	var IMPOTS_CAPPAR = new KPIModel('IMPOTS_CAPPAR', 'Etat - Charges à payer et produits à recevoir ', DATA_TYPE.NUMBER, true, null, IMPOTS_CAPPAR_FORMULE);
	var IMPOTS_CAPPAR_PA = new KPIModel('IMPOTS_CAPPAR_PA', 'Etat - Charges à payer et produits à recevoir ', DATA_TYPE.NUMBER, true, null, IMPOTS_CAPPAR_PA_FORMULE);

    var componentBilGraph = new ComponentDefinition('zSUFzlBcQiWNhD6ECCr3rQ', 'Bilan', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [SUBVENTIONAR.key,IMPOTS_RECOUVRABLE.key,IMPOTS_RECOUVRABLE_PA.key,IMPOTS_IS.key,IMPOTS_IS_PA.key,IMPOTS_CA.key,IMPOTS_CA_PA.key,IMPOTS_CAPPAR.key,IMPOTS_CAPPAR_PA.key]);

    analysisDef.addUniqueKpiModels([SUBVENTIONAR,IMPOTS_RECOUVRABLE,IMPOTS_RECOUVRABLE_PA,IMPOTS_IS,IMPOTS_IS_PA,IMPOTS_CA,IMPOTS_CA_PA,IMPOTS_CAPPAR,IMPOTS_CAPPAR_PA]);
    analysisDef.componentDefinitions.push(componentBilGraph);

    //Graphique Compte de résultat
    var IMPOTS_TAXES_ET_VERSEMENTS_ASSIMILES = new KPIModel('IMPOTS_TAXES_ET_VERSEMENTS_ASSIMILES', 'Impots, taxes et versements assimilés', DATA_TYPE.NUMBER, true, null, IMPOTS_TAXES_ET_VERSEMENTS_ASSIMILES_FORMULE);
    var PART_DES_SALARIES_IS_ET_ASSIMILES = new KPIModel('PART_DES_SALARIES_IS_ET_ASSIMILES', 'Part. des salaires, IS et assimilés', DATA_TYPE.NUMBER, true, null, PART_DES_SALARIES_IS_ET_ASSIMILES_FORMULE);

    var componentCdRGraph = new ComponentDefinition('jRzio1pWSk27c5MVZdHn3Q', 'Compte de résultat', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [IMPOTS_TAXES_ET_VERSEMENTS_ASSIMILES.key,PART_DES_SALARIES_IS_ET_ASSIMILES.key]);
    
    analysisDef.addUniqueKpiModels([IMPOTS_TAXES_ET_VERSEMENTS_ASSIMILES,PART_DES_SALARIES_IS_ET_ASSIMILES]);
    analysisDef.componentDefinitions.push(componentCdRGraph);
    
    wpw.injectedAnalysisDefinitions.push(analysisDef);
})(wpw);