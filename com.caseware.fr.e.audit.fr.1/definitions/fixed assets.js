(function(wpw) {
    'use strict';
  
    if (!wpw.injectedAnalysisDefinitions)
      wpw.injectedAnalysisDefinitions = [];
  
    var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  
    var DATA_TYPE = wpw.analysis.DataType;
    var COMPONENT_TYPES = wpw.analysis.ComponentType;
    var CHART_TYPES = wpw.analysis.ChartType;
    var KPIModel = wpw.analysis.KPIModel;
    var ComponentDefinition = wpw.analysis.ComponentDefinition;

    /********************* Formulas ***************************/
    var IMMOBILISATIONS_INCORPORELLES_FORMULE = TAG('BA.2.01');
    var IMMOBILISATIONS_CORPORELLES_FORMULE = TAG('BA.2.02');
    var DOT_AMORT_DEPRECIATIONS_ET_PROVISIONS_FORMULE = TAG('RC.1.09');
    var PRODUCTION_IMMOBILISEE_FORMULE = TAG('RP.1.04');
    var REP_SUR_AMORT_DEPRECIATIONS_ET_PROVISIONS_FORMULE = TAG('RP.1.06.01');
    /********************* Formulas ends **********************/

    var analysisDef = new wpw.analysis.AnalysisDefinition();
    analysisDef.id = 'TQ2BrcOhQWebMaMT0zaxSA';
    analysisDef.name = 'Immobilisations';

    // Immobilisation Bilan
    var componentBil = new ComponentDefinition('CLmMZc62REOXIhIPNzMWKw', 'Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentBil.breakdownGroupNumbers = ['BA.2.01.01','BA.2.01.02','BA.2.01.03.01','BA.2.01.03.02','BA.2.01.03.03','BA.2.01.04.01','BA.2.01.04.02','BA.2.01.05.01','BA.2.01.05.02','BA.2.01.06.01','BA.2.01.06.02','BA.2.01.06.03','BA.2.01.06.04','BA.2.01.06.05','BA.2.01.07.01','BA.2.01.07.02','BA.2.01.07.03','BA.2.01.08','BA.2.01.09','BA.2.02','BA.2.02.01','BA.2.02.02.01','BA.2.02.02.02','BA.2.02.02.03','BA.2.02.02.04','BA.2.02.02.05','BA.2.02.02.06','BA.2.02.03.01','BA.2.02.03.02','BA.2.02.03.03','BA.2.02.03.04','BA.2.02.03.05','BA.2.02.04.01','BA.2.02.04.02','BA.2.02.04.03','BA.2.02.05.01','BA.2.02.05.02','BA.2.02.05.03','BA.2.02.05.04','BA.2.02.05.05','BA.2.02.05.06','BA.2.02.06','BA.2.02.06.01','BA.2.02.06.02','BA.2.02.06.03','BA.2.02.07','BA.3.01.01.01','BA.3.03.02.35'];
    componentBil.allowEdit = false;
    analysisDef.componentDefinitions.push(componentBil);

    // Immobilisation Compte de résultat
    var componentCdR = new ComponentDefinition('t4oF8yhLSwKNqITwFxxD8g', 'Compte de résultat', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentCdR.breakdownGroupNumbers = ['RC.1.09.01','RC.1.09.02','RC.1.09.03','RC.4.02.02','RC.4.02.03','RC.4.02.04','RC.4.03.01','RC.4.03.02','RP.1.04.01','RP.1.04.02','RP.1.04.03','RP.1.06.01','RP.1.06.03','RP.1.06.04','RP.3.04.04','RP.4.02.02','RP.4.02.03','RP.4.02.04','RP.4.03.01','RP.4.03.02'];
    componentCdR.allowEdit = false;
    analysisDef.componentDefinitions.push(componentCdR);

    //Graphique Bilan
    var IMMOBILISATIONS_INCORPORELLES = new KPIModel('IMMOBILISATIONS_INCORPORELLES', 'Immobilisations incorporelles', DATA_TYPE.NUMBER, true, null, IMMOBILISATIONS_INCORPORELLES_FORMULE);
    var IMMOBILISATIONS_CORPORELLES = new KPIModel('IMMOBILISATIONS_CORPORELLES', 'Immobilisations corporelles', DATA_TYPE.NUMBER, true, null, IMMOBILISATIONS_CORPORELLES_FORMULE);
    
    var componentBilGraph = new ComponentDefinition('yPsgCxlCTp2_z-maRkD0Uw', 'Bilan', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [IMMOBILISATIONS_INCORPORELLES.key,IMMOBILISATIONS_CORPORELLES.key]);
    
    analysisDef.addUniqueKpiModels([IMMOBILISATIONS_INCORPORELLES,IMMOBILISATIONS_CORPORELLES]);
    analysisDef.componentDefinitions.push(componentBilGraph);
    
    //Graphique Compte de résultat
    var DOT_AMORT_DEPRECIATIONS_ET_PROVISIONS = new KPIModel('DOT_AMORT_DEPRECIATIONS_ET_PROVISIONS', 'Dot. Amort. dépréciations et provisions', DATA_TYPE.NUMBER, true, null, DOT_AMORT_DEPRECIATIONS_ET_PROVISIONS_FORMULE);
    var PRODUCTION_IMMOBILISEE = new KPIModel('PRODUCTION_IMMOBILISEE', 'Production immobilisée', DATA_TYPE.NUMBER, true, null, PRODUCTION_IMMOBILISEE_FORMULE);
    var REP_SUR_AMORT_DEPRECIATIONS_ET_PROVISIONS = new KPIModel('REP_SUR_AMORT_DEPRECIATIONS_ET_PROVISIONS', 'Rep. sur amort. dépréciations et provisions', DATA_TYPE.NUMBER, true, null, REP_SUR_AMORT_DEPRECIATIONS_ET_PROVISIONS_FORMULE);

    var componentCdRGraph = new ComponentDefinition('5nLa6eKTS5-gaMrzawrpkA', 'Compte de résultat', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [DOT_AMORT_DEPRECIATIONS_ET_PROVISIONS.key,PRODUCTION_IMMOBILISEE.key,REP_SUR_AMORT_DEPRECIATIONS_ET_PROVISIONS.key]);

    analysisDef.addUniqueKpiModels([DOT_AMORT_DEPRECIATIONS_ET_PROVISIONS,PRODUCTION_IMMOBILISEE,REP_SUR_AMORT_DEPRECIATIONS_ET_PROVISIONS]);
    analysisDef.componentDefinitions.push(componentCdRGraph);

    wpw.injectedAnalysisDefinitions.push(analysisDef);
})(wpw);