(function(wpw) {
    'use strict';
  
    if (!wpw.injectedAnalysisDefinitions)
      wpw.injectedAnalysisDefinitions = [];
  
    var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  
    var DATA_TYPE = wpw.analysis.DataType;
    var COMPONENT_TYPES = wpw.analysis.ComponentType;
    var CHART_TYPES = wpw.analysis.ChartType;
    var KPIModel = wpw.analysis.KPIModel;
    var ComponentDefinition = wpw.analysis.ComponentDefinition;

    /********************* Formulas ***************************/
    var CHARGES_EXCEPTIONNELLES_FORMULE = TAG('RC.4');
    var PRODUITS_EXCEPTIONNELS_FORMULE = TAG('RP.4');
    /********************* Formulas ends **********************/

    var analysisDef = new wpw.analysis.AnalysisDefinition();
    analysisDef.id = '6whdIuWbRtuhX7EV2iGxeA';
    analysisDef.name = 'Résultat exceptionnel';

    // Résultat exceptionnel
    var component = new ComponentDefinition('m61Ypa3STzK9GGwqFI-XOw', 'Résultat exceptionnel', COMPONENT_TYPES.ACCOUNTS_TABLE);
    component.breakdownGroupNumbers = ['RC.4.01.01','RC.4.01.02','RC.4.01.03','RC.4.01.04','RC.4.01.05','RC.4.02.01','RC.4.02.05','RC.4.02.06','RP.4.01.01','RP.4.01.02','RP.4.01.03','RP.4.02.01','RP.4.02.05','RP.4.02.06','RP.4.02.07'];
    component.allowEdit = false;
    analysisDef.componentDefinitions.push(component);

    //Graphique Résultat exceptionnel
    var CHARGES_EXCEPTIONNELLES = new KPIModel('CHARGES_EXCEPTIONNELLES', 'Charges exceptionnelles', DATA_TYPE.NUMBER, true, null, CHARGES_EXCEPTIONNELLES_FORMULE);
    var PRODUITS_EXCEPTIONNELS = new KPIModel('PRODUITS_EXCEPTIONNELS', 'Produits exceptionnels', DATA_TYPE.NUMBER, true, null, PRODUITS_EXCEPTIONNELS_FORMULE);
    
    var componentGraph = new ComponentDefinition('ovfkeuzKTZuuYuTPKOwDCw', 'Résultat exceptionnel', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [CHARGES_EXCEPTIONNELLES.key,PRODUITS_EXCEPTIONNELS.key]);
    
    analysisDef.addUniqueKpiModels([CHARGES_EXCEPTIONNELLES,PRODUITS_EXCEPTIONNELS]);
    analysisDef.componentDefinitions.push(componentGraph);
    
    wpw.injectedAnalysisDefinitions.push(analysisDef);
})(wpw);