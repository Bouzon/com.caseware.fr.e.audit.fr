(function(wpw) {
    'use strict';
  
    if (!wpw.injectedAnalysisDefinitions)
      wpw.injectedAnalysisDefinitions = [];
  
    var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  
    var DATA_TYPE = wpw.analysis.DataType;
    var COMPONENT_TYPES = wpw.analysis.ComponentType;
    var CHART_TYPES = wpw.analysis.ChartType;
    var KPIModel = wpw.analysis.KPIModel;
    var ComponentDefinition = wpw.analysis.ComponentDefinition;

    /********************* Formulas ***************************/
    var CLIENTS_FORMULE = TAG('BA.3.03.01.01');
	var CLIENTS_CRED_ANORMAUX_FORMULE = TAG('BP.4.08.28');
    var CLIENTS_EFFETS_FORMULE = TAG('BA.3.03.01.02');
	var CLIENTS_DOUTEUX_FORMULE = TAG('BA.3.03.01.03');
	var CLIENTS_PRODUITS_NONFACT_FORMULE = TAG('BA.3.03.01.04');
	var CLIENTS_DEPRECIATION_FORMULE = TAG('BA.3.03.01.05');
	var CLIENTS_CREDITEURS_FORMULE = TAG('BP.4.08.22');
	var PCA_FORMULE = TAG('BP.4.08.22');
	var VENTE_MARCHANDISE_FORMULE = TAG('RP.1.01');
    var VENTES_DE_PRODUITS_FINIS_FORMULE = TAG('RP.1.02')
    /********************* Formulas ends **********************/

    var analysisDef = new wpw.analysis.AnalysisDefinition();
    analysisDef.id = 'n2ukRlmjS1GZFhzHrgTtKQ';
    analysisDef.name = 'Clients, ventes et prestations';

    // Client Bilan
    var componentBil = new ComponentDefinition('TqBkY80UT22QW_uapccakQ', 'Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentBil.breakdownGroupNumbers = ['BA.3.03.01.01','BA.3.03.01.02','BA.3.03.01.03','BA.3.03.01.04','BA.3.03.01.05','BP.4.08.22','BP.4.10.01','BP.4.08.28','BP.4.08.31','BP.4.08.32','BP.4.08.33','BP.4.08.24'];
    componentBil.allowEdit = false;
    analysisDef.componentDefinitions.push(componentBil);

    // Clients Compte de résultat
    var componentCdR = new ComponentDefinition('cxtWhDj6Q3ipeV0khUhYDw', 'Compte de Résultat', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentCdR.breakdownGroupNumbers = ['RC.1.08.07.01','RC.1.08.07.03','RC.1.10.03','RC.3.02.09','RP.1.01','RP.1.02.01','RP.1.02.02','RP.1.02.03','RP.1.02.04','RP.1.02.05','RP.1.02.06','RP.1.02.07',	'RP.1.02.08',	'RP.1.06.06',	'RP.3.03.02'];
    componentCdR.allowEdit = false;
    analysisDef.componentDefinitions.push(componentCdR);

    //Graphique Bilan
    var CLIENTS = new KPIModel('CLIENTS', 'Clients', DATA_TYPE.NUMBER, true, null, CLIENTS_FORMULE);
	var CLIENTS_CRED_ANORMAUX = new KPIModel('CLIENTS_CRED_ANORMAUX', 'Clients créditeurs', DATA_TYPE.NUMBER, true, null, CLIENTS_CRED_ANORMAUX_FORMULE);
	var CLIENTS_EFFETS = new KPIModel('CLIENTS_EFFETS', 'Clients - Effets à recevoir', DATA_TYPE.NUMBER, true, null, CLIENTS_EFFETS_FORMULE);
	var CLIENTS_DOUTEUX = new KPIModel('CLIENTS_DOUTEUX', 'Clients douteux ou litigieux ', DATA_TYPE.NUMBER, true, null, CLIENTS_DOUTEUX_FORMULE);
	var CLIENTS_PRODUITS_NONFACT = new KPIModel('CLIENTS_PRODUITS_NONFACT', 'Clients - Produits non encore facturés', DATA_TYPE.NUMBER, true, null, CLIENTS_PRODUITS_NONFACT_FORMULE);
	var CLIENTS_DEPRECIATION = new KPIModel('CLIENTS_DEPRECIATION', 'Clients - Produits non encore facturés', DATA_TYPE.NUMBER, true, null, CLIENTS_DEPRECIATION_FORMULE);
	var CLIENTS_CREDITEURS = new KPIModel('CLIENTS_CREDITEURS', 'Clients créditeurs', DATA_TYPE.NUMBER, true, null, CLIENTS_CREDITEURS_FORMULE);
	var PCA = new KPIModel('PCA', 'Produits constatés d\'avance', DATA_TYPE.NUMBER, true, null, PCA_FORMULE);
    var componentBilGraph = new ComponentDefinition('xGyJ2fsqRJ6iUxUDRrH68w', 'Bilan', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [CLIENTS.key,CLIENTS_CRED_ANORMAUX.key,CLIENTS_EFFETS.key,CLIENTS_DOUTEUX.key,CLIENTS_PRODUITS_NONFACT.key,CLIENTS_DEPRECIATION.key,CLIENTS_CREDITEURS.key,PCA.key]);
    
    analysisDef.addUniqueKpiModels([CLIENTS,CLIENTS_CRED_ANORMAUX,CLIENTS_EFFETS,CLIENTS_DOUTEUX,CLIENTS_PRODUITS_NONFACT,CLIENTS_DEPRECIATION,CLIENTS_CREDITEURS,PCA]);
    analysisDef.componentDefinitions.push(componentBilGraph);

    //Graphique Compte de résultat
    var VENTE_MARCHANDISE = new KPIModel('VENTE_MARCHANDISE', 'Ventes de marchandises', DATA_TYPE.NUMBER, true, null, VENTE_MARCHANDISE_FORMULE);
    var VENTES_DE_PRODUITS_FINIS = new KPIModel('VENTES_DE_PRODUITS_FINIS', 'Ventes de produits finis', DATA_TYPE.NUMBER, true, null, VENTES_DE_PRODUITS_FINIS_FORMULE);

    var componentCdRGraph = new ComponentDefinition('uzFk7hvoQjyjkVirmX13Jg', 'Compte de Résultat', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [VENTE_MARCHANDISE.key,VENTES_DE_PRODUITS_FINIS.key]);

    analysisDef.addUniqueKpiModels([VENTE_MARCHANDISE,VENTES_DE_PRODUITS_FINIS]);
    analysisDef.componentDefinitions.push(componentCdRGraph);

    wpw.injectedAnalysisDefinitions.push(analysisDef);
})(wpw);
