(function(wpw) {
    'use strict';
  
    if (!wpw.injectedAnalysisDefinitions)
      wpw.injectedAnalysisDefinitions = [];
  
    var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  
    var DATA_TYPE = wpw.analysis.DataType;
    var COMPONENT_TYPES = wpw.analysis.ComponentType;
    var CHART_TYPES = wpw.analysis.ChartType;
    var KPIModel = wpw.analysis.KPIModel;
    var ComponentDefinition = wpw.analysis.ComponentDefinition;

    /********************* Formulas ***************************/
    var PERSONNEL_REMUNERATION_FORMULE = TAG('BA.3.03.02.07');
	var PERSONNEL_CAPPAR_FORMULE = TAG('BA.3.03.02.13');
    var SÉCURITÉ_SOCIALE_FORMULE = TAG('BA.3.03.02.14');
    var AUTRES_ORGA_SOC_FORMULE = TAG('BA.3.03.02.15');
	var PERSONNEL_REMUNERATION_PA_FORMULE = TAG('BP.4.06.02');
	var PERSONNEL_CAPPAR_PA_FORMULE = TAG('BP.4.06.09');
    var SÉCURITÉ_SOCIALE_PA_FORMULE = TAG('BP.4.06.14');
    var AUTRES_ORGA_SOC_PA_FORMULE = TAG('BP.4.06.15');
	var CHARGES_DE_PERSONNEL_FORMULE = TAG('RC.1.07');
	var CHARGES_SOC_FORMULE = TAG('RC.1.08')
    /********************* Formulas ends **********************/

    var analysisDef = new wpw.analysis.AnalysisDefinition();
    analysisDef.id = 'd9mDeS-uRiuqSxJFi_UXNA';
    analysisDef.name = 'Personnel';

    // Personnel Bilan
    var componentBil = new ComponentDefinition('-Kee3OSqQ2CtxqQuNSFSfA', 'Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentBil.breakdownGroupNumbers = ['BA.3.03.02.07','BA.3.03.02.08','BA.3.03.02.09','BA.3.03.02.10','BA.3.03.02.11','BA.3.03.02.12','BA.3.03.02.13','BA.3.03.02.14','BA.3.03.02.15','BA.3.03.02.16','BP.4.06.02','BP.4.06.03','BP.4.06.04','BP.4.06.07','BP.4.04.17','BP.4.06.08','BP.4.06.09','BP.4.06.14','BP.4.06.15','BP.4.06.16'];
    componentBil.allowEdit = false;
    analysisDef.componentDefinitions.push(componentBil);

    // Personnel Compte de résultat
    var componentCdR = new ComponentDefinition('66kxTca0SIeReO470Xbe8A', 'Compte de Résultat', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentCdR.breakdownGroupNumbers = ['RC.1.07.01','RC.1.07.02','RC.1.08.01','RC.1.08.02','RC.1.08.03','RC.1.08.04','RC.1.08.05'];
    componentCdR.allowEdit = false;
    analysisDef.componentDefinitions.push(componentCdR);

    //Graphique Bilan
    var PERSONNEL_REMUNERATION = new KPIModel('PERSONNEL_REMUNERATION', 'Personnel - Rémunérations', DATA_TYPE.NUMBER, true, null, PERSONNEL_REMUNERATION_FORMULE);
	var PERSONNEL_REMUNERATION_PA = new KPIModel('PERSONNEL_REMUNERATION_PA', 'Personnel - Rémunérations', DATA_TYPE.NUMBER, true, null, PERSONNEL_REMUNERATION_PA_FORMULE);
    var PERSONNEL_CAPPAR = new KPIModel('PERSONNEL_CAPPAR', 'Personnel - Charges à payer et produits à recevoir', DATA_TYPE.NUMBER, true, null, PERSONNEL_CAPPAR_FORMULE);
	var PERSONNEL_CAPPAR_PA = new KPIModel('PERSONNEL_CAPPAR_PA', 'Personnel - Charges à payer et produits à recevoir', DATA_TYPE.NUMBER, true, null, PERSONNEL_CAPPAR_PA_FORMULE);
	var SÉCURITÉ_SOCIALE = new KPIModel('SÉCURITÉ_SOCIALE', 'Sécurité sociale', DATA_TYPE.NUMBER, true, null, SÉCURITÉ_SOCIALE_FORMULE);
	var SÉCURITÉ_SOCIALE_PA = new KPIModel('SÉCURITÉ_SOCIALE_PA', 'Sécurité sociale', DATA_TYPE.NUMBER, true, null, SÉCURITÉ_SOCIALE_PA_FORMULE);
    var AUTRES_ORGA_SOC = new KPIModel('AUTRES_ORGA_SOC', 'Autres organismes sociaux', DATA_TYPE.NUMBER, true, null, AUTRES_ORGA_SOC_FORMULE);
    var AUTRES_ORGA_SOC_PA = new KPIModel('AUTRES_ORGA_SOC_PA', 'Autres organismes sociaux', DATA_TYPE.NUMBER, true, null, AUTRES_ORGA_SOC_PA_FORMULE);
	var componentBilGraph = new ComponentDefinition('XNxOsmOvSAeTYRKl8bBZag', 'Bilan', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [PERSONNEL_REMUNERATION.key,PERSONNEL_REMUNERATION_PA.key,PERSONNEL_CAPPAR.key,PERSONNEL_CAPPAR_PA.key,SÉCURITÉ_SOCIALE.key,SÉCURITÉ_SOCIALE_PA.key,AUTRES_ORGA_SOC.key,AUTRES_ORGA_SOC_PA.key]);

    analysisDef.addUniqueKpiModels([PERSONNEL_REMUNERATION,PERSONNEL_REMUNERATION_PA,PERSONNEL_CAPPAR,PERSONNEL_CAPPAR_PA,SÉCURITÉ_SOCIALE,SÉCURITÉ_SOCIALE_PA,AUTRES_ORGA_SOC,AUTRES_ORGA_SOC_PA]);
    analysisDef.componentDefinitions.push(componentBilGraph);

    //Graphique Compte de résultat
    var CHARGES_DE_PERSONNEL = new KPIModel('CHARGES_DE_PERSONNEL', 'Charge de personnel', DATA_TYPE.NUMBER, true, null, CHARGES_DE_PERSONNEL_FORMULE);
	var CHARGES_SOC = new KPIModel('CHARGES_SOC', 'Charges sociales', DATA_TYPE.NUMBER, true, null, CHARGES_SOC_FORMULE);

    var componentCdRGraph = new ComponentDefinition('iHT7W5EiRqqQvOsVCAGFiw', 'Compte de Résultat', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [CHARGES_DE_PERSONNEL.key,CHARGES_SOC.key]);
    
    analysisDef.addUniqueKpiModels([CHARGES_DE_PERSONNEL,CHARGES_SOC]);
    analysisDef.componentDefinitions.push(componentCdRGraph);
    
    wpw.injectedAnalysisDefinitions.push(analysisDef);
})(wpw);