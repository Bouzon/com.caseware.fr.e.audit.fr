(function(wpw) {
    'use strict';
  
    if (!wpw.injectedAnalysisDefinitions)
      wpw.injectedAnalysisDefinitions = [];
  
    var TAG = wpw.analysis.AnalysisFormula.FORMULAS.tag;
  
    var DATA_TYPE = wpw.analysis.DataType;
    var COMPONENT_TYPES = wpw.analysis.ComponentType;
    var CHART_TYPES = wpw.analysis.ChartType;
    var KPIModel = wpw.analysis.KPIModel;
    var ComponentDefinition = wpw.analysis.ComponentDefinition;

    /********************* Formulas ***************************/
    var STOCKS_DE_MATIÈRES_PREMIÈRES_ET_FOURNIT_FORMULE = TAG('BA.3.01.01.02');
    var AUTRES_APPROVISIONNEMENTS_FORMULE = TAG('BA.3.01.01.03');
    var EN_COURS_DE_PRODUCTION_DE_BIENS_FORMULE =TAG('BA.3.01.02.01');
    var STOCKS_DE_PRODUITS_FORMULE = TAG('BA.3.01.03');
    var STOCKS_PROVENANT_D_IMMOBILISATIONS_FORMULE = TAG('BA.3.01.01.04');
    var STOCKS_DE_MARCHANDISES_FORMULE = TAG('BA.3.01.04');
    var STOCKS_EN_VOIE_D_ACHEMINEMENT_FORMULE = TAG('BA.3.01.01.05');
    var VAR_STOCKS_MARCHANDISE_FORMULE = TAG('RC.1.02');
    var VAR_STOCKS_MP_FORMULE = TAG('RC.1.04.01');
	var VAR_STOCKS_AUTREAPP_FORMULE = TAG('RC.1.04.02');
	var VAR_STOCKS_PRODUITS_FORMULE = TAG('RP.1.03')
    /********************* Formulas ends **********************/

    var analysisDef = new wpw.analysis.AnalysisDefinition();
    analysisDef.id = 'RgUZmSEVS2W9h9cBXQlpAg';
    analysisDef.name = 'Stocks';

    // Stocks Bilan
    var componentBil = new ComponentDefinition('8DfGMM-uT-eGxQzwpA_aag', 'Stocks Bilan', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentBil.breakdownGroupNumbers = ['BA.3.01.01.02','BA.3.01.01.03','BA.3.01.01.04','BA.3.01.01.05','BA.3.01.01.06','BA.3.01.01.07','BA.3.01.01.08','BA.3.01.01.09','BA.3.01.01.10','BA.3.01.01.11','BA.3.01.02.01','BA.3.01.02.02','BA.3.01.02.03','BA.3.01.02.04','BA.3.01.02.05','BA.3.01.02.06','BA.3.01.02.07','BA.3.01.02.08','BA.3.01.03','BA.3.01.03.01','BA.3.01.03.02','BA.3.01.03.03','BA.3.01.04','BA.3.01.04.01'];
    componentBil.allowEdit = false;
    analysisDef.componentDefinitions.push(componentBil);

    // Stocks Compte de résultat
    var componentCdR = new ComponentDefinition('2u9va3LHTsmOyTLpvwD-Dg', 'Stocks Compte de Résultat', COMPONENT_TYPES.ACCOUNTS_TABLE);
    componentCdR.breakdownGroupNumbers = ['RC.1.02','RC.1.04.01','RC.1.04.02','RC.1.08.07.02','RC.4.03.03','RP.1.03','RP.1.06.05','RP.4.03.05'];
    componentCdR.allowEdit = false;
    analysisDef.componentDefinitions.push(componentCdR);

    //Graphique Bilan
    var STOCKS_DE_MATIÈRES_PREMIÈRES_ET_FOURNIT = new KPIModel('STOCKS_DE_MATIÈRES_PREMIÈRES_ET_FOURNIT', 'Stocks de matières premières et fournit', DATA_TYPE.NUMBER, true, null, STOCKS_DE_MATIÈRES_PREMIÈRES_ET_FOURNIT_FORMULE);
    var AUTRES_APPROVISIONNEMENTS = new KPIModel('AUTRES_APPROVISIONNEMENTS', 'Autres approvisionnement', DATA_TYPE.NUMBER, true, null, AUTRES_APPROVISIONNEMENTS_FORMULE);
    var EN_COURS_DE_PRODUCTION_DE_BIENS = new KPIModel('EN_COURS_DE_PRODUCTION_DE_BIENS', 'En-cours de production de biens', DATA_TYPE.NUMBER, true, null, EN_COURS_DE_PRODUCTION_DE_BIENS_FORMULE);
    var STOCKS_DE_PRODUITS = new KPIModel('STOCKS_DE_PRODUITS', 'Stocks de produits', DATA_TYPE.NUMBER, true, null, STOCKS_DE_PRODUITS_FORMULE);
    var STOCKS_PROVENANT_D_IMMOBILISATIONS = new KPIModel('STOCKS_PROVENANT_D_IMMOBILISATIONS', 'Stocks provenant d\'immobilisations', DATA_TYPE.NUMBER, true, null, STOCKS_PROVENANT_D_IMMOBILISATIONS_FORMULE);
    var STOCKS_DE_MARCHANDISES = new KPIModel('STOCKS_DE_MARCHANDISES', 'Stocks de marchandises', DATA_TYPE.NUMBER, true, null, STOCKS_DE_MARCHANDISES_FORMULE);
    var STOCKS_EN_VOIE_D_ACHEMINEMENT = new KPIModel('STOCKS_EN_VOIE_D_ACHEMINEMENT', 'Stocks en voie d\'acheminement', DATA_TYPE.NUMBER, true, null, STOCKS_EN_VOIE_D_ACHEMINEMENT_FORMULE);

    var componentBilGraph = new ComponentDefinition('-cwNcmMkTa6EEoxmPpW9-g', 'Bilan', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [STOCKS_DE_MATIÈRES_PREMIÈRES_ET_FOURNIT.key,AUTRES_APPROVISIONNEMENTS.key,EN_COURS_DE_PRODUCTION_DE_BIENS.key,STOCKS_DE_PRODUITS.key,STOCKS_PROVENANT_D_IMMOBILISATIONS.key,STOCKS_DE_MARCHANDISES.key,STOCKS_EN_VOIE_D_ACHEMINEMENT.key]);
    
    analysisDef.addUniqueKpiModels([STOCKS_DE_MATIÈRES_PREMIÈRES_ET_FOURNIT,AUTRES_APPROVISIONNEMENTS,EN_COURS_DE_PRODUCTION_DE_BIENS,STOCKS_DE_PRODUITS,STOCKS_PROVENANT_D_IMMOBILISATIONS,STOCKS_DE_MARCHANDISES,STOCKS_EN_VOIE_D_ACHEMINEMENT]);
    analysisDef.componentDefinitions.push(componentBilGraph);

    //Graphique Compte de résultat
    var VAR_STOCKS_MARCHANDISE = new KPIModel('VAR_STOCKS_MARCHANDISE', 'Variation de stocks marchandise', DATA_TYPE.NUMBER, true, null, VAR_STOCKS_MARCHANDISE_FORMULE);
    var VAR_STOCKS_MP = new KPIModel('VAR_STOCKS_MP', 'Var. des stocks de mat. premières', DATA_TYPE.NUMBER, true, null, VAR_STOCKS_MP_FORMULE);
	var VAR_STOCKS_AUTREAPP = new KPIModel('VAR_STOCKS_AUTREAPP', 'Var. des stocks d\'autres app', DATA_TYPE.NUMBER, true, null, VAR_STOCKS_AUTREAPP_FORMULE);
	var VAR_STOCKS_PRODUITS = new KPIModel('VAR_STOCKS_PRODUITS', 'Variation de stocks produits', DATA_TYPE.NUMBER, true, null, VAR_STOCKS_PRODUITS_FORMULE);

    var componentCdRGraph = new ComponentDefinition('laMqdjaSSGWTlDxvFJMd6A', 'Compte de Résultat', COMPONENT_TYPES.CHART, CHART_TYPES.COLUMN, [VAR_STOCKS_MARCHANDISE.key,VAR_STOCKS_MP.key,VAR_STOCKS_AUTREAPP.key,VAR_STOCKS_PRODUITS.key]);

    analysisDef.addUniqueKpiModels([VAR_STOCKS_MARCHANDISE,VAR_STOCKS_MP,VAR_STOCKS_AUTREAPP,VAR_STOCKS_PRODUITS]);
    analysisDef.componentDefinitions.push(componentCdRGraph);

    wpw.injectedAnalysisDefinitions.push(analysisDef);
})(wpw);